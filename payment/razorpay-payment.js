const Razorpay = require('razorpay');
const axios = require('axios');

const config = require('../config');

// configuring the razorpay credentials
// const razorpayInstance = new Razorpay({
//     key_id: config.LIVE_RAZORPAY_KEY_ID,
//     key_secret: config.LIVE_RAZORPAY_KEY_SECRET,
// });
//
// const axiosInstance = axios.create({
//     baseURL: 'https://api.razorpay.com/v1',
//     auth: {
//         username: config.LIVE_RAZORPAY_KEY_ID,
//         password: config.LIVE_RAZORPAY_KEY_SECRET,
//     },
// });
//
// const RAZORPAY_ACCOUNT = '4564566761005902';

const razorpayInstance = new Razorpay({
    key_id: config.TEST_RAZORPAY_KEY_ID,
    key_secret: config.TEST_RAZORPAY_KEY_SECRET,
});

const axiosInstance = axios.create({
    baseURL: 'https://api.razorpay.com/v1',
    auth: {
        username: config.TEST_RAZORPAY_KEY_ID,
        password: config.TEST_RAZORPAY_KEY_SECRET,
    },
});

const RAZORPAY_ACCOUNT = '2323230009936640';

module.exports = { razorpayInstance, axiosInstance, RAZORPAY_ACCOUNT };
