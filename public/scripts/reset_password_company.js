window.addEventListener('load', (event) => {
    const form = document.querySelector('#update-password-form');

    const url = new URL(window.location.href);
    const token = url.searchParams.get('token');
    const email = url.searchParams.get('email');

    form.addEventListener('submit', async (e) => {
        e.preventDefault();

        const pwd = document.querySelector('#password').value;
        const cnfPwd = document.querySelector('#confirm-password').value;

        if (pwd !== cnfPwd) {
            alert('Passwords not matching');
        } else if (pwd.length < 5 || cnfPwd.length < 5) {
            alert('Passwords too short');
        } else {
            const response = await fetch('/company/reset-password', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                },
                mode: 'same-origin',
                body: JSON.stringify({
                    password: pwd,
                    token: token,
                }),
            });

            if (response.status === 200) {
                alert('Password reset successful');
                form.reset();
            } else {
                alert('Some error occured');
            }
        }
    });
});
