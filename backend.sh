if [[ $NODE_ENV = "prod" ]]
then
        sudo git pull origin master;
        echo -e "\033[1;33mMaster Branch Pulled.\033[0m";
else
        sudo git pull origin dev;
        echo -e "\033[1;33mDev Branch Pulled.\033[0m";
fi

sudo npm install
echo -e "\033[1;33mDependencies installed.\033[0m";
sudo pm2 reload app;
echo -e "\033[1;33mpm2 instance restarted.\033[0m";
sudo service nginx restart;
echo -e "\033[1;33mnginx service restarted.\033[0m";
