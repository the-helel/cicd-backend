const agenda = require('../agenda');
const agendaMera = require('agenda');

/**
 * Date prototypes
 */
Date.prototype.getDaysInMonth = function () {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.isLeapYear = function (year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
};

Date.getDaysInMonth = function (year, month) {
    return [31, Date.isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
    return Date.isLeapYear(this.getFullYear());
};

Date.prototype.addMonths = function (value) {
    let n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};

/**
 * New active-campaign job is created when campaign is created. It will
 * change the status to Active and the campaign will be available for
 * influencers to apply.
 *
 * @param id Campaign id
 * @param startDate Campaign start date
 */
const startCampaignJob = async (id, startDate) => {
    const jobName = 'active-campaign';

    const jobs = await agenda.jobs({
        name: 'active-campaign',
    });

    await agenda.schedule(startDate, 'active-campaign', {
        id: id,
    });
};

/**
 * New expire-campaign job is created when campaign is created. It will
 * change the status to Closed and the campaign will not be available for
 * influencers to apply.
 *
 * @param id Campaign id
 * @param endDate Campaign end date
 */
const closeCampaignJob = async (id, endDate) => {
    const jobName = 'close-campaign';

    const jobs = await agenda.jobs({
        name: 'close-campaign',
    });

    await agenda.schedule(endDate, 'close-campaign', {
        id: id,
    });
};

/**
 * Campaign post update job, it will keep updating post information for after
 * 1 month completion of campaign. It will automatically delete the campaign related
 * jobs by id after 1 month.
 */
const initCampaignPostJob = async (id, endDate) => {
    const jobName = 'close-campaign';

    const jobs = await agenda.jobs({
        name: 'update-campaign-post-job',
    });

    let date = new Date(endDate);
    const tillJobDate = date.addMonths(1);

    await agenda.every('12 hours', 'update-campaign-post-job', {
        id: id,
        lastUpdateDate: tillJobDate,
    });
};

module.exports = {
    startCampaignJob,
    closeCampaignJob,
    initCampaignPostJob,
};
