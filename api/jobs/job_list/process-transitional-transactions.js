const { processTransitionalTransactions } = require('../../controllers/influencer/wallet/wallet');

module.exports = (agenda) => {
    agenda.define('process-transitional-transactions', processTransitionalTransactions);
};
