const Campaign = require('../../models/campaign/campaign');
const constants = require('../../constants');
const agenda = require('../agenda');

module.exports = (agenda) => {
    agenda.define('active-campaign', async (job, done) => {
        await Campaign.findById(job.attrs.data.id).then((campaign) => {
            if (campaign != null) {
                campaign.status = constants.ACTIVE;
                campaign
                    .save()
                    .then((r) => {
                        done();
                    })
                    .catch((e) => done());
            } else {
                done();
            }
        });
    });
};
