const Post = require('../../models/campaign/post.js');
const Timeline = require('../../models/campaign/timeline.js');
const GraphAPI = require('../../controllers/util/instagram-api.js');
const { getFacebookPageTokenByUserId, getInstagramById, getInstagramByApplicantId } = require('../../services/common/social-media-utils');
const constants = require('../../constants.js');

module.exports = (agenda) => {
    agenda.define('update-campaign-posts', { priority: 'high' }, async (job, done) => {
        await Post.find().then(async (posts) => {
            //if (platform == constants.INSTAGRAM) {
            // Use instagram API to get data
            // const wait = await Promise.all(
            posts.map(async (p) => {
                // Check the last updated date with current
                // Only update post analytics if the time is more than 1 minute.

                // TODO: The analytics will be updated T+2 months of end of campaign. (Here T=End date of campaign)

                const curr = Date.now();
                const lastUpdate = p.updatedAt;
                const lastFetchedDuration = parseInt((curr - lastUpdate) / (1000 * 60 * 60));
                //one_day means 1000*60*60*24
                //one_hour means 1000*60*60
                //one_minute means 1000*60
                //one_second means 1000
                console.log('Last fetch duration: ', lastFetchedDuration, 'hours ago');

                // The APIs will be only called when the last fetch duration is greater than
                // 6 hours. It ensures that no Facebook page access token is overused.
                if (lastFetchedDuration > 5) {
                    // Get the current user facebook token
                    await getFacebookPageTokenByUserId(p.influencer).then(async (facebook) => {
                        // console.log('Facebook token is', facebook.pageAccessToken);
                        const api = new GraphAPI(facebook.pageAccessToken);
                        const instagram = await getInstagramByApplicantId(p.applicantId);
                        // We are not checking for REELS type posts. As we don't accept them
                        // otherwise

                        // Update likes and comment count
                        await api
                            .getMediaPublicMetrics(p.media.id)
                            .then(async (metric) => {
                                p.caption = metric.caption;
                                p.like_count = metric.like_count;
                                p.comment_count = metric.is_comment_enabled ? metric.comments_count : 0;

                                // Update analytics of post
                                await api
                                    .getMediaInsight(p.media.id, p.media.type)
                                    .then(async (insights) => {
                                        p.impressions = insights.data[0].values[0].value;
                                        p.engagement = insights.data[1].values[0].value;
                                        p.actual_reach = insights.data[2].values[0].value;
                                        p.engagement_rate_by_impression = (p.engagement / p.impressions) * 100;
                                        p.engagement_rate_by_reach = (p.engagement / p.actual_reach) * 100;
                                        p.engagement_rate_by_followers = (p.engagement / instagram.numberOfFollowers) * 100;

                                        if (metric.media_type !== 'IMAGE') {
                                            p.video_view_count = insights.data[3].values[0].value;
                                        }

                                        p.markModified();
                                        await p.save();

                                        // Update timeline field ararys with new analytics
                                        await Timeline.findOne({
                                            post: p._id,
                                        })
                                            .then(async (t) => {
                                                console.log('Previous timeline data ', t);
                                                t.impressions.push({ timestamp: Date.now(), value: p.impressions });
                                                t.markModified('impressions');
                                                t.likes.push({ timestamp: Date.now(), value: p.like_count });
                                                t.markModified('likes');
                                                t.engagements.push({ timestamp: Date.now(), value: p.engagement });
                                                t.markModified('engagements');

                                                if (metric.media_type !== 'IMAGE') {
                                                    t.video_views.push({ timestamp: Date.now(), value: p.video_view_count });
                                                    t.markModified('video_views');
                                                }

                                                console.log('Update timeline data of ', p._id);

                                                await t.save().then((r) => console.log('Update timeline is ', r));
                                            })
                                            .catch((err) => console.log('Record not found for ', p._id));

                                        //console.log('Update already campaign posts insights');
                                    })
                                    .catch((e) => console.log('Getting media insight error while updating', e));
                            })
                            .catch((e) => console.log('Post analytics error', e));
                    });
                } else {
                    console.log('Dont try to spam api');
                }
            });
            //);
        });
        done();
    });
};
