/**
 * IMPORTANT: Do not run this before you finish defining all of your jobs. If you do, you will nuke your database of jobs.
 *
 * @param agenda
 */
module.exports = (agenda) => {
    agenda.define('purge-old-jobs', async (job, done) => {
        await agenda.purge();
    });
};
