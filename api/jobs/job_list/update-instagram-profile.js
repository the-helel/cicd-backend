const Facebook = require('../../models/social/facebook');
const Instagram = require('../../models/social/instagram');
const getInstagramProfile = require('../../controllers/auth/facebook/get-instagram-profile');
const GraphAPI = require('../../controllers/util/instagram-api');

module.exports = (agenda) => {
    agenda.define('update-instagram-profile', async (job, done) => {
        await Instagram.find().then(async (instagramProfiles) => {
            for (let instagramProfile of instagramProfiles) {
                await Facebook.findOne({ influencer: instagramProfile.influencer })
                    .then(async (facebookProfile) => {
                        if (facebookProfile != null) {
                            await getInstagramProfile(instagramProfile.userId, facebookProfile.pageAccessToken)
                                .then(async (instagramData) => {
                                    // Get the engagement of latest 10 posts
                                    const api = new GraphAPI(facebookProfile.pageAccessToken);

                                    // Limit to only latest 10 posted content
                                    const postToFetch = 12;
                                    const fetchPosts = await api.fetchPosts(instagramProfile.userId, postToFetch);

                                    // Iterate over each and find engagement rate
                                    let temp = {
                                        engagement: 0,
                                        impression: 0,
                                    };

                                    let data = await Promise.all(
                                        fetchPosts.data.map(async (e, index) => {
                                            await api.getMediaInsight(e.id, e.media_type).then((media) => {
                                                temp.engagement = media.data[1].values[0].value;
                                                temp.impression = media.data[0].values[0].value;
                                            });

                                            return temp.engagement / temp.impression;
                                        })
                                    );

                                    let sum = 0;
                                    data.forEach((e) => (sum += e));

                                    instagramProfile.engagementRate = sum / postToFetch;
                                    instagramProfile.igId = instagramData.ig_id;
                                    instagramProfile.username = instagramData.username;
                                    instagramProfile.website = instagramData.website;
                                    instagramProfile.numberOfFollowers = instagramData.followers_count;
                                    instagramProfile.numberOfFollowing = instagramData.follows_count;
                                    instagramProfile.fullName = instagramData.name;
                                    instagramProfile.profilePicture = instagramData.profile_picture_url;
                                    instagramProfile.biography = instagramData.biography;
                                    await instagramProfile.save();
                                })
                                .catch((error) => {
                                    console.log(error.message);
                                });
                        } else {
                            console.log('Facebook not found.');
                        }
                    })
                    .catch((error) => {
                        console.log(error.message);
                    });
            }
        });

        done();
    });
};
