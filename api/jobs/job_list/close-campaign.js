const Campaign = require('../../models/campaign/campaign');
const constants = require('../../constants');

module.exports = (agenda) => {
    agenda.define('close-campaign', async (job, done) => {
        await Campaign.findById(job.attrs.data.id).then((campaign) => {
            if (campaign != null) {
                campaign.status = constants.EXPIRED;
                campaign
                    .save()
                    .then((r) => {
                        done();
                    })
                    .catch((e) => done());
            } else {
                done();
            }
        });
    });
};
