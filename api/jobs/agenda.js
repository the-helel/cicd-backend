const Agenda = require('agenda');
const mongoose = require('mongoose');

// creating the agenda instance
const agenda = new Agenda({
    mongo: mongoose.connection,
    db: { collection: 'jobs' },
});

const jobTypes = [
    'close-campaign',
    'process-transitional-transactions',
    'update-instagram-profile',
    'update-campaign-posts',
    'active-campaign',
    'update-campaign-post-job',
];

// loop through the job_list folder and pass in the agenda instance to
// each job so that it has access to its API.
jobTypes.forEach((type) => {
    // the type name should match the file name in the jobs_list folder
    require('./job_list/' + type)(agenda);
});

if (jobTypes.length) {
    // if there are jobs in the jobsTypes array set up
    agenda.on('ready', async () => await agenda.start());
}

let graceful = () => {
    agenda.stop(() => process.exit(0));
};

process.on('SIGTERM', graceful);
process.on('SIGINT', graceful);

module.exports = agenda;
