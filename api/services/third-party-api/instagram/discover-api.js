const axios = require('axios');
const { response } = require('express');

module.exports = class InstagramDiscoverAPI {
    /**
     * Get facebook page access token
     * @param token Access token from facebook collection
     */
    constructor(token) {
        this.token = token;
    }

    /**
     * NOTE: Each Facebook Graph API response includes a X-Page-Usage header
     * with call_count, total_cputime and total_time percentage values (There
     * is no header if the Page's utilization is effectively 0%). When any of
     * these metrics exceed 100, the app managing that page will be rate
     * limited. Use that values to evaluate your API usage so you can balance
     * it and never get blocked. Example of page utilization header:
     *
     *       X-Page-Usage : {'call_count' : 85, 'total_cputime' : 56, 'total_time' : 60}
     */

    /**
     * Get publicly available data from Instagram discover API
     * @param username
     * @returns {Promise<AxiosResponse<any>>}
     */
    getMetadataByUserName(username) {
        if (this.token) {
            const url = `https://graph.facebook.com/17841410785844316?access_token=${this.token}&fields=business_discovery.username(${username}){followers_count,media_count,biography,id,username,website,profile_picture_url}`;

            return axios
                .get(url)
                .then((response) => {
                    return response.data;
                })
                .catch((e) => {
                    return Promise.reject(e.response.error);
                });
        }
    }

    getMedias(username) {
        if (this.token) {
            const url = `https://graph.facebook.com/17841410785844316?access_token=${this.token}&fields=business_discovery.username(${username}){media{caption,comments_count,id,like_count,media_product_type,media_type,media_url,owner,permalink}}`;

            return axios
                .get(url)
                .then((response) => {
                    return response.data;
                })
                .catch((e) => {
                    return Promise.reject(e.response.error);
                });
        }
    }
};
