const { CampaignApplicantAggregate } = require('./aggregate-campaign-applicant');
const { AddToBookmarkCampaign } = require('./bookmark-campaign');
const { ListCampaignPayoutInfluencers } = require('./campaign-payout-influencers');
const { GetCampaignApplicants } = require('./applicant');
const { GetCampaignReport } = require('./get-campaign-report');
const { ListCampaigns } = require('./list-campaigns');
const ApplicantService = require('./applicant');

module.exports = {
    CampaignApplicantAggregate,
    AddToBookmarkCampaign,
    ListCampaignPayoutInfluencers,
    GetCampaignApplicants,
    GetCampaignReport,
    ApplicantService,
    ListCampaigns,
};
