const { getInstagramById } = require('../common/social-media-utils.js');
const moment = require('moment');
const { getTimeSince, convertNumToAbb } = require('../../../utils/formatter');
const { GetEngagementRateProfile } = require('../influencer/social/instagram/get-instagram-profile');

/**
 * This method is used to add amount, crossBid, finalBidAmount to the applicant response.
 *
 * Find usage here: {@link getCampaignApplicant}(../../controllers/company/campaign/applicant.js')
 * @param {*} applicant
 * @returns
 */
async function setApplicantFields(applicant, deliverables) {
    applicant = applicant.toObject();
    // The starting bid amount
    applicant.amount = applicant.bids[0].influencer.amount;

    // Initially the cross bid amount will be null
    applicant.crossBid = null;
    applicant.finalBidAmount = null;
    applicant.influencerId.since = getTimeSince(applicant.influencerId.createdAt);
    applicant.delivered = applicant.link.length + '/' + deliverables;

    // Condition: To check if there is some cross bid available or not
    if (applicant.bids[applicant.bids.length - 1].company !== null) {
        if (
            applicant.bids[applicant.bids.length - 1].influencer.status === 'Rejected' &&
            applicant.bids[applicant.bids.length - 1].company.status !== 'Approved'
        ) {
            applicant.crossBid = applicant.bids[applicant.bids.length - 1].company.amount;
        }
    }

    // Condition: When brand agress on the influencer bid amount
    if (
        applicant.bids[applicant.bids.length - 1].influencer.status === 'Approved' &&
        applicant.bids[applicant.bids.length - 1].influencer.status !== null
    ) {
        applicant.finalBidAmount = applicant.amount;
    }

    // Condition: When influencer accepts the cross bid of brand.
    if (applicant.bids[applicant.bids.length - 1].company !== null) {
        if (applicant.bids[applicant.bids.length - 1].company.status === 'Approved') {
            applicant.finalBidAmount = applicant.bids[applicant.bids.length - 1].company.amount;
        }
    }
    let social;

    // Get the social media data of current applicant
    if (applicant.applied_platform === 'Instagram') {
        try {
            const result = await getInstagramById(applicant.social);
            // When influencer deletes the account data is no more available
            // TODO: Fix this issue
            if (!result) {
                applicant.followers = 0;
                applicant.instagramEngagement = 0;
            } else {
                social = result;
                applicant.followers = social.numberOfFollowers;
                applicant.instagramEngagement = social.engagementRate.toFixed(2);
            }
        } catch (e) {}
    }

    // Get the total engagement of all posts
    let totalEngagement = 0;
    for (let i = 0; i < applicant.link.length; i++) {
        totalEngagement += applicant.link[i].engagement;
    }

    let totalVideoViews = 0;
    for (let i = 0; i < applicant.link.length; i++) {
        totalVideoViews += applicant.link[i].video_view_count;
    }

    let totalReach = 0;
    for (let i = 0; i < applicant.link.length; i++) {
        totalReach += applicant.link[i].actual_reach;
    }

    let totalImpressions = 0;
    for (let i = 0; i < applicant.link.length; i++) {
        totalImpressions += applicant.link[i].impressions;
    }

    let averageEngagementRateByImpression = 0;
    let averageEngagementRateByReach = 0;
    let averageEngagementRateByFollower = 0;
    for (let i = 0; i < applicant.link.length; i++) {
        averageEngagementRateByImpression += applicant.link[i].engagement_rate_by_impression;
        averageEngagementRateByReach += applicant.link[i].engagement_rate_by_reach;
        averageEngagementRateByFollower += applicant.link[i].engagement_rate_by_followers;

        // Get the average rate of engagement
        averageEngagementRateByImpression = averageEngagementRateByImpression / applicant.link.length;
        averageEngagementRateByReach = averageEngagementRateByReach / applicant.link.length;
        averageEngagementRateByFollower = averageEngagementRateByFollower / applicant.link.length;
    }

    applicant.engagementRate = {
        averageEngagementRateByImpression: averageEngagementRateByImpression.toFixed(2),
        averageEngagementRateByReach: averageEngagementRateByReach.toFixed(2),
        averageEngagementRateByFollower: averageEngagementRateByFollower.toFixed(2),
    };

    applicant.campaignMetrics = {
        engagement: {
            label: 'Engagement',
            value: totalEngagement,
            short: convertNumToAbb(totalEngagement),
        },
        videoViews: {
            label: 'Video Views',
            value: totalVideoViews,
            short: convertNumToAbb(totalVideoViews),
        },
        reach: {
            label: 'Reach',
            value: totalReach,
            short: convertNumToAbb(totalReach),
        },
        impressions: {
            label: 'Impressions',
            value: totalImpressions,
            short: convertNumToAbb(totalImpressions),
        },
    };

    // Add CPP, CPE and CPV to the applicant response
    if (applicant.finalBidAmount !== null) {
        let cpp = 0;
        let cpv = 0;
        let cpe = 0;
        if (applicant.link.length > 0) {
            cpp = applicant.finalBidAmount / applicant.link.length;
        }
        if (totalEngagement !== 0) cpe = applicant.finalBidAmount / totalEngagement;
        if (totalVideoViews !== 0) cpv = applicant.finalBidAmount / totalVideoViews;
        // let cr = parseFloat(applicant.followers / totalReach).toFixed(2);

        applicant.costing = {
            cpp: cpp.toFixed(2),
            cpe: cpe.toFixed(2),
            cpv: cpv.toFixed(2),
        };
    }

    // TODO: Add other cases for different platforms
    return applicant;
}

function setAmountFields(bids) {
    let result = {};
    result.amount = bids[0].influencer.amount;
    result.finalBidAmount = null;
    result.crossBid = null;

    if (bids[bids.length - 1].company !== null) {
        if (bids[bids.length - 1].influencer.status === 'Rejected' && bids[bids.length - 1].company.status !== 'Approved') {
            result.crossBid = bids[bids.length - 1].company.amount;
        }
    }

    // Condition: When brand agress on the influencer bid amount
    if (bids[bids.length - 1].influencer.status === 'Approved' && bids[bids.length - 1].influencer.status !== null) {
        result.finalBidAmount = result.amount;
    }

    // Condition: When influencer accepts the cross bid of brand.
    if (bids[bids.length - 1].company !== null) {
        if (bids[bids.length - 1].company.status === 'Approved') {
            result.finalBidAmount = bids[bids.length - 1].company.amount;
        }
    }

    return result;
}

module.exports = {
    setApplicantFields,
    setAmountFields,
};
