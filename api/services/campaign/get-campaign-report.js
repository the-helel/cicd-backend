// Get the total selected influencer, posts and estimated reach of all posts
const kAsync = require('async');
const CampaignApplicantAggregate = require('./aggregate-campaign-applicant');
const { CampaignPostAgg, CampaignTopPostsHashtags, PostTimelineAgg } = require('../post');
const { convertNumToAbb } = require('../../../utils/formatter');
const Campaign = require('../../models/campaign/campaign');

const GetCampaignReport = async (campaignId, cb) => {
    await kAsync.parallel(
        {
            influencerAgg: function (callback) {
                CampaignApplicantAggregate(campaignId, callback);
            },
            postAgg: function (callback) {
                CampaignPostAgg(campaignId, callback);
            },
            top_hashtags: function (callback) {
                CampaignTopPostsHashtags(campaignId, callback);
            },
            timelines: function (callback) {
                PostTimelineAgg(campaignId, callback);
            },
            campaignInfo: function (callback) {
                GetCampaignById(campaignId, callback);
            },
        },
        function (err, result) {
            if (err) {
                return cb(
                    {
                        error: true,
                        message: err.message,
                    },
                    null
                );
            }

            const r1 = result.influencerAgg;
            const r2 = result.postAgg;
            const r5 = result.campaignInfo;

            let overview = {
                influencer: 0,
                posts: 0,
                reach: 0,
                impression: 0,
                budgetSpend: 0,
                totalBudget: 0,
                product: 0,
                productSent: 0,
            };

            let summary = {
                posts: 0,
                engagement: 0,
                video_views: 0,
                likes: 0,
                engagement_short: '0',
                comments: 0,
            };

            if (r1.length !== 0) {
                overview.influencer = r1[0].total_selected_count + r1[0].total_completed_count;
                overview.budgetSpend = r1[0].total_spend_budget_on_influencer;
            }

            if (r2.length !== 0) {
                overview.reach = r2[0].total_reach;
                overview.reach_short = convertNumToAbb(r2[0].total_reach);
                overview.impression = r2[0].total_impression;
                overview.impression_short = convertNumToAbb(r2[0].total_impression);
                overview.posts = r2[0].total_post_count;
                summary.posts = r2[0].total_post_count;
                summary.engagement = r2[0].total_engagement;
                summary.engagement_short = convertNumToAbb(r2[0].total_engagement);
                summary.video_views = r2[0].total_video_view_count;
                summary.likes = r2[0].total_like_count;
                summary.comments = r2[0].total_comment_count;
            }

            if (r5.length !== 0) {
                overview.totalBudget = r5.budget;
            }

            cb(null, {
                error: false,
                data: {
                    top_hashtags: result.top_hashtags,
                    timelines: result.timelines[0],
                    overview: overview,
                    summary: summary,
                },
            });
        }
    );
};

const GetCampaignById = async (campaignId, cb) => {
    try {
        const campaign = await Campaign.findById(campaignId);
        if (campaign !== null) {
            return cb(null, {
                budget: campaign.payment.budget,
            });
        }
    } catch (e) {
        console.log('Error occured in GetCampaignById: ', e);
        return cb(e, null);
    }
};

module.exports = {
    GetCampaignReport,
};
