const Campaign = require('../../models/campaign/campaign');
const constants = require('../../../constants');

const ListCampaignPayoutInfluencers = async (status, page, cb) => {
    if (!status) {
        status = constants.EXPIRED;
    }
    // Find campaigns which are closed and has to be paid
    await Campaign.aggregate(
        [
            {
                $match: { status: status },
            },
            {
                $lookup: {
                    from: 'brands',
                    let: { company_id: '$brandId' },
                    pipeline: [
                        {
                            $match: {
                                $expr: { $eq: ['$_id', '$$company_id'] },
                            },
                        },
                        // TODO: Can add status whether the brand is still active or not
                        {
                            $project: {
                                _id: 0,
                                name: 1,
                                logo: 1,
                            },
                        },
                    ],
                    as: 'brandName',
                },
            },
            // {
            //     $lookup: {
            //         from: 'campaign-applicants',
            //         let: { company_id: '$_id' },
            //         pipeline: [
            //             {
            //                 $match: {
            //                     $expr: { $eq: ['$campaignId', '$$company_id'] },
            //                 },
            //             },
            //             // Add condition to also match who are paid as well
            //             {
            //                 $match: {
            //                     $or: [
            //                         {
            //                             status: constants.APPROVED,
            //                         },
            //                         {
            //                             status: constants.COMPLETED,
            //                         },
            //                     ],
            //                 },
            //             },
            //             {
            //                 $lookup: {
            //                     from: 'influencers',
            //                     localField: 'influencerId',
            //                     foreignField: '_id',
            //                     as: 'influencer',
            //                 },
            //             },
            //             {
            //                 $lookup: {
            //                     from: 'campaign-posts',
            //                     let: {
            //                         postIds: '$link',
            //                     },
            //                     pipeline: [
            //                         {
            //                             $match: {
            //                                 $expr: {
            //                                     $in: ['$_id', '$$postIds'],
            //                                 },
            //                             },
            //                         },
            //                     ],
            //                     as: 'influencer-campaign-posts',
            //                 },
            //             },
            //             {
            //                 $project: {
            //                     _id: 1,
            //                     name: {
            //                         $arrayElemAt: ['$influencer.name', 0],
            //                     },
            //                     influencerId: {
            //                         $arrayElemAt: ['$influencer._id', 0],
            //                     },
            //                     email: {
            //                         $arrayElemAt: ['$influencer.email', 0],
            //                     },
            //                     phone: {
            //                         $arrayElemAt: ['$influencer.phoneNumber', 0],
            //                     },
            //                     profilePicture: {
            //                         $arrayElemAt: ['$influencer.profilePicture', 0],
            //                     },
            //                     finalBidAmount: 1,
            //                     status: 1,
            //                     submissions: '$influencer-campaign-posts',
            //                 },
            //             },
            //         ],
            //         as: 'participants',
            //     },
            // },
            // TODO: Check if the influencer is paid or not
            {
                $project: {
                    _id: 1,
                    title: 1,
                    status: 1,
                    budget: '$payment.budget',
                    campaignType: 1,
                    brand: {
                        $arrayElemAt: ['$brandName', 0],
                    },
                    // participants: '$participants',
                },
            },
            // Pagination
            {
                $skip: page - 1,
            },
            {
                $limit: constants.PAGE_SIZE,
            },
        ],
        async function (err, result) {
            if (err) {
                return cb({
                    error: true,
                    message: err,
                });
            }
            return cb(null, {
                error: false,
                data: result,
            });
        }
    );
};

module.exports = {
    ListCampaignPayoutInfluencers,
};
