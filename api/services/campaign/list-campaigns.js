const constants = require('../../constants');
const mongoose = require('mongoose');
const Influencers = require('../../models/influencer/influencer');
const Campaigns = require('../../models/campaign/campaign');

const ListCampaigns = async (queryparams, pathVars, userId, cb) => {
    let campaignType = queryparams.type || null;
    let pageNo = queryparams.pageNo ? parseInt(queryparams.pageNo) : 1;
    let size = constants.PAGE_SIZE || 10;
    let sortBy = queryparams.sortBy || 'createdAt';
    let sortType = queryparams.sortType || 1;
    let isGetBookmarkedOnly = queryparams.bookmarkOnly;

    // Path variable of brandId to fetch campaigns by only that brand
    // See usage: {@link campaign-routes.js}
    let brandId = pathVars.id || null;

    if (sortType === 'asc' || sortType === true || sortType === 1) sortType = 1;
    if (sortType === 'desc' || sortType === false || sortType === -1) sortType = -1;

    // Filter for campaign by category
    let categories = queryparams.categories ? queryparams.categories : [];
    for (let id in categories) {
        categories[id] = mongoose.Types.ObjectId(categories[id]);
    }
    let queryObj;

    if (categories.length == 0) {
        queryObj = {
            status: constants.ACTIVE,
        };
    } else {
        queryObj = {
            status: constants.ACTIVE,
            'brief.categories': {
                $in: categories,
            },
        };
    }

    if (brandId !== null) {
        queryObj = {
            status: constants.ACTIVE,
            brandId: mongoose.Types.ObjectId(brandId),
        };
    }

    if (campaignType !== null) queryObj.campaignType = campaignType;

    let filters = {
        status: [constants.DRAFT, constants.ACTIVE, constants.EXPIRED, constants.COMPLETED, constants.ARCHIVE],
        type: [constants.CASH, constants.PRODUCT],
    };
    let sort = ['title', 'createdAt'];

    if (pageNo <= 0)
        return cb(
            {
                error: true,
                message: ['Invalid page number, should start with 1'],
            },
            null
        );

    const query = [
        {
            $match: queryObj,
        },
        {
            $lookup: {
                from: 'bookmarked-campaigns',
                let: {
                    campaignId: '$_id',
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    {
                                        $eq: ['$campaign', '$$campaignId'],
                                    },
                                    {
                                        $eq: ['$influencer', mongoose.Types.ObjectId(userId)],
                                    },
                                ],
                            },
                        },
                    },
                ],
                as: 'check',
            },
        },
        {
            $lookup: {
                from: 'categories',
                localField: 'brief.categories',
                foreignField: '_id',
                as: 'brief.categories',
            },
        },
        {
            $addFields: {
                isBookmarked: { $anyElementTrue: ['$check'] },
            },
        },
        {
            $unset: ['check', 'report'],
        },
        {
            $sort: {
                [sortBy]: sortType,
            },
        },
    ];

    // Check if want to fetch only bookmarked campaigns
    if (isGetBookmarkedOnly === 'true') {
        query.push({
            $match: {
                isBookmarked: true,
            },
        });
    }

    // Pagination
    query.push({
        $skip: (pageNo - 1) * size,
    });

    Influencers.findById(userId)
        .lean()
        .then(async (user) => {
            if (user != null) {
                await Campaigns.aggregate(query).then((campaigns) => {
                    return cb(null, {
                        message: constants.SUCCESS,
                        error: false,
                        data: campaigns,
                        filters: filters,
                        sort: sort,
                    });
                });
            } else {
                return cb(
                    {
                        error: true,
                        message: ['Not authorized to do this operation'],
                    },
                    null
                );
            }
        });
};

module.exports = {
    ListCampaigns,
};
