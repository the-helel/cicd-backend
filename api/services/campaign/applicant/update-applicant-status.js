const Applicant = require('../../../models/campaign/applicant');
const constants = require('../../../constants');

const UpdateApplicantStatus = async (applicantId, cb) => {
    try {
        const applicant = await Applicant.findById(applicantId);
        if (!applicant) {
            return cb(
                {
                    error: true,
                    message: 'Applicant not found',
                },
                null
            );
        }

        if (applicant.status === constants.COMPLETED) {
            return cb(null, {
                error: false,
                message: 'Influencer has already completed campaign',
            });
        }

        applicant.status = constants.COMPLETED;
        await applicant.save();

        return cb(null, {
            error: false,
            message: 'Marked campaign as completed successfully',
        });
    } catch (e) {
        return cb(
            {
                error: true,
                message: constants.SERVER_ERR,
            },
            null
        );
    }
};

module.exports = { UpdateApplicantStatus };
