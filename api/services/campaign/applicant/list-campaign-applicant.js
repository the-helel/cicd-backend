const CampaignApplicant = require('../../../models/campaign/applicant');
const { getTimeSince } = require('../../../../utils/formatter');
const { setApplicantFields } = require('../utils.js');
const Campaign = require('../../../models/campaign/campaign.js');
const constants = require('../../../constants.js');

/**
 * List campaign applicants for campaign overview and listing
 *
 * @param query The find query on Applicant model
 * @param pageNo Page number to fetch
 * @param size Defaults to 10 {constants.PAGE_SIZE}
 * @param cb Callback which return err and result
 * @returns {Promise<void>}
 * @constructor
 */
// TODO: Refactor this function to support more filters, can use aggregation
const GetCampaignApplicants = async (query, sortBy, pageNo, size, cb) => {
    let deliverables = 0;
    console.log('Sorting used', sortBy);

    // Get campaign tasks
    await Campaign.findOne({ _id: query.campaignId }, async (err, campaign) => {
        if (campaign === null) {
            return cb('Campaign not found', null);
        }

        if (campaign.brief.campaignPlatform === constants.INSTAGRAM) {
            Object.keys(campaign.criteria.task.instagram).forEach((key) => {
                deliverables += campaign.criteria.task.instagram[key];
            });
        }

        await CampaignApplicant.find(query)
            .populate({
                path: 'influencerId',
                model: 'influencer',
                select: { name: 1, profilePicture: 1, city: 1, createdAt: 1 },
            })
            .populate({
                path: 'link',
                transform: (doc, id) => {
                    doc = doc.toObject();
                    doc.since = getTimeSince(doc.posted_at);
                    return doc;
                },
            })
            .skip(size * (pageNo - 1))
            .limit(size)
            .exec(async (err, applicant) => {
                if (err) {
                    return cb(err, null);
                }

                if (applicant !== null) {
                    // Count number of records available
                    const countQuery = await CampaignApplicant.where(query).countDocuments();

                    applicant = await Promise.all(
                        applicant.map(async function (e) {
                            return await setApplicantFields(e, deliverables);
                        })
                    );

                    // Sort applicant object based on followers
                    // if (sortBy === 'followers') {
                    //     applicant = applicant.sort((a, b) => {
                    //         Ascending order
                    //         return a.followers - b.followers;
                    //         Descending order
                    //         return b.followers - a.followers;
                    //     });
                    // }

                    return cb(null, {
                        count: countQuery,
                        data: applicant,
                    });
                } else {
                    return cb(null, { message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            });
    });
};

module.exports = {
    GetCampaignApplicants,
};
