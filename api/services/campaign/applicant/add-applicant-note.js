const constants = require('../../../constants.js');
const Note = require('../../../models/campaign/applicant-note.js');
const mongoose = require('mongoose');

/**
 * Organizations can add short notes when selecting influencers.
 *
 * @param {*} req
 * @param {*} cb
 * @returns
 */
const AddApplicantNote = async (req, cb) => {
    const { applicantId, note } = req.body;

    try {
        await Note.create({
            applicantId,
            note,
            addedBy: req.user,
        });

        return cb(null, {
            error: false,
            message: [constants.SUCCESS],
        });
    } catch (err) {
        return cb(err);
    }
};

/**
 * List notes created be members for the given applicant.
 *
 * @param {*} req
 * @param {*} cb
 * @returns
 */
const GetApplicantNotes = async (req, cb) => {
    const { applicantId } = req.params;

    const { page } = req.query;

    try {
        const notes = await Note.aggregate([
            {
                $match: {
                    applicantId: mongoose.Types.ObjectId(applicantId),
                },
            },
            {
                $sort: {
                    createdAt: -1,
                },
            },
            {
                $skip: page * constants.PAGE_SIZE,
            },
            {
                $limit: constants.PAGE_SIZE,
            },
            // Populate addedBy field
            {
                $lookup: {
                    from: 'companies',
                    let: {
                        uid: '$addedBy',
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ['$_id', '$$uid'],
                                },
                            },
                        },
                        {
                            $addFields: {
                                isMine: {
                                    $eq: ['$_id', mongoose.Types.ObjectId(req.user)],
                                },
                            },
                        },
                    ],
                    as: 'postedBy',
                },
            },
            {
                $unwind: {
                    path: '$postedBy',
                },
            },
            {
                $project: {
                    'postedBy.role': 1,
                    'postedBy.email': 1,
                    'postedBy.accountHolderName': 1,
                    'postedBy.isMine': 1,
                    note: 1,
                },
            },
        ]);

        return cb(null, {
            error: false,
            data: notes,
        });
    } catch (e) {
        return cb({
            error: true,
            message: [e.message],
        });
    }
};

/**
 * Delete specific note created by the current user.
 */
const DeleteNote = async (req, cb) => {
    const { noteId } = req.params;
    try {
        await Note.findById(noteId).then(async (obj) => {
            if (obj === null) {
                return cb({
                    error: true,
                    message: ['Not found'],
                });
            }

            if (obj.addedBy.toString() !== req.user) {
                return cb({
                    error: true,
                    message: ['You cannot delete this note'],
                });
            } else {
                await obj.remove();
                return cb(null, {
                    error: false,
                    message: [constants.SUCCESS],
                });
            }
        });
    } catch (e) {
        return cb({
            error: true,
            message: [e.message],
        });
    }
};

/**
 * Update already created note.
 */
const UpdateNote = async (req, cb) => {
    const { noteId } = req.params;
    const { note } = req.body;

    try {
        await Note.findById(noteId).then(async (obj) => {
            if (obj === null) {
                return cb({
                    error: true,
                    message: ['Not found'],
                });
            }

            if (obj.addedBy.toString() !== req.user) {
                return cb({
                    error: true,
                    message: ['You cannot edit this note'],
                });
            } else {
                obj.note = note;
                await obj.save();
                return cb(null, {
                    error: false,
                    message: [constants.SUCCESS],
                });
            }
        });
    } catch (e) {
        return cb({
            error: true,
            message: [e.message],
        });
    }
};

module.exports = { AddApplicantNote, GetApplicantNotes, DeleteNote, UpdateNote };
