const { GetCampaignApplicants } = require('./list-campaign-applicant');
const { UpdateApplicantStatus } = require('./update-applicant-status');
const { AddApplicantNote, GetApplicantNotes, DeleteNote, UpdateNote } = require('./add-applicant-note.js');

module.exports = {
    GetCampaignApplicants,
    UpdateApplicantStatus,
    AddApplicantNote,
    GetApplicantNotes,
    DeleteNote,
    UpdateNote,
};
