const BookmarkCampaign = require('../../models/campaign/bookmark-campaign');
const constants = require('../../../constants.js');

const AddToBookmarkCampaign = async function bookmarkCampaign(id, influencerId, isBookmarked, cb) {
    if (id !== null) {
        const query = {
            influencer: influencerId,
            campaign: id,
        };

        console.log('Add to bookmark');

        await BookmarkCampaign.findOne(query)
            .then(async (result) => {
                if (result !== null) {
                    return await BookmarkCampaign.deleteOne(query)
                        .then((result) => {
                            cb(null, {
                                error: false,
                                message: constants.SUCCESS,
                            });
                        })
                        .catch((err) => cb(err, null));
                } else {
                    const bookmarked = await BookmarkCampaign.create(query);
                    await bookmarked.save();
                    return cb(null, {
                        error: false,
                        message: constants.SUCCESS,
                    });
                }
            })
            .catch((err) => {
                cb(err, null);
            });
    } else {
        return cb(
            {
                error: true,
                message: 'CampaignId is required',
            },
            null
        );
    }
};

module.exports = {
    AddToBookmarkCampaign,
};
