const Applicants = require('../../models/campaign/applicant.js');
const mongoose = require('mongoose');
const constants = require('../../../constants.js');

/**
 * Aggregate the campaign applicants metadata like total selected, rejected or approved
 * influencers.
 *
 * @param {*} id The _id of campaign
 * @returns Aggregation operation on Applicant schema
 */
const CampaignApplicantAggregate = async function aggregateApplicant(id, cb) {
    Applicants.aggregate([
        {
            $match: {
                campaignId: mongoose.Types.ObjectId(id),
            },
        },
        {
            $group: {
                _id: null,
                total_applicant_count: {
                    $sum: 1,
                },
                total_selected_count: {
                    $sum: {
                        $cond: [
                            {
                                $eq: ['$status', constants.APPROVED],
                            },
                            1,
                            0,
                        ],
                    },
                },
                total_rejected_count: {
                    $sum: {
                        $cond: [
                            {
                                $eq: ['$status', constants.REJECTED],
                            },
                            1,
                            0,
                        ],
                    },
                },
                total_completed_count: {
                    $sum: {
                        $cond: [
                            {
                                $eq: ['$status', constants.COMPLETED],
                            },
                            1,
                            0,
                        ],
                    },
                },
                total_spend_budget_on_influencer: {
                    $sum: '$finalBidAmount',
                },
            },
        },
    ])
        .then((result) => {
            return cb(null, result);
        })
        .catch((err) => cb(err, null));
};

module.exports = CampaignApplicantAggregate;
