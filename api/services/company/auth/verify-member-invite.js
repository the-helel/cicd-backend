const Company = require('../../../models/company/company.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../../../config.js');

const VerifyMemberInvite = async (req, cb) => {
    try {
        const { id, password, token } = req.body;
        await Company.findById(id).then(async (member) => {
            if (!member) {
                return cb({
                    status: 404,
                    message: 'Company not found',
                });
            }

            // Create encrypted password
            jwt.verify(token, config.JWT_SECRET);

            await bcrypt.genSalt(10).then(async (salt) => {
                await bcrypt.hash(password, salt).then((hash) => {
                    member.password = hash;
                    member.verificationStatus = true;
                    member.save();
                });
            });

            return cb(null, {
                status: true,
                message: 'Success',
                member: member,
            });
        });
    } catch (err) {
        return cb({
            error: true,
            message: err.message,
        });
    }
};

module.exports = {
    VerifyMemberInvite,
};
