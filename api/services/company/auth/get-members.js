const Company = require('../../../models/company/company.js');

/**
 * List all the members of organization
 * @param {*} req
 * @param {*} cb
 * @returns
 */
const GetMembers = async (req, cb) => {
    try {
        const members = await Company.find(
            {
                companyId: req.uid,
            },
            'role mobile verificationStatus email accountHolderName createdAt'
        );
        return cb(null, {
            status: true,
            message: 'Get Members',
            data: members,
        });
    } catch (e) {
        return cb(e);
    }
};

module.exports = {
    GetMembers,
};
