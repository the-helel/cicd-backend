module.exports = {
    template: (name, url) => `
    <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        div,
        p {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.6;
        }

        .button {
            background-color: #ffc344;
            padding: 12px 12px;
            display: inline-block;
            color: #fff;
            text-decoration: none;
            border-radius: 8px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
            -webkit-text-size-adjust: none;
            box-sizing: border-box;

        }

        a {
            color: white;
        }

        a:visited {
            color: white;
        }

        .content-table {
            border-collapse:collapse;background:#ffffff;width:590.545px;margin:0 auto;
        }
    </style>
</head>

<body>

    <!-- Reset Text -->
    <div
        style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
        <div style="
        color: #000000;
        font-size: 0px;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-weight: 400;
        letter-spacing: normal;
        text-align: center;
        text-indent: 0px;
        text-transform: none;
        white-space: normal;
        word-spacing: 0px;
        background: 0px 0px transparent;
        text-decoration-style: initial;
        text-decoration-color: initial;
        margin: 0px auto;
        max-width: 600px;
      ">
            <table style="
          border-collapse: collapse;
          background: 0px 0px transparent;
          width: 590.545px;
        " cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                    <tr>
                        <td style="
                border-collapse: collapse;
                direction: ltr;
                font-size: 0px;
                padding: 0px;
                text-align: center;
              ">
                            <div style="
                  max-width: 100%;
                  width: 590.545px;
                  font-size: 0px;
                  text-align: left;
                  direction: ltr;
                  display: inline-block;
                  vertical-align: top;
                ">
                                <table style="border-collapse: collapse" width="100%" cellspacing="0" cellpadding="0"
                                    border="0">
                                    <tbody>
                                        <tr>
                                            <td style="
                          border-collapse: collapse;
                          background-color: transparent;
                          vertical-align: top;
                          padding: 0px;
                        ">
                                                <table style="border-collapse: collapse" width="100%" cellspacing="0"
                                                    cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="
                                  border-collapse: collapse;
                                  font-size: 0px;
                                  padding: 0px;
                                  word-break: break-word;
                                " align="center">
                                                                <table style="
                                    border-collapse: collapse;
                                    border-spacing: 0px;
                                  " cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="
                                          border-collapse: collapse;
                                          width: 100px;
                                        ">
                                                                                <img alt="Famstar Logo"
                                                                                    src="https://famstar.s3.ap-south-1.amazonaws.com/logo.png"
                                                                                    style="
                                            border: 0px;
                                            height: auto;
                                            line-height: 13px;
                                            outline: 0px;
                                            text-decoration: none;
                                            border-radius: 0px;
                                            display: block;
                                            width: 100.364px;
                                            font-size: 13px;
                                            margin-top:  10px;
                                          " width="100" height="auto" class="CToWUd" />
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
            border="0" align="center">
            <tbody>
                <tr>
                    <td
                        style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
                        <div
                            style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                            <table style="border-collapse:collapse;background-color:transparent;vertical-align:top"
                                width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word"
                                            align="left">
                                            <div
                                                style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                                                <p style="display:block;margin:0px">
                                                <h2>Hi ${name},</h2>
                                                </p>
                                                <p style="display:block;margin:0px">&nbsp;</p>
                                                <p style="display:block;margin:0px">You have been invited by ${name} to
                                                    join the organization on Famstar.</p>
                                                <p style="display:block;margin:0px">By joining, your name and email
                                                    address will be visible to other members of the organization.
                                                    Only join organizations you recognize.</p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div>
        <table cellspacing="0" cellpadding="0"
            border="0" align="center">
            <tbody>
                <tr>
                    <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:16px;text-align:center">
                        <div
                            style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                            <table style="border-collapse:collapse;background-color:transparent;vertical-align:top"
                                width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word"
                                            align="left">
                                            <div
                                                style="font-size:24px;letter-spacing:0px;line-height:1.5;text-align:left;color:#000000">
                                                <p style="display:block;margin:0px;text-align:center"><a href=${url}
                                                        class="button">
                                                        Accept Invitation
                                                    </a></p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- Contact info starts -->
    <div
        style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#f8faf6;margin:0px auto;max-width:600px">
        <table style="border-collapse:collapse;background:#f8faf6;width:590.545px" cellspacing="0" cellpadding="0"
            border="0" align="center">
            <tbody>
                <tr>
                    <td
                        style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px 16px 0px;text-align:center">
                        <div
                            style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                            <table style="border-collapse:collapse;background-color:transparent;vertical-align:top"
                                width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td style="border-collapse:collapse;font-size:0px;padding:0px 16px 5px;word-break:break-word"
                                            align="left">
                                            <div
                                                style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                                                <p style="display:block;margin:0px">In case you have any queries or
                                                    feedback, please write to us at <a href="mailto:hello@famstar.in"
                                                        style="color: #393262;">hello@famstar.in</a></p>

                                                <p style="display:block;margin:0px">&nbsp;</p>
                                                <p style="display:block;margin:0px"><span style="color:#1b2631">We read
                                                        every email :)</span></p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- contact info ends -->
    <!-- ******************************************* -->

    <!-- End text starts  -->
    <div
        style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
        <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
            border="0" align="center">
            <tbody>
                <tr>
                    <td
                        style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
                        <div
                            style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                            <table style="border-collapse:collapse;background-color:transparent;vertical-align:top"
                                width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word"
                                            align="left">
                                            <div
                                                style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                                                <p style="display:block;margin:0px"><span style="color:#1b2631">Happy
                                                        Influencing,</span></p>
                                                <p style="display:block;margin:0px"><span style="color:#393262"><b>Team
                                                            Famstar.</b></span></p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- End text ends -->
    <!-- *************************************************** -->
    <!-- Social Icons starts here -->
    <div
        style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
        <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
            border="0" align="center">
            <tbody>
                <tr>
                    <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
                        <div
                            style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                            <table style="border-collapse:collapse;background-color:transparent;vertical-align:top"
                                width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td
                                            style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                                            <div style="height:12px">
                                                &nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word"
                                            align="center">
                                            <table style="border-collapse:collapse;float:none;display:inline-table"
                                                cellspacing="0" cellpadding="0" border="0" align="center">
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                                                            <table
                                                                style="border-collapse:collapse;border-radius:3px;width:24px"
                                                                cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td
                                                                            style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px">
                                                                            <a href="https://twitter.com/famstarhq"
                                                                                rel="noopener" target="_blank"
                                                                                data-saferedirecturl="https://twitter.com/famstarhq"><img
                                                                                    alt="twitter"
                                                                                    src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png"
                                                                                    style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block"
                                                                                    width="24" height="24"
                                                                                    class="CToWUd"></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.linkedin.com/company/famstar/" rel="noopener" target="_blank" data-saferedirecturl="https://www.linkedin.com/company/famstar/"><img alt="linkedin" src="https://ci5.googleusercontent.com/proxy/D79Rrzm2yqxpbyaq9obVZRGTSzhvMEgIqLu463HdX9OIutpMOcEoE78JXfpHrslgXMmE87Bv-jTJrIo9ill_sj1nW9WkHBIMjjRjvqcWktcmvBrFpFYwXINwpJvLz6jRYtK35wLfZfrjPsNNeSJyyQKMd6yFX6T9pUxJYm43Sst3-cy0O04F-GCymI1Fqk0KSKc8tLm0yw=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845188311_linkedin.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                                            <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218493000&amp;usg=AFQjCNETS6jnN8bYjJe-ymkGNd_Ab1fP_w"><img alt="youtube" src="https://ci4.googleusercontent.com/proxy/W1l9kK-GAECVakgi6Jz4Nxb78e7vl33uaFoSVjDxD_wjLUl3uzCd8_wzD-4AR2AV5Urcvc9lHix0ZhmYwW-Fdph4vKhj49Jqz4cCcfdgIDUJgXJusc20b-X1vQ87TjOT60KwRsJXSuO62ja5H20KCLfnKRcBk_wFZLfqYWyrowYELbPGzl2gwriCHBYqzyNXkmlu3Owb=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845199657_youtube.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                                            <table style="border-collapse:collapse;float:none;display:inline-table"
                                                cellspacing="0" cellpadding="0" border="0" align="center">
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                                                            <table
                                                                style="border-collapse:collapse;border-radius:3px;width:24px"
                                                                cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td
                                                                            style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px">
                                                                            <a href="https://www.facebook.com/famstarHQ"
                                                                                rel="noopener" target="_blank"
                                                                                data-saferedirecturl="https://www.facebook.com/famstarHQ"><img
                                                                                    alt="facebook"
                                                                                    src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png"
                                                                                    style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block"
                                                                                    width="24" height="24"
                                                                                    class="CToWUd"></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style="border-collapse:collapse;float:none;display:inline-table"
                                                cellspacing="0" cellpadding="0" border="0" align="center">
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                                                            <table
                                                                style="border-collapse:collapse;border-radius:3px;width:24px"
                                                                cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td
                                                                            style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px">
                                                                            <a href="https://www.instagram.com/famstarhq/"
                                                                                rel="noopener" target="_blank"
                                                                                data-saferedirecturl="https://www.instagram.com/famstarhq/"><img
                                                                                    alt="instagram"
                                                                                    src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png"
                                                                                    style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block"
                                                                                    width="24" height="24"
                                                                                    class="CToWUd"></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word"
                                            align="left">
                                            <div
                                                style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                                                <p style="display:block;margin-top:10px;text-align:center">You are
                                                    receiving this mail because you are a member at Famstar</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td
                                            style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                                            <div style="height:8px">
                                                &nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- Social icons ends here -->
    <!-- ********************************************************* -->
    <div
        style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
        <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0"
            cellpadding="0" border="0" align="center">
            <tbody>
                <tr>
                    <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
                        <div
                            style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                            <table style="border-collapse:collapse;background-color:transparent;vertical-align:top"
                                width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td
                                            style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                                            <div style="height:2px">
                                                &nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word"
                                            align="left">
                                            <div
                                                style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                                                <p style="display:block;margin:0px;text-align:center"><span
                                                        style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span
                                                        style="color:#84919e;font-size:12px"><i>© 2021</i></span><span
                                                        style="font-size:12px"><i><span>&nbsp;</span></i></span><span
                                                        style="color:#888888;font-size:12px">Famstar.in</span></p>
                                                <p style="display:block;margin:0px;text-align:center"><span
                                                        style="color:#888888;font-size:10px"><i>Made with ❤️ in
                                                            India</i></span></p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

</body>

</html>
  `,
};
