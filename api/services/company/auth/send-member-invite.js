const mongoose = require('mongoose');
const Company = require('../../../models/company/company');
const constants = require('../../../constants');
const sendEmail = require('../../../controllers/util/email.js');
var jwt = require('jsonwebtoken');
const setPasswordTemplate = require('./templates/set-password-member-invite.js');
const config = require('../../../config.js');

const SendMemberInvite = async (req, cb) => {
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
        const { email, role, accountHolderName } = req.body;

        // If member is already invited
        const check = await Company.findOne({ email: email });

        if (check !== null) {
            return cb(
                {
                    error: true,
                    message: 'Member already invited',
                },
                null
            );
        }

        // Get the company details
        let company = await Company.findById(req.user).catch((e) => {
            throw Error('Company not found');
        });

        if (![constants.ORGANIZATION, constants.MEMBER].includes(role)) {
            return cb({
                error: false,
                message: [`${role} is not valid type`],
            });
        }

        if (company === null) {
            return cb({
                error: true,
                message: ['Company not found'],
            });
        }

        // Create new user to invite with this email for this organization
        let member = await Company.create({
            email: email,
            companyId: req.user,
            role: role,
            password: null,
            accountHolderName,
            mobile: null,
            name: company.name,
        });

        // Sending invite verification link
        let verificationToken = jwt.sign(
            {
                userId: member._id,
                userType: member.role,
                role: member.role,
            },
            config.JWT_SECRET
        );

        // Send email to the user to verify and complete user registration profile
        await sendEmail(email, {
            subject: 'Invitation to join ' + company.name,
            body: setPasswordTemplate.template(
                accountHolderName,
                ` http://${req.headers.host}/company/invite/?id=${member._id}&token=${verificationToken}`
            ),
        }).catch(async (err) => {
            console.log(error, 'Error while sending email to', email);
            await session.abortTransaction();
            session.endSession();
            return cb({ error: true, message: [constants.SERVER_ERR] }, null);
        });

        // commiting the transaction and end session
        await session.commitTransaction();
        session.endSession();

        return cb(null, {
            error: false,
            message: `Send invite on ${email} for ${role}`,
        });
    } catch (e) {
        await session.abortTransaction();
        session.endSession();
        return cb({
            error: true,
            message: [e.message],
        });
    }
};

module.exports = { SendMemberInvite };
