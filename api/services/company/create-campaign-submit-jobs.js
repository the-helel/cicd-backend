const constants = require('../../constants');
const Campaign = require('../../models/campaign/campaign');
const kAsync = require('async');
const { closeCampaignJob, startCampaignJob, initCampaignPostJob } = require('../../jobs/adapters/campaign-jobs');
const { campaigns } = require('./index');

const createCampaignJobs = async function (id, cb) {
    // Get campaign data
    await Campaign.findById(id, async (err, result) => {
        if (result === null) {
            return cb({
                error: true,
                message: 'Campaign not found',
            });
        }
        if (result.status === constants.SUBMITTED) {
            return cb('You cannot again submit this campaign', null);
        } else if (result.status === constants.DRAFT) {
            const endDate = result.brief.endDate;
            const startDate = result.brief.startDate;

            await kAsync.series(
                {
                    liveTask: function (callback) {
                        startCampaignJob(id, startDate).then(() =>
                            callback(null, {
                                message: 'Success',
                            })
                        );
                    },
                    closeTask: function (callback) {
                        closeCampaignJob(id, endDate).then(() =>
                            callback(null, {
                                message: 'Success',
                            })
                        );
                    },
                    // TODO: When submitted created new CRON Job to update this campaign posts till T+1 month after campaign.
                    updateCampaignPostsTask: function (callback) {
                        initCampaignPostJob(id, endDate).then(() =>
                            callback(null, {
                                message: 'Update campaign post job',
                            })
                        );
                    },
                    updateCampaignStatus: function (callback) {
                        result.status = constants.SUBMITTED;
                        result.markModified('status');
                        result.save().then((result, err) =>
                            callback(null, {
                                message: 'Updated status of campaign',
                            })
                        );
                    },
                },
                function (err, result) {
                    if (err) {
                        return cb(err, null);
                    }
                    return cb(null, {
                        error: false,
                        message: constants.SUCCESS,
                        jobStatus: result,
                    });
                }
            );
        } else {
            return cb(null, {
                error: false,
                message: 'Campaign is already submitted',
            });
        }
    });

    // Create start active job

    // Create expire campaign job
};

module.exports = {
    createCampaignJobs,
};
