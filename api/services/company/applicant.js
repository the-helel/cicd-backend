const Applicants = require('../../models/campaign/applicant');
const { updateCampaignPostLinks } = require('../influencer');
const constants = require('../../constants.js');

/**
 * The influencer id will be passed here then update its post.
 *
 * @param {*} req
 * @param {*} res
 * @param {*} influencer
 * @param {*} next
 */
async function updateApplicantCampaignPosts(req, campaignId, next) {
    console.log(`Campaign id: ${campaignId}`);
    console.log(`User is ${req.user}`);
    try {
        // Get all applicants
        Applicants.find({
            campaignId: campaignId,
            status: constants.APPROVED,
        })
            .then((applicants) => console.log(applicants))
            .catch((err) => console.log('Error occured getting applicants', err));
        await updateCampaignPostLinks();
    } catch (e) {
        console.log('Update applicant campaign post error', e);
    }
}

module.exports = {
    updateApplicantCampaignPosts,
};
