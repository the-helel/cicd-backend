const Campaign = require('../../models/campaign/campaign');
const Company = require('../../models/company/company');
const constants = require('../../../constants');

/**
 * Get the company campaigns for listing. Fetch them by company id.
 *
 * Filters can be status Active, Draft, Completed, Archive
 *
 * Sort by is available for:
 *  Title, Created Time,
 *
 * @param req
 * @param done
 * @returns {Promise<*>}
 */
async function getCompanyCampaigns(req, done) {
    // Get query parameters
    let status = req.query.status || null;
    let type = req.query.type || null;
    let pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    let size = constants.PAGE_SIZE || 10;
    let sortBy = req.query.sortBy || 'createdAt';
    let sortType = req.query.sortType || -1;

    if (sortType === 'asc' || sortType === true || sortType === 1) sortType = 1;
    if (sortType === 'desc' || sortType === false || sortType === -1) sortType = -1;

    let queryObj = {
        companyId: req.uid,
    };

    if (status !== null) {
        const statusFields = status.split(',');
        queryObj.status = { $in: statusFields };
    }
    // not showing archive data if explicitly not defined
    else queryObj.status = { $ne: constants.ARCHIVE };

    if (type !== null) {
        const campaignType = type.split(',');
        queryObj.campaignType = { $in: campaignType };
    }

    let filters = {
        status: [constants.DRAFT, constants.ACTIVE, constants.EXPIRED, constants.COMPLETED, constants.ARCHIVE],
        type: [constants.CASH, constants.PRODUCT],
    };
    let sort = ['title', 'createdAt'];

    if (pageNo <= 0)
        return done(
            {
                error: true,
                message: 'Invalid page number, should start with 1',
            },
            null
        );

    Company.findById(req.uid)
        .lean()
        .then(async (user) => {
            if (user != null) {
                const countQuery = await Campaign.where(queryObj).countDocuments();

                await Campaign.find(queryObj)
                    .skip(size * (pageNo - 1))
                    .limit(size)
                    .sort([[sortBy, sortType]])
                    .lean()
                    .then((campaigns) => {
                        if (campaigns != null && campaigns.length > 0) {
                            return done(null, {
                                message: ' success',
                                count: countQuery,
                                data: campaigns,
                                filters: filters,
                                sort: sort,
                            });
                        }

                        return done(null, {
                            message: 'No result found',
                            count: countQuery,
                            data: campaigns,
                            filters: filters,
                            sort: sort,
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                        return done({ error: true, message: 'Error occurred while fetching the record' }, null);
                    });
            } else {
                return done({
                    error: true,
                    message: 'Not authorized to perform this operation',
                });
            }
        });
}

module.exports = {
    getCompanyCampaigns,
};
