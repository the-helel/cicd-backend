const InstagramDiscoverAPI = require('../../third-party-api/instagram/discover-api');

// TODO: Get the engagement rate of user
async function getEngagementOnInstagramPosts() {}

/**
 * Find the influence by platform and get data about their profile and some metrics like engagement rate using official
 * APIs.
 *
 * NOTE: Currently only support Instagram platform.
 *
 * @param platform
 * @param username
 * @param cb
 * @returns {Promise<*>}
 * @constructor
 */
const SearchInfluencer = async (platform, username, cb) => {
    // TODO: Add logic to automatically change token after some time.
    const token =
        'EAAHFrLiMKMMBAPtKb1vIQzwBORZBilqaHPEMBxz1RWCt8ZCErkXcNw8udMpZAM0aa2e8Isg5UwTY072rJaMl3Y8jbWGV5Nvs8UGKiK0lZAqi4sq5mCfRHtghaUfNtOZBasD4mtwbq8V5eW6j3ndTZA9ceD53dKZBk9TCalAxM0JBguecogyJ5g2';

    const discoverInstance = new InstagramDiscoverAPI(token);

    // TODO: Check if we have already data for this influencer in elastic.
    try {
        const profile = await discoverInstance.getMetadataByUserName(username);
        const medias = await discoverInstance.getMedias(username);

        // Aggregate likes and comments on top 10 posts
        const postLimit = 10;
        let totalEngagement = 0;

        medias['business_discovery'].media.data.map((el, index) => {
            if (index > postLimit - 1) {
                return;
            }
            let engagementValue = el.comments_count + el.like_count;
            totalEngagement += engagementValue;
        });

        // Engagement rate of user
        const engagementRate = ((totalEngagement / postLimit / profile.business_discovery.followers_count) * 100).toFixed(2);

        // TODO: If data is not present, push this new data to elastic and return result.

        return cb(null, {
            error: false,
            data: {
                profile: profile.business_discovery,
            },
            count: medias.business_discovery.media.data.length,
            content: medias.business_discovery.media.data,
            metric: {
                engagementRate,
            },
        });
    } catch (e) {
        return cb(
            {
                error: true,
                message: 'Data not found',
            },
            null
        );
    }
};

module.exports = SearchInfluencer;
