const constants = require('../../../constants');
const Subscription = require('../../../models/notification/subscriber');
const _ = require('lodash');
const fcm = require('../../../firebase/fcm');

const SendNotificationOnlyInfluencers = async (title, body, image, cb) => {
    try {
        const topic = 'influencers';
        // Notification options
        const notificationOptions = {
            priority: 'high',
            timeToLive: 60 * 60 * 24,
        };

        const data = {
            click_action: 'FLUTTER_NOTIFICATION_CLICK',
        };

        const notificationObject = { title: title, body: body };

        if (image !== null) {
            notificationObject.image = image;
        }

        let message = { notification: notificationObject, data: data, topic: topic };

        await fcm.messaging().send(message);

        return cb(null, constants.SUCCESS);
    } catch (e) {
        return cb(e, null);
    }
};

module.exports = {
    SendNotificationOnlyInfluencers,
};
