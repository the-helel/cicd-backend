const { UpdateCampaignStatus } = require('./update-campaign-status');

module.exports = {
    UpdateCampaignStatus,
};
