const Campaign = require('../../../models/campaign/campaign');

/**
 * Admin will update the status of campaign.
 *
 * @param campaignId
 * @param status
 * @param cb
 * @returns {Promise<*>}
 * @constructor
 */
const UpdateCampaignStatus = async (campaignId, status, cb) => {
    try {
        Campaign.findById(campaignId)
            .then(async (campaign) => {
                if (campaign === null) {
                    return cb({
                        error: true,
                        message: 'Campaign not found',
                    });
                }

                campaign.status = status;
                await campaign.save();
                return cb(null, {
                    error: false,
                    message: `Marked campaign as ${status}`,
                    data: campaign,
                });
            })
            .catch((e) =>
                cb(
                    {
                        error: true,
                        message: e.toString(),
                    },
                    null
                )
            );
    } catch (e) {
        return cb(e, null);
    }
};

module.exports = {
    UpdateCampaignStatus,
};
