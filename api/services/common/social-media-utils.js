const Facebook = require('../../models/social/facebook');
const Instagram = require('../../models/social/instagram');
const Applicant = require('../../models/campaign/applicant.js');

/**
 * Get the facebook token of user by unique influencer id.
 * @param userId The userId of influencer
 */
const getFacebookPageTokenByUserId = async (userId) => {
    return Facebook.findOne({ influencer: userId })
        .then((facebook) => {
            return facebook;
        })
        .catch((e) => console.log(e));
};

/**
 * Get the instagram basic info user by id.
 * @param instagramId The _id of connected instagram account of influencer.
 */
const getInstagramById = (id) => {
    return Instagram.findById(id)
        .then(async (instagram) => {
            return instagram;
        })
        .catch((e) => Promise.reject(e));
};

/**
 * Get the instagram basic info user by applicant id.
 */
const getInstagramByApplicantId = (applicantId) => {
    return Applicant.findById(applicantId)
        .then(async (applicant) => {
            return getInstagramById(applicant.social);
        })
        .catch((e) => Promise.reject(e));
};

module.exports = {
    getFacebookPageTokenByUserId,
    getInstagramById,
    getInstagramByApplicantId,
};
