const CampaignPost = require('../../models/campaign/post.js');

/**
 * Get all the top campaign posts by sorting them according to their engagement
 * and limit to 10 results and get their hashtags for word cloud.
 *
 * @param id The campaign id
 * @param cb The callback method
 * @returns {Promise<void>}
 */
const CampaignTopPostsHashtags = async function campaignTopPosts(id, cb) {
    CampaignPost.find({
        campaignId: id,
    })
        .sort({ engagement: -1 })
        .limit(5)
        .exec(function (err, result) {
            if (err) {
                return cb(err, null);
            }

            // Extract caption and find hashtags
            let wordCloud = [];
            result.map((e) => {
                const hashtags = scrapeHashtags(e.caption);
                wordCloud.push(...hashtags);
            });

            cb(null, arrToInstanceCountObj(wordCloud));
        });
};

const arrToInstanceCountObj = (arr) =>
    arr.reduce((obj, e) => {
        obj[e] = (obj[e] || 0) + 1;
        return obj;
    }, {});

const scrapeHashtags = (caption) => {
    const regex = /(?:^|\s)(?:#)([a-zA-Z\d]+)/gm;
    let matches = [];
    let match;

    while ((match = regex.exec(caption))) {
        matches.push(match[1]);
    }

    return matches;
};

module.exports = {
    CampaignTopPostsHashtags,
};
