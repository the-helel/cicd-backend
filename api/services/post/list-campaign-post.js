const CampaignPost = require('../../models/campaign/post.js');
const mongoose = require('mongoose');

/**
 * Aggregate the campaign applicants metadata like total selected, rejected or approved
 * influencers.
 *
 * @param {*} id The _id of campaign
 * @param cb The callback function of async parallel methods
 * @returns Aggregation operation on Applicant schema
 */
const CampaignPostAgg = async function aggregateCampaignPosts(id, cb) {
    CampaignPost.aggregate([
        {
            $match: {
                campaignId: mongoose.Types.ObjectId(id),
            },
        },
        {
            $group: {
                _id: null,
                total_post_count: {
                    $sum: 1,
                },
                total_like_count: {
                    $sum: '$like_count',
                },
                total_comment_count: {
                    $sum: '$comment_count',
                },
                total_engagement: {
                    $sum: '$engagement',
                },
                total_video_view_count: {
                    $sum: '$video_view_count',
                },
                total_impression: {
                    $sum: '$impressions',
                },
                total_reach: {
                    $sum: '$actual_reach',
                },
            },
        },
    ])
        .then((result) => cb(null, result))
        .catch((err) => cb(err, null));
};

module.exports = {
    CampaignPostAgg,
};
