const PostTimeline = require('../../models/campaign/timeline');
const mongoose = require('mongoose');

/**
 * Get posts timeline metrics aggregate of engagement, likes, impression and video views.
 *
 * @param id Campaign id
 * @param cb Callback of async parallel
 * @returns {Promise<void>}
 */
const PostTimelineAgg = async function campaignPostTimelines(id, cb) {
    const sortQuery = {
        $sort: {
            _id: 1,
        },
    };

    const dateFormatQuery = '%Y-%m-%d-%H-%M-%S';

    PostTimeline.aggregate([
        {
            $match: { campaign: mongoose.Types.ObjectId(id) },
        },
        {
            $facet: {
                engagement_timeline: [
                    { $unwind: '$engagements' },
                    {
                        $group: {
                            _id: {
                                $dateToString: {
                                    format: dateFormatQuery,
                                    date: {
                                        $convert: {
                                            input: '$engagements.timestamp',
                                            to: 'date',
                                        },
                                    },
                                },
                            },
                            value: { $sum: '$engagements.value' },
                        },
                    },
                    sortQuery,
                ],
                likes_timeline: [
                    { $unwind: '$likes' },
                    {
                        $group: {
                            _id: {
                                $dateToString: {
                                    format: dateFormatQuery,
                                    date: {
                                        $convert: {
                                            input: '$likes.timestamp',
                                            to: 'date',
                                        },
                                    },
                                },
                            },
                            value: { $sum: '$likes.value' },
                        },
                    },
                    sortQuery,
                ],
                impressions_timeline: [
                    { $unwind: '$impressions' },
                    {
                        $group: {
                            _id: {
                                $dateToString: {
                                    format: dateFormatQuery,
                                    date: {
                                        $convert: {
                                            input: '$impressions.timestamp',
                                            to: 'date',
                                        },
                                    },
                                },
                            },
                            value: { $sum: '$impressions.value' },
                        },
                    },
                    sortQuery,
                ],
                video_view_timeline: [
                    { $unwind: '$video_views' },
                    {
                        $group: {
                            _id: {
                                $dateToString: {
                                    format: dateFormatQuery,
                                    date: {
                                        $convert: {
                                            input: '$video_views.timestamp',
                                            to: 'date',
                                        },
                                    },
                                },
                            },
                            value: { $sum: '$video_views.value' },
                        },
                    },
                    sortQuery,
                ],
            },
        },
    ])
        .then((result) => {
            cb(null, result);
        })
        .catch((err) => cb(err, null));
};

module.exports = {
    PostTimelineAgg,
};
