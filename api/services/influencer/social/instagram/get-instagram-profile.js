const GraphAPI = require('../../../../controllers/util/instagram-api');
const Instagram = require('../../../../models/social/instagram');
const Facebook = require('../../../../models/social/facebook');

/**
 * Currently not using this code. Check {@link update-instagram-profile}
 *
 * @param influencerId
 * @param cb
 * @returns {Promise<void>}
 * @constructor
 */
const GetInstagramProfile = async (influencerId, cb) => {
    console.log('Get engagement rate');
    // Get all the instagram objects
    await Instagram.find()
        .then(async (ig) => {
            ig.map(async (igProfile) => {
                console.log('=============');
                await Facebook.findOne(
                    {
                        influencer: igProfile.influencer,
                    },
                    async (err, facebook) => {
                        if (facebook !== null) {
                            // Get the engagement of latest 10 posts
                            const api = new GraphAPI(facebook.pageAccessToken);

                            // Limit to only latest 10 posted content
                            const postToFetch = 12;
                            const fetchPosts = await api.fetchPosts(igProfile.userId, postToFetch);

                            // Iterate over each and find engagement rate
                            let temp = {
                                engagement: 0,
                                impression: 0,
                            };

                            let data = await Promise.all(
                                fetchPosts.data.map(async (e, index) => {
                                    await api.getMediaInsight(e.id, e.media_type).then((media) => {
                                        temp.engagement = media.data[1].values[0].value;
                                        temp.impression = media.data[0].values[0].value;
                                    });

                                    return temp.engagement / temp.impression;
                                })
                            );

                            let sum = 0;
                            data.forEach((e) => (sum += e));
                            console.log(sum);
                            igProfile.engagementRate = sum / postToFetch;
                            await igProfile.save();
                        }
                    }
                );
            });
        })
        .catch((err) => console.log("Can't find instagram"));
};

module.exports = { GetEngagementRateProfile: GetInstagramProfile };
