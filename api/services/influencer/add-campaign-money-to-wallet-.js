const Wallet = require('../../models/wallet/wallet');
const Transaction = require('../../models/wallet/transaction');
const mongoose = require('mongoose');
const constants = require('../../constants');
const Applicant = require('../../models/campaign/applicant');
const Brand = require('../../models/company/brand.js');
const NotificationSDK = require('../../controllers/util/notification.js');

const AddCampaignMoneyToWallet = async (influencerId, applicantId, brandId, cb) => {
    const session = await mongoose.startSession();
    // Step 2: Optional. Define options to use for the transaction
    const transactionOptions = {
        readPreference: 'primary',
        readConcern: { level: 'local' },
        writeConcern: { w: 'majority' },
    };

    try {
        if (!applicantId) {
            return cb({
                error: true,
                message: 'Applicant id is not provided',
            });
        }

        // Find wallet of influencer
        await Wallet.findOne({
            influencer: influencerId,
        }).then(async (wallet) => {
            if (wallet) {
                await session
                    .withTransaction(async () => {
                        const applicant = await Applicant.findById(applicantId);
                        let brand;

                        if (!applicant) {
                            return cb(
                                {
                                    error: true,
                                    message: 'Applicant not found',
                                },
                                null
                            );
                        }

                        brand = await Brand.findById(brandId);

                        if (!brand) {
                            return cb(
                                {
                                    error: true,
                                    message: 'Brand not found',
                                },
                                null
                            );
                        }

                        if (applicant.status === constants.COMPLETED) {
                            return cb(null, {
                                error: false,
                                message: 'Influencer is already paid',
                            });
                        }

                        // Add the amount to influencers wallet available amount.
                        wallet.totalEarnings += applicant.finalBidAmount;
                        wallet.moneyToBeWithdrawn += applicant.finalBidAmount;

                        await wallet.save();

                        // Create a new transaction record
                        const transaction = new Transaction();
                        transaction.influencer = influencerId;
                        transaction.amount = applicant.finalBidAmount;
                        transaction.metadata.description = `Amount credited for successful collaboration with ${brand.name} campaign.`;

                        await transaction
                            .save()
                            .then(async () => {
                                // Mark the current influencer applicant id as completed
                                applicant.status = constants.COMPLETED;
                                await applicant.save();

                                // Send notification to influencer on receiving payment.
                                await NotificationSDK.sendOnlyPushNotification(
                                    influencerId,
                                    'Payment received',
                                    `Congratulation for collaboration with ${brand.name} campaign.`,
                                    brand.logo.url
                                );

                                return cb(null, {
                                    error: false,
                                    data: wallet,
                                    transaction,
                                    applicant,
                                });
                            })
                            .catch((e) => {
                                return cb(
                                    {
                                        error: true,
                                        message: e,
                                    },
                                    null
                                );
                            });
                    }, transactionOptions)
                    .catch((e) => {
                        return cb({
                            error: true,
                            message: e,
                        });
                    });
            } else {
                return cb(null, {
                    error: false,
                    message: 'Influencer wallet not found',
                });
            }
        });
    } catch (e) {
        return cb(
            {
                error: true,
                message: e,
            },
            null
        );
    } finally {
        await session.endSession();
    }
};

module.exports = { AddMoneyToWallet: AddCampaignMoneyToWallet };
