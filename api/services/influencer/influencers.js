const Applicants = require('../../models/campaign/applicant');
const Instagram = require('../../models/social/instagram');
const constants = require('../../constants.js');
const { getFacebookPageTokenByUserId, getInstagramById, getInstagramByApplicantId } = require('../common/social-media-utils');
const GraphAPI = require('../../controllers/util/instagram-api');
const Post = require('../../models/campaign/post');
const { uploadUsingUrl } = require('../../controllers/util/s3.js');
const Timeline = require('../../models/campaign/timeline.js');
require('async-foreach');

/**
 * Influencer campaign post submission. We have to call several APIs to get
 * initial insights and store them. Then later perform updates on campaign
 * posts periodically.
 */
async function updateCampaignPostLinks(req, res, next) {
    const { postLink } = req.body;

    // If we are updating from Company side then use influencerId from applicant schema.
    const userId = req.user;

    Applicants.findById(req.params.applicantId)
        .then(async (applicant) => {
            // Check applicant if exists
            if (!applicant)
                return res.status(404).send({
                    error: true,
                    message: ['Cannot find any approved application with unverified link using given details'],
                });

            // The influencer can submit campaign posts only if approved by brand. In other cases, throw
            // not eligible right now.
            if (applicant.status !== constants.APPROVED) {
                return res.status(400).send({
                    error: true,
                    message: ['Campaign has not approved to to update link'],
                });
            }

            // Influencer will pass array of links
            let err;
            if (postLink) {
                if (applicant.applied_platform === 'Instagram') {
                    // Get all the shortCodes submitted till now
                    // url.match(/^(?:.*\/p\/)([\d\w\-_]+)/)
                    // Get which type of media is being uploaded
                    // Get the token from facebook collection and create this ig-media
                    // post insight object in collection.
                    // Find the social accounts related data of user and get its token.
                    await getFacebookPageTokenByUserId(req.user)
                        .then(async (facebook) => {
                            console.log('Instagram _id: ', applicant.social);
                            await getInstagramById(applicant.social).then(async (instagram) => {
                                const api = new GraphAPI(facebook.pageAccessToken);

                                // This regex will return link, type, shortcode of instagram post
                                let shortCode = postLink.match(/^(?:.*\/)(.*\/)([\d\w\-_]+)/);

                                // Get the media url type from url
                                const type = shortCode[1].substr(0, shortCode[1].length - 1);

                                // TODO: Check if that shortcode is already submitted or not
                                // Quickfix: Doing it one frontend

                                // Right now instagram does not provide insights for [REEL]
                                // More info here: {@link https://developers.facebook.com/docs/instagram-api/reference/ig-media}
                                if (type === 'reel' || type == 'REEL') {
                                    return res.status(400).json({
                                        error: false,
                                        message: ['Reels are not support right now'],
                                    });
                                }

                                api.searchMediaIdByShortCode(instagram.userId, shortCode[2])
                                    .then((media) => {
                                        let post = new Post();

                                        post.influencer = req.user;
                                        // Create a new Post instance and save its objectId in applicant
                                        // Set the IG object id to the owner of the post
                                        post.campaignId = applicant.campaignId;
                                        post.applicantId = applicant._id;
                                        post.platform = constants.INSTAGRAM;
                                        post.short_code = shortCode[2];

                                        api.getMediaPublicMetrics(media.id)
                                            .then(async (metric) => {
                                                post.post_link = metric.permalink;
                                                post.posted_at = Date.now();
                                                post.media.type = metric.media_type;
                                                post.media.id = media.id;
                                                post.caption = metric.caption;
                                                post.like_count = metric.like_count;
                                                post.comment_count = metric.is_comment_enabled ? metric.comments_count : 0;

                                                // Upload the IMAGE to S3 to use as thumbnail
                                                let thumb_url;
                                                console.log(metric);
                                                // When media type is [VIDEO] use thumbnail_url
                                                if (metric.media_type === 'VIDEO') {
                                                    thumb_url = metric.thumbnail_url;
                                                } else {
                                                    thumb_url = metric.media_url;
                                                }

                                                // Save thumbnail url to S3 bucket
                                                const url = await uploadUsingUrl(thumb_url);
                                                post.media.display_url = url[0];

                                                api.getMediaInsight(media.id, metric.media_type)
                                                    .then(async (insights) => {
                                                        post.impressions = insights.data[0].values[0].value;
                                                        post.engagement = insights.data[1].values[0].value;
                                                        post.actual_reach = insights.data[2].values[0].value;
                                                        post.engagement_rate_by_impression = (post.engagement / post.impressions) * 100;
                                                        post.engagement_rate_by_reach = (post.engagement / post.actual_reach) * 100;
                                                        post.engagement_rate_by_followers =
                                                            (post.engagement / instagram.numberOfFollowers) * 100;

                                                        let impression = { timestamp: Date.now(), value: post.impressions };
                                                        let likes = { timestamp: Date.now(), value: post.like_count };
                                                        let engagements = { timestamp: Date.now(), value: post.engagement };

                                                        let video_views;
                                                        if (metric.media_type !== 'IMAGE') {
                                                            post.video_view_count = insights.data[3].values[0].value;
                                                            video_views = { timestamp: Date.now(), value: post.video_view_count };
                                                        }

                                                        post.save()
                                                            .then(async (data) => {
                                                                const saveToApplicant = data._id;

                                                                applicant.link.push(saveToApplicant);
                                                                applicant.markModified('link');

                                                                // Update applicant details
                                                                applicant.status = constants.APPROVED;
                                                                applicant.save(async (err, appliedData) => {
                                                                    // Create a new timeline document for current post and set
                                                                    // inital timestamp. The campaign post update cron job will
                                                                    // update these fields periodically.
                                                                    let timeline = Timeline();
                                                                    timeline.influencer = req.user;
                                                                    timeline.applicant = applicant._id;
                                                                    timeline.post = data._id;
                                                                    timeline.campaign = post.campaignId;
                                                                    timeline.impressions.push(impression);
                                                                    timeline.likes.push(likes);
                                                                    timeline.engagements.push(engagements);

                                                                    if (metric.media_type !== 'IMAGE') {
                                                                        timeline.video_views.push(video_views);
                                                                    }

                                                                    await timeline.save();

                                                                    if (err)
                                                                        res.status(500).send({
                                                                            error: true,
                                                                            message: [err.message],
                                                                        });

                                                                    res.status(200).send({
                                                                        error: false,
                                                                        message: 'Success',
                                                                    });
                                                                });
                                                            })
                                                            .catch((e) => console.log('Error saving campaign-post ', e));
                                                    })
                                                    .catch((e) => Promise.reject(e));
                                            })
                                            .catch((e) => {
                                                console.log('getMediaPublicMetrics error: ', e);
                                            });
                                    })
                                    .catch((e) => {
                                        // Handle catch when we can't find metadata of the user post
                                        err = e;
                                        return res.status(400).json({
                                            error: false,
                                            message: [err],
                                        });
                                    });
                            });
                        })
                        .catch((e) => console.log(e));
                }
            } else {
                return res.status(400).send({
                    error: true,
                    message: ['Campaign post link not provided'],
                });
            }
        })
        .catch((e) =>
            res.status(400).send({
                error: true,
                message: constants.SERVER_ERR,
            })
        );
}

module.exports = {
    updateCampaignPostLinks,
};
