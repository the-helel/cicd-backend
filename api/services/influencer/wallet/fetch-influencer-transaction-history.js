const Transaction = require('../../../models/wallet/transaction');
const constants = require('../../../constants');

const FetchInfluencerTransactionHistory = async (influencerId, page, cb) => {
    Transaction.find({
        influencer: influencerId,
    })
        .skip(constants.PAGE_SIZE * (page - 1))
        .limit(constants.PAGE_SIZE)
        .sort({ createdAt: -1 })
        .lean()
        .then(async (records) => {
            return cb(null, records);
        })
        .catch((e) => cb(e, null));
};

module.exports = {
    FetchInfluencerTransactionHistory,
};
