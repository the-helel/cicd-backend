const { validationResult } = require('express-validator');

const ContactMessage = require('../models/contact-message');

const constants = require('../constants');
const sendEmail = require('./util/email');

const middlewares = {};

middlewares.addContactMessage = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const message = new ContactMessage();
        message.type = req.body.type;
        message.name = req.body.name;
        message.phoneNumber = req.body.phoneNumber;
        message.email = req.body.email;
        message.body = req.body.body;

        message
            .save()
            .then(async () => {
                let [sentToRia, errRia] = await sendEmail('ria@famstar.in', {
                    subject: 'Famstar User Contacted Website',
                    body: `
                        <b>${req.body.name} (${req.body.type}) </b> <br />
                        <b>Email: ${req.body.email} tel: ${req.body.phoneNumber}</b> <br />
                        <p>${req.body.body}</p>
                    `,
                });
                let [sentToHello, errHello] = await sendEmail('hello@famstar.in', {
                    subject: 'Famstar User Contacted Website',
                    body: `
                        <b>${req.body.name} (${req.body.type}) </b> <br />
                        <b>Email: ${req.body.email} tel: ${req.body.phoneNumber}</b> <br />
                        <p>${req.body.body}</p>
                    `,
                });

                if (errRia) {
                    throw new Error(errRia);
                }
                if (errHello) {
                    throw new Error(errHello);
                }

                res.status(200).json({ error: false, message: [constants.SUCCESS] });
            })
            .catch((error) => {
                res.status(400).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.readContact = async (req, res, next) => {
    let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    let filter = {};
    let count;

    if (req.query.search) {
        filter['$text'] = { $search: req.query.search };
    }

    const limit = parseInt(size);
    const skip = parseInt((page - 1) * size);

    try {
        let data = await ContactMessage.find(filter)
            .collation({ locale: 'en', strength: 2 })
            .sort({ _id: -1 })
            .skip(skip)
            .limit(limit)
            .lean();

        if (req.query.search) {
            count = await ContactMessage.countDocuments(filter);
        } else {
            count = await ContactMessage.estimatedDocumentCount();
        }

        return res.status(200).json({ error: false, result: data, nPages: Math.ceil(count / limit) });
    } catch (err) {
        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
    }
};

module.exports = middlewares;
