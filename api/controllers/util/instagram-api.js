const axios = require('axios');

module.exports = class InstagramGraphAPI {
    /**
     * Get the facebook page access token from Facebooks collection
     *
     **/
    constructor(token) {
        this.token = token;
    }

    getMediaInsight(mediaId, mediaType) {
        let metrics;

        // NOTE: The video_views metric is not available if media object is image, the api
        // will return 400 error if metrics are not correct as per type of media.
        if (mediaType == 'IMAGE') {
            metrics = 'impressions,engagement,reach';
        } else {
            metrics = 'impressions,engagement,reach,video_views';
        }

        if (this.token) {
            const url = `https://graph.facebook.com/v11.0/${mediaId}/insights?metric=${metrics}&access_token=${this.token}`;

            return axios
                .get(url)
                .then((response) => {
                    return response.data;
                })
                .catch((e) => {
                    return Promise.reject(e.response.error);
                });
        }
    }

    searchMediaIdByShortCode(instagramId, shortCode) {
        if (this.token) {
            const url = `https://graph.facebook.com/v10.0/${instagramId}/media?fields=caption,media_type,media_url,like_count,engagement,shortcode,video_title&access_token=${this.token}&limit=100`;

            return axios
                .get(url)
                .then((response) => {
                    // Filter the id for given shortCode

                    let object = response.data.data.filter((val) => val.shortcode == shortCode);

                    if (object.length == 0) {
                        throw Error('Error occured');
                    }
                    return object[0];
                })
                .catch((e) => {
                    return Promise.reject('URL seems to be not yours');
                    // console.log('Error in searchMediaIdByShortCode ', e.message);
                    // return Promise.reject(e.response.error);
                });
        }
    }

    fetchPosts(instagramId, limit) {
        if (this.token) {
            const url = `https://graph.facebook.com/v10.0/${instagramId}/media?fields=caption,media_type,media_url,like_count,engagement,shortcode,video_title&access_token=${this.token}&limit=${limit}`;

            return axios
                .get(url)
                .then((response) => {
                    // Filter the id for given shortCode
                    return response.data;
                })
                .catch((e) => {
                    return Promise.reject('URL seems to be not yours');
                    // console.log('Error in searchMediaIdByShortCode ', e.message);
                    // return Promise.reject(e.response.error);
                });
        }
    }

    /**
     * For reference, check this documentation
     * @{link} https://developers.facebook.com/docs/instagram-api/reference/ig-media
     *
     * @param {*} mediaId The media id of post
     * @returns
     */
    getMediaPublicMetrics(mediaId) {
        const fields =
            'caption,comments_count,id,ig_id,is_comment_enabled,like_count,media_product_type,media_type,media_url,owner,permalink,shortcode,video_title,username,thumbnail_url';
        if (this.token) {
            if (mediaId == undefined) {
                throw new Error('Reels are not supported yet or some other technical issue');
            }

            const url = `https://graph.facebook.com/v11.0/${mediaId}?fields=${fields}&access_token=${this.token}&limit=100`;

            try {
                return axios
                    .get(url)
                    .then((response) => {
                        return response.data;
                    })
                    .catch((e) => {
                        console.log('getMediaPublicMetrics error: ', e.response.data);
                        throw new Error('Reels are not supported yet');
                    });
            } catch (e) {
                console.log('Some error occured');
            }
        }
    }
};
// 615b05a5b46907efb9b0b057
