const AWS = require('aws-sdk');
const { default: axios } = require('axios');

const config = require('../../config');

const s3Config = {
    secretAccessKey: config.S3_SECRET_ACCESS_KEY,
    accessKeyId: config.S3_ACCESS_KEY_ID,
    region: config.S3_REGION,
};

const middlewares = {};

/**
 * Function to get profile picture by userId and upload it to s3
 * @param {String} url URL of the string to upload on s3
 * @param {String?} name Filename
 * @returns {Array} [s3URL, err] return [s3URL, null] on sucess else [null, err]
 */
middlewares.uploadUsingFaceBookUserId = (userId) => {
    return new Promise(async (resolve, reject) => {
        if (!userId) reject(new Error('Required params not present'));

        const s3 = new AWS.S3(s3Config);

        try {
            let response = await axios.get(`https://graph.facebook.com/v10.0/${userId}/picture?type=large`, {
                responseType: 'arraybuffer',
            });

            let picture = await response.data;
            let key = userId + new Date().getTime().toString();

            let params = {
                Body: picture,
                Bucket: config.S3_BUCKET_NAME,
                ACL: 'public-read',
                Key: key,
                ContentType: 'image/png',
            };

            let fileS3MetaData = await s3.upload(params).promise();

            resolve([fileS3MetaData.Location, null]);
        } catch (err) {
            reject([null, err]);
        }
    });
};

/**
 * Function to upload to s3 using url
 * @param {String} url URL of image file to upload on s3
 */
middlewares.uploadUsingUrl = async (url) => {
    return new Promise(async (resolve, reject) => {
        if (!url) reject(new Error('Required params not present'));

        if (typeof url !== 'string') reject(new TypeError(`Expected url to be string, got ${typeof url}`));

        const s3 = new AWS.S3(s3Config);

        try {
            let response = await axios.get(url, {
                responseType: 'arraybuffer',
            });

            let picture = await response.data;
            let key = new Date().getTime().toString();

            let params = {
                Body: picture,
                Bucket: config.S3_BUCKET_NAME,
                ACL: 'public-read',
                Key: key,
                ContentType: 'image/png',
            };

            let fileS3MetaData = await s3.upload(params).promise();

            resolve([fileS3MetaData.Location, null]);
        } catch (err) {
            reject([null, err]);
        }
    });
};

module.exports = middlewares;
