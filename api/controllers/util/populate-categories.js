const Category = require('../../models/category');

const populateCategories = async (categoryIds) => {
    const categories = [];
    for (let categoryId of categoryIds) {
        await Category.findById(categoryId)
            .select({ _id: 1, name: 1, icon: 1 })
            .lean()
            .then((category) => {
                if (category != null) {
                    categories.push(category);
                }
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }
    return categories;
};

module.exports = populateCategories;
