const Location = require('../../models/location');

const middlewares = {};

middlewares.getLocations = (req, res, next) => {
    Location.find()
        .lean()
        .then((locations) => {
            res.status(200).json({ error: false, result: locations });
        })
        .catch((error) => {
            res.status(400).json({ error: true, message: [err.message] });
        });
};

module.exports = middlewares;
