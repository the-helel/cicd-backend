/**
 * SDK to send notifications
 */

const fcm = require('../../firebase/fcm');
const _ = require('lodash');

const Subscription = require('../../models/notification/subscriber');
const Notification = require('../../models/notification/notification');
const admin = require('../../firebase/fcm');
const constants = require('../../constants');
const Influencer = require('../../models/influencer/influencer');

const NotificationSDK = {};

/**
 * Function to send notifications to all influencers
 * @param {String} title Title of notification
 * @param {String} body Body text of notification
 * @param {String=} image Image url of image to show in notification
 * @param {String} type Type of notification
 * @param {String} subtype subtype of notification
 * @param {String=}  parameter parameters for notification
 * @returns {Promise<String | Error>} Promise resolve 'SUCCESS' or err
 * @example
 *      sendNotificationToAllInfluencers(
 *          'New Campaigns',
 *          'Open famstar app to see new campaigns',
 *          'https://somedomain.com/image.jpg',
 *          'ADMIN_NOTIF'
 *      )
 *      .then(() => { console.log("Notifications sent successufully")})
 *      .catch((error) =>  throw new Error(error))
 */
NotificationSDK.sendNotificationToAllInfluencers = async (title, body, image, type, subType = null, parameter = null) => {
    return Subscription.find({ userType: constants.INFLUENCER })
        .lean()
        .then(async (subscriptions) => {
            for (let subscription of subscriptions) {
                const notificationOptions = {
                    priority: 'high',
                    timeToLive: 60 * 60 * 24,
                };

                await createNotification(title, body, image, subscription.userId, type, subType, parameter).then(async (message) => {
                    const data = {
                        type: type,
                        subType: subType,
                        click_action: 'FLUTTER_NOTIFICATION_CLICK',
                    };
                    for (const param in parameter) {
                        data[param] = parameter[param].toString();
                    }

                    const notificationObject = { title: title, body: body };
                    if (image != null) {
                        notificationObject.image = image;
                    }

                    await fcm.messaging().sendToDevice(
                        doc.fcmToken,
                        {
                            notification: notificationObject,
                            data: data,
                        },
                        notificationOptions
                    );
                });
            }

            console.log('Resolved');
            return;
        })
        .catch((err) => {
            return Promise.reject(err);
        });
};

/**
 *
 * @param {String} title Title of notification
 * @param {String} body Body of Notification
 * @param {String=} image Image Url , if notification have image
 * @param {String} type Type of notification
 * @param {String=} subType Subtype of notification
 * @param {String=} parameter Parameters for notification
 * @param {Boolean=} saveNotification Create Notification in notifications collection or not
 */
NotificationSDK.sendPushNotificationToAll = async (
    title,
    body,
    image,
    type,
    subType = null,
    parameter = null,
    saveNotification = false
) => {
    return new Promise(async function (resolve, reject) {
        if (!title || !body || !type) {
            throw new Error('Params title, body and type are required');
        }

        if (typeof title !== 'string' || typeof body !== 'string' || typeof type !== 'string') {
            throw new TypeError('Invalid param types. Expected Strings');
        }

        try {
            if (saveNotification) {
                let influencers = await Influencer.find({}, { _id: 1 });
                let influencersId = influencers.flatMap((influencer) => influencer._id);

                let notificationCreatePromises = [];
                notificationCreatePromises = _.map(influencersId, (id) => {
                    return createNotification(title, body, image, id, type, subType, parameter);
                });

                await Promise.all(notificationCreatePromises).then((data) => console.log(data));
            }

            const notificationOptions = {
                priority: 'high',
                timeToLive: 60 * 60 * 24,
            };

            const data = {
                type: type,
                subType: subType ? subType : '',
                click_action: 'FLUTTER_NOTIFICATION_CLICK',
            };
            for (const param in parameter) {
                data[param] = parameter[param].toString();
            }

            const notificationObject = { title: title, body: body };
            if (image != null) {
                notificationObject.image = image;
            }

            let fcmTokens = await Subscription.find({}, { fcmToken: 1, _id: 0 });

            let tokens = fcmTokens.flatMap((x) => x.fcmToken);
            let chunkedTokens = _.chunk(tokens, 1); // creating chunks, with 1000 tokens in each token
            let notificationPayload = { notification: notificationObject, data: data };

            let promises = [];

            promises = _.map(chunkedTokens, async (chunk) => {
                return fcm.messaging().sendToDevice(chunk[0], notificationPayload, notificationOptions);
            });

            await Promise.all(promises).then((data) => {
                resolve([true, null]);
            });
        } catch (err) {
            reject([null, err]);
        }
    });
};

/**
 * Function to send notification to particular user
 * @param {string} id mongoId of influencer
 * @param {String} title Title of notification
 * @param {String} body Body text of notification
 * @param {String=} image Image url of image to show in notification
 * @param {String} type Type of notification
 * @param {String} subtype subtype of notification
 * @param {Object=}  parameter parameters for notification
 * @returns {Promise<[data, err]>} Resolves or Rejects
 *
 * @example
 */
NotificationSDK.sendNotificationById = (id, title, body, image, type, subType = null, parameter = null) => {
    return new Promise(function (resolve, reject) {
        if (!title || !body || !type) {
            throw new Error('Params title, body and type are required');
        }

        if (typeof title !== 'string' || typeof body !== 'string' || typeof type !== 'string') {
            throw new TypeError('Invalid param types. Expected Strings');
        }

        Subscription.findOne({ userId: id })
            .lean()
            .then(async (doc) => {
                const notificationOptions = {
                    priority: 'high',
                    timeToLive: 60 * 60 * 24,
                };

                createNotification(title, body, image, doc.userId, type, subType, parameter)
                    .then(async (message) => {
                        const data = {
                            type: type,
                            subType: subType,
                            click_action: 'FLUTTER_NOTIFICATION_CLICK',
                        };
                        for (const param in parameter) {
                            data[param] = parameter[param].toString();
                        }

                        const notificationObject = { title: title, body: body };
                        if (image != null) {
                            notificationObject.image = image;
                        }

                        fcm.messaging()
                            .sendToDevice(
                                doc.fcmToken,
                                {
                                    notification: notificationObject,
                                    data: data,
                                },
                                notificationOptions
                            )
                            .then((response) => {
                                console.log('Notification sent:', response);
                                return resolve([true, null]);
                            })
                            .catch((err) => {
                                console.log(err);
                                Promise.reject([null, err]);
                            });
                    })
                    .catch((error) => {
                        console.log(error);
                        Promise.reject([null, error.message]);
                    });
            })
            .catch((err) => {
                console.log(err);
                Promise.reject([null, err]);
            });
    });
};

NotificationSDK.sendOnlyPushNotification = (id, title, body, image) => {
    return new Promise(function (resolve, reject) {
        if (!title || !body) {
            throw new Error('Params title, body and type are required');
        }

        if (typeof title !== 'string' || typeof body !== 'string') {
            throw new TypeError('Invalid param types. Expected Strings');
        }

        console.log('Some error');

        Subscription.findOne({ userId: id })
            .lean()
            .then(async (doc) => {
                const notificationOptions = {
                    priority: 'high',
                    timeToLive: 60 * 60 * 24,
                };

                const data = {
                    click_action: 'FLUTTER_NOTIFICATION_CLICK',
                };

                const notificationObject = { title: title, body: body };
                if (image != null) {
                    notificationObject.image = image;
                }

                fcm.messaging()
                    .sendToDevice(
                        doc.fcmToken,
                        {
                            notification: notificationObject,
                            data: data,
                        },
                        notificationOptions
                    )
                    .then((response) => {
                        console.log('Notification sent:', response);
                        return resolve([true, null]);
                    })
                    .catch((err) => {
                        console.log(err);
                        Promise.reject([null, err]);
                    });
            })
            .catch((err) => {
                console.log(err);
                Promise.reject([null, err]);
            });
    });
};

async function createNotification(title, body, image, userId, type, subType = null, parameter = null) {
    const notification = new Notification();
    notification.user = userId;
    notification.title = title;
    notification.body = body;
    if (image != null) notification.image = image;
    notification.type = type;
    notification.subType = subType;
    notification.parameter = parameter;

    return await notification
        .save()
        .then(async () => {
            return constants.SUCCESS;
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject([error.message, null]);
        });
}

module.exports = NotificationSDK;
