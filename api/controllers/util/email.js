/**
 * SDK to send email
 */

const AWS = require('aws-sdk');

const config = require('../../config');

// configuring SES
const SESConfig = {
    piVersion: '2010-12-01',
    accessKeyId: config.SES_ACCESS_KEY_ID,
    secretAccessKey: config.SES_SECRET_ACCESS_KEY,
    region: config.SES_REGION,
};

/**
 * Function to send email to a single email address
 * @param {String} to - Email Address of reciever
 * @param {object} emailBody - Object containing subject and body of email. Body can have string with HTML attributes
 * @returns Promise
 * @example
 *  Using .then() .catch()
 *  >>> sendEmail('jhon.doe@gmail.com', { subject: 'Amazing Email', body: 'Lorem Ipsum})
 *          .then(() => {})
 *          .catch((err) => console.log(err) )
 * @example
 *  Using async await
 *  >>> let [sent, error] = await sendEmail('jhon.doe@gmail.com', { subject: 'Amazing Email', body: 'Lorem Ipsum})
 *      if(error) console.log(error)
 *      if(sent) // handle success
 *
 */
let sendEmail = (to, emailBody) => {
    return new Promise(function (resolve, reject) {
        if (!to) {
            throw new Error('Param to is required');
        }

        if (!emailBody) {
            throw new Error('Param emailBody is required');
        }

        if (!emailBody.subject || !emailBody.body) {
            throw new Error('Param emailBody should have subject and body');
        }

        if (typeof to !== 'string') {
            throw new TypeError(`Expected param to, to be a string. Received ${typeof to}`);
        }

        if (typeof emailBody !== 'object') {
            throw new TypeError(`Expected param emailbody, to be an object. Recieved ${typeof emailBody}`);
        }

        var params = {
            Source: `${config.SES_FROM_DEFAULT}`,
            Destination: {
                ToAddresses: [`${to}`],
            },
            ReplyToAddresses: [`${config.SES_FROM_DEFAULT}`],
            Message: {
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: `${emailBody.body}`,
                    },
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: `${emailBody.subject}`,
                },
            },
        };

        new AWS.SES(SESConfig)
            .sendEmail(params)
            .promise()
            .then((res) => {
                resolve([true, null]);
            })
            .catch((err) => {
                reject([null, err]);
            });
    });
};

module.exports = sendEmail;
