const cityDB = require('indian-cities-database');
const mongoose = require('mongoose');

const Location = require('../../models/location');
const config = require('../../config');

mongoose
    .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to MongoDB instance');
        Location.create(cityDB.cities)
            .then(() => {
                console.log('All cities populated.');
            })
            .catch(() => {
                console.log('Error in populating locations.');
            });
    })
    .catch((error) => {
        console.log(error);
    });
