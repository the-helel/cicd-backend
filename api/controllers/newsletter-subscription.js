const { validationResult } = require('express-validator');

const NewsletterSubscription = require('../models/newsletter-subscription');

const constants = require('../constants.js');

const middlewares = {};

middlewares.addNewsletterSubscription = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const subscription = new NewsletterSubscription();
        subscription.type = req.body.type;
        subscription.email = req.body.email;

        subscription
            .save()
            .then(() => {
                res.status(200).json({ error: false, message: [constants.SUCCESS] });
            })
            .catch((error) => {
                res.status(400).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
