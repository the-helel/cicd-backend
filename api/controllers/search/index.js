const esclient = require('../../elasticsearch');

const addIndexMappings = async (index, schema) => {
    return await esclient.indices
        .exists({ index: index })
        .then(async ({ body, statusCode }) => {
            if (!body && statusCode == 200) {
                const error = await esclient.indices
                    .create({ index: index })
                    .then(({ body, statusCode }) => {
                        if (statusCode != 200) {
                            return Promise.reject(`${index} creation failure.`);
                        }
                        return;
                    })
                    .catch((error) => {
                        return Promise.reject(error);
                    });
                if (error) {
                    return error;
                }
            }

            return await esclient.indices
                .putMapping({
                    index: index,
                    body: schema,
                })
                .then(({ body, statusCode }) => {
                    if (statusCode != 200) {
                        return Promise.reject(`Failure in updating the mappings for ${index}.`);
                    }
                    return;
                })
                .catch((error) => {
                    return Promise.reject(error);
                });
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

const exec = async () => {
    await addIndexMappings('influencers', {
        properties: {
            name: { type: 'text' },
            aliasName: { type: 'text' },
            gender: { type: 'keyword' },
            city: { type: 'keyword' },
            categories: { type: 'nested' },
            username: { type: 'keyword' },
            followers: { type: 'long' },
            followings: { type: 'long' },
            bio: { type: 'text' },
        },
    }).catch((error) => {
        console.log(error);
    });
    return;
};

exec();
