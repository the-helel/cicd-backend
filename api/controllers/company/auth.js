const { validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var crypto = require('crypto');
const mongoose = require('mongoose');
const jwtDecode = require('jwt-decode');

var config = require('../../config');
const constants = require('../../constants');

// models
const Company = require('../../models/company/company');

// utils
const sendEmail = require('../util/email');
const templates = require('../../templates.js');
const CompanyVerificationEmailTemplate = require('../../services/company/auth/templates/company-verification-email.js');
const CompanyAuthService = require('../../services/company/auth');

const middlewares = {};

/**
 * Creates company Sends a verification email
 * to the email used during sign up.
 */
middlewares.createCompany = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: errors.array() });
    } else {
        let { email, password, name, mobile, accountHolderName, role } = req.body;

        try {
            // restricting the api to not create admin user via this route
            if (role === constants.ADMIN) {
                return res
                    .status(500)
                    .json({ error: true, message: [constants.SERVER_ERR, 'You cannot create admin user using this api.'] });
            }

            // starting a mongoose session
            const session = await mongoose.startSession();
            session.startTransaction();

            let company = await Company.findOne({ email: email }).session(session);

            if (company !== null) {
                if (company.verificationStatus === false) {
                    let verificationToken = jwt.sign(
                        {
                            userId: company._id,
                            userType: constants.COMPANY,
                            role: company.role,
                        },
                        config.JWT_SECRET,
                        {
                            expiresIn: constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES,
                        }
                    );

                    let [sent, error] = await sendEmail(email, {
                        subject: 'Please verify your Famstar Account',
                        body: CompanyVerificationEmailTemplate.template(
                            name,
                            `http://${req.headers.host}/company/register/verify/${verificationToken}`
                        ),
                    });

                    if (error) {
                        await session.abortTransaction();
                        session.endSession();
                        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
                    }

                    return res.status(200).json({ error: false, message: ['Verification link has been sent to email.'] });
                }

                await session.abortTransaction();
                session.endSession();
                return res.status(400).json({ error: true, message: ['Company with same email already registered'] });
            }

            let salt = await bcrypt.genSalt();
            let hash = await bcrypt.hash(password, salt);
            let profileRole = role ? role : constants.ORGANIZATION;

            let newCompany = new Company({ email, password: hash, name, role: profileRole, mobile, accountHolderName });

            newCompany.companyId = newCompany._id;
            await newCompany.save({ session: session });

            // sending verification link
            let verificationToken = jwt.sign(
                {
                    userId: newCompany._id,
                    userType: constants.COMPANY,
                    role: newCompany.role,
                },
                config.JWT_SECRET,
                {
                    expiresIn: constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES,
                }
            );

            let [sent, error] = await sendEmail(email, {
                subject: 'Please verify your Famstar Account',
                body: CompanyVerificationEmailTemplate.template(
                    name,
                    `http://${req.headers.host}/company/register/verify/${verificationToken}`
                ),
            });

            if (error) {
                console.log(error, 'Error while sending email to', email);
                await session.abortTransaction();
                session.endSession();
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            }

            // commiting the transaction and end session
            await session.commitTransaction();
            session.endSession();
            return res
                .status(200)
                .json({ error: false, message: [constants.SUCCESS, 'Verification link has been sent to email.'], data: req.body });
        } catch (error) {
            console.log(error, 'Error while commiting transaction and creating company', req.body);
            // await session.abortTransaction();
            // session.endSession();

            return res.status(500).json({ error: true, message: [constants.SERVER_ERR], data: req.body });
        }
    }
};

middlewares.resendVerificationLink = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    } else {
        let { email } = req.body;

        try {
            let company = await Company.findOne({ email: email });

            if (company === null) {
                return res.status(404).json({ error: true, message: ['Account not found. Signup first'] });
            }

            if (company.verificationStatus === true) {
                return res.status(200).send({ error: false, message: ['Email already verified!'] });
            }

            // if company found and is not verified
            let verificationToken = jwt.sign(
                {
                    userId: company._id,
                    userType: constants.COMPANY,
                    role: company.role,
                },
                config.JWT_SECRET,
                {
                    expiresIn: constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES,
                }
            );

            let [sent, error] = await sendEmail(email, {
                subject: 'Please verify your Famstar Account',
                body: `
                    http://${req.headers.host}/company/register/verify/${verificationToken}
                `,
            });
        } catch (error) {
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    }
};

// Verify token and update verification status
middlewares.verifyUser = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    } else {
        let token = req.params.token;
        let redirect_link = req.query.redirect_link ? req.query.redirect_link : null;

        try {
            jwt.verify(token, config.JWT_SECRET);

            const decodedToken = jwtDecode(token);

            let company = await Company.findById(decodedToken.userId);
            console.log(decodedToken);

            if (company === null) {
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            }

            company.verificationStatus = true;

            await company.save();

            if (redirect_link !== null) {
                console.log('Redirect link found. please ensure it is correct redirect url', redirect_link);
                res.cookie('token', token);
                return res.status(301).redirect(redirect_link);
            }

            return res.render('verified-company');
        } catch (err) {
            console.log('Error in verifyUser');
            return res.render('Something went wrong!');
        }
    }
};

// function to login a user
middlewares.login = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: errors.array() });
    } else {
        let { email, password } = req.body;

        let company = await Company.findOne({ email: email });

        // company found
        if (company !== null) {
            // company found but not verified email
            if (company.verificationStatus === false) {
                return res.status(400).json({ error: true, message: ['Email not verified'] });
            }

            try {
                let isPasswordMatched = await bcrypt.compare(password, company.password);

                // for master user company password and user password should match
                if (isPasswordMatched) {
                    let token = jwt.sign(
                        {
                            userId: company._id,
                            uid: company.companyId,
                            userType: constants.COMPANY,
                            role: company.role,
                        },
                        config.JWT_SECRET
                    );

                    return res.status(200).json({ error: false, auth: true, token });
                }

                return res.status(401).json({ error: true, message: ['Invalid Credentials'] });
            } catch (err) {
                console.log(err);
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            }
        }

        // company not found
        return res.status(404).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Company')] });
    }
};

middlewares.createMember = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: errors.array() });
    } else {
        await CompanyAuthService.SendMemberInvite(req, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    }
};

middlewares.verifyMember = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: errors.array() });
    } else {
        await CompanyAuthService.VerifyMemberInvite(req, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    }
};

middlewares.getMembers = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: errors.array() });
    } else {
        await CompanyAuthService.GetMembers(req, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    }
};

module.exports = middlewares;
