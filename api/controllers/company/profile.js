const { validationResult } = require('express-validator');

const Company = require('../../models/company/company');

const constants = require('../../constants');

const middlewares = {};

/**
 * Member cannot edit settings
 */

// function to get the profile details of the company
middlewares.getProfile = (req, res, next) => {
    // get the company object using the jwt token from headers
    Company.findById(
        req.uid,
        {
            _id: 1,
            email: 1,
            name: 1,
            accountHolderName: 1,
            mobile: 1,
            verificationStatus: 1,
            brands: 1,
            address: 1,
            details: 1,
            role: 1,
        },
        (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['There is a problem finding the user.'] });
            if (!user) return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('User')] });

            if (req.role === constants.MEMBER) {
                Company.findById(req.user)
                    .then((company) => {
                        if (company !== null) {
                            user.accountHolderName = company.accountHolderName;
                            user.role = company.role;
                            user.mobile = company.mobile;
                        }
                        return res.status(200).send({ error: false, result: user });
                    })
                    .catch((err) => {
                        return res.status(400).send({ error: true, message: [err.message] });
                    });
            } else return res.status(200).send({ error: false, result: user });
        }
    );
};

// function to edit the profile details of the company
middlewares.editProfile = (req, res, next) => {
    Company.findById(
        req.uid,
        {
            _id: 1,
            email: 1,
            name: 1,
            accountHolderName: 1,
            mobile: 1,
            verificationStatus: 1,
            brands: 1,
            address: 1,
            details: 1,
        },
        (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });

            // checking each parameter if it is available,
            // if available update it.

            if (req.body.mobile) {
                user.mobile = req.body.mobile;
            }

            if (req.body.accountHolderName) {
                user.accountHolderName = req.body.accountHolderName;
            }
            if (req.body.address) {
                if (req.body.address.base) user.address.address.base = req.body.address.base;
                if (req.body.address.city) user.address.address.city = req.body.address.city;
                if (req.body.address.state) user.address.address.state = req.body.address.state;
                if (req.body.address.pincode) user.address.address.pincode = req.body.address.pincode;
                if (
                    user.address.address.base !== '' &&
                    user.address.address.city !== '' &&
                    user.address.address.state !== '' &&
                    user.address.address.pincode !== 0
                )
                    user.address.available = true;
            }

            if (req.body.details) {
                if (req.body.details.logo) user.details.logo = req.body.details.logo;
                if (req.body.details.uid) user.details.uid = req.body.details.uid;
                if (req.body.details.regNo) user.details.regNo = req.body.details.regNo;
                if (req.body.details.gst) user.details.gst = req.body.details.gst;
                if (req.body.details.pan) user.details.pan = req.body.details.pan;
            }

            user.save((err, updatedUser) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                return res.status(200).send({ error: false, data: updatedUser, message: constants.SUCCESS });
            });
        }
    );
};

module.exports = middlewares;
