const { validationResult } = require('express-validator');

const Company = require('../../models/company/company');
const Influencer = require('../../models/influencer/influencer');

const constants = require('../../constants');

const bookmarkMiddlewares = {};

// function to add a influencer in the bookmark list of influencers by company
bookmarkMiddlewares.bookmarkInfluencer = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // geting company to which the influencer is to be added
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(404).send({ error: true, message: ['No user found'] });

            // checking if the influencer id is valid or not
            Influencer.findById(req.body.influencerId, (err, influencer) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!influencer) return res.status(404).send({ error: true, message: ['invalid influencer id'] });

                // checking if the influencer is already in the list or not
                if (!user.favInfluencers.includes(influencer._id)) {
                    user.favInfluencers.push(influencer._id);
                }
                return res.status(200).send({ error: false, result: { favInfluencers: user.favInfluencers } });
            });
        });
    }
};

module.exports = bookmarkMiddlewares;
