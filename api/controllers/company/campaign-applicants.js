const { validationResult } = require('express-validator');

const Applicant = require('../../models/campaign/cash-campaign/cash-campaign-applicant');
const CashCampaign = require('../../models/campaign/cash-campaign/cash-campaign');

const constants = require('../../constants');

const middlewares = {};

// function to get the applicants on the given campaignId
middlewares.getCampaignApplicants = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // list of valid status
        const validStatus = [constants.APPLIED, constants.APPROVED, constants.REJECTED, constants.SHORTLISTED, constants.SUBMITTED, 'All'];

        if (validStatus.includes(req.params.status)) {
            if (req.query.campaignId) {
                // fetching the campaign by given campaign Id
                CashCampaign.findById(req.query.campaignId, (err, campaign) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!campaign) return res.status(400).send({ error: true, message: ['Invalid campaignId'] });

                    // checking if the user is authorized to access the campaign
                    if (campaign.company == req.user) {
                        // setting pagination parameters
                        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
                        const size = req.query.size ? parseInt(req.query.size) : 20;

                        const findFilter = {};
                        findFilter['campaign'] = campaign._id;

                        // setting the findFilter parameter based on given status
                        if (req.params.status === constants.APPLIED) findFilter['status'] = constants.APPLIED;
                        if (req.params.status === constants.APPROVED) findFilter['status'] = constants.APPROVED;
                        if (req.params.status === constants.REJECTED) findFilter['status'] = constants.REJECTED;
                        if (req.params.status === constants.SHORTLISTED) findFilter['status'] = constants.SHORTLISTED;
                        if (req.params.status === constants.SUBMITTED) findFilter['status'] = constants.SUBMITTED;

                        if (pageNo <= 0)
                            return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

                        // making the query to fetch the applicants
                        Applicant.find(findFilter)
                            .skip(size * (pageNo - 1))
                            .limit(size)
                            .select({ status: 1, influencer: 1, biddingAmount: 1, tags: 1, moodboard: 1 })
                            .lean()
                            .exec((err, applicants) => {
                                if (err) return res.status(500).send({ error: true, message: [err.message] });

                                return res.status(200).send({ error: false, result: { applicants: applicants } });
                            });
                    } else {
                        return res.status(401).send({ error: true, message: ['Not authorized to perform this operation'] });
                    }
                });
            } else {
                return res.status(400).send({ error: true, message: ['campaignId is required'] });
            }
        } else {
            return res.status(400).send({ error: true, message: ['Invalid URL'] });
        }
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

// function to update the status of the applicant
middlewares.updateStatus = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // list of valid updates that can be done
        const validUpdates = [constants.SHORTLIST, constants.APPROVE, constants.REJECT];

        // checking if the user has sent the valid update value
        if (!validUpdates.includes(req.body.updateStatus)) return res.status(400).send({ error: true, message: ['Invalid update status'] });
        if (req.body.applicantId) {
            // fetcing the applicant using the given applicantId
            Applicant.findById(req.body.applicantId, (err, applicant) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!applicant) return res.status(400).send({ error: true, message: ['Invalid applicant Id'] });

                // fetching the campaign with the id in applicant object
                CashCampaign.findById(applicant.campaign, (err, campaign) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    // checking if user is authorized to perform this operation
                    if (campaign.company == req.user) {
                        // list of terminated status and non terminated status
                        const terminatedStatus = [constants.APPROVED, constants.REJECTED];
                        const nonTerminatedStatus = [constants.APPLIED, constants.SHORTLISTED];

                        // updating the status of applicant based on the given operation
                        if (terminatedStatus.includes(applicant.status))
                            return res.status(400).send({ error: true, message: ['cannot change the status of this applicant'] });
                        if (nonTerminatedStatus.includes(applicant.status)) {
                            if (req.body.updateStatus === constants.SHORTLIST) {
                                if (applicant.status === constants.SHORTLISTED)
                                    return res.status(400).send({ error: true, message: ['Already shortlisted, cannot update status'] });
                                if (applicant.status === constants.APPLIED) applicant.status = constants.SHORTLISTED;
                            }
                            if (req.body.updateStatus === constants.APPROVE) applicant.status = constants.APPROVED;
                            if (req.body.updateStatus === constants.REJECT) applicant.status = constants.REJECTED;

                            // saving the applicant
                            applicant.save((err, applicant) => {
                                if (err) return res.status(500).send({ error: true, message: [err.message] });
                                return res
                                    .status(200)
                                    .send({ error: false, result: { message: 'Status updated successfully', applicant: applicant } });
                            });
                        }
                    } else {
                        return res.status(401).send({ error: true, message: ['Not authorized to perform this operation'] });
                    }
                });
            });
        } else {
            return res.status(400).send({ error: true, message: ['applicant Id is requried'] });
        }
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
