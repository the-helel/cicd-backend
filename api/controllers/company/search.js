const { validationResult } = require('express-validator');

const Company = require('../../models/company/company');

const searchMiddlewares = {};

searchMiddlewares.search = (req, res, next) => {
    var query = {}; // no filter

    if (req.query.name) {
        query.name = req.query.name;
    } // company_name filter
    if (req.query.accountHolderName) {
        query.accountHolderName = req.query.accountHolderName;
    } // full_name filter
    if (req.query.email) {
        query.email = req.query.email;
    } // email filter

    // getting the company based on above filters
    Company.find(query, { _id: 1, name: 1, email: 1, accountHolderName: 1 }, (err, users) => {
        if (err) return res.status(500).send({ error: true, message: ['There is a problem finding the user.'] });

        return res.status(200).send({ error: false, result: users });
    });
};

module.exports = searchMiddlewares;
