const { validationResult } = require('express-validator');

const Company = require('../../models/company/company');
const Brand = require('../../models/company/brand');

const constants = require('../../constants');

const brandMiddlewares = {};

/**
 * Create new brand for company.
 *
 * POST /company/brand/create
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
brandMiddlewares.createBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // searching for a company using the JWT
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // creating new brand
            Brand.create(
                {
                    company: req.user,
                    name: req.body.name,
                    pageUrl: req.body.pageUrl ? req.body.pageUrl : null,
                    coverUrl: req.body.coverUrl ? req.body.coverUrl : null,
                    logo: {
                        url: req.body.logo,
                    },
                    about: req.body.about,
                    isActive: true,
                },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    brand.save();
                    user.brands.push(brand._id);
                    user.save();
                    return res.status(200).send({ error: false, result: { brand: brand } });
                }
            );
        });
    }
};

// function to create a new brand
brandMiddlewares.editBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // searching for a company using the JWT
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // updating if the correct brand id
            if (user.brands.includes(req.body.id)) {
                Brand.findById(req.body.id, { name: 1, pageUrl: 1, logo: 1, about: 1, coverUrl: 1, rating: 1 }, (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    if (brand === null) {
                        return res.status(404).send({
                            error: false,
                            message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                        });
                    }

                    // checking which values are there
                    if (req.body.name) {
                        brand.name = req.body.name;
                    } // checking which values are there
                    if (req.body.pageUrl) {
                        brand.name = req.body.pageUrl;
                    } // checking the name parameter
                    if (req.body.logo) {
                        brand.logo.url = req.body.logo;
                    } // checking the logo parameter
                    if (req.body.about) {
                        brand.about = req.body.about;
                    } // checking the about parameter
                    if (req.body.coverUrl) {
                        brand.coverUrl = req.body.coverUrl;
                    } // checking the coverUrl parameter
                    // updating the brand
                    brand.save((err, brand) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({ error: true, result: { brand: brand } });
                    });
                });
            } else {
                return res.status(400).send({ error: true, message: ['Not authorized to edit this brand.'] });
            }
        });
    }
};

// function to fetch all the brands of a company
brandMiddlewares.viewBrands = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        let query = req.query.q;
        let pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        let size = constants.PAGE_SIZE || 10;

        Company.findById(req.uid, async (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            output = [];
            let regex;
            if (query) {
                regex = new RegExp(query, 'i');
                await Brand.find(
                    { company: req.uid, name: regex, isActive: true },
                    { id: 1, name: 1, pageUrl: 1, logo: 1, about: 1, company: 1, coverUrl: 1, rating: 1, isActive: 1 },
                    { skip: (pageNo - 1) * size, limit: size },
                    (err, items) => {
                        if (err) console.log(err);
                        // output.push(items);
                        return res.status(200).send({ error: false, result: { data: items } });
                    }
                );
            }

            // for (const brand of user.brands) {
            else
                await Brand.find(
                    { company: req.uid, isActive: true },
                    { id: 1, name: 1, pageUrl: 1, logo: 1, about: 1, company: 1, coverUrl: 1, rating: 1, isActive: 1 },
                    { skip: (pageNo - 1) * size, limit: size },
                    (err, items) => {
                        if (err) console.log(err);
                        // output.push(items);
                        return res.status(200).send({ error: false, result: { data: items } });
                    }
                );
            // }
            // return res.status(200).send({ error: false, result: { brands: output } });
        });
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.viewBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // checking if the user is the owner of the request brand
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

            // checking if the brand exists and returns if it exists
            // if (req.params.id && user.brands.includes(req.params.id)) {
            Brand.findById(
                req.params.id,
                { _id: 1, logo: 1, name: 1, pageUrl: 1, about: 1, company: 1, coverUrl: 1, rating: 1, isActive: 1 },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')] });

                    return res.status(200).send({ error: false, result: { data: brand } });
                }
            );
        });
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.deleteBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // checking if the user is the owner of the request brand
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

            // checking if the brand exists and returns if it exists
            if (req.params.id && user.brands.includes(req.params.id)) {
                Brand.findById(req.params.id, async (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (brand === null) {
                        return res.status(404).send({
                            error: false,
                            message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                        });
                    }

                    brand.isActive = false;
                    await brand.save();
                    return res.status(200).send({
                        error: false,
                        result: {
                            deleteType: 'brand',
                            id: req.params.id,
                            message: `Successfully deleted the brand of id ${req.params.id}`,
                        },
                    });
                });
            } else {
                return res.status(400).send({ error: true, message: ['Not authorized to access this brand'] });
            }
        });
    }
};

module.exports = brandMiddlewares;
