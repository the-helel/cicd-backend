const searchConfigs = require('./search-configs');

const getInstagramInfluencerList = (queryValue, fieldsToBeSearched, includes, sortByFields, filters = [], size = 10, from = 0) => {
    const matchQueries = [
        {
            multi_match: {
                query: queryValue,
                fields: fieldsToBeSearched,
                fuzziness: searchConfigs.fuzziness,
            },
        },
        ...filters,
    ];

    return {
        size: size,
        from: from,
        _source: {
            includes: includes,
        },
        query: {
            bool: {
                must: matchQueries,
            },
        },
        sort: sortByFields,
    };
};

const getSuggestions = (queryValue, size = 10, from = 0) => {
    const matchQueries = [
        {
            multi_match: {
                query: queryValue,
                fields: ['name'],
                fuzziness: searchConfigs.fuzziness,
            },
        },
    ];

    return {
        size: size,
        from: from,
        _source: {
            includes: ['name'],
        },
        query: {
            bool: {
                must: matchQueries,
            },
        },
    };
};

const getInstagramInfluencerProfile = (id, excludes = []) => {
    return {
        _source: {
            excludes: excludes,
        },
        query: {
            match: {
                id: id,
            },
        },
    };
};

module.exports = {
    getInstagramInfluencerList,
    getSuggestions,
    getInstagramInfluencerProfile,
};
