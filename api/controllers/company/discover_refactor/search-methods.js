const { validationResult } = require('express-validator');

const client = require('../../../elasticsearch');
const config = require('../../../config');
const searchQueries = require('./search-queries');

const index = config.ELASTICSEARCH_INDEX;

const searchMethods = {};

searchMethods.getInstagramInfluencerList = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const query = req.body.query;
        const fieldsToBeSearched = ['name', 'username'];
        const includes = ['id', 'name', 'instagram.insta_handle', 'profile_image', 'instagram.bio', 'instagram.insta_followers'];
        const sortBy = '' == req.body.sortBy ? [] : req.bodysortBy;
        const filters = [];
        const size = req.body.size;
        const page = req.body.page;
        const from = (page - 1) * size;

        if (req.body.gender == 'male' || req.body.gender == 'female') {
            filters.push({
                match: {
                    gender: {
                        query: req.body.gender,
                    },
                },
            });
        }

        if (req.body.lower_followers_count && req.body.higher_followers_count) {
            filters.push({
                range: {
                    'instagram.followers': {
                        gte: req.body.lower_followers_count,
                        lte: req.body.higher_followers_count,
                    },
                },
            });
        }

        const searchQuery = searchQueries.getInstagramInfluencerList(query, fieldsToBeSearched, includes, sortBy, filters, size, from);

        client
            .search({ index: index, body: searchQuery })
            .then((response) => {
                res.status(200).json({ error: false, result: response });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

searchMethods.getSuggestions = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const query = req.body.query;
        const size = req.body.size;
        const page = req.body.page;
        const from = (page - 1) * size;

        const searchQuery = searchQueries.getSuggestions(query, size, from);

        client
            .search({ index: index, body: searchQuery })
            .then((response) => {
                res.status(200).json({ error: false, result: response });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

searchMethods.getInstagramInfluencerProfile = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const id = req.body.id;

        const searchQuery = searchQueries.getInstagramInfluencerProfile(id);

        client
            .search({ index: index, body: searchQuery })
            .then((response) => {
                res.status(200).json({ error: false, result: response });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = searchMethods;
