const constants = require('../../../constants.js');
const productCampaignController = require('./product-campaign');

async function assignCampaignType(campaignPayload) {
    if (campaignPayload.campaignType === constants.PRODUCT_CAMPAIGN) {
        console.log('hi');
        let x = productCampaignController(campaignPayload);

        console.log(x);
    }
}

module.exports = assignCampaignType;
