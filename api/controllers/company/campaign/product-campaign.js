/**
 * Controllers to create, edit and draft campaign
 */

const Company = require('../../../models/company/company');
const Brand = require('../../../models/company/brand');
const ProductCampaign = require('../../../models/campaign/product-campaign/product-campaign');

const assignProductCampaign = require('../../campaign/product-campaign/assign-campaign');

const constants = require('../../../constants.js');

/**
 * Function to create, edit, draft, cam
 * @param {Object} campaignPayload campaign payload having feilds related to product campaign
 *
 */
async function productCampaignController(campaignPayload) {
    let response = {};

    console.log('here');

    try {
        const company = await Company.findById(campaignPayload.company);
        const brand = await Brand.findById(campaignPayload.brand);

        if (company === null || brand === null) {
            response = {
                status_code: 500,
                error: true,
                message: [constants.SERVER_ERR],
            };
        }

        if (!company.brands.includes('' + brand._id))
            response = {
                status_code: 403,
                error: true,
                message: ['Unauthorized access'],
            };

        let productCampaignInsance = null;
        if (campaignPayload.campaignId) {
            productCampaignInstance = await ProductCampaign.findById(campaignPayload.campaignId);
        }

        if (productCampaignInstance === null) {
            productCampaignInstance = new ProductCampaign();
        }

        assignProductCampaign(campaignPayload, productCampaignInstance);

        await productCampaign.save();

        response = {
            status_code: 200,
            error: false,
            message: [constants.SUCCESS],
        };
    } catch (err) {
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
    }
    console.log(response, 'response');
    return response;
}

module.exports = productCampaignController;
