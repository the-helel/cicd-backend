const { validationResult } = require('express-validator');

const CampaignApplicant = require('../../../models/campaign/applicant');
const CampaignPost = require('../../../models/campaign/post');
const constants = require('../../../constants.js');
const { setApplicantFields } = require('../../../services/campaign/utils.js');
const moment = require('moment');
const { getTimeSince } = require('../../../../utils/formatter');
const CampaignApplicantService = require('../../../services/campaign/applicant');

// ########### Below are the api's to fetch campaign applicant and post details

const middlewares = {};
assignCampaignApplicant = (applicant, body) => {
    if (body.status) applicant.status = body.status;
    if (body.pitch) applicant.pitch = body.pitch;
    if (body.bids) applicant.bids = body.bids;
    if (body.workSample) applicant.workSample = body.workSample;
    if (body.link) applicant.link = body.link;
    if (body.applied_platform) applicant.applied_platform = body.applied_platform;
    if (body.name) applicant.name = body.name;
    if (body.post_count) applicant.post_count = body.post_count;
    if (body.engagement) applicant.engagement = body.engagement;
    if (body.video_views) applicant.video_views = body.video_views;
    if (body.like_count) applicant.like_count = body.like_count;
    if (body.location) applicant.location = body.location;
    if (body.category) applicant.category = body.category;
    if (body.platforms) applicant.platforms = body.platforms;
};

assignCampaignPost = (post, body) => {
    if (body.short_code) post.short_code = body.short_code;
    if (body.post_link) post.post_link = body.post_link;
    if (body.posted_at) post.posted_at = body.posted_at;
    if (body.status) post.status = body.status;
    if (body.media && body.media.type) post.media.type = body.media.type;
    if (body.media && body.media.display_url) post.media.display_url = body.media.display_url;
    if (body.caption) post.caption = body.caption;
    if (body.like_count) post.like_count = body.like_count;
    if (body.comment_count) post.comment_count = body.comment_count;
    if (body.engagement) post.engagement = body.engagement;
    if (body.video_view_count) post.video_view_count = body.video_view_count;
    if (body.impressions) post.impressions = body.impressions;
    if (body.owner) {
        if (body.owner.influencerId) post.owner.influencerId = body.owner.influencerId;
        if (body.owner.username) post.owner.username = body.owner.username;
        if (body.owner.followers_count) post.owner.followers_count = body.owner.followers_count;
        if (body.owner.average_interaction) post.owner.average_interaction = body.owner.average_interaction;
        if (body.owner.full_name) post.owner.full_name = body.owner.full_name;
        if (body.owner.average_views) post.owner.average_views = body.owner.average_views;
        if (body.owner.score) post.owner.score = body.owner.score;
        if (body.owner.average_comments) post.owner.average_comments = body.owner.average_comments;
        if (body.owner.average_likes) post.owner.average_likes = body.owner.average_likes;
    }
    if (body.estimated_reach) post.estimated_reach = body.estimated_reach;
    if (body.actual_reach) post.actual_reach = body.actual_reach;
    if (body.timeline) post.timeline = body.timeline;
};

middlewares.createCampaignApplicant = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const campaignId = req.body.campaignId || null;
        const influencerId = req.body.influencerId || null;

        if (campaignId === null || influencerId === null) {
            return res.status(400).json({ error: true, message: 'Missing required argument campaignId or influencerId' });
        }
        let applicant = new CampaignApplicant();
        applicant.campaignId = campaignId;
        applicant.influencerId = influencerId;
        assignCampaignApplicant(applicant, req.body);

        CampaignApplicant.create(applicant)
            // .lean()
            .then((applicant) => {
                if (applicant != null) {
                    res.status(200).json({ error: false, result: { data: applicant } });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.getCampaignApplicant = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const campaignId = req.params.campaignId;
        let pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        let size = constants.PAGE_SIZE;
        let sortBy = req.query.sortBy ? req.query.sortBy : 'createdAt';

        let filterByStatus = req.query.status;
        let findQuery;

        console.log(req.query);

        if (filterByStatus) {
            const fields = filterByStatus.split(',');
            findQuery = {
                campaignId: campaignId,
                status: {
                    $in: fields,
                },
            };
        }
        if (!filterByStatus) {
            findQuery = {
                campaignId: campaignId,
            };
        }

        await CampaignApplicantService.GetCampaignApplicants(findQuery, sortBy, pageNo, size, (err, result) => {
            if (err) {
                return res.status(400).json({
                    error: true,
                    message: err,
                });
            } else {
                return res.status(200).json({
                    error: false,
                    result: result,
                });
            }
        });
    } else {
        return res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.updateApplicantStatus = (req, res, next) => {
    const errors = validationResult(req);
    try {
        if (errors.isEmpty()) {
            const campaignId = req.params.campaignId; // if validation needed on campaign level we can do

            CampaignApplicant.findById(req.params.applicantId, (err, applicant) => {
                if (applicant != null) {
                    applicant.status = req.body.status;

                    applicant.save((err, updatedApplicant) => {
                        if (err) {
                            res.status(400).json({ error: true, message: ['Unable to update data in server'] });
                        }
                        res.status(200).json({ error: false, result: { data: updatedApplicant } });
                    });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            });
        } else {
            console.log('Unable to update the data due to failed req validation');
            res.status(400).json({ error: true, message: errors.array() });
        }
    } catch (error) {
        console.log('Unable to update the data due to ', error.message);
        res.status(500).json({ error: true, message: [error.message] });
    }
};

middlewares.updateApplicantbid = (req, res, next) => {
    const errors = validationResult(req);
    try {
        if (errors.isEmpty()) {
            const campaignId = req.params.campaignId; // if validation needed on campaign level we can do

            CampaignApplicant.findById(req.params.applicantId, (err, applicant) => {
                if (applicant != null) {
                    const companyBid = {};
                    companyBid.biddingAmount = parseInt(req.body.biddingAmount);
                    companyBid.status = null;
                    companyBid.timestamp = new Date();

                    const bid = {
                        company: companyBid,
                    };
                    applicant.bids.push(bid);

                    applicant.save((err, updatedApplicant) => {
                        if (err) {
                            res.status(400).json({ error: true, message: ['Unable to update data in server'] });
                        }
                        res.status(200).json({ error: false, result: { data: updatedApplicant } });
                    });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            });
        } else {
            console.log('Unable to update the data due to failed req validation');
            res.status(400).json({ error: true, message: errors.array() });
        }
    } catch (error) {
        console.log('Unable to update the data due to ', error.message);
        res.status(500).json({ error: true, message: [error.message] });
    }
};

//This method can retrieve posts of particular campaign and also of any particular influencer
middlewares.getCampaignPosts = async (req, res, next) => {
    console.log('Get campaign posts');
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        let query = {
            campaignId: req.params.campaignId,
        };

        let pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        let size = constants.PAGE_SIZE || 10;

        if (req.query.applicantId) {
            query = {
                applicantId: req.query.applicantId,
            };
        }

        const countQuery = await CampaignPost.where(query).countDocuments();

        CampaignPost.find(query)
            .populate({
                path: 'influencer',
                select: {
                    name: 1,
                    profilePicture: 1,
                },
            })
            .skip(size * (pageNo - 1))
            .limit(size)
            .lean()
            .then(async (applicant) => {
                if (applicant != null) {
                    await Promise.all(
                        applicant.map(async (applicant) => {
                            applicant.since = moment(applicant.createdAt).fromNow();
                        })
                    );

                    res.status(200).json({ error: false, result: { count: countQuery, data: applicant } });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.createCampaignPost = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const campaignId = req.body.campaignId || null;
        const applicantId = req.body.applicantId || null;

        if (campaignId === null || applicantId === null) {
            return res.status(400).json({ error: true, message: 'Missing required argument campaignId or influencerId' });
        }
        let post = new CampaignPost();
        post.campaignId = campaignId;
        post.applicantId = applicantId;
        assignCampaignPost(post, req.body);

        CampaignPost.create(post)
            // .lean()
            .then((post) => {
                if (post != null) {
                    res.status(200).json({ error: false, result: { data: post } });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.addNote = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).send({ error: true, message: errors.array() });
    }

    try {
        await CampaignApplicantService.AddApplicantNote(req, (err, result) => {
            if (err) {
                return res.status(500).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    } catch (error) {
        return res.status(500).send({ error: true, message: [error.message] });
    }
};

middlewares.getNotes = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).send({ error: true, message: errors.array() });
    }

    try {
        await CampaignApplicantService.GetApplicantNotes(req, (err, result) => {
            if (err) {
                return res.status(500).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    } catch (error) {
        return res.status(500).send({ error: true, message: [error.message] });
    }
};

middlewares.deleteNote = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).send({ error: true, message: errors.array() });
    }

    try {
        await CampaignApplicantService.DeleteNote(req, (err, result) => {
            if (err) {
                return res.status(500).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    } catch (error) {
        return res.status(500).send({ error: true, message: [error.message] });
    }
};

middlewares.updateNote = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).send({ error: true, message: errors.array() });
    }

    try {
        await CampaignApplicantService.UpdateNote(req, (err, result) => {
            if (err) {
                return res.status(500).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    } catch (error) {
        return res.status(500).send({ error: true, message: [error.message] });
    }
};

module.exports = middlewares;
