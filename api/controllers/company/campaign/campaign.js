/**
 * Middlewares to create, edit and draft campaign
 */
const { validationResult } = require('express-validator');
const Company = require('../../../models/company/company');
const Brand = require('../../../models/company/brand');
const ProductCampaign = require('../../../models/campaign/product-campaign/product-campaign');
const CashCampaign = require('../../../models/campaign/cash-campaign/cash-campaign');
const Campaign = require('../../../models/campaign/campaign');

const assignProductCampaign = require('../../campaign/product-campaign/assign-campaign');
const assignCashCampaign = require('../../campaign/cash-campaign/assign-campaign');
const assignCampaign = require('../../campaign/assign-campaign');
const constants = require('../../../constants.js');
const Category = require('../../../models/category.js');
const CampaignApplicantAggregate = require('../../../services/campaign/aggregate-campaign-applicant');
const kAsync = require('async');
const { CampaignPostAgg } = require('../../../services/post');
const CompanyCampaignService = require('../../../services/company');
const mongoose = require('mongoose');
const { createCampaignJobs } = require('../../../services/company');
const middlewares = {};

middlewares.assignCampaignType = async (req, res) => {
    let { companyId, brandId, campaignType, campaignId } = req.body;
    let response = { status_code: 500, error: true, message: [constants.SERVER_ERR] };

    if (!companyId || !brandId || !campaignType) {
        response = {
            status_code: 400,
            error: true,
            message: [constants.VALIDATION_ERROR],
        };
        return res.status(response.status_code).json(response);
    }

    try {
        const company = await Company.findById(companyId);
        const brand = await Brand.findById(brandId);

        if (company === null || brand === null) {
            response = {
                status_code: 500,
                error: true,
                message: [constants.SERVER_ERR],
            };
        }

        if (!company.brands.includes('' + brand._id))
            response = {
                status_code: 403,
                error: true,
                message: ['Unauthorized access'],
            };

        if (campaignType === constants.PRODUCT_CAMPAIGN) {
            let productCampaignInstance = null;
            if (campaignId) {
                productCampaignInstance = await ProductCampaign.findById(campaignPayload.campaignId);
            }

            if (productCampaignInstance === null) {
                productCampaignInstance = new ProductCampaign();
            }

            console.log('CREATING PRODUCT CAMPAIGN');
            console.log('Brand is', brand);

            productCampaignInstance.brand = brand._id;
            productCampaignInstance.brandName = brand.name;
            productCampaignInstance.brandLogo = brand.logo.url;
            productCampaignInstance.aboutBrand = brand.about;
            assignProductCampaign(req.body, productCampaignInstance);

            await productCampaignInstance.save();
        } else if (campaignType === constants.CASH_CAMPAIGN) {
            let cashCampaignInstance = null;
            if (campaignId) {
                cashCampaignInstance = await CashCampaign.findById(campaignPayload.campaignId);
            }

            if (cashCampaignInstance === null) {
                cashCampaignInstance = new CashCampaign();
            }

            cashCampaignInstance.brand = brand._id;
            cashCampaignInstance.brandName = brand.name;
            cashCampaignInstance.brandLogo = brand.logo.url;
            cashCampaignInstance.aboutBrand = brand.about;
            assignCashCampaign(req.body, cashCampaignInstance);

            await cashCampaignInstance.save();
        }

        response = {
            status_code: 200,
            error: false,
            message: [constants.SUCCESS],
        };
    } catch (err) {
        console.log(err);
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
    }

    return res.status(response.status_code).json(response);
};

middlewares.createCampaign = async (req, res) => {
    let { companyId, brandId, campaignType } = req.body;
    let response = { status_code: 500, error: true, message: [constants.SERVER_ERR] };

    if (!companyId || !brandId || !campaignType) {
        response = {
            status_code: 400,
            error: true,
            message: ['Missing required Arguments'],
        };
        return res.status(response.status_code).json(response);
    }

    try {
        const company = await Company.findById(req.user);
        const brand = await Brand.findById(brandId);

        if (company === null || brand === null) {
            response = {
                status_code: 500,
                error: true,
                message: [constants.SERVER_ERR],
            };
        }

        if (brand === null || !company.brands.includes('' + brand._id)) {
            response = {
                status_code: 403,
                error: true,
                message: ['Unauthorized access or brand does not exist '],
            };
            return res.status(response.status_code).json(response);
        }
        // Creating the campaign by intialising the proper campaign type
        campaignInstance = new Campaign();

        console.log('New campaign instance:', campaignInstance);

        campaignInstance.brandId = brand._id;
        campaignInstance.companyId = company._id;

        campaignInstance.brandLogo = brand.logo.url;
        campaignInstance.aboutBrand = brand.about;
        campaignInstance.brandName = brand.name;

        assignCampaign(req.body, campaignInstance);

        let createdCampaign = await Campaign.create(campaignInstance);

        response = {
            status_code: 200,
            error: false,
            data: createdCampaign,
            message: [constants.SUCCESS],
        };
    } catch (err) {
        console.log(err);
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
    }

    return res.status(response.status_code).json(response);
};

middlewares.editCampaign = async (req, res) => {
    let { brandId, campaignType, campaignId } = req.body;
    // If role is [Admin] then don't update company id
    const role = req.role;
    let response = { status_code: 500, error: true, message: [constants.SERVER_ERR] };

    if (!brandId || !campaignType || !campaignId) {
        response = {
            status_code: 400,
            error: true,
            message: [`Missing required arguments, ${brandId} ${campaignType} ${campaignId}`],
        };
        return res.status(response.status_code).json(response);
    }

    try {
        const company = await Company.findById(req.user);
        const brand = await Brand.findById(brandId);

        if (company === null || brand === null) {
            response = {
                status_code: 500,
                error: true,
                message: 'Company or Brand is not valid',
            };
            return res.status(response.status_code).json(response);
        }

        if (!company.brands.includes('' + brand._id) && req.role !== constants.ADMIN) {
            response = {
                status_code: 403,
                error: true,
                message: ['Unauthorized access'],
            };

            return res.status(response.status_code).json(response);
        }
        // Creating the campaign by initialising the proper campaign type
        let campaignInstance = (await Campaign.findById(campaignId)) || null;

        // Campaign cannot be edited after submission
        if (
            [constants.SUBMITTED, constants.ACTIVE, constants.PAUSE, constants.EXPIRED, constants.ARCHIVE, constants.COMPLETED].includes(
                campaignInstance.status
            )
        ) {
            response = {
                status_code: 400,
                error: true,
                message: 'You cannot edit campaign after submission.',
            };

            return res.status(response.status_code).json(response);
        }
        if (campaignInstance === null) {
            response = {
                status_code: 404,
                error: true,
                message: ['Resource not found'],
            };
            return res.status(response.status_code).json(response);
        }

        if (campaignInstance.status === constants.ACTIVE) {
            response = {
                status_code: 400,
                error: true,
                message: ['Active campaign cannot be edited.'],
            };

            return res.status(response.status_code).json(response);
        }

        campaignInstance.brandId = brand._id;
        if (role !== constants.ADMIN) campaignInstance.companyId = company._id;

        await assignCampaign(req.body, campaignInstance);

        let savedCampaign = await campaignInstance.save();

        response = {
            status_code: 200,
            error: false,
            data: savedCampaign,
            message: [constants.SUCCESS],
        };
    } catch (err) {
        console.log('Edit campaign error: ', err);
        response = {
            status_code: 500,
            error: true,
            message: constants.SERVER_ERR,
        };
    }

    return res.status(response.status_code).json(response);
};

middlewares.editCampaignStatus = async (req, res) => {
    let { brandId, campaignId, status } = req.body;
    let response = { status_code: 500, error: true, message: [constants.SERVER_ERR] };

    if (!brandId || !status || !campaignId) {
        response = {
            error: true,
            message: 'Missing required arguments',
        };
        return res.status(400).json(response);
    }

    try {
        const company = await Company.findById(req.user);
        const brand = await Brand.findById(brandId);

        if (company === null || brand === null) {
            response = {
                status_code: 500,
                error: true,
                message: [constants.SERVER_ERR],
            };
            return res.status(response.status_code).json(response);
        }

        if (!company.brands.includes('' + brand._id)) {
            response = {
                status_code: 403,
                error: true,
                message: ['Unauthorized access'],
            };
            return res.status(response.status_code).json(response);
        }
        // Creating the campaign by intialising the proper campaign type
        let campaignInstance = (await Campaign.findById(campaignId)) || null;
        if (campaignInstance === null) {
            response = {
                status_code: 404,
                error: true,
                message: ['Resource not found'],
            };
            return res.status(response.status_code).json(response);
        }

        if (status === constants.ACTIVE) {
            if (
                campaignInstance.brief === {} ||
                campaignInstance.brief === null ||
                campaignInstance.payment === {} ||
                campaignInstance.payment === null ||
                campaignInstance.product === {} ||
                campaignInstance.product === null
            ) {
                response = {
                    status_code: 400,
                    error: true,
                    message: ['Please make sure to fill all required data before submission.'],
                };
                return res.status(response.status_code).json(response);
            }
        }
        campaignInstance.status = status;

        let savedCampaign = await campaignInstance.save();

        response = {
            status_code: 200,
            error: false,
            data: savedCampaign,
            message: [constants.SUCCESS],
        };
    } catch (err) {
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
    }

    return res.status(response.status_code).json(response);
};

/**
 * When campaign is created and submitted, create two jobs which will make the
 * campaign live and expire according to start and end date.
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
middlewares.liveCampaign = async (req, res) => {
    const { campaignId } = req.body;

    if (campaignId !== null) {
        try {
            await createCampaignJobs(campaignId, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        error: true,
                        message: err,
                    });
                }
                return res.status(200).json(result);
            });
        } catch (e) {
            return res.status(500).json({
                status_code: 500,
                error: true,
                message: [constants.SERVER_ERR],
            });
        }
    }
};

middlewares.copyCampaign = async (req, res) => {
    let { brandId, campaignType, campaignId } = req.body;
    let response = { status_code: 500, error: true, message: [constants.SERVER_ERR] };

    if (!brandId || !campaignType || !campaignId) {
        response = {
            status_code: 400,
            error: true,
            message: ['Missing required arguments'],
        };
        return res.status(response.status_code).json(response);
    }

    try {
        const company = await Company.findById(req.user);
        const brand = await Brand.findById(brandId);

        if (company === null || brand === null) {
            response = {
                status_code: 500,
                error: true,
                message: [constants.SERVER_ERR],
            };
            return res.status(response.status_code).json(response);
        }

        if (!company.brands.includes('' + brand._id)) {
            response = {
                status_code: 403,
                error: true,
                message: ['Unauthorized access'],
            };
            return res.status(response.status_code).json(response);
        }
        // Creating the campaign by intialising the proper campaign type
        let campaignInstance =
            (await Campaign.findById(campaignId).select({ _id: 0, __v: 0, createdAt: 0, updatedAt: 0, report: 0, overview: 0 })) || null;

        if (campaignInstance === null) {
            response = {
                status_code: 404,
                error: true,
                message: ['Resource not found'],
            };
            return res.status(response.status_code).json(response);
        }
        let copyInstance = new Campaign();

        copyInstance.brandId = brand._id;
        copyInstance.companyId = company._id;
        copyInstance.brandName = brand.name;
        copyInstance.brandLogo = brand.logo.url;
        copyInstance.aboutBrand = brand.about;

        assignCampaign(campaignInstance, copyInstance);
        copyInstance.status = constants.DRAFT; // adding every copied campaign as draft

        let copiedCampaign = await Campaign.create(copyInstance);

        response = {
            status_code: 200,
            error: false,
            data: copiedCampaign,
            message: [constants.SUCCESS],
        };
    } catch (err) {
        console.log(err);
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
    }

    return res.status(response.status_code).json(response);
};

/**
 * @desc Get all the campaigns of the Organization.
 *
 * @GET /company/campaigns
 * @Access Organization
 */
middlewares.getCampaigns = async (req, res, next) => {
    try {
        const errors = validationResult(req);

        if (errors.isEmpty()) {
            await CompanyCampaignService.campaigns.getCompanyCampaigns(req, (err, result) => {
                if (err) {
                    return res.status(400).json(err);
                }
                return res.status(200).json(result);
            });
        }
    } catch (error) {
        console.log('Unexpected error occurred while fetching the collection of campaigns:: ', error.message);
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
        return res.status(response.status_code).json(response);
    }
};

// Get campaign by ID and show its overview
middlewares.getCampaignById = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: errors.array() });
    } else {
        const { campaignId } = req.params;

        // checking if the campaign id exists and returns campaign object if it exists
        if (campaignId) {
            await Campaign.aggregate(
                [
                    {
                        $match: {
                            _id: mongoose.Types.ObjectId(campaignId),
                        },
                    },

                    {
                        $lookup: {
                            from: 'companies',
                            let: {
                                company_id: '$companyId',
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ['$_id', '$$company_id'],
                                        },
                                    },
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        name: 1,
                                        details: 1,
                                        mobile: 1,
                                        address: 1,
                                    },
                                },
                            ],
                            as: 'payment.contact',
                        },
                    },
                    {
                        $unwind: '$payment.contact',
                    },
                    {
                        $lookup: {
                            from: 'categories',
                            localField: 'brief.categories',
                            foreignField: '_id',
                            as: 'brief.categories',
                        },
                    },
                ],
                async (err, campaign) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!campaign) return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                    if (campaign.length === 0)
                        return res.status(400).send({ error: true, message: constants.RESOURCE_NOT_FOUND_ERROR('Campaign') });

                    campaign = Object.assign(campaign[0]);

                    let applicantStats;

                    // Update campaign posts data whenever the campaign needs to
                    // check performance of posts.
                    // await updatePostAnalytics(campaignId, campaign.brief.campaignPlatform);

                    // Find the selected influencer from applicant
                    await kAsync.parallel(
                        {
                            influencerAggregate: function (cb) {
                                CampaignApplicantAggregate(req.params.campaignId, cb);
                            },
                            campaignPostAggregate: function (cb) {
                                CampaignPostAgg(req.params.campaignId, cb);
                            },
                        },
                        function (err, result) {
                            if (err) {
                                console.log('Async parallel error', err);
                                return;
                            }

                            const influencerAgg = result.influencerAggregate;

                            if (influencerAgg.length !== 0) {
                                applicantStats = result[0];
                                let summary = campaign.report.summary.influencer_count;
                                summary.total = influencerAgg[0].total_applicant_count;
                                summary.selected = influencerAgg[0].total_selected_count;
                                summary.rejected = influencerAgg[0].total_rejected_count;
                                summary.completed = influencerAgg[0].total_completed_count;
                            }

                            const postAgg = result.campaignPostAggregate;

                            if (postAgg.length !== 0) {
                                let postStats = campaign.report.progress.engagement;
                                postStats.post_count = postAgg[0].total_post_count;
                                postStats.likes = postAgg[0].total_like_count;
                                postStats.comments = postAgg[0].total_comment_count;
                                // TODO: Return engagement rate in percentage
                                postStats.engagement_rate = postAgg[0].total_engagement;
                                postStats.video_views = postAgg[0].total_video_view_count;
                                postStats.impression = postAgg[0].total_impression;
                            }

                            res.status(200).json({
                                error: false,
                                message: 'success',
                                data: campaign,
                                stats: applicantStats,
                            });
                        }
                    );
                }
            );
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this campaign'] });
        }
    }
};

// archive campaign by id
middlewares.deleteCampaignById = (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400).json({ error: true, message: errors.array() });
        } else {
            // checking if the user is the owner of the request campaign
            Company.findById(req.user, (err, user) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

                // checking if the campaign id exists and returns campaign object if it exists
                if (req.params.campaignId) {
                    Campaign.findById(req.params.campaignId, (err, campaign) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });
                        if (!campaign)
                            return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('campaign')] });

                        //Not deleting the campaign just archiving it later we can delete if required
                        campaign.status = constants.ARCHIVE;
                        // updating the campaign status
                        campaign.save((err, archivedCampaign) => {
                            if (err) return res.status(500).send({ error: true, message: [err.message] });

                            return res.status(200).json({
                                error: false,
                                message: 'success',
                                data: archivedCampaign,
                            });
                        });
                    });
                } else {
                    return res.status(400).send({ error: true, message: ['Not authorized to access this campaign'] });
                }
            });
        }
    } catch (error) {
        console.log(error);
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
    }
};

module.exports = middlewares;
