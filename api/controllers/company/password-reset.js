const { validationResult } = require('express-validator');
var bcrypt = require('bcryptjs');
const jwtDecode = require('jwt-decode');
var jwt = require('jsonwebtoken');
var Company = require('../../models/company/company');

// utils
const sendEmail = require('../util/email');

const constants = require('../../constants');
const config = require('../../config');
const templates = require('../../templates');
const middlewares = {};

// send reset password link
middlewares.sendResetPasswordLink = async (req, res) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        let { email } = req.body;
        let redirect_link = '/company/reset';

        try {
            let company = await Company.findOne({ email: email }).lean();

            if (company === null) {
                return res.status(404).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('User')] });
            }

            let verificationToken = jwt.sign(
                {
                    userId: company._id,
                    userType: constants.COMPANY,
                },
                config.JWT_SECRET,
                {
                    expiresIn: constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES,
                }
            );

            let [sent, error] = await sendEmail(email, {
                subject: 'Please reset of Famstar Account',
                body: templates.EMAIL_PASSWORD_RESET(company.name, `http://${req.headers.host}/company/reset?token=${verificationToken}`),
            });

            if (error) {
                console.log('Error occured while sending the reset password link ', err);
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            }

            return res.status(200).json({ error: false, message: [constants.SUCCESS] });
        } catch (error) {
            console.log('Unexpected error occured while sending the reset password link from sendResetPasswordLink ', error);
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

middlewares.resetPassword = async (req, res) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const { token, password } = req.body;
        const decodedToken = jwtDecode(token);

        await Company.findById(decodedToken.userId)
            .then(async (company) => {
                if (company === null)
                    return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Company')] });

                try {
                    jwt.verify(token, config.JWT_SECRET);

                    await bcrypt
                        .genSalt(10)
                        .then(async (salt) => {
                            await bcrypt
                                .hash(password, salt)
                                .then((hash) => {
                                    company.password = hash;
                                })
                                .catch((err) => {
                                    return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                                });
                        })
                        .catch((err) => {
                            console.log('Error occured while encrypting the password ', err);
                            return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                        });

                    company
                        .save()
                        .then(() => {
                            return res.status(200).send({ error: false, message: [constants.SUCCESS] });
                        })
                        .catch(() => {
                            console.log('Error occured while saving the password in db ', err);
                            return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                        });
                } catch (err) {
                    console.log(err);
                    return res.render('expired');
                }
            })
            .catch((err) => {
                console.log('Unexpected error in resetPassword method', err);
                return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
            });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
