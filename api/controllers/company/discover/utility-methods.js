const esb = require('elastic-builder');
const config = require('../../../config');

function buildFrom(current, resultsPerPage) {
    if (!current || !resultsPerPage) return;
    return (current - 1) * resultsPerPage;
}

function buildSort(sort) {
    if (sort && sort.dir && sort.field && sort.type === 'range') {
        return esb.sort(sort.field, sort.dir);
    }

    if (sort && sort.dir && sort.field && sort.type === 'term') {
        return esb.sort(`${sort.field}.keyword`, sort.dir);
    }

    return esb.sort('created_at', 'desc');
}

function buildMatch(searchTerm) {
    const multiMatchFields = ['instagram_handle', 'name', 'categories', 'label', 'instagram.bio'];
    return searchTerm
        ? esb.multiMatchQuery(multiMatchFields, searchTerm).operator('AND').type('best_fields').fuzziness('AUTO')
        : esb.matchAllQuery({});
}

function getTermFilter(filter) {
    if (filter.values === false || filter.values === true) {
        return esb.termQuery(`${filter.field}`, filter.values);
    }

    return esb.termQuery(`${filter.field}.keyword`, filter.values);
}

function getRangeFilter(filter) {
    if (filter.field) {
        return esb.rangeQuery(filter.field).gte(filter.values.from).lte(filter.values.to);
    }
    return;
}

function buildRequestFilter(filters) {
    let filterMap = { must: [], must_not: [], should: [] };
    try {
        if (!filters) return filterMap;
        console.log(`Filter in body is ${filters}`);
        filters = filters.reduce((acc, filter) => {
            if (['term'].includes(filter.subtype)) {
                filterMap[filter.type].push(getTermFilter(filter));
                return [...acc, getTermFilter(filter)];
            }
            if (['range'].includes(filter.subtype)) {
                filterMap[filter.type].push(getRangeFilter(filter));
                return [...acc, getRangeFilter(filter)];
            }
            return acc;
        }, []);

        if (filters.length < 1) return;
        return filterMap;
    } catch (error) {
        console.log('Unexpected Error found while mapping filters in buildRequestFilter ', error.message);
        return filterMap;
    }
}

module.exports = {
    buildFrom,
    buildMatch,
    buildRequestFilter,
    buildSort,
};
