const client = require('../../../elasticsearch');
const config = require('../../../config');
const esb = require('elastic-builder'); //the builder
const index = config.ELASTICSEARCH_INDEX;
const utilityMethod = require('./utility-methods');

// This method is used to get data from elastic with size
const searchAll = (current, resultsPerPage, searchTerm, filters, sortbody, include, done) => {
    try {
        const sort = utilityMethod.buildSort(sortbody);
        const match = utilityMethod.buildMatch(searchTerm);
        const size = resultsPerPage;
        const from = utilityMethod.buildFrom(current, size);
        const filter = utilityMethod.buildRequestFilter(filters);
        // let from = page <= 0 || page === 1 ? 1 : page * size - size;
        // let searchText = querytext;

        let dateRangesForCreatedAt = [
            { from: '1995-07-24', to: '2000-07-25' },
            { from: '2000-07-25', to: '2005-07-26' },
            { from: '2005-07-26', to: '2010-07-27' },
        ];
        let followersRange = [
            { from: 10000, to: 100000, key: '10000 - 100000' },
            { from: 100000, to: 200000, key: '100000 - 200000' },
            { from: 200000, to: 5000000, key: '200000 - 5000000' },
        ];
        let excludes = ['email', 'phone', 'source', 'barter_ready'];
        let includes = [];

        const requestBody = esb
            .requestBodySearch()
            .size(size)
            .from(from)
            .source({
                includes: include,
                excludes: excludes,
            })
            .query(esb.boolQuery().must(match).filter(filter.must).mustNot(filter.must_not).should(filter.should))
            .agg(esb.termsAggregation('group_by_category', 'categories').size(10))
            .agg(esb.termsAggregation('group_by_label', 'label.keyword').size(10))
            .agg(esb.termsAggregation('group_by_gender', 'gender.keyword').size(10))
            .agg(
                esb.rangeAggregation('group_by_created_at', 'created_at').ranges(dateRangesForCreatedAt)
                // .format('yyyy-mm-dd')
            )
            .agg(esb.rangeAggregation('group_by_followers', 'instagram.insta_followers').ranges(followersRange))
            .sort(sort);

        // Making search api call to elastic server
        console.log(requestBody.toJSON());
        client.search({ index: index, body: requestBody.toJSON() }, function (error, response, status) {
            if (error) {
                console.log('Error occured while fetching the data from elastic search ' + error);
                return done(error, null);
            } else {
                console.log('Response found from elastic search query');
                return done(null, response);
            }
        });
    } catch (error) {
        console.log('Unexpected Error occured while fetching the data from elastic search' + error);
        return done(error, null);
    }
};

// The suggestion API, it will list the top influencers according to the keyword from
// name searched
const suggestInfluencers = (page, platform, username, done) => {
    const from = utilityMethod.buildFrom(page, 15);
    console.log('Suggest for ', platform, username);
    const buildMatch = utilityMethod.buildMatch(username);

    // const requestBody = new esb.requestBodySearch().size(15).from(from).query(esb.matchAllQuery());
    // Query #1
    const includeFields = ['name', `${platform}_handle`, 'profile_image'];
    const requestBody = new esb.requestBodySearch()
        .source({
            includes: includeFields,
        })
        .query(esb.boolQuery().must(buildMatch));

    // Query #2
    // .query(esb.wildcardQuery(`${platform}.username`, `*${username}*`)
    //             .boost(2.0))

    console.log(requestBody.toJSON());

    client.search(
        {
            index: index,
            body: requestBody.toJSON(),
        },
        function (err, res, status) {
            if (err) {
                return done(err, null);
            } else {
                return done(null, res);
            }
        }
    );
};

const filterInfluencer = (page, platform, category, followers, done) => {
    const from = utilityMethod.buildFrom(page, 15);

    const requestBody = new esb.requestBodySearch().size(15).from(from).query(esb.matchAllQuery());
    // const requestBody = new esb.requestBodySearch()
    //     .query(esb.wildcardQuery(`${platform}.username`, `*${username}*`)
    //                 .boost(2.0))

    console.log(requestBody.toJSON());

    client.search(
        {
            index: index,
            body: requestBody.toJSON(),
        },
        function (err, res, status) {
            if (err) {
                return done(err, null);
            } else {
                return done(null, res);
            }
        }
    );
};

module.exports = {
    searchAll,
    suggestInfluencers,
    filterInfluencer,
};
