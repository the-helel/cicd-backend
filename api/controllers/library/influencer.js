const { validationResult } = require('express-validator');

const Brand = require('../../models/company/brand');
const Library = require('../../models/library');

const constants = require('../../constants');

const middlewares = {};

// function to create a new content
middlewares.createContent = async (req, res, next) => {
    // checking for validation error
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // forming the data structure from body
        const contentData = {};
        contentData.influencerId = req.user;
        contentData.url = req.file.location ? req.file.location : '';
        contentData.price = req.body.price ? req.body.price : '';
        contentData.caption = req.body.caption ? req.body.caption : '';
        if (req.body.brandId != 'null') {
            contentData.tag = {};
            // if (req.body.brandId !== 'None') {
            if (await Brand.exists({ _id: req.body.brandId })) {
                await Brand.findById(req.body.brandId, (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    contentData.tag.brandId = brand._id;
                    contentData.tag.brandName = brand.name;
                    contentData.tag.company = brand.company;
                });
            } else {
                return res.status(400).send({ error: true, message: ['Invalid brandId'] });
            }
            // } else {
            //     contentData.tag.brandName = req.body.tag.brandName ? req.body.tag.brandName.toLowerCase() : 'open';
            // }
        }

        // creating the content object in db
        Library.create(contentData, (err, content) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            return res.status(201).send({ error: false, result: { content: content } });
        });
    } else {
        return res.status(500).send({ error: true, message: [constants.VALIDATION_RESULT] });
    }
};

// function to edit an existing content
middlewares.editContent = (req, res, next) => {
    // checking for validation error
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // accessing the required content using given contentId
        Library.findById(req.body.contentId, (err, content) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!content) return res.status(400).send({ error: true, message: ['invalid contentId'] });
            if (content.paid) return res.status(400).send({ error: true, message: ['cannot make changes to this content'] });

            // if user is owner then info can be updated
            if (content.influencerId.toString() === req.user) {
                content.price = req.body.price ? req.body.price : content.price;
                content.caption = req.body.caption ? req.body.caption : content.caption;

                if (req.body.brandId && req.body.brandId !== content.tag.brandId) {
                    // if (req.body.tag.brandId != 'None') {
                    Brand.findById(req.body.brandId, (err, brand) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });
                        content.tag.brandId = brand._id;
                        content.tag.brandName = brand.name;
                        content.tag.company = brand.company;
                    });
                    // } else {
                    //     content.tag.brandId = null;
                    //     content.tag.companyId = null;
                    //     content.tag.brandName = req.body.tag.brandName ? req.body.tag.brandName.toLowerCase() : 'Open';
                    // }
                }

                // saving the object with the given details
                content.save((err, content) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    return res.status(200).send({ error: false, result: { content: content } });
                });
            } else {
                return res.status(400).send({ error: true, message: ['not authorized to do this operation'] });
            }
        });
    } else {
        return res.status(400).send({ error: true, message: [errors.message] });
    }
};

// function to get a specific content created by influencer
middlewares.viewContent = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // getting the content using the given id
        Library.findById(
            req.params.id,
            {
                'tag.brandName': 1,
                'tag.brandId': 1,
                url: 1,
                price: 1,
                caption: 1,
                paidPartnershipAllowed: 1,
                paid: 1,
                _id: 1,
                createdAt: 1,
                influencerId: 1,
            },
            (err, content) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!content) return res.status(400).send({ error: true, message: ['invalid content Id'] });

                // verifying if the influencer is the owner or not
                if (req.user === content.influencerId.toString()) {
                    return res.status(200).send({ error: false, result: { content: content } });
                } else {
                    return res.status(401).send({ error: true, message: ['not authorized to perform this operation'] });
                }
            }
        );
    } else {
        return res.status(400).send({ error: true, message: [errors.message] });
    }
};

// function to view all the contents of an influencer with pagination and filtering based on
// paid status of the content
middlewares.viewContents = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // parameters for pagination
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        // generating the filter
        const findFilter = { influencerId: req.user };
        if (req.query.paid) {
            findFilter.paid = req.query.paid === 'true' ? true : false;
        }

        // making the query
        Library.find(findFilter)
            .skip(size * (pageNo - 1))
            .limit(size)
            .sort(req.query.sortBy)
            .select({
                url: 1,
                price: 1,
                caption: 1,
                paidPartnershipAllowed: 1,
                paid: 1,
                'tag.brandId': 1,
                'tag.brandName': 1,
                createdAt: 1,
            })
            .exec((err, contents) => {
                if (err) return res.status(400).send({ error: true, message: [err.message] });
                return res.status(200).send({ error: false, result: { contents: contents } });
            });
    } else {
        return res.status(400).send({ error: true, message: [errors.message] });
    }
};

// function to delete a content
middlewares.deleteContent = (req, res, next) => {
    // validating inputs
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // accessing the content
        Library.findById(req.query.id, (err, content) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!content) return res.status(400).send({ error: true, message: ['invalid content Id'] });

            // checking if user is the owner
            if (content.influencerId.toString() === req.user) {
                // checking if the content is paid for or not
                if (content.paid) return res.status(401).send({ error: true, message: ['cannot delete this content'] });
                // deleting the content
                content.deleteOne((err) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    return res.status(200).send({ error: false, result: [constants.SUCCESS] });
                });
            } else {
                return res.status(400).send({ error: true, message: ['not authorized to perform this operation'] });
            }
        });
    } else {
        return res.status(400).send({ error: true, message: [errors.message] });
    }
};

module.exports = middlewares;
