const { validationResult } = require('express-validator');

const Company = require('../../models/company/company');
const Brand = require('../../models/company/brand');
const CashCampaign = require('../../models/campaign/cash-campaign/cash-campaign');
const ProductCampaign = require('../../models/campaign/product-campaign/product-campaign');

//New imports for new routes
const Campaigns = require('../../models/campaign/campaign');
const Applicants = require('../../models/campaign/applicant');
const Facebook = require('../../models/social/facebook');
const Post = require('../../models/campaign/post');
const Instagram = require('../../models/social/instagram');
const constants = require('../../constants.js');
const GraphAPI = require('../util/instagram-api');
const { sendNotification } = require('../notification/notification');
const { updateCampaignPostLinks } = require('../../services/influencer');
const kAsync = require('async');
const CampaignService = require('../../services/campaign');
const { PostTimelineAgg, CampaignPostAgg, CampaignTopPosts } = require('../../services/post');
const { convertNumToAbb } = require('../../../utils/formatter');
const { setAmountFields } = require('../../services/campaign/utils.js');

const middlewares = {};

/**
 * @deprecated Function to get all the campaigns of the user
 */
middlewares.getCampaigns = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Company.findById(req.user)
            .lean()
            .then(async (user) => {
                if (user != null) {
                    let campaignsList = [];
                    for (const brandId of user.brands) {
                        await Brand.findById(brandId)
                            .then(async (brand) => {
                                if (brand != null) {
                                    for (const campaignId of brand.campaigns) {
                                        await CashCampaign.findById(campaignId)
                                            .then((campaign) => {
                                                if (campaign != null) {
                                                    campaignsList.push(campaign);
                                                }
                                            })
                                            .catch((err) => {
                                                console.log(err);
                                            });
                                    }
                                }
                            })
                            .catch((err) => {
                                console.log(err);
                            });
                    }

                    return res.status(200).send(campaignsList);
                } else {
                    return res.status(400).send({
                        error: true,
                        message: ['Not authorized to do this operation'],
                    });
                }
            });
    }
};

// function to get a specific campaign
middlewares.getCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Company.findById(req.user, (err, user) => {
            if (err)
                return res.status(500).send({
                    error: true,
                    message: [err.message],
                });
            if (!user)
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to perform this operation'],
                });

            CashCampaign.findById(req.params.id, (err, campaign) => {
                if (err)
                    return res.status(500).send({
                        error: true,
                        message: ['Invalid campaign Id'],
                    });

                return res.status(200).send({
                    error: false,
                    result: {
                        campaign: campaign,
                    },
                });
            });
        });
    }
};

// functions to get all the campaigns of a specific brand of the user
middlewares.getCampaignsByBrandId = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Brand.findById(req.params.id)
            .lean()
            .then(async (brand) => {
                if (brand != null) {
                    let campaigns = [];
                    for (const campaignId of brand.campaigns) {
                        await CashCampaign.findById(campaignId)
                            .lean()
                            .then((campaign) => {
                                if (campaign != null) {
                                    campaigns.push(campaign);
                                }
                            })
                            .catch((error) => {
                                console.log(error);
                            });
                    }
                    res.status(200).json({
                        error: false,
                        result: campaigns,
                    });
                } else {
                    res.status(400).json({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error.message],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

// function to delete a campaign
middlewares.deleteCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Company.findById(req.user, (err, user) => {
            if (err)
                return res.status(500).send({
                    error: true,
                    message: [err.message],
                });
            if (!user)
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to perform this operation'],
                });

            // checking if the user is the owner of the brand
            if (user.brands.includes(req.query.brandId)) {
                Brand.findById(req.query.brandId, (err, brand) => {
                    if (err)
                        return res.status(500).send({
                            error: true,
                            message: [err.message],
                        });
                    if (!brand)
                        return res.status(400).send({
                            error: true,
                            message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                        });

                    // checking if the campaign is of specified brand
                    if (brand.campaigns.includes(req.query.campaignId)) {
                        CashCampaign.findByIdAndDelete(req.query.campaignId, (err) => {
                            if (err)
                                return res.status(500).send({
                                    error: true,
                                    message: [err.message],
                                });

                            // deleting the campaign
                            const campaignIndex = brand.campaigns.indexOf(req.query.campaignId);
                            brand.campaigns.splice(campaignIndex, 1);
                            brand.save((err, savedBrand) => {
                                if (!err)
                                    return res.status(200).send({
                                        error: false,
                                        message: [constants.SUCCESS],
                                    });
                            });
                        });
                    } else {
                        return res.status(400).send({
                            error: true,
                            message: ['This campaign is not accessible'],
                        });
                    }
                });
            } else {
                return res.status(400).send({
                    error: true,
                    message: ['This brandId is not accessible'],
                });
            }
        });
    }
};

// function to get all the campaign according to their status
middlewares.getCampaignsByStatus = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // checking if the user is linkid or not
        Company.findById(req.user, async (err, user) => {
            if (err)
                return res.status(500).send({
                    error: true,
                    message: [err.message],
                });
            if (!user)
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to perform this operation'],
                });

            let filteredCampaigns = [];

            // finding all the brands of the user
            for (const brandId of user.brands) {
                await Brand.findById(brandId)
                    .lean()
                    .then(async (brand) => {
                        // getting all the campaigns and checking the status of each campaign
                        for (const campaignId of brand.campaigns) {
                            await CashCampaign.findById(campaignId)
                                .lean()
                                .then((campaign) => {
                                    // if the status is All then push all of them in the list
                                    if (req.params.status === 'All') {
                                        filteredCampaigns.push(campaign);
                                    } else {
                                        //  else check which campaigns to push
                                        if (campaign.status === req.params.status) {
                                            filteredCampaigns.push(campaign);
                                        }
                                    }
                                })
                                .catch((err) => console.log(err));
                        }
                    })
                    .catch((err) => console.log(err));
            }
            return res.status(200).send({
                error: false,
                result: {
                    campaigns: filteredCampaigns,
                },
            });
        }).catch((err) => console.log(err));
    }
};
// End of depricated middlewares

// { New middlewares for only campaign }

middlewares.applyCompanyCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty) {
        middlewares
            .checkEligibility(req.params.campaignId, req.body.social, req.user)
            .then(() => {
                Applicants.findOne({
                    influencerId: req.user,
                    campaignId: req.params.campaignId,
                })
                    .then((applicant) => {
                        console.log('Apply campaign body', req.body);
                        if (applicant == null) {
                            const applicant = new Applicants();
                            applicant.campaignId = req.params.campaignId;
                            applicant.influencerId = req.user;
                            applicant.status = constants.APPLIED;
                            applicant.applied_platform = req.body.applied_platform ? req.body.applied_platform : constants.INSTAGRAM;

                            const influencerStartBid = {};
                            influencerStartBid.amount = parseInt(req.body.amount || 0);
                            influencerStartBid.status = null;
                            influencerStartBid.timestamp = new Date();

                            const bid = {
                                influencer: influencerStartBid,
                                company: null,
                            };

                            applicant.amount = req.body.amount;

                            applicant.social = req.body.social;
                            applicant.pitch = req.body.pitch;

                            applicant.bids.push(bid);

                            for (let file of req.files) {
                                applicant.workSample.push(file.location);
                            }

                            if (req.body.applicant) {
                                applicant.name = req.body.applicant.name ? req.body.applicant.name : '';
                                applicant.post_count = req.body.applicant.post_count ? req.body.applicant.post_count : '';
                                applicant.engagement = req.body.applicant.engagement ? req.body.applicant.engagement : '';
                                applicant.video_views = req.body.applicant.video_views ? req.body.applicant.video_views : '';
                                applicant.like_count = req.body.applicant.like_count ? req.body.applicant.like_count : '';
                                applicant.location = req.body.applicant.location ? req.body.applicant.location : '';
                                applicant.category = req.body.applicant.category ? req.body.applicant.category : '';
                                applicant.platforms = req.body.applicant.platforms ? req.body.applicant.platforms : '';
                            }

                            applicant
                                .save()
                                .then((record) => {
                                    console.log(`Successfully applied into campaign ${record.campaignId} with applied id ${record._id} `);
                                    res.status(200).json({
                                        error: false,
                                        message: [constants.SUCCESS],
                                        data: record,
                                    });
                                })
                                .catch((error) => {
                                    res.status(500).json({
                                        error: false,
                                        message: [error.message],
                                    });
                                });
                        } else {
                            res.status(400).json({
                                error: true,
                                message: ['Already applied to this campaign.'],
                            });
                        }
                    })
                    .catch((error) => {
                        res.status(500).json({
                            error: false,
                            message: [error.message],
                        });
                    });
            })
            .catch((error) => {
                console.log('Error occured while applying campaign', error);
                return res.status(400).json({
                    error: true,
                    message: [error],
                });
            });
    } else {
        return res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.checkEligibility = async (campaign, social, influencer) => {
    return await Campaigns.findById(campaign)
        .lean()
        .then((campaign) => {
            if (campaign) {
                if (campaign.status == 'Active') {
                    Instagram.findOne({
                        influencer: influencer,
                        _id: social,
                    })
                        .lean()
                        .then((instagram) => {
                            //####Need to fix this linkidation
                            // if (instagram) {
                            //     if (
                            //         instagram.numberOfFollowers >= campaign.criteria.reachCount.min &&
                            //         instagram.numberOfFollowers <= campaign.criteria.reachCount.max
                            //     ) {
                            //         return;
                            //     } else {
                            //         return Promise.reject('You are not eligible for this campaign');
                            //     }
                            // } else {
                            //     return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Instagram'));
                            // }
                            return;
                        })
                        .catch((error) => {
                            return Promise.reject(error.message);
                        });
                } else {
                    return Promise.reject('You cannot apply to this campaign.');
                }
            } else {
                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Campaign'));
            }
        })
        .catch((error) => {
            return Promise.reject(error.message);
        });
};

middlewares.influencerCrossBid = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Applicants.findOne({
            influencerId: req.user,
            campaignId: req.body.campaignId,
            status: constants.APPLIED,
        })
            .then((applicant) => {
                if (applicant != null) {
                    const length = applicant.bids.length;
                    if (applicant.bids[length - 1].company != null) {
                        // When influencer accepts the offering bid of company.
                        if (req.body.status == constants.APPROVE) {
                            applicant.bids[length - 1].company.status = constants.APPROVED;
                            applicant.status = constants.APPROVED;
                        }
                        // When influencer rejects the incoming bid of brand.
                        else if (req.body.status == constants.REJECT) {
                            applicant.bids[length - 1].company.status = constants.REJECTED;

                            const influencerStartBid = {};
                            influencerStartBid.amount = parseInt(req.body.amount);
                            influencerStartBid.status = null;
                            influencerStartBid.timestamp = new Date();

                            const bid = {
                                influencer: influencerStartBid,
                                company: null,
                            };

                            applicant.bids.push(bid);
                        } else {
                            res.status(400).json({
                                error: true,
                                message: ['Inlinkid status type.'],
                            });
                            return;
                        }
                        applicant.markModified('bids');
                        const updatedField = setAmountFields(applicant.bids);

                        applicant.finalBidAmount = updatedField.finalBidAmount;
                        applicant.crossBidAmount = updatedField.crossBid;
                        applicant
                            .save()
                            .then((record) => {
                                res.status(200).json({
                                    error: false,
                                    applicant: record,
                                });
                            })
                            .catch((error) => {
                                res.status(500).json({
                                    error: false,
                                    message: [error.message],
                                });
                            });
                    } else {
                        res.status(400).json({
                            error: true,
                            message: ['Cannot bid at this moment.'],
                        });
                    }
                } else {
                    res.status(400).json({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: false,
                    message: [error.message],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

//########### Need to refactor the code for company bidding
middlewares.companyCrossBid = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Campaigns.findById(req.body.campaignId)
            .lean()
            .then((campaign) => {
                if (campaign != null) {
                    Brand.findById(campaign.brandId)
                        .lean()
                        .then((brand) => {
                            if (brand != null) {
                                if (req.user == brand.company) {
                                    Applicants.findOne({
                                        _id: req.body.applicantId,
                                        status: constants.APPLIED,
                                    })
                                        .then((applicant) => {
                                            if (applicant != null) {
                                                let body;
                                                const length = applicant.bids.length;
                                                if (applicant.bids[length - 1].company == null) {
                                                    if (req.body.status == constants.APPROVE) {
                                                        applicant.bids[length - 1].influencer.status = constants.APPROVED;
                                                        applicant.status = constants.APPROVED;
                                                        body = `${brand.name} has approved your submission for campaign.`;
                                                    } else if (req.body.status == constants.REJECT) {
                                                        applicant.bids[length - 1].influencer.status = constants.REJECTED;
                                                        applicant.status = constants.REJECTED;
                                                        body = `${brand.name} has rejected your submission for campaign.`;
                                                    } else if (req.body.status == constants.CROSSBID) {
                                                        applicant.bids[length - 1].influencer.status = constants.REJECTED;

                                                        const companyBid = {};
                                                        companyBid.amount = parseInt(req.body.amount);
                                                        companyBid.status = null;
                                                        companyBid.timestamp = new Date();

                                                        applicant.bids[length - 1].company = companyBid;
                                                        body = `${brand.name} has a new cross-bid of ${companyBid.amount}.`;
                                                    } else {
                                                        res.status(400).json({
                                                            error: true,
                                                            message: 'Invalid status type.',
                                                        });
                                                        return;
                                                    }

                                                    applicant.markModified('bids');
                                                    const updatedField = setAmountFields(applicant.bids);
                                                    console.log('Updated amount fields:', updatedField);
                                                    applicant.finalBidAmount = updatedField.finalBidAmount;
                                                    applicant.crossBidAmount = updatedField.crossBid;
                                                    console.log(applicant);
                                                    applicant.save().then((applicant) => {
                                                        sendNotification(
                                                            brand.name,
                                                            body,
                                                            null,
                                                            applicant.influencerId,
                                                            constants.CASH,
                                                            constants.NOTIFICATION_API_APPLICANT_RESPONSE,
                                                            {
                                                                campaign: campaign._id,
                                                                applicant: applicant._id,
                                                                platform: applicant.applied_platform,
                                                            }
                                                        )
                                                            .then(() => {
                                                                res.status(200).json({
                                                                    error: false,
                                                                    message: constants.SUCCESS,
                                                                });
                                                            })
                                                            .catch((err) => {
                                                                res.status(500).json({
                                                                    error: true,
                                                                    message: err,
                                                                });
                                                            });
                                                    });
                                                } else {
                                                    res.status(400).json({
                                                        error: true,
                                                        message: ['Cannot bid at this moment.'],
                                                    });
                                                }
                                            } else {
                                                res.status(400).json({
                                                    error: true,
                                                    message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')],
                                                });
                                            }
                                        })
                                        .catch((error) => {
                                            res.status(500).json({
                                                error: false,
                                                message: [error.message],
                                            });
                                        });
                                } else {
                                    res.status(400).json({
                                        error: true,
                                        message: ['You are not authorized'],
                                    });
                                }
                            } else {
                                res.status(400).json({
                                    error: true,
                                    message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                                });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({
                                error: false,
                                message: [error.message],
                            });
                        });
                } else {
                    res.status(400).json({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: false,
                    message: [error.message],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

/**
 * Middleware to submit the link after application is approved.
 *
 * The influencer makes a POST request and sends the array of links which can
 * be of any platforms like Instagram or YouTube. We have to filter the links based on
 * {applied_platform} value.
 *
 * RegEx are used to find the pattern from submitted links. The duplicate links are removed
 * from {req.body.postLink} and only unique links will be saved to database.
 */
middlewares.updateApplicantPostLink = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        try {
            await updateCampaignPostLinks(req, res, next);
        } catch (e) {
            return res.status(400).json({
                error: true,
                message: [e.message],
            });
        }
    } else {
        return res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

/**
 * The campaign reporting API. It will get all the data from different schemas
 * and put all them together to make a final campaign report.
 */
middlewares.getCampaignReport = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        try {
            await CampaignService.GetCampaignReport(req.params.campaignId, (err, result) => {
                if (err) {
                    return res.status(400).json(err);
                } else {
                    return res.status(200).json(result);
                }
            });
        } catch (e) {
            return Promise.reject(e);
        }
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

/**
 * Get campaign by brand id
 *
 * GET /influencer/campaign/brand/:id
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
middlewares.getBrandCampaignsById = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        let pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        let size = constants.PAGE_SIZE;

        Campaigns.find({ brandId: req.params.id, status: constants.ACTIVE })
            .skip(size * (pageNo - 1))
            .limit(size)
            .populate({
                path: 'brief.categories',
                select: {
                    name: 1,
                    icon: 1,
                },
            })
            .lean()
            .then(async (campaigns) => {
                if (campaigns != null) {
                    res.status(200).json({
                        error: false,
                        data: campaigns,
                    });
                } else {
                    res.status(400).json({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error.message],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

module.exports = middlewares;
