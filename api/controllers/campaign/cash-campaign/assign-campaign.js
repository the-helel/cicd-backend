const constants = require('../../../constants.js');
const agenda = require('../../../jobs/agenda');

const assignCampaign = async (body, campaign) => {
    campaign.brief = {};
    if (body.brief) {
        if (body.inviteOnly === true) {
            campaign.inviteOnly = true;
            if (body.influencers) campaign.influencers = body.influencers;
        }
        if (body.brief.objective) campaign.brief.objective = body.brief.objective;
        if (body.brief.title) campaign.brief.title = body.brief.title;
        if (body.brief.brief) campaign.brief.brief = body.brief.brief;
        campaign.brief.tasks = {};
        if (body.brief.tasks) {
            campaign.brief.tasks.instagram = {};
            if (body.brief.tasks.instagram) {
                if (body.brief.tasks.instagram.stories) {
                    campaign.brief.tasks.instagram.stories = body.brief.tasks.instagram.stories;
                }
                if (body.brief.tasks.instagram.posts) campaign.brief.tasks.instagram.posts = body.brief.tasks.instagram.posts;
            }
        }
        if (body.brief.type) campaign.brief.type = body.brief.type;
        if (body.brief.pageUrl) campaign.brief.pageUrl = body.brief.pageUrl;
        if (body.brief.coverPhoto && body.brief.coverPhoto.url) campaign.brief.coverPhoto.url = body.brief.coverPhoto.url;
        if (body.brief.caption) campaign.brief.caption = body.brief.caption;
        campaign.brief.product = {};
        if (body.brief.product) {
            campaign.brief.product.isProvidedByBrand = body.brief.product.isProvidedByBrand;
            if (body.brief.product.optionalDetails) campaign.brief.product.optionalDetails = body.brief.product.optionalDetails;
        }
    }
    campaign.criteria = {};
    if (body.criteria) {
        if (body.criteria.gender) campaign.criteria.gender = body.criteria.gender;
        if (body.criteria.city) campaign.criteria.city = body.criteria.city;
        campaign.criteria.age = {};
        if (body.criteria.age) {
            if (body.criteria.age.min) campaign.criteria.age.min = body.criteria.age.min;
            if (body.criteria.age.max) campaign.criteria.age.max = body.criteria.age.max;
        }
        campaign.criteria.reachCount = {};
        if (body.criteria.reachCount) {
            if (body.criteria.reachCount.min) campaign.criteria.reachCount.min = body.criteria.reachCount.min;
            if (body.criteria.reachCount.max) campaign.criteria.reachCount.max = body.criteria.reachCount.max;
        }

        if (body.criteria.categories) campaign.criteria.categories = body.criteria.categories;
        if (body.criteria.moodboard) campaign.criteria.moodboard = body.criteria.moodboard;
        if (body.criteria.description) campaign.criteria.description = body.criteria.description;
        if (body.criteria.dos) campaign.criteria.dos = body.criteria.dos;
        if (body.criteria.donts) campaign.criteria.donts = body.criteria.donts;
        if (body.criteria.hashtags) campaign.criteria.hashtags = body.criteria.hashtags;
        if (body.criteria.taggedProfiles) campaign.criteria.taggedProfiles = body.criteria.taggedProfiles;
        if (body.criteria.caption) campaign.criteria.caption = body.criteria.caption;
    }

    if (body.socialType) campaign.socialType = body.socialType;
    else campaign.socialType = constants.INSTAGRAM;

    if (body.budget) campaign.budget = body.budget;
    if (body.startDate) campaign.startDate = body.startDate;
    if (body.endDate) {
        if (body.status && body.status === constants.ACTIVE) {
            campaign.endDate = body.endDate;
            const jobs = await agenda.jobs({ name: 'expire-campaign' });
            for (const job of jobs) {
                if (job.attrs.data.campaignId === campaign._id) {
                    try {
                        await job.remove();
                        console.log('Successfully removed job from collection');
                    } catch (e) {
                        console.error('Error removing job from collection');
                    }
                }
            }
            agenda.schedule(campaign.endDate, 'expire-campaign', { campaignId: campaign._id });
        }
    }
    if (body.status) {
        campaign.status = body.status;
    } else {
        campaign.status = constants.DRAFT;
    }
};

module.exports = assignCampaign;
