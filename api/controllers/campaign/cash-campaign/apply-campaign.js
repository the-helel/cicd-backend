const { validationResult } = require('express-validator');

const mongoose = require('mongoose');

const Instagram = require('../../../models/social/instagram');
const Applicant = require('../../../models/campaign/cash-campaign/cash-campaign-applicant');
const Campaign = require('../../../models/campaign/cash-campaign/cash-campaign');
const Brand = require('../../../models/company/brand');

const { sendNotification } = require('../../notification/notification');

const constants = require('../../../constants');

const middlewares = {};

middlewares.applyCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty) {
        middlewares
            .checkEligibility(req.body.campaignId, req.body.social, req.user)
            .then(() => {
                Applicant.findOne({
                    influencer: req.user,
                    campaign: req.body.campaignId,
                })
                    .then((applicant) => {
                        if (applicant == null) {
                            const applicant = new Applicant();
                            applicant.influencer = req.user;
                            applicant.social = req.body.social;
                            applicant.campaign = req.body.campaignId;
                            applicant.pitch = req.body.pitch;

                            const influencerStartBid = {};
                            influencerStartBid.biddingAmount = parseInt(req.body.biddingAmount);
                            influencerStartBid.status = null;
                            influencerStartBid.timestamp = new Date();

                            const bid = {
                                influencer: influencerStartBid,
                                company: null,
                            };

                            // Initial bid will be equal to influencer's starting bid
                            applicant.amount = influencerStartBid.biddingAmount;

                            applicant.bids.push(bid);

                            for (let file of req.files) {
                                applicant.workSample.push(file.location);
                            }
                            applicant
                                .save()
                                .then((record) => {
                                    res.status(200).json({
                                        error: false,
                                        message: [constants.SUCCESS],
                                    });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: false, message: [error.message] });
                                });
                        } else {
                            res.status(400).json({ error: true, message: ['Already applied to this campaign.'] });
                        }
                    })
                    .catch((error) => {
                        res.status(500).json({ error: false, message: [error.message] });
                    });
            })
            .catch((error) => {
                res.status(400).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.checkEligibility = async (campaign, social, influencer) => {
    return await Campaign.findById(campaign)
        .lean()
        .then((campaign) => {
            if (campaign) {
                if (campaign.status == 'Active') {
                    Instagram.findOne({ influencer: influencer, _id: social })
                        .lean()
                        .then((instagram) => {
                            if (instagram) {
                                if (
                                    instagram.numberOfFollowers >= campaign.criteria.reachCount.min &&
                                    instagram.numberOfFollowers <= campaign.criteria.reachCount.max
                                ) {
                                    return;
                                } else {
                                    return Promise.reject('You are not eligible for this campaign');
                                }
                            } else {
                                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Instagram'));
                            }
                        })
                        .catch((error) => {
                            return Promise.reject(error.message);
                        });
                } else {
                    return Promise.reject('You cannot apply to this campaign.');
                }
            } else {
                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Campaign'));
            }
        })
        .catch((error) => {
            return Promise.reject(error.message);
        });
};

middlewares.influencerCrossBid = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Applicant.findOne({ influencer: req.user, campaign: req.body.campaign, status: constants.APPLIED })
            .then((applicant) => {
                if (applicant != null) {
                    const length = applicant.bids.length;
                    if (applicant.bids[length - 1].company != null) {
                        if (req.body.status == constants.APPROVE) {
                            applicant.bids[length - 1].company.status = constants.APPROVED;
                            applicant.status = constants.APPROVED;
                        } else if (req.body.status == constants.REJECT) {
                            applicant.bids[length - 1].company.status = constants.REJECTED;

                            const influencerStartBid = {};
                            influencerStartBid.biddingAmount = parseInt(req.body.biddingAmount);
                            influencerStartBid.status = null;
                            influencerStartBid.timestamp = new Date();

                            const bid = {
                                influencer: influencerStartBid,
                                company: null,
                            };

                            applicant.bids.push(bid);
                        } else {
                            res.status(400).json({ error: true, message: ['Invalid status type.'] });
                            return;
                        }
                        applicant.markModified('bids');
                        applicant
                            .save()
                            .then((record) => {
                                res.status(200).json({ error: false, applicant: record });
                            })
                            .catch((error) => {
                                res.status(500).json({ error: false, message: [error.message] });
                            });
                    } else {
                        res.status(400).json({ error: true, message: ['Cannot bid at this moment.'] });
                    }
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: false, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.companyCrossBid = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Campaign.findById(req.body.campaign)
            .lean()
            .then((campaign) => {
                if (campaign != null) {
                    Brand.findById(campaign.brand)
                        .lean()
                        .then((brand) => {
                            if (brand != null) {
                                if (req.user == brand.company) {
                                    Applicant.findOne({ _id: req.body.applicant, status: constants.APPLIED })
                                        .then((applicant) => {
                                            if (applicant != null) {
                                                let body;
                                                const length = applicant.bids.length;
                                                if (applicant.bids[length - 1].company == null) {
                                                    if (req.body.status == constants.APPROVE) {
                                                        applicant.bids[length - 1].influencer.status = constants.APPROVED;
                                                        applicant.status = constants.APPROVED;
                                                        body = `${brand.name} has approved your bid.`;
                                                    } else if (req.body.status == constants.REJECT) {
                                                        applicant.bids[length - 1].influencer.status = constants.REJECTED;
                                                        applicant.status = constants.REJECTED;
                                                        body = `${brand.name} has rejected your bid.`;
                                                    } else if (req.body.status == 'Cross Bid') {
                                                        applicant.bids[length - 1].influencer.status = constants.REJECTED;

                                                        const companyBid = {};
                                                        companyBid.biddingAmount = parseInt(req.body.biddingAmount);
                                                        companyBid.status = null;
                                                        companyBid.timestamp = new Date();

                                                        applicant.bids[length - 1].company = companyBid;
                                                        body = `${brand.name} has a new cross-bid for you.`;
                                                    } else {
                                                        res.status(400).json({ error: true, message: ['Invalid status type.'] });
                                                        return;
                                                    }

                                                    applicant.markModified('bids');
                                                    applicant
                                                        .save()
                                                        .then((record) => {
                                                            sendNotification(
                                                                brand.name,
                                                                body,
                                                                applicant.influencer,
                                                                constants.CASH_CAMPAIGN,
                                                                'applicant-response',
                                                                {
                                                                    campaign: campaign._id,
                                                                    applicant: applicant._id,
                                                                }
                                                            )
                                                                .then(() => {
                                                                    res.status(200).json({ error: false, applicant: record });
                                                                })
                                                                .catch((error) => {
                                                                    res.status(500).json({ error: true, message: [error] });
                                                                });
                                                        })
                                                        .catch((error) => {
                                                            res.status(500).json({ error: false, message: [error.message] });
                                                        });
                                                } else {
                                                    res.status(400).json({ error: true, message: ['Cannot bid at this moment.'] });
                                                }
                                            } else {
                                                res.status(400).json({
                                                    error: true,
                                                    message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')],
                                                });
                                            }
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: false, message: [error.message] });
                                        });
                                } else {
                                    res.status(400).json({ error: true, message: ['You are not authorized'] });
                                }
                            } else {
                                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: false, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: false, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

// function to submit the link after application is approved
middlewares.updateLink = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // fetching the approved applicant object with the given details
        Applicant.findOne(
            {
                status: constants.APPROVED,
                influencer: mongoose.Types.ObjectId(req.user),
                campaign: mongoose.Types.ObjectId(req.body.campaignId),
            },
            (err, applicant) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!applicant)
                    return res
                        .status(404)
                        .send({ error: true, message: ['Cannot find any approved application with unverified link using given details'] });

                if (req.body.postLink) {
                    applicant.link = req.body.postLink;
                    applicant.status = constants.SUBMITTED;
                    applicant.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({
                            error: false,
                            result: { message: 'The status of Post link verification will be updated after 2 days' },
                        });
                    });
                } else {
                    return res.status(400).send({ error: true, message: ['post Link is required'] });
                }
            }
        );
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
