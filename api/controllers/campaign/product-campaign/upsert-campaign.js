const { validationResult } = require('express-validator');

const Company = require('../../../models/company/company');
const Brand = require('../../../models/company/brand');
const ProductCampaign = require('../../../models/campaign/product-campaign/product-campaign');

const assignCampaign = require('./assign-campaign');

const constants = require('../../../constants');

const middlewares = {};

// function to create a new campaign if not exist
// or to update the existing one
middlewares.upsertCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // getting user from jwt token
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to do this operation'] });

            // checking if brand id is provided or not
            if (req.body.brandId) {
                // checking if brand is there in company's list
                if (user.brands.includes(req.body.brandId)) {
                    Brand.findById(req.body.brandId, (err, brand) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        if (req.body.campaignId) {
                            ProductCampaign.findById(req.body.campaignId).then(async (campaign) => {
                                if (campaign) {
                                    if (campaign.status === constants.ACTIVE)
                                        return res
                                            .status(400)
                                            .send({ error: true, message: ['Cannot make changes to this campaign as it is activated.'] });

                                    assignCampaign(req.body, campaign);

                                    campaign
                                        .save()
                                        .then((campaign) => {
                                            return res.status(200).send({ error: false, result: { campaign: campaign } });
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: true, message: [error.message] });
                                        });
                                } else {
                                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                                }
                            });
                        } else {
                            const campaign = new ProductCampaign();
                            campaign.brand = brand._id;
                            campaign.company = req.user;

                            assignCampaign(req.body, campaign);

                            campaign
                                .save()
                                .then((campaign) => {
                                    return res.status(200).send({ error: false, result: { campaign: campaign } });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error.message] });
                                });
                        }
                    });
                } else {
                    return res.status(400).send({ error: true, message: ['You are not authorized to do this operation'] });
                }
            } else {
                return res.status(500).send({ error: true, message: ['Brand Id is not provided'] });
            }
        });
    }
};

module.exports = middlewares;
