const { validationResult } = require('express-validator');

const mongoose = require('mongoose');

const Instagram = require('../../../models/social/instagram');
const Applicant = require('../../../models/campaign/product-campaign/product-campaign-applicant');
const Campaign = require('../../../models/campaign/product-campaign/product-campaign');
const Brand = require('../../../models/company/brand');

const { sendNotification } = require('../../notification/notification');

const constants = require('../../../constants');

const middlewares = {};

middlewares.applyCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty) {
        middlewares
            .checkEligibility(req.body.campaignId, req.body.social, req.user)
            .then(() => {
                Applicant.findOne({
                    influencer: req.user,
                    campaign: req.body.campaignId,
                })
                    .then((applicant) => {
                        if (applicant == null) {
                            const applicant = new Applicant();
                            applicant.influencer = req.user;
                            applicant.social = req.body.social;
                            applicant.campaign = req.body.campaignId;
                            applicant.pitch = req.body.pitch;

                            for (let file of req.files) {
                                applicant.workSample.push(file.location);
                            }
                            applicant
                                .save()
                                .then((record) => {
                                    res.status(200).json({
                                        error: false,
                                        message: [constants.SUCCESS],
                                    });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: false, message: [error.message] });
                                });
                        } else {
                            res.status(400).json({ error: true, message: ['Already applied to this campaign.'] });
                        }
                    })
                    .catch((error) => {
                        res.status(500).json({ error: false, message: [error.message] });
                    });
            })
            .catch((error) => {
                res.status(400).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.checkEligibility = async (campaign, social, influencer) => {
    return await Campaign.findById(campaign)
        .lean()
        .then((campaign) => {
            if (campaign) {
                if (campaign.status == 'Active') {
                    Instagram.findOne({ influencer: influencer, _id: social })
                        .lean()
                        .then((instagram) => {
                            if (instagram) {
                                if (
                                    instagram.numberOfFollowers >= campaign.criteria.reachCount.min &&
                                    instagram.numberOfFollowers <= campaign.criteria.reachCount.max
                                ) {
                                    return;
                                } else {
                                    return Promise.reject('You are not eligible for this campaign');
                                }
                            } else {
                                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Instagram'));
                            }
                        })
                        .catch((error) => {
                            return Promise.reject(error.message);
                        });
                } else {
                    return Promise.reject('You cannot apply to this campaign.');
                }
            } else {
                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Campaign'));
            }
        })
        .catch((error) => {
            return Promise.reject(error.message);
        });
};

middlewares.updateApplicantStatus = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Campaign.findById(req.body.campaign)
            .lean()
            .then((campaign) => {
                if (campaign != null) {
                    Brand.findById(campaign.brand)
                        .lean()
                        .then((brand) => {
                            if (brand != null) {
                                if (brand.company == req.user) {
                                    Applicant.findOne({ _id: req.body.applicant, status: constants.APPLIED })
                                        .then((applicant) => {
                                            if (applicant != null) {
                                                let body;
                                                if (req.body.status == constants.APPROVE) {
                                                    applicant.status = constants.APPROVED;
                                                    body = `${brand.name} has approved your bid.`;
                                                } else if (req.body.status == constants.REJECT) {
                                                    applicant.status = constants.REJECTED;
                                                    body = `${brand.name} has rejected your bid.`;
                                                } else {
                                                    res.status(400).json({ error: true, message: ['Invalid status type.'] });
                                                    return;
                                                }
                                                applicant
                                                    .save()
                                                    .then((record) => {
                                                        sendNotification(
                                                            brand.name,
                                                            body,
                                                            applicant.influencer,
                                                            constants.PRODUCT_CAMPAIGN,
                                                            'applicant-response',
                                                            {
                                                                campaign: campaign._id,
                                                                applicant: applicant._id,
                                                            }
                                                        )
                                                            .then(() => {
                                                                res.status(200).json({ error: false, applicant: record });
                                                            })
                                                            .catch((error) => {
                                                                res.status(500).json({ error: true, message: [error] });
                                                            });
                                                    })
                                                    .catch((error) => {
                                                        res.status(500).json({ error: false, message: [error.message] });
                                                    });
                                            } else {
                                                res.status(400).json({
                                                    error: true,
                                                    message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')],
                                                });
                                            }
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: false, message: [error.message] });
                                        });
                                } else {
                                    res.status(400).json({ error: true, message: ['You are not authorized'] });
                                }
                            } else {
                                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: false, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: false, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

// function to submit the link after application is approved
middlewares.updateLink = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // fetching the approved applicant object with the given details
        Applicant.findOne(
            {
                status: constants.APPROVED,
                influencer: mongoose.Types.ObjectId(req.user),
                campaign: mongoose.Types.ObjectId(req.body.campaignId),
            },
            (err, applicant) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!applicant)
                    return res
                        .status(404)
                        .send({ error: true, message: ['Cannot find any approved application with unverified link using given details'] });

                // if (applicant.linkVerified) return res.status(400).send({ error: true, message: ['Link Already Verified'] });
                // updating the post link in the applicant's application
                if (req.body.postLink) {
                    applicant.link = req.body.postLink;
                    applicant.status = constants.SUBMITTED;
                    applicant.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        // setting the schedule to verify the link
                        // agenda.schedule('5 seconds', 'verify link', { applicantId: applicant._id, counter: 3 });
                        // console.log('verification scheduled');

                        return res.status(200).send({
                            error: false,
                            result: { message: 'The status of Post link verification will be updated after 2 days' },
                        });
                    });
                } else {
                    return res.status(400).send({ error: true, message: ['post Link is required'] });
                }
            }
        );
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
