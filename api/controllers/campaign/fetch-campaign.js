const Brand = require('../../models/company/brand');
const CashCampaign = require('../../models/campaign/cash-campaign/cash-campaign');
const ProductCampaign = require('../../models/campaign/product-campaign/product-campaign');
const Company = require('../../models/company/company');

const populateCategories = require('../util/populate-categories');

const constants = require('../../constants');

const populateCriteria = async (criteria) => {
    const criteriaObject = {
        age: criteria.age,
        reachCount: criteria.reachCount,
        gender: criteria.gender,
        city: criteria.city,
        categories: await populateCategories(criteria.categories),
        moodboard: criteria.moodboard,
        description: criteria.description,
        dos: criteria.dos,
        donts: criteria.donts,
        hashtags: criteria.hashtags,
        taggedProfiles: criteria.taggedProfiles,
        caption: criteria.caption,
    };

    return criteriaObject;
};

const fetchCampaignData = async (type, campaign, brand, company, isSaved = null) => {
    const campaignObject = {
        id: campaign._id,
        type: type,
        brandName: brand.name,
        logo: brand.logo,
        about: brand.about,
        companyName: company.name,
        status: campaign.status,
        brief: campaign.brief,
        criteria: await populateCriteria(campaign.criteria),
        budget: campaign.budget,
        startDate: campaign.startDate,
        endDate: campaign.endDate,
        createdAt: campaign.createdAt,
        updatedAt: campaign.updatedAt,
        socialType: campaign.socialType,
    };

    if (isSaved != null) {
        campaignObject.isSaved = isSaved;
    }

    return campaignObject;
};

const fetchCampaign = async (type, campaignId, isSaved = null) => {
    let Campaign = type == constants.CASH_CAMPAIGN ? CashCampaign : ProductCampaign;

    return await Campaign.findById(campaignId)
        .lean()
        .then((campaign) => {
            if (campaign != null) {
                return Brand.findById(campaign.brand)
                    .lean()
                    .then((brand) => {
                        if (brand != null) {
                            return Company.findById(brand.company)
                                .lean()
                                .then(async (company) => {
                                    if (company != null) {
                                        return await fetchCampaignData(type, campaign, brand, company, isSaved);
                                    } else {
                                        return Promise.reject([constants.RESOURCE_NOT_FOUND_ERROR(constants.COMPANY)]);
                                    }
                                })
                                .catch((error) => {
                                    return Promise.reject([error.message]);
                                });
                        } else {
                            return Promise.reject([constants.RESOURCE_NOT_FOUND_ERROR('Brand')]);
                        }
                    })
                    .catch((error) => {
                        return Promise.reject([error.message]);
                    });
            } else {
                return Promise.reject([constants.RESOURCE_NOT_FOUND_ERROR('Campaign')]);
            }
        })
        .catch((error) => {
            return Promise.reject([error.message]);
        });
};

module.exports = { fetchCampaign: fetchCampaign, fetchCampaignData: fetchCampaignData };
