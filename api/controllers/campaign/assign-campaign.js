const constants = require('../../constants.js');
const agenda = require('../../jobs/agenda');
const mongoose = require('mongoose');

const assignCampaign = async (body, campaign) => {
    if (body.inHouse) campaign.inHouse = body.inHouse;
    if (body.inviteOnly) campaign.inviteOnly = body.inviteOnly;
    if (body.campaignType) campaign.campaignType = body.campaignType;
    if (body.title) campaign.title = body.title;
    if (body.aboutBrand) campaign.aboutBrand = body.aboutBrand;
    if (body.coverImage) campaign.coverImage = body.coverImage;
    if (body.caption) campaign.caption = body.caption;

    if (body.status) {
        campaign.status = body.status;
    } else {
        campaign.status = constants.DRAFT;
    }

    if (body.brief) {
        if (body.brief.campaignPlatform) campaign.brief.campaignPlatform = body.brief.campaignPlatform;
        else campaign.brief.campaignPlatform = constants.INSTAGRAM;

        if (body.brief.campaignUrl) campaign.brief.campaignUrl = body.brief.campaignUrl;
        if (body.brief.categories) campaign.brief.categories = body.brief.categories;
        if (body.brief.moodboard) campaign.brief.moodboard = body.brief.moodboard;
        if (body.brief.description) campaign.brief.description = body.brief.description;
        if (body.brief.dos) campaign.brief.dos = body.brief.dos;
        if (body.brief.donts) campaign.brief.donts = body.brief.donts;
        if (body.brief.hashtags) campaign.brief.hashtags = body.brief.hashtags;
        if (body.brief.taggedProfiles) campaign.brief.taggedProfiles = body.brief.taggedProfiles;
        if (body.brief.startDate) campaign.brief.startDate = body.brief.startDate;
        if (body.brief.endDate) {
            campaign.brief.endDate = body.brief.endDate;
        }
    }

    if (body.product) {
        campaign.product.isProvidedByBrand = body.product.isProvidedByBrand;
        if (body.product.optionalDetails) campaign.product.optionalDetails = body.product.optionalDetails;
        if (body.product.valueOfProduct) campaign.product.valueOfProduct = body.product.valueOfProduct;
    }

    // campaign.criteria = {};

    if (body.criteria) {
        if (body.criteria.gender) campaign.criteria.gender = body.criteria.gender;
        if (body.criteria.city) campaign.criteria.city = body.criteria.city;
        // campaign.criteria.age = {};
        if (body.criteria.age) {
            if (body.criteria.age.min) campaign.criteria.age.min = body.criteria.age.min;
            if (body.criteria.age.max) campaign.criteria.age.max = body.criteria.age.max;
        }
        // campaign.criteria.followers = {};
        if (body.criteria.followers) {
            if (body.criteria.followers.min) campaign.criteria.followers.min = body.criteria.followers.min;
            if (body.criteria.followers.max) campaign.criteria.followers.max = body.criteria.followers.max;
        }

        // campaign.criteria.task = {};
        if (body.criteria.task) {
            if (body.criteria.task.posts) campaign.criteria.task.posts = body.criteria.task.posts;
            if (body.criteria.task.videos) campaign.criteria.task.videos = body.criteria.task.videos;
            if (body.criteria.task.instagram) campaign.criteria.task.instagram = body.criteria.task.instagram;
        }
    }

    if (body.payment) {
        if (body.payment.budget) campaign.payment.budget = body.payment.budget;
        if (body.payment.method) campaign.payment.method = body.payment.method;
        if (body.payment.remarks) campaign.payment.remarks = body.payment.remarks;
        if (body.payment.utrno) campaign.payment.utrno = body.payment.utrno;
        // Product count is the number of products giveaway in case of product campaign
        if (body.payment.productCount) campaign.payment.productCount = body.payment.productCount;
    }
};

module.exports = assignCampaign;
