const mongoose = require('mongoose');

const Instagram = require('../../../models/socials/instagram');
const config = require('../../../config');

const { getProfile, assignProfile } = require('./get-profile');

mongoose
    .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async () => {
        console.log('Connected to MongoDB instance');
        await Instagram.find({})
            .then(async (instagrams) => {
                for (const instagram of instagrams) {
                    await getProfile(instagram.username)
                        .then(async (profile) => {
                            assignProfile(profile, instagram);
                            await instagram
                                .save()
                                .then((record) => {
                                    console.log('Influencer saved', instagram.username);
                                })
                                .catch((error) => {
                                    console.log('error', instagram.username);
                                });
                        })
                        .catch((error) => {
                            console.log('error', instagram.username);
                        });
                }
            })
            .catch((error) => {
                console.log('error');
            });
        console.log("Influencer's instagram profiles populated.");
    })
    .catch((error) => {
        console.log(error);
    });
