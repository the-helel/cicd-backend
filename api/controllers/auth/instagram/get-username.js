const axios = require('axios');

const getUsername = (accessToken) => {
    const url = `https://graph.instagram.com/me?fields=id,username,account_type,media,media_count&access_token=${accessToken}`;
    return axios
        .get(url)
        .then((response) => {
            return response.data.username;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = getUsername;
