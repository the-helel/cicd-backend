const axios = require('axios');

const config = require('../../../config');
const getLongLivedAccessToken = require('./get-long-lived-access-token');

const exchangeInstagramToken = (code) => {
    const url = `https://api.instagram.com/oauth/access_token`;

    const data = `client_id=${config.APP_ID}&client_secret=${config.APP_SECRET}&grant_type=authorization_code&redirect_uri=${config.REDIRECT_URI}&code=${code}`;

    return axios
        .post(url, data)
        .then((response) => {
            return getLongLivedAccessToken(response.data.access_token)
                .then((object) => {
                    object.userId = response.data.user_id;
                    return object;
                })
                .catch((error) => {
                    return Promise.reject(error);
                });
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = exchangeInstagramToken;
