const axios = require('axios');

const calculateEngagement = (profile) => {
    const numberofFollowers = profile.edge_followed_by.count;
    let sum = 0.0;
    for (let edge of profile.edge_felix_video_timeline.edges) {
        sum += edge.edge_liked_by.count;
    }
    if (profile.edge_felix_video_timeline.count) {
        profile.engagement = (parseFloat(sum) / (numberofFollowers * profile.edge_felix_video_timeline.count)) * 100.0;
    } else {
        profile.engagement = 0.0;
    }
};

const getProfile = (username) => {
    const url = `https://www.instagram.com/${username}/?__a=1`;
    return axios
        .get(url)
        .then((response) => {
            const profile = response.data.graphql.user;
            calculateEngagement(profile);
            return profile;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

const assignProfile = (profile, instagram) => {
    instagram.username = profile.username;
    instagram.numberOfFollowers = profile.edge_followed_by.count;
    instagram.numberOfFollowings = profile.edge_follow.count;
    instagram.fullName = profile.full_name;
    instagram.profilePicture = profile.profile_pic_url;
    instagram.profilePictureHD = profile.profile_pic_url_hd;
    instagram.engagement = profile.engagement;
    instagram.biography = profile.biography;
};

module.exports = {
    getProfile: getProfile,
    assignProfile: assignProfile,
};
