const jwt = require('jsonwebtoken');
const config = require('../../config');
const constants = require('../../constants');

// const accessMiddleWare = {}

const accessMiddleWare = (access, permissionCode) => {
    return (req, res, next) => {
        jwt.verify(req.headers['authorization'], config.JWT_SECRET, (error, decoded_token) => {
            if (error) {
                return res.status(401).json({ error: true, message: [error.message] });
            } else {
                let profileRole = config.ROLES[decoded_token.role];

                if (profileRole.value && profileRole.value < permissionCode) {
                    return res.status(401).json({ error: true, message: ['User not authorized'] });
                } else {
                    req.user = decoded_token.userId;
                    req.uid = decoded_token.uid;
                    req.userType = decoded_token.userType;
                    req.role = decoded_token.role;
                    next();
                }
            }
        });
    };
};

module.exports = accessMiddleWare;
