const isExpired = (date) => {
    return new Date() > date;
};

module.exports = isExpired;
