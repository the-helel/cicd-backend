const aws = require('../../../aws/aws-sns');

const sendOtp = (record) => {
    const otpToken = Math.floor(Math.random() * 900000 + 100000);
    record.otpToken = otpToken;
    const date = new Date();
    date.setMinutes(date.getMinutes() + 10);
    record.otpExpiry = date;
    const sns_parameters = {
        Message: `Your verification token is ${record.otpToken} .`,
        PhoneNumber: record.phoneNumber,
    };
    const publishTextPromise = aws.publish(sns_parameters).promise();
    return publishTextPromise;
};

module.exports = sendOtp;
