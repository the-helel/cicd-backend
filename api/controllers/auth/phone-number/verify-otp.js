const isExpired = require('../is-expired');

const verifyOtp = (record, otpToken) => {
    if (!isExpired(record.otpExpiry)) {
        if (record.otpToken == otpToken) {
            return [true, null];
        } else {
            return [false, { message: 'OTP does not match, OTP is being resent.' }];
        }
    } else {
        return [false, { message: 'OTP has been expired, OTP is being resent.' }];
    }
};

module.exports = verifyOtp;
