const axios = require('axios');

const config = require('../../../config');

const getInfluencerPages = async (accessToken) => {
    const url = config.FACEBOOK_GRAPH + `/me/accounts?access_token=${accessToken}`;

    return await axios
        .get(url)
        .then((response) => {
            console.log(response.data);
            return response.data;
        })
        .catch((error) => Promise.reject(error.response.data));
};

module.exports = getInfluencerPages;
