const axios = require('axios');

const config = require('../../../config');

const getInfluencerIgAccount = (pageId, accessToken) => {
    const url = config.FACEBOOK_GRAPH + `/${pageId}?fields=instagram_business_account&access_token=${accessToken}`;

    return axios
        .get(url)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = getInfluencerIgAccount;
