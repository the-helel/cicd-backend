const axios = require('axios');

const config = require('../../../config');

const getInstagramProfile = (userId, accessToken) => {
    const url =
        config.FACEBOOK_GRAPH +
        `/${userId}?fields=biography,id,ig_id,followers_count,follows_count,media_count,name,profile_picture_url,username,website&access_token=${accessToken}`;

    return axios
        .get(url)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = getInstagramProfile;
