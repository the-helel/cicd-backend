/**
 * @author Rahul Bera
 *
 * Controller for purchase inventory
 */

const { validationResult } = require('express-validator');
const { Types } = require('mongoose');

// models
const Inventory = require('../../../models/reward/inventory');
const Product = require('../../../models/reward/product');

const constants = require('../../../constants');

const middlewares = {};

middlewares.addPurchase = (req, res, next) => {
    const {
        productName,
        productType,
        issuer,
        productRedeemPoints,
        quantityPurchased,
        unitPrice,
        totalCosting,
        productImages,
        billImage,
        dateOfPurchase,
        productDescription,
        brandDescription,
    } = req.body;

    const newProduct = new Product({
        productName,
        productImages: productImages,
        productType: productType,
        productRedeemPoints: productRedeemPoints,
        quantity: quantityPurchased,
        brandDescription,
        productDescription,
        brandName: issuer,
    });

    newProduct
        .save()
        .then((product) => {
            const newPurchase = new Inventory({
                productId: product._id,
                quantityPurchased,
                unitPrice,
                totalCosting,
                issuer,
                dateOfPurchase,
                billImage,
            });
            newPurchase
                .save()
                .then(() => {
                    return res.status(201).send({ error: false, message: [constants.SUCCESS] });
                })
                .catch(() => {
                    return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                });
        })
        .catch(() => {
            return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
        });
};

middlewares.addPurchaseOfExistingProduct = (req, res, next) => {
    const { productId, quantity, unitPrice, totalCosting, billImage, issuer, dateOfPurchase } = req.body;

    Product.findByIdAndUpdate(productId, { $inc: { quantity: quantity } }, { new: true }, (err, doc) => {
        if (err) return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Product')] });

        const newPurchase = new Inventory({
            productId,
            quantityPurchased: quantity,
            unitPrice,
            totalCosting: totalCosting,
            billImage,
            issuer,
            dateOfPurchase,
        });

        newPurchase
            .save()
            .then(() => {
                return res.status(201).send({ error: false, message: [constants.SUCCESS] });
            })
            .catch(() => {
                return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
            });
    });
};

module.exports = middlewares;
