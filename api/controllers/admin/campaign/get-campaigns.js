const mongoose = require('mongoose');
const { validationResult } = require('express-validator');
const { fetchCampaign } = require('../../campaign/fetch-campaign');

const CashCampaign = require('../../../models/campaign/cash-campaign/cash-campaign');
const ProductCampaign = require('../../../models/campaign/product-campaign/product-campaign');
const Campaigns = require('../../../models/campaign/campaign');
const constants = require('../../../constants');
const CampaignService = require('../../../services/campaign');
const InfluencerService = require('../../../services/influencer');
const AdminService = require('../../../services/admin/campaign');

const middlewares = {};

/**
 * function to fetch campaigns for influencers with filtering, pagination and sorting features
 * @deprecated
 */
middlewares.getCampaigns = async (req, res, next) => {
    // getting all the parameters
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;
    let sortBy = {};
    if (req.query.sortBy) {
        sortBy[req.query.sortBy] = -1;
    } else {
        sortBy['startDate'] = -1;
    }

    let categories = req.query.categories ? req.query.categories : [];
    for (let id in categories) {
        categories[id] = mongoose.Types.ObjectId(categories[id]);
    }

    if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

    let cashQuery;
    let productQuery;

    let cashQueryPipeline = [
        {
            $lookup: {
                from: 'cash-campaign-applicants',
                localField: '_id',
                foreignField: 'campaign',
                as: 'applicants',
            },
        },
        { $addFields: { type: constants.CASH_CAMPAIGN, applicantCount: { $size: '$applicants' } } },
        { $project: { _id: 1, type: 1, applicantCount: 1, createdAt: 1, updatedAt: 1, startDate: 1, endDate: 1 } },
    ];

    let productQueryPipeline = [
        {
            $lookup: {
                from: 'product-campaign-applicants',
                localField: '_id',
                foreignField: 'campaign',
                as: 'applicants',
            },
        },
        { $addFields: { type: constants.PRODUCT_CAMPAIGN, applicantCount: { $size: '$applicants' } } },
        { $project: { _id: 1, type: 1, applicantCount: 1, createdAt: 1, updatedAt: 1, startDate: 1, endDate: 1 } },
    ];

    if (categories.length !== 0) {
        cashQuery = { 'criteria.categories': { $in: categories } };
        productQuery = { 'criteria.categories': { $in: categories } };

        cashQueryPipeline.unshift({ $match: cashQuery });
        productQueryPipeline.unshift({ $match: productQuery });
    }

    let query = [
        ...cashQueryPipeline,
        {
            $unionWith: {
                coll: 'product-campaigns',
                pipeline: [...productQueryPipeline],
            },
        },
    ];
    let CampaignModel = CashCampaign;

    if (req.query.campaignType == constants.CASH_CAMPAIGN) {
        query = [...cashQueryPipeline];
    } else if (req.query.campaignType == constants.PRODUCT_CAMPAIGN) {
        query = [...productQueryPipeline];
        CampaignModel = ProductCampaign;
    }

    // TODO: Optimize this nested query and find a efficient way to count pages
    await CampaignModel.aggregate(query).then(async (totalData) => {
        await CampaignModel.aggregate(query)
            .sort({ _id: -1 })
            .skip(size * (pageNo - 1))
            .limit(size)
            .exec(async (err, campaigns) => {
                if (err) return res.status(400).send({ error: true, message: [err.message] });
                else {
                    const resultCampaigns = [];
                    for (const campaign of campaigns) {
                        await fetchCampaign(campaign.type, campaign._id)
                            .then((result) => {
                                resultCampaigns.push(result);
                                return;
                            })
                            .catch((error) => {
                                console.log(error[0]);
                                return;
                            });
                    }
                    return res.status(200).send({ error: false, result: resultCampaigns, nPages: Math.ceil(totalData.length / size) });
                }
            });
    });
};

/**
 * Fetch campaigns listing for admin and company.
 */
middlewares.getAllCampaigns = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        // get search_criteria from query parameter
        // build a query object with it
        // send data to the frontend
        let status = req.query.status || null;
        let campaignType = req.query.type || null;
        let pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        let size = constants.PAGE_SIZE || 10;
        let sortBy = req.query.sortBy || 'createdAt';
        let sortType = req.query.sortType || -1;

        if (sortType === 'asc' || sortType === true || sortType === 1) sortType = 1;
        if (sortType === 'desc' || sortType === false || sortType === -1) sortType = -1;

        let queryObj = {
            // companyId: req.user,
        };

        if (status !== null) queryObj.status = status;
        else queryObj.status = { $ne: constants.ARCHIVE }; // not showing archive data if explicitly not defined

        if (campaignType !== null) queryObj.campaignType = campaignType;

        let filters = {
            status: [constants.DRAFT, constants.ACTIVE, constants.EXPIRED, constants.COMPLETED, constants.ARCHIVE],
            type: [constants.CASH, constants.PRODUCT],
        };
        let sort = ['title', 'createdAt'];

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        console.log('Query of fetching campaign for admin ', queryObj, pageNo, size, sortBy, sortType);
        if (errors.isEmpty()) {
            await Campaigns.find(queryObj)
                .skip(size * (pageNo - 1))
                .limit(size)
                .sort([[sortBy, sortType]])
                .lean()
                .then((campaigns) => {
                    if (campaigns != null) {
                        return res.status(200).json({
                            message: ' success',
                            data: campaigns,
                            filters: filters,
                            sort: sort,
                        });
                    }

                    return res.status(200).json({
                        message: 'No result found',
                        data: campaigns,
                        filters: filters,
                        sort: sort,
                    });
                })
                .catch((err) => {
                    console.log(err);
                    return res.status(400).send({ error: true, message: ['Error occurred while fetching the record'] });
                });
        }
    } catch (error) {
        console.log('Unexpected error occurred while fetching the collection of campaigns:: ', error.message);
        response = {
            status_code: 500,
            error: true,
            message: [constants.SERVER_ERR],
        };
        return res.status(response.status_code).json(response);
    }
};

middlewares.listCompletedCampaigns = async (req, res) => {
    // Fetch all campaigns with Closed status
    try {
        const { page, status } = req.query;

        await CampaignService.ListCampaignPayoutInfluencers(status, page, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            }
            return res.status(200).json(result);
        });
    } catch (e) {
        return res.status(500).json({
            error: true,
            message: `Error occured: ${e}`,
        });
    }
};

middlewares.listCompletedCampaignInfluencers = async (req, res) => {
    try {
        const { campaignId, page } = req.query;
        let sortBy = req.query.sortBy ? req.query.sortBy : 'createdAt';

        let size = constants.PAGE_SIZE;

        let query = {
            campaignId: campaignId,
            status: {
                $in: [constants.APPROVED, constants.COMPLETED],
            },
        };

        await CampaignService.GetCampaignApplicants(query, sortBy, page, size, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            }
            return res.status(200).json(result);
        });
    } catch (e) {
        return res.status(500).json({
            error: true,
            message: `Error occured: ${e}`,
        });
    }
};

/**
 * Admin marks the campaign as completed for influencer and based on campaign type
 * the required steps are taken.
 *
 * If the campaign is cash campaign, then the amount is added to influencer's wallet.
 * If the campaign is product campaign, then simply the campaign is marked as completed.
 */
middlewares.markCampaignCompleteInfluencer = async (req, res) => {
    // Fetch all campaigns with Closed status
    try {
        const { influencerId, applicantId, campaignType, brandId } = req.body;

        if (campaignType === null || !campaignType) {
            return res.status(400).json({
                error: true,
                message: 'Arguments not passed',
            });
        }

        if (campaignType === constants.CASH) {
            await InfluencerService.AddMoneyToWallet(influencerId, applicantId, brandId, (err, result) => {
                if (err) {
                    return res.status(400).json(err);
                }
                return res.status(200).json(result);
            }).catch((e) => {
                return res.status(500).json({
                    error: true,
                    message: e,
                });
            });
        } else if (campaignType === constants.PRODUCT) {
            await CampaignService.ApplicantService.UpdateApplicantStatus(applicantId, (err, result) => {
                if (err) {
                    return res.status(400).json(err);
                }
                return res.status(200).json(result);
            }).catch((e) => {
                return res.status(500).json({
                    error: true,
                    message: e,
                });
            });
        } else {
            return res.status(400).json({
                error: true,
                message: 'Campaign type value not correct',
            });
        }
    } catch (e) {
        return res.status(500).json({
            error: true,
            message: `Error occured: ${e}`,
        });
    }
};

middlewares.updateCampaignStatus = async (req, res) => {
    try {
        const { campaignId } = req.params;
        const { status } = req.body;

        await AdminService.UpdateCampaignStatus(campaignId, status, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    } catch (e) {
        throw e;
    }
};

module.exports = middlewares;
