const { validationResult } = require('express-validator');

const Applicant = require('../../../../models/campaign/product-campaign/product-campaign-applicant');
const Campaign = require('../../../../models/campaign/product-campaign/product-campaign');
const Brand = require('../../../../models/company/brand');
const { sendNotification } = require('../../../notification/notification');

const constants = require('../../../../constants');

const middlewares = {};

middlewares.updateLinkStatus = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        console.log(req.body.applicant);
        Applicant.findOne({ _id: req.body.applicant, status: constants.SUBMITTED }).then(async (applicant) => {
            console.log(applicant);
            if (applicant) {
                if (req.body.status == constants.COMPLETED) {
                    applicant.status = constants.COMPLETED;
                    applicant
                        .save()
                        .then(async () => {
                            Campaign.findById(applicant.campaign)
                                .select({ brand: 1 })
                                .lean()
                                .then(async (campaign) => {
                                    if (campaign) {
                                        Brand.findById(campaign.brand)
                                            .select({ name: 1 })
                                            .lean()
                                            .then(async (brand) => {
                                                if (brand) {
                                                    await sendNotification(
                                                        brand.name,
                                                        'Your submission has been approved.',
                                                        null,
                                                        applicant.influencer,
                                                        constants.PRODUCT_CAMPAIGN,
                                                        'submission-response',
                                                        {
                                                            campaign: applicant.campaign,
                                                            applicant: applicant._id,
                                                        }
                                                    )
                                                        .then(() => {
                                                            res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                                        })
                                                        .catch((error) => {
                                                            res.status(500).json({ error: true, message: [error] });
                                                        });
                                                } else {
                                                    res.status(400).json({
                                                        error: true,
                                                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                                                    });
                                                }
                                            })
                                            .catch((error) => {
                                                res.status(500).json({ error: true, message: [error.message] });
                                            });
                                    } else {
                                        res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                                    }
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error.message] });
                                });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: ['Invalid status.'] });
                }
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
            }
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
