const { validationResult } = require('express-validator');

const Applicant = require('../../../../models/campaign/product-campaign/product-campaign-applicant');
const Campaign = require('../../../../models/campaign/product-campaign/product-campaign');
const Brand = require('../../../../models/company/brand');

const { sendNotification } = require('../../../notification/notification');
const { fetchInfluencer } = require('../../../influencer/influencer');

const constants = require('../../../../constants');
const Influencer = require('../../../../models/influencer/influencer');
const sendEmail = require('../../../util/email');
const templates = require('../../../../templates');

const middlewares = {};

middlewares.getApplicants = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Applicant.find({ campaign: req.query.campaign })
            .lean()
            .then(async (applicants) => {
                const hydratedApplicants = [];
                for (let applicant of applicants) {
                    await fetchInfluencer(applicant.influencer)
                        .then((influencer) => {
                            hydratedApplicants.push({ applicant: applicant, influencer: influencer });
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                }
                res.status(200).json({ error: false, result: hydratedApplicants });
            })
            .catch((error) => {
                res.status(400).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.updateApplicantStatus = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Campaign.findById(req.body.campaign)
            .lean()
            .then((campaign) => {
                if (campaign != null) {
                    Brand.findById(campaign.brand)
                        .lean()
                        .then((brand) => {
                            if (brand != null) {
                                Applicant.findOne({ _id: req.body.applicant, status: constants.APPLIED })
                                    .then((applicant) => {
                                        if (applicant != null) {
                                            let body;
                                            if (req.body.status == constants.APPROVE) {
                                                applicant.status = constants.APPROVED;
                                                body = `${brand.name} has approved your bid.`;
                                            } else if (req.body.status == constants.REJECT) {
                                                applicant.status = constants.REJECTED;
                                                body = `${brand.name} has rejected your bid.`;
                                            } else {
                                                res.status(400).json({ error: true, message: ['Invalid status type.'] });
                                                return;
                                            }
                                            applicant
                                                .save()
                                                .then((record) => {
                                                    sendNotification(
                                                        brand.name,
                                                        body,
                                                        null,
                                                        applicant.influencer,
                                                        constants.PRODUCT_CAMPAIGN,
                                                        'applicant-response',
                                                        {
                                                            campaign: campaign._id,
                                                            applicant: applicant._id,
                                                        }
                                                    )
                                                        .then(async () => {
                                                            let { email, name } = await Influencer.findById(applicant.influencer, {
                                                                email: 1,
                                                                name: 1,
                                                                _id: 0,
                                                            });

                                                            if (email) {
                                                                if (applicant.status === constants.APPROVED) {
                                                                    try {
                                                                        await sendEmail(email, {
                                                                            subject: 'Collaboration update from Famstar!',
                                                                            body: templates.INFLUENCER_CAMPAIGN_ACCEPTANCE_MAIL(
                                                                                name,
                                                                                brand.name
                                                                            ),
                                                                        });
                                                                    } catch (err) {
                                                                        console.log(err);
                                                                    }
                                                                } else {
                                                                    try {
                                                                        await sendEmail(email, {
                                                                            subject: 'Collaboration update from Famstar!',
                                                                            body: templates.INFLUENCER_CAMPAIGN_REJECTION_MAIL(
                                                                                name,
                                                                                brand.name
                                                                            ),
                                                                        });
                                                                    } catch (err) {
                                                                        console.log(err);
                                                                    }
                                                                }
                                                            }

                                                            res.status(200).json({ error: false, applicant: record });
                                                        })
                                                        .catch((error) => {
                                                            res.status(500).json({ error: true, message: [error] });
                                                        });
                                                })
                                                .catch((error) => {
                                                    res.status(500).json({ error: false, message: [error.message] });
                                                });
                                        } else {
                                            res.status(400).json({
                                                error: true,
                                                message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')],
                                            });
                                        }
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: false, message: [error.message] });
                                    });
                            } else {
                                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: false, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: false, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
