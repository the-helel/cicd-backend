const { validationResult } = require('express-validator');

const Applicant = require('../../../../models/campaign/cash-campaign/cash-campaign-applicant');
const Campaign = require('../../../../models/campaign/cash-campaign/cash-campaign');
const Brand = require('../../../../models/company/brand');

const { sendNotification } = require('../../../notification/notification');
const { fetchInfluencer } = require('../../../influencer/influencer');

const constants = require('../../../../constants');

const middlewares = {};

middlewares.getApplicants = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Applicant.find({ campaign: req.query.campaign })
            .lean()
            .then(async (applicants) => {
                console.log(applicants);
                const hydratedApplicants = [];
                for (let applicant of applicants) {
                    await fetchInfluencer(applicant.influencer)
                        .then((influencer) => {
                            hydratedApplicants.push({ applicant: applicant, influencer: influencer });
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                }
                res.status(200).json({ error: false, result: hydratedApplicants });
            })
            .catch((error) => {
                res.status(400).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.adminCrossBid = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Campaign.findById(req.body.campaign)
            .lean()
            .then((campaign) => {
                if (campaign != null) {
                    Brand.findById(campaign.brand)
                        .lean()
                        .then((brand) => {
                            if (brand != null) {
                                Applicant.findOne({ _id: req.body.applicant, status: constants.APPLIED })
                                    .then((applicant) => {
                                        if (applicant != null) {
                                            let body;
                                            const length = applicant.bids.length;
                                            if (applicant.bids[length - 1].company == null) {
                                                if (req.body.status == constants.APPROVE) {
                                                    applicant.bids[length - 1].influencer.status = constants.APPROVED;
                                                    applicant.status = constants.APPROVED;
                                                    body = `${brand.name} has approved your bid.`;
                                                } else if (req.body.status == constants.REJECT) {
                                                    applicant.bids[length - 1].influencer.status = constants.REJECTED;
                                                    applicant.status = constants.REJECTED;
                                                    body = `${brand.name} has rejected your bid.`;
                                                } else if (req.body.status == 'Cross Bid') {
                                                    applicant.bids[length - 1].influencer.status = constants.REJECTED;

                                                    const companyBid = {};
                                                    companyBid.biddingAmount = parseInt(req.body.biddingAmount);
                                                    companyBid.status = null;
                                                    companyBid.timestamp = new Date();

                                                    applicant.bids[length - 1].company = companyBid;
                                                    body = `${brand.name} has a new cross-bid for you.`;
                                                } else {
                                                    res.status(400).json({ error: true, message: ['Invalid status type.'] });
                                                    return;
                                                }

                                                applicant.markModified('bids');
                                                applicant
                                                    .save()
                                                    .then((record) => {
                                                        sendNotification(
                                                            brand.name,
                                                            body,
                                                            null,
                                                            applicant.influencer,
                                                            constants.CASH_CAMPAIGN,
                                                            'applicant-response',
                                                            {
                                                                campaign: campaign._id,
                                                                applicant: applicant._id,
                                                            }
                                                        )
                                                            .then(() => {
                                                                res.status(200).json({ error: false, applicant: record });
                                                            })
                                                            .catch((error) => {
                                                                res.status(500).json({ error: true, message: [error] });
                                                            });
                                                    })
                                                    .catch((error) => {
                                                        res.status(500).json({ error: false, message: [error.message] });
                                                    });
                                            } else {
                                                res.status(400).json({ error: true, message: ['Cannot bid at this moment.'] });
                                            }
                                        } else {
                                            res.status(400).json({
                                                error: true,
                                                message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')],
                                            });
                                        }
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: false, message: [error.message] });
                                    });
                            } else {
                                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: false, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: false, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
