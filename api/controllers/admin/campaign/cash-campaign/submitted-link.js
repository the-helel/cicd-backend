const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const Applicant = require('../../../../models/campaign/cash-campaign/cash-campaign-applicant');
const Campaign = require('../../../../models/campaign/cash-campaign/cash-campaign');
const Brand = require('../../../../models/company/brand');

const { addMoneyToWallet } = require('../../../influencer/wallet/wallet');
const { sendNotification } = require('../../../notification/notification');

const constants = require('../../../../constants');

const middlewares = {};

middlewares.updateLinkStatus = async (req, res, next) => {
    const errors = validationResult(req);
    const session = await mongoose.startSession();
    if (errors.isEmpty()) {
        Applicant.findOne({ _id: req.body.applicant, status: constants.SUBMITTED }).then(async (applicant) => {
            if (applicant) {
                if (req.body.status == constants.COMPLETED) {
                    await session.startTransaction();
                    applicant.status = constants.COMPLETED;
                    applicant
                        .save({ session })
                        .then(async () => {
                            const biddingLength = applicant.bids.length;
                            const latestBid = applicant.bids[biddingLength - 1];
                            const keys = Object.keys(latestBid);
                            let amount = null;
                            if (latestBid.company != null) amount = latestBid.company.biddingAmount;
                            else amount = latestBid.influencer.biddingAmount;
                            await addMoneyToWallet(applicant.influencer, amount, constants.CASH_CAMPAIGN, applicant.campaign, session)
                                .then(() => {
                                    Campaign.findById(applicant.campaign)
                                        .select({ brand: 1 })
                                        .lean()
                                        .then(async (campaign) => {
                                            if (campaign) {
                                                Brand.findById(campaign.brand)
                                                    .select({ name: 1 })
                                                    .lean()
                                                    .then(async (brand) => {
                                                        if (brand) {
                                                            await sendNotification(
                                                                brand.name,
                                                                'Your submission has been approved.',
                                                                null,
                                                                applicant.influencer,
                                                                constants.CASH_CAMPAIGN,
                                                                'submission-response',
                                                                {
                                                                    campaign: applicant.campaign,
                                                                    applicant: applicant._id,
                                                                }
                                                            )
                                                                .then(async () => {
                                                                    res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                                                    await session.commitTransaction();
                                                                    session.endSession();
                                                                })
                                                                .catch(async (error) => {
                                                                    res.status(500).json({ error: true, message: [error] });
                                                                    await session.abortTransaction();
                                                                    session.endSession();
                                                                });
                                                        } else {
                                                            res.status(400).json({
                                                                error: true,
                                                                message: [constants.RESOURCE_NOT_FOUND_ERROR('Brand')],
                                                            });
                                                            await session.abortTransaction();
                                                            session.endSession();
                                                        }
                                                    })
                                                    .catch(async (error) => {
                                                        res.status(500).json({ error: true, message: [error.message] });
                                                        await session.abortTransaction();
                                                        session.endSession();
                                                    });
                                            } else {
                                                res.status(400).json({
                                                    error: true,
                                                    message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')],
                                                });
                                                await session.abortTransaction();
                                                session.endSession();
                                            }
                                        })
                                        .catch(async (error) => {
                                            res.status(500).json({ error: true, message: [error.message] });
                                            await session.abortTransaction();
                                            session.endSession();
                                        });
                                })
                                .catch(async (error) => {
                                    res.status(500).json({ error: true, message: [error] });
                                    await session.abortTransaction();
                                    session.endSession();
                                });
                        })
                        .catch(async (error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                            await session.abortTransaction();
                            session.endSession();
                        });
                } else {
                    res.status(400).json({ error: true, message: ['Invalid status.'] });
                    session.endSession();
                }
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                session.endSession();
            }
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
        session.endSession();
    }
};

module.exports = middlewares;
