const { validationResult } = require('express-validator');

const Company = require('../../../../models/company/company');
const Brand = require('../../../../models/company/brand');
const CashCampaign = require('../../../../models/campaign/cash-campaign/cash-campaign');

const assignCampaign = require('../../../campaign/cash-campaign/assign-campaign');

const mongoose = require('mongoose');

const constants = require('../../../../constants');

const middlewares = {};

// function to create a new campaign if not exist
// or to update the existing one
middlewares.upsertCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // getting user from jwt token
        Company.findById(req.body.companyId)
            .lean()
            .then((user) => {
                if (!user) return res.status(400).send({ error: true, message: ['Not authorized to do this operation'] });
                // checking if brand id is provided or not
                if (req.body.brandId) {
                    Brand.findOne({ _id: req.body.brandId, company: req.body.companyId })
                        .then((brand) => {
                            if (brand === null)
                                return res.status(400).send({ error: true, message: ['You are not authorized to do this operation'] });

                            if (req.body.campaignId) {
                                CashCampaign.findById(req.body.campaignId)
                                    .then((campaign) => {
                                        if (campaign === null)
                                            return res
                                                .status(404)
                                                .json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });

                                        assignCampaign(req.body, campaign);

                                        campaign
                                            .save()
                                            .then((campaign) => {
                                                return res.status(200).send({ error: false, result: { campaign: campaign } });
                                            })
                                            .catch((error) => {
                                                res.status(500).json({ error: true, message: [error.message] });
                                            });
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: true, message: [error.message] });
                                    });
                            } else {
                                const campaign = new CashCampaign();
                                campaign.brand = brand._id;
                                campaign.company = req.body.companyId;
                                campaign.status = req.body.status;

                                assignCampaign(req.body, campaign);

                                campaign
                                    .save()
                                    .then((campaign) => {
                                        return res.status(200).send({ error: false, result: { campaign: campaign } });
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: true, message: [error.message] });
                                    });
                            }
                        })
                        .catch((err) => {
                            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
                        });
                } else {
                    return res.status(500).send({ error: true, message: ['Brand Id is not provided'] });
                }
            });
    }
};

// Get cash campaign by id, also find the parent company of the brand
middlewares.getCampaignById = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        const id = req.query.id;

        CashCampaign.aggregate([
            {
                $match: { _id: mongoose.Types.ObjectId(id) },
            },
            {
                $lookup: {
                    from: 'brands',
                    localField: 'brand',
                    foreignField: '_id',
                    as: 'brandData',
                },
            },
            {
                $unwind: '$brandData',
            },
            {
                $project: {
                    'brandData.company': 1,
                    brief: 1,
                    criteria: 1,
                    inHouse: 1,
                    inviteOnly: 1,
                    influencers: 1,
                    status: 1,
                    influencerRange: 1,
                    startDate: 1,
                    endDate: 1,
                    brand: 1,
                    createdAt: 1,
                    updatedAt: 1,
                    budget: 1,
                    _id: 1,
                },
            },
        ])
            .then((campaign) => {
                return res.status(200).json({ error: false, result: campaign[0] });
            })
            .catch((err) => {
                return res.status(500).json({ error: true, message: [err.message] });
            });
    }
};

module.exports = middlewares;
