const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const config = require('../../config');

// const Admin = require('../../models/admin');
const Company = require('../../models/company/company');

const constants = require('../../constants');

const middlewares = {};

// function to create a new user
middlewares.createUser = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        Company.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['something went wrong'] });
            if (user) {
                return res.status(401).send({ error: true, message: ['User with this email already exists'] });
            }

            // hasing the password for security purposes
            var hashedPassword = bcrypt.hashSync(req.body.password);

            // creating instance of user collection
            console.log('admin details ', req.body.email, req.body.password);
            Company.create(
                {
                    email: req.body.email,
                    password: hashedPassword,
                    name: req.body.name,
                    role: constants.ADMIN,
                    mobile: req.body.mobile,
                    accountHolderName: req.body.accountHolderName,
                },
                (err, user) => {
                    // check if there is any error
                    if (err) {
                        console.log('Error while creating admin user', err);
                        return res.status(500).send({ error: true, message: ['There was a problem registering the user.'] });
                    }

                    user.save((err, admin) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({ error: false, message: `Successfully registered.`, data: admin });
                    });
                }
            );
        });
    }
};

// function to login a user
middlewares.login = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // searching for a user instance and error handling
        Company.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['Error on the server'] });
            if (!user) return res.status(404).send({ error: true, message: ['No User Found.'] });

            // comparing the password by hashing and comparing
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ error: true, message: ['Invalid credentials'] });

            // generating token for the user
            var token = jwt.sign(
                {
                    userId: user._id,
                    userType: constants.ADMIN,
                    role: user.role,
                },
                config.JWT_SECRET
            );

            // sending the response
            res.status(200).send({ error: false, result: { auth: true, token: token } });
        });
    }
};

module.exports = middlewares;
