/**
 * Basic analytics for admin panel.
 */

const mongoose = require('mongoose');

const Influencer = require('../../../models/influencer/influencer');
const EmailAuths = require('../../../models/influencer/email-auth');
const Referrals = require('../../../models/activity/referral.js');

const constants = require('../../../constants');

const middlewares = {};

/**
 * Base analytics
 *  1. Total users
 *  2. Total Email signups
 *  3. Total Signups with Facebook
 *  4. Daily signup of past 7 days
 *
 */

middlewares.baseAnalytics = async (req, res, next) => {
    try {
        let analyticsData = {};

        const now = new Date();
        const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

        analyticsData.totalUsers = await Influencer.estimatedDocumentCount();
        analyticsData.emailSignups = await EmailAuths.countDocuments({ isVerified: true, userId: { $ne: null } });
        analyticsData.facebookSignups = parseInt(analyticsData.totalUsers) - parseInt(analyticsData.emailSignups);
        analyticsData.dailysignUps = await Influencer.countDocuments({ createdAt: { $gte: today } });

        // TODO: Point 4

        return res.status(200).json({ error: false, result: analyticsData });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
    }
};

middlewares.usersInfo = async (req, res, next) => {
    let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = parseInt((page - 1) * size);

    let filter = {};
    let count;

    let stages = [
        {
            $sort: {
                _id: -1,
            },
        },
        {
            $skip: skip,
        },
        {
            $limit: limit,
        },
        {
            $lookup: {
                from: 'facebooks',
                localField: '_id',
                foreignField: 'influencer',
                as: 'facebook',
            },
        },
        {
            $lookup: {
                from: 'instagrams',
                localField: '_id',
                foreignField: 'influencer',
                as: 'instagram',
            },
        },
        {
            $lookup: {
                from: 'email-auths',
                foreignField: 'userId',
                localField: '_id',
                as: 'emailAuth',
            },
        },
    ];

    if (req.query.search) {
        stages.unshift({
            $match: {
                $text: {
                    $search: req.query.search,
                },
            },
        });
    }

    try {
        let data = await Influencer.aggregate(stages);

        if (req.query.search) {
            count = await Influencer.countDocuments(filter);
        } else {
            count = await Influencer.estimatedDocumentCount();
        }

        return res.status(200).json({ error: true, result: data, nPages: Math.ceil(count / limit) });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
    }
};

middlewares.referralsInfo = async (req, res, next) => {
    let { page, size } = req.query;
    if (!page) page = 1;
    if (!size) size = 10;

    // Search referred user by specific email id
    if (req.query.email) {
        const email = req.query.email;
        const filter = {
            email: email,
        };
    }

    // Aggregate pipeline populate referee and influencer
    Referrals.aggregate([
        {
            $lookup: {
                from: 'influencers',
                let: { influencerId: '$influencer' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ['$_id', '$$influencerId'],
                            },
                        },
                    },
                    {
                        $project: {
                            _id: 1,
                            name: 1,
                            email: 1,
                            phone: 1,
                            profilePicture: 1,
                        },
                    },
                ],
                as: 'influencer',
            },
        },
        {
            $lookup: {
                from: 'influencers',
                let: { influencerId: '$referee' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ['$_id', '$$influencerId'],
                            },
                        },
                    },
                    {
                        $project: {
                            _id: 1,
                            name: 1,
                            email: 1,
                            phoneNumber: 1,
                            profilePicture: 1,
                        },
                    },
                ],
                as: 'referee',
            },
        },
        {
            $sort: {
                _id: -1,
            },
        },
        {
            $skip: parseInt((page - 1) * size),
        },
        {
            $limit: parseInt(size),
        },
    ]).exec((err, data) => {
        if (err) {
            return res.status(500).json({ error: true, message: [err.message] });
        }
        return res.status(200).json({ error: false, result: data });
    });
};

module.exports = middlewares;
