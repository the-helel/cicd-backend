const { validationResult } = require('express-validator');
const AdminDiscoverService = require('../../../services/admin/discover');
const middlewares = {};

middlewares.searchInfluencer = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const { platform, username } = req.body;

        if (!platform || !username) {
            return res.status(400).json({
                message: 'All arguments not passed',
            });
        }

        await AdminDiscoverService.SearchInfluencer(platform, username, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    } else {
        return res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

module.exports = middlewares;
