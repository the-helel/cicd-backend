const { validationResult } = require('express-validator');

const Video = require('../../models/learn/video');

const constants = require('../../constants');

const middlewares = {};

middlewares.getVideos = async (req, res, next) => {
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;
    let sortBy = {};
    if (req.query.sortBy) {
        sortBy[req.query.sortBy] = -1;
    } else {
        sortBy['createdAt'] = -1;
    }

    Video.find()
        .skip(size * (pageNo - 1))
        .limit(size)
        .sort(sortBy)
        .lean()
        .then((videos) => {
            res.status(200).json({ error: false, result: videos });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.addVideo = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const video = new Video();
        video.videoCode = req.body.videoCode;
        video.title = req.body.title;
        video.description = req.body.description;
        video.category = req.body.category;
        video.keyMoments = req.body.keyMoments;

        video
            .save()
            .then(() => {
                res.status(200).json({ error: false, result: [constants.SUCCESS] });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.editVideo = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Video.findById(req.body.videoId).then((video) => {
            if (video != null) {
                video.videoCode = req.body.videoCode;
                video.title = req.body.title;
                video.description = req.body.description;
                video.category = req.body.category;
                video.keyMoments = req.body.keyMoments;

                video
                    .save()
                    .then(() => {
                        res.status(200).json({ error: false, result: [constants.SUCCESS] });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Video')] });
            }
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
