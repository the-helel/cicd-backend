const { validationResult } = require('express-validator');
var bcrypt = require('bcryptjs');
var crypto = require('crypto');

var Company = require('../../../models/company/company');
var Brand = require('../../../models/company/brand');

const constants = require('../../../constants');

const middlewares = {};

// function to create a new user
middlewares.createCompany = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        Company.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['something went wrong'] });
            if (user) {
                return res.status(401).send({ error: true, message: ['User with this email already exists'] });
            }

            // hasing the password for security purposes
            var hashedPassword = bcrypt.hashSync(req.body.password);

            // creating instance of user collection
            Company.create(
                {
                    email: req.body.email,
                    password: hashedPassword,
                    name: req.body.name,
                    mobile: req.body.mobile,
                    accountHolderName: req.body.accountHolderName,
                },
                (err, user) => {
                    // check if there is any error
                    if (err) return res.status(500).send({ error: true, message: ['There was a problem registering the user.'] });

                    // Generate and set password reset token
                    user.verificationToken = crypto.randomBytes(20).toString('hex');
                    user.verificationStatus = true;
                    user.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });
                        return res.status(200).send({ error: false, result: [constants.SUCCESS] });
                    });
                }
            );
        });
    }
};

middlewares.getAllCompanies = (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        const page = parseInt(req.query.page) || 1;
        const size = parseInt(req.query.size) || 10;

        Company.find(
            {},
            { brands: 1, email: 1, name: 1, accountHolderName: 1, createdAt: 1, mobile: 1, verificationStatus: 1 },
            (err, docs) => {
                if (err) return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                else return res.status(200).send({ error: false, result: docs, nPages: Math.ceil(docs.length / size) });
            }
        )
            .skip(size * (page - 1))
            .limit(size)
            .sort({ createdAt: -1 });
    }
};

middlewares.getCompanyBrands = (req, res, next) => {
    const errors = validationResult(req);

    const id = req.params['id'];
    const page = parseInt(req.query.page) || 1;
    const size = parseInt(req.query.size) || 10;

    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        Brand.find({ company: id }, { name: 1, logo: 1, pageUrl: 1, about: 1, createdAt: 1 }, (err, docs) => {
            if (err) return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
            else return res.status(200).send({ error: false, result: docs, nPages: Math.ceil(docs.length / size) });
        })
            .skip(size * (page - 1))
            .limit(size)
            .sort({ createdAt: -1 });
    }
};

// update company
middlewares.updateCompany = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        Company.findById(req.query.company, { email: 1, name: 1, mobile: 1, accountHolderName: 1 }).then((company) => {
            if (company === null) return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Company')] });

            // checking which feilds have to be updated
            if (req.body.email) {
                company.email = req.body.email;
            }
            if (req.body.name) {
                company.name = req.body.name;
            }
            if (req.body.mobile) {
                company.mobile = req.body.mobile;
            }
            if (req.body.accountHolderName) {
                company.accountHolderName = req.body.accountHolderName;
            }

            company
                .save()
                .then((company) => {
                    return res.status(200).send({ error: true, result: { company: company } });
                })
                .catch((err) => {
                    return res.status(500).send({ error: true, message: [err.message] });
                });
        });
    }
};

// get info about a company
middlewares.getCompany = (req, res, next) => {
    const errors = validationResult(req);

    const id = req.params['id'];

    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        Company.findById(id, { email: 1, name: 1, mobile: 1, accountHolderName: 1 })
            .lean()
            .then((company) => {
                if (company === null)
                    return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Company')] });

                return res.status(200).send({ error: false, result: company });
            })
            .catch((err) => {
                return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
            });
    }
};
module.exports = middlewares;
