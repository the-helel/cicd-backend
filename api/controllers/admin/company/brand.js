const { validationResult } = require('express-validator');

const Company = require('../../../models/company/company');
const Brand = require('../../../models/company/brand');

const constants = require('../../../constants');

const brandMiddlewares = {};

// function to create a new brand
brandMiddlewares.createBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // searching for a company using the JWT
        Company.findById(req.body.company, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // creating new brand
            Brand.create(
                {
                    company: req.body.company,
                    name: req.body.name,
                    pageUrl: req.body.pageUrl,
                    coverUrl: req.body.coverUrl,
                    logo: {
                        url: req.body.logo,
                    },
                    about: req.body.about,
                },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    brand.save();
                    user.brands.push(brand._id);
                    user.save();
                    return res.status(200).send({ error: false, result: { brand: brand } });
                }
            );
        });
    }
};

// function to create a new brand
brandMiddlewares.editBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // searching for a company using the company feild in body
        Company.findById(req.body.company, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            Brand.findOne(
                { _id: req.body.id, company: req.body.company },
                { name: 1, pageUrl: 1, logo: 1, about: 1, coverUrl: 1, rating: 1 }
            )
                .then((brand) => {
                    if (brand === null) return res.status(400).send({ error: true, message: ['Not authorized to access this brand'] });

                    // checking which values are there
                    if (req.body.name) {
                        brand.name = req.body.name;
                    } // checking which values are there
                    console.log(brand);
                    if (req.body.pageUrl) {
                        brand.pageUrl = req.body.pageUrl;
                    } // checking the name parameter
                    if (req.body.logo) {
                        brand.logo.url = req.body.logo;
                    } // checking the logo parameter
                    if (req.body.about) {
                        brand.about = req.body.about;
                    } // checking the about parameter
                    if (req.body.coverUrl) {
                        brand.coverUrl = req.body.coverUrl;
                    } // checking the coverUrl parameter

                    // updating the brand
                    brand
                        .save()
                        .then((brand) => {
                            return res.status(200).send({ error: true, result: { brand: brand } });
                        })
                        .catch((err) => {
                            return res.status(500).send({ error: true, message: [err.message] });
                        });
                })
                .catch((err) => {
                    return res.status(500).send({ error: true, message: [err.message] });
                });
        });
    }
};

// function to fetch all the brands of a company
brandMiddlewares.viewBrands = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // getting the company to get all its brands
        Company.findById(req.body.company, async (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // getting details of all brands
            output = [];
            for (const brand of user.brands) {
                await Brand.findById(
                    brand,
                    { id: 1, name: 1, pageUrl: 1, logo: 1, about: 1, company: 1, coverUrl: 1, rating: 1 },
                    (err, item) => {
                        if (err) console.log(err);
                        output.push(item);
                    }
                );
            }
            return res.status(200).send({ error: false, result: { brands: output } });
        });
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.viewBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        // checking if the user is the owner of the request brand
        Company.findById(req.query.company, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

            Brand.findOne(
                { _id: req.params.id, company: req.query.company },
                { _id: 1, logo: 1, name: 1, pageUrl: 1, about: 1, company: 1, coverUrl: 1, rating: 1 }
            )
                .lean()
                .then((brand) => {
                    if (brand === null) return res.status(400).send({ error: true, message: ['Not authorized to access this brand'] });
                    return res.status(200).send({ error: false, result: { brand: brand } });
                })
                .catch((err) => {
                    return res.status(500).send({ error: true, message: [err.message] });
                });
        });
    }
};

module.exports = brandMiddlewares;
