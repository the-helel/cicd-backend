const { validationResult } = require('express-validator');

const VerifyBankAccount = require('../../../models/wallet/verify-bank-account');
const Wallet = require('../../../models/wallet/wallet');

const constants = require('../../../constants');

const middlewares = {};

/**
 * This should only be used by admin. It will list all the bank accounts which are verified
 * successfully.
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
middlewares.getVerifyBankAccountRequests = (req, res, next) => {
    VerifyBankAccount.find({ status: constants.VERIFYING })
        .lean()
        .then((verifyBankAccounts) => {
            res.status(200).json({ error: false, kycs: verifyBankAccounts });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

/**
 * When user adds a bank account it goes under REVIEW to admin to check whether
 * all the details are correct or not. This method will change the bank account
 * status. It will accept the [status] parameter which can be any one of the
 * following:-
 * 1. Verified
 * 2. Rejected
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
middlewares.verifyBankAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        VerifyBankAccount.findOne({ _id: req.body.accountId, status: constants.PENDING })
            .then((account) => {
                if (account != null) {
                    Wallet.findOne({ influencer: account.influencer })
                        .then(async (wallet) => {
                            if (wallet != null) {
                                if (req.body.status == constants.VERIFIED) {
                                    account.status = constants.VERIFIED;
                                    wallet.bankAccounts[account.fundsAccountId].status = constants.VERIFIED;
                                } else if (req.body.status == constants.REJECTED) {
                                    account.status = constants.REJECTED;
                                    account.feedback = req.body.feedback;
                                    delete wallet.bankAccounts[account.fundsAccountId];
                                } else {
                                    res.status(400).json({ error: true, message: ['Invalid status field.'] });
                                    return;
                                }
                                wallet.markModified('bankAccounts');
                                await account.save().then(async () => {
                                    await wallet.save().then(() => {
                                        res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                        return;
                                    });
                                });
                            } else {
                                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Wallet')] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Verify Bank Account')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
