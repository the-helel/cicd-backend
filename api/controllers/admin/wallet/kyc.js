const { validationResult } = require('express-validator');

const KYC = require('../../../models/wallet/kyc');
const Referral = require('../../../models/activity/referral');
const RewardWallet = require('../../../models/reward/rewards-wallet');

const constants = require('../../../constants');
const NotificationSDK = require('../../util/notification');
const mongoose = require('mongoose');
const Constant = require('../../../models/constant');
const createTransaction = require('../../influencer/reward/create-transaction');
const Influencer = require('../../../models/influencer/influencer');
const PointTransaction = require('../../../models/reward/points-transaction');

const middlewares = {};

/**
 * List all the kyc requestes which are not yet verified.
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
middlewares.getKYCRequests = async (req, res, next) => {
    let { page, size, status } = req.query;

    if (!status) {
        status = constants.VERIFYING;
    }

    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = parseInt((page - 1) * size);

    try {
        let count = await KYC.countDocuments({ status: status });
        let data = await KYC.aggregate([
            {
                $match: {
                    status: status,
                },
            },
            {
                $sort: {
                    _id: -1,
                },
            },

            {
                $skip: skip,
            },
            {
                $limit: limit,
            },
            {
                $lookup: {
                    from: 'influencers',
                    localField: 'influencer',
                    foreignField: '_id',
                    as: 'influencer',
                },
            },
        ]);

        return res.status(200).json({ error: true, result: data, total: count });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
    }
};

/**
 * The admin can change the status of KYC application submitted by users.
 * The user is notified by notification that their status has been changed.
 */
middlewares.verifyKYC = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        KYC.findOne({ _id: req.body.kycId, status: constants.VERIFYING })
            .then(async (kyc) => {
                if (kyc != null) {
                    let feedback = 'Your KYC Request have been verified. Now you can connect UPI and Bank account to your famstar account.';
                    if (req.body.status == constants.VERIFIED) {
                        kyc.status = constants.VERIFIED;
                        // Add reward money to the influencer if invited by some other user
                        await Referral.findOne({
                            influencer: kyc.influencer,
                            isVerified: false,
                        }).then(async (ref) => {
                            if (ref !== null) {
                                if (ref.isVerified === false) {
                                    ref.isVerified = true;
                                    await ref.save();

                                    await RewardWallet.findOne({ influencer: ref.referee })
                                        .then(async (refereeWallet) => {
                                            if (refereeWallet != null) {
                                                await Constant.findOne({ name: 'referralAmountForReferrer' })
                                                    .then(async (constant) => {
                                                        refereeWallet.points += parseInt(constant.value);
                                                        await refereeWallet
                                                            .save()
                                                            .then(async (wallet) => {
                                                                let influencerReferred = { name: null, _id: null };

                                                                influencerReferred = await Influencer.findById(ref.influencer, {
                                                                    name: 1,
                                                                    _id: 0,
                                                                }).lean();

                                                                if (influencerReferred !== null) {
                                                                    console.log('Create transaction');
                                                                }

                                                                await PointTransaction.create({
                                                                    transactionType: constants.CREDIT,
                                                                    userId: ref.referee,
                                                                    points: constant.value,
                                                                    metadata: {
                                                                        source: constants.REFERRAL,
                                                                        sourceId: kyc.influencer,
                                                                        name: influencerReferred.name,
                                                                    },
                                                                }).then(async (result) => {
                                                                    console.log('Referral has been given points');
                                                                });
                                                            })
                                                            .catch(async (error) => {
                                                                console.log('Error occured 1', error);
                                                            });
                                                    })
                                                    .catch(async (error) => {
                                                        console.log('Error occured 2');
                                                    });
                                            }
                                        })
                                        .catch(async (error) => {
                                            console.log('Error occured 3');
                                        });
                                }
                            }
                        });
                    } else if (req.body.status == constants.REJECTED) {
                        kyc.status = constants.REJECTED;
                        kyc.feedback = req.body.feedback;
                        feedback = `Your KYC request have been rejected. ${req.body.feedback}`;
                        console.log('Rejected');
                    } else {
                        return res.status(400).json({ error: true, message: 'Invalid status field.' });
                    }

                    await kyc.save().then(async (kycInstance) => {
                        await NotificationSDK.sendNotificationById(
                            String(kyc.influencer),
                            'KYC Request Update',
                            feedback,
                            null,
                            'ADMIN',
                            'ADMIN_KYC'
                        );

                        return res.status(200).json({ error: false, message: 'KYC request updated.' });
                    });
                } else {
                    return res.status(404).json({ error: true, message: constants.RESOURCE_NOT_FOUND_ERROR('KYC') });
                }
            })
            .catch((error) => {
                return res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        return res.status(400).json({ error: true, message: errors.array() });
    }
};

/**
 * TODO: Update KYC status in case if user KYC is not approved but resolved later.
 */

module.exports = middlewares;
