const { validationResult } = require('express-validator');
var fs = require('fs');
var parse = require('csv-parse');

const Tip = require('../../models/tip');

const constants = require('../../constants');

const middlewares = {};

middlewares.getTips = async (req, res, next) => {
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;
    let sortBy = {};
    if (req.query.sortBy) {
        sortBy[req.query.sortBy] = -1;
    } else {
        sortBy['createdAt'] = -1;
    }

    let count = null;
    await Tip.countDocuments()
        .then((c) => {
            count = c;
        })
        .catch((error) => {
            console.log(error);
        });

    if (count == null) {
        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
    }

    const pages = Math.ceil(count / parseFloat(size));

    Tip.find()
        .skip(size * (pageNo - 1))
        .limit(size)
        .sort(sortBy)
        .lean()
        .then((tips) => {
            res.status(200).json({ error: false, result: tips, pages: pages });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.addTip = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const tip = new Tip();
        tip.tip = req.body.tip;

        tip.save()
            .then(() => {
                res.status(200).json({ error: false, result: [constants.SUCCESS] });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.addTips = async (req, res, next) => {
    const parser = parse({ columns: true }, async (err, records) => {
        if (err) {
            return res.status(400).json({ error: true, message: [err] });
        } else if (records.length == 0) {
            return res.status(400).json({ error: true, message: ['No tips to add.'] });
        } else if (!Object.keys(records[0]).includes('tip') || Object.keys(records[0]).length > 1) {
            return res.status(400).json({ error: true, message: ['CSV format not valid.'] });
        }

        await Tip.insertMany(records)
            .then(() => {
                return res.status(200).json({ error: false, result: [constants.SUCCESS] });
            })
            .catch((error) => {
                return res.status(500).json({ error: true, message: [error] });
            });
    });

    await fs.createReadStream(req.file.path).pipe(parser);
};

middlewares.editTip = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Tip.findById(req.body.tipId).then((tip) => {
            if (tip != null) {
                tip.tip = req.body.tip;

                tip.save()
                    .then(() => {
                        res.status(200).json({ error: false, result: [constants.SUCCESS] });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Tip')] });
            }
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
