const { validationResult } = require('express-validator');
const constants = require('../../../constants');
const NotificationSDK = require('../../util/notification');
const AdminNotificationService = require('../../../services/admin/push_notification');

const middlewares = {};

middlewares.sendToAllInfluencers = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const title = req.body.title;
        const body = req.body.body;
        const type = req.body.type;
        const saveNotification = req.body.saveNotification ? req.body.saveNotification : false;

        let image = null,
            subType = null,
            parameter = null;

        if (req.body.image) image = req.body.image;
        if (req.body.subType) subType = req.body.subType;
        if (req.body.parameter) parameter = req.body.parameter;

        try {
            let [success, err] = await NotificationSDK.sendPushNotificationToAll(
                title,
                body,
                image,
                type,
                subType,
                parameter,
                saveNotification
            );

            if (err) {
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            }
            return res.status(200).json({ error: false, message: [constants.SUCCESS] });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.sendNotificationInfluencer = async (req, res, next) => {
    try {
        let { title, body, image } = req.body;

        if (!title || !body) {
            return res.status(400).json({
                error: true,
                message: 'Body and Title are required for notification',
            });
        }

        if (!image) {
            image = null;
        }

        await AdminNotificationService.SendNotificationOnlyInfluencers(title, body, image, (err, result) => {
            if (err) {
                return res.status(500).json({ error: true, message: err });
            }
            return res.status(200).json({ error: false, message: result });
        });
    } catch (e) {
        return res.status(400).json({
            error: true,
            message: e,
        });
    }
};

module.exports = middlewares;
