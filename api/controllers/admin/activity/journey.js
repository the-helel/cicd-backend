/**
 * Controller for admin side of story and earn, here
 * admin can review unreviwed blogs and update their status
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 */

const { validationResult } = require('express-validator');

const Journey = require('../../../models/activity/journey');
const RewardsWallet = require('../../../models/reward/rewards-wallet');
const pointsTransactions = require('../../../models/reward/points-transaction');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getAllSubmittedJourney = async (req, res, next) => {
    let { page = 1, size = 10 } = req.query;

    const limit = parseInt(size);
    const skip = (page - 1) * size;

    const count = await Journey.count();
    let n_pages = Math.ceil(count / size);

    if (n_pages < 1) n_pages = 1;

    try {
        const response = await Journey.find().skip(skip).limit(limit).sort({ createdAt: -1 });

        if (response.length === 0) return res.status(404).send({ error: true, message: ['Nothing to review'] });
        else {
            return res.status(200).send({ error: false, result: { count, n_pages, response } });
        }
    } catch (err) {
        return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
    }
};

middlewares.updateStatus = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        Journey.findByIdAndUpdate(
            req.body.Id,
            { feedback: req.body.feedback, status: req.body.status, approvedStoryURL: req.body.approvedStoryURL },
            { new: true },
            // opts,
            async (err, doc) => {
                if (err) {
                    // await session.abortTransaction();
                    // session.endSession();

                    return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Blog')] });
                }

                if (req.body.status === constants.APPROVED) {
                    RewardsWallet.findOneAndUpdate(
                        { influencer: doc.postedBy },
                        { $inc: { points: constants.BLOG_OR_JOURNEY_APPROVAL_POINTS } },
                        { new: true },
                        // opts,
                        async (err, doc) => {
                            if (err) {
                                // await session.abortTransaction();
                                // session.endSession();

                                return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')] });
                            }

                            var newPointsTransaction = new pointsTransactions({
                                userId: doc.influencer,
                                points: constants.BLOG_OR_JOURNEY_APPROVAL_POINTS,
                                metadata: {
                                    source: constants.JOURNEY,
                                    sourceId: req.body.Id,
                                },
                                transactionType: constants.CREDIT,
                            });

                            newPointsTransaction
                                .save()
                                .then(async () => {
                                    return res.status(200).send({ error: false, message: [constants.SUCCESS] });
                                })
                                .catch(async (err) => {
                                    return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                                });
                        }
                    );
                }
            }
        );
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
