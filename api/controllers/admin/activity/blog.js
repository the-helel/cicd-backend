/**
 * Controller for admin side of blog and earn, here
 * admin can review unreviwed blogs and update their status
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 */

const { validationResult } = require('express-validator');

// models
const Blog = require('../../../models/activity/blog');
const rewardWallet = require('../../../models/reward/rewards-wallet');
const pointsTransactions = require('../../../models/reward/points-transaction');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getAllSubmittedBlogs = async (req, res, next) => {
    let { page = 1, size = 10 } = req.query;

    const limit = parseInt(size);
    const skip = (page - 1) * size;

    const count = await Blog.count();
    const n_pages = Math.ceil(count / size);

    if (n_pages < 1) n_pages = 1;

    try {
        const response = await Blog.aggregate([
            {
                $sort: {
                    createdAt: -1,
                },
            },
            {
                $skip: skip,
            },
            {
                $limit: limit,
            },
            {
                $lookup: {
                    from: 'facebooks',
                    localField: 'postedBy',
                    foreignField: 'influencer',
                    as: 'facebookCollection',
                },
            },
            {
                $lookup: {
                    from: 'influencers',
                    localField: 'postedBy',
                    foreignField: '_id',
                    as: 'influencerCollection',
                },
            },
            {
                $project: {
                    'facebookCollection.profilePicture': 1,
                    'influencerCollection.name': 1,
                    _id: 1,
                    status: 1,
                    blogTitle: 1,
                    blogURL: 1,
                    feedback: 1,
                    approvedBlogURL: 1,
                    blogDescription: 1,
                    postedBy: 1,
                    createdAt: 1,
                    updatedAt: 1,
                },
            },
        ]);

        if (response.length === 0) return res.status(404).send({ error: true, message: ['Nothing to review'] });
        else {
            return res.status(200).send({ error: false, result: { count, n_pages, response } });
        }
    } catch (err) {
        return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
    }
};

middlewares.getAllSubmittedBlogsByInfluencer = (req, res, next) => {};

middlewares.updateStatus = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        Blog.findByIdAndUpdate(
            req.body.blogId,
            { feedback: req.body.feedback, status: req.body.status, approvedBlogURL: req.body.approvedBlogURL },
            { new: true },
            (err, doc) => {
                if (err) {
                    return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Blog')] });
                }

                if (req.body.status === constants.APPROVED) {
                    rewardWallet.findOneAndUpdate(
                        { influencer: doc.postedBy },
                        { $inc: { points: constants.BLOG_OR_STORY_APPROVAL_POINTS } },
                        { new: true },
                        (err, doc) => {
                            if (err) {
                                return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')] });
                            }

                            var newPointsTransaction = new pointsTransactions({
                                userId: doc.influencer,
                                points: constants.BLOG_OR_STORY_APPROVAL_POINTS,
                                metadata: {
                                    source: constants.BLOG,
                                    sourceId: req.body.blogId,
                                },
                                transactionType: constants.CREDIT,
                            });

                            newPointsTransaction
                                .save()
                                .then(() => {
                                    return res.status(200).send({ error: false, message: [constants.SUCCESS] });
                                })
                                .catch((err) => {
                                    return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                                });
                        }
                    );
                } else {
                    return res.status(200).send({ error: false, message: [constants.SUCCESS] });
                }
            }
        );
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
