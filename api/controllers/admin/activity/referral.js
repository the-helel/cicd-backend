const { validationResult } = require('express-validator');

const Constant = require('../../../models/constant');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getReferralAmountForReferrer = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferrer' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, referralAmount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getReferralAmountForReferred = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferred' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, referralAmount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getReferralPercentage = (req, res, next) => {
    Constant.findOne({ name: 'referralPercentage' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, referralPercentage: parseFloat(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getReferralMaxAmount = (req, res, next) => {
    Constant.findOne({ name: 'referralMaxAmount' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, referralMaxAmount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralAmountForReferrer = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferrer' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralAmountForReferrer';
            }
            constant.value = String(req.body.referralAmountForReferrer);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralAmountForReferred = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferred' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralAmountForReferred';
            }
            constant.value = String(req.body.referralAmountForReferred);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralPercentage = (req, res, next) => {
    Constant.findOne({ name: 'referralPercentage' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralPercentage';
            }
            constant.value = String(req.body.referralPercentage);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralMaxAmount = (req, res, next) => {
    Constant.findOne({ name: 'referralMaxAmount' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralMaxAmount';
            }
            constant.value = String(req.body.referralMaxAmount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getDivision1Amount = (req, res, next) => {
    Constant.findOne({ name: 'div1Insta' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, amount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getDivision2Amount = (req, res, next) => {
    Constant.findOne({ name: 'div2Insta' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, amount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getDivision3Amount = (req, res, next) => {
    Constant.findOne({ name: 'div3Insta' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, amount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getDivision4Amount = (req, res, next) => {
    Constant.findOne({ name: 'div4Insta' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, amount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getDivision5Amount = (req, res, next) => {
    Constant.findOne({ name: 'div5Insta' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, amount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getInstagramConnectAmount = (req, res, next) => {
    Constant.findOne({ name: 'instaConnect' })
        .lean()
        .then((constant) => {
            res.status(200).json({ error: false, amount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveDivision1Amount = (req, res, next) => {
    Constant.findOne({ name: 'div1Insta' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'div1Insta';
            }
            constant.value = String(req.body.amount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveDivision2Amount = (req, res, next) => {
    Constant.findOne({ name: 'div2Insta' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'div2Insta';
            }
            constant.value = String(req.body.amount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveDivision3Amount = (req, res, next) => {
    Constant.findOne({ name: 'div3Insta' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'div3Insta';
            }
            constant.value = String(req.body.amount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveDivision4Amount = (req, res, next) => {
    Constant.findOne({ name: 'div4Insta' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'div4Insta';
            }
            constant.value = String(req.body.amount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveDivision5Amount = (req, res, next) => {
    Constant.findOne({ name: 'div5Insta' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'div5Insta';
            }
            constant.value = String(req.body.amount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveInstagramConnectAmount = (req, res, next) => {
    Constant.findOne({ name: 'instaConnect' })
        .lean()
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'instaConnect';
            }
            constant.value = String(req.body.amount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

module.exports = middlewares;
