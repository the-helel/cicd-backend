const { validationResult } = require('express-validator');

const Category = require('../../models/category');

const constants = require('../../constants');

const middlewares = {};

middlewares.getAllCategories = (req, res, next) => {
    Category.find()
        .lean()
        .then((categories) => {
            res.status(200).json({ error: false, categories: categories });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.addCategory = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const category = new Category();
        category.name = req.body.name;
        category.description = req.body.description;
        category.icon = req.body.icon;
        category
            .save()
            .then((record) => {
                res.status(200).json({ error: false, message: [constants.SUCCESS] });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.updateCategory = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Category.findById(req.body.id).then((category) => {
            if (category != null) {
                category.name = req.body.name;
                category.description = req.body.description;
                category.icon = req.body.icon;
                category
                    .save()
                    .then((record) => {
                        res.status(200).json({ error: false, message: [constants.SUCCESS] });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND('Category')] });
            }
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.removeCategory = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Category.findByIdAndDelete(req.body.categoryId)
            .then(() => {
                res.status(200).json({ error: false, message: [constants.SUCCESS] });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
