const { validationResult } = require('express-validator');

const Order = require('../../models/company/order');
const Campaign = require('../../models/campaign/cash-campaign/cash-campaign');
const Library = require('../../models/library');

const { razorpayInstance } = require('../../../payment/razorpay-payment.js');
const orderUtil = require('./ordersUtil');
const mongoose = require('mongoose');

const constants = require('../../constants');

const middlewares = {};

middlewares.checkOrder = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Order.findOne(
            { entity: req.query.entityType, 'entityDetails.entityId': req.query.entityId, company: req.user },
            {
                entity: true,
                entityDetails: true,
                orderAmount: true,
                rpOrderId: true,
                currency: true,
                invoiceId: true,
                status: true,
                createdAt: true,
            },
            (err, order) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!order) return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Order')] });

                return res.status(200).send({ error: false, result: { order: order } });
            }
        );
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

// function to create a new order
middlewares.createOrder = async (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const entities = ['Campaign', 'Content'];
        const currencies = ['INR'];

        // creating data object according to the given inputs
        // and basic error handling
        const data = {};
        data.entityDetails = {};

        // checking for entity paramter
        if (req.body.entity && entities.includes(req.body.entity)) {
            // checking for currency paramter
            if (req.body.currency && currencies.includes(req.body.currency)) {
                // checking if entityId is given or not
                if (req.body.entityId) {
                    // checking if any order with given entityId already exists or not
                    if (await Order.exists({ 'entityDetails.entityId': req.body.entityId }))
                        return res.status(400).send({ error: true, message: ['Order with given entityId already exists'] });
                    // assigning fields according to given entity type
                    if (req.body.entity === 'Campaign') {
                        await Campaign.findById(req.body.entityId, async (err, campaign) => {
                            if (err) return res.status(500).send({ error: true, message: [err.message] });
                            if (!campaign) return res.status(400).send({ error: true, message: ['Invalid entityId for campagin'] });

                            data.Id = req.user;
                            data.entity = 'Campaign';

                            data.entityDetails.entityId = req.body.entityId;
                            data.entityDetails.amount = campaign.budget * 100;
                            data.entityDetails.serviceFee = 10;
                            data.entityDetails.tax = 18;

                            data.orderAmount = orderUtil.calculateTotal(
                                data.entityDetails.amount,
                                data.entityDetails.serviceFee,
                                data.entityDetails.tax
                            );
                            data.currency = req.body.currency;
                            await Order.countDocuments({}, (err, count) => {
                                if (err) data.invoiceId = '';
                                data.invoiceId = `receipt_${count + 1}`;
                            });
                            await razorpayInstance.orders.create(
                                { amount: data.orderAmount, currency: data.currency, receipt: data.invoiceId },
                                (err, order) => {
                                    if (err) data.rpOrderId = '';
                                    data.rpOrderId = order.id;
                                }
                            );
                            data.status = 'created';

                            // creating the order object
                            if (Object.keys(data).length === 8) {
                                Order.create(data, (err, order) => {
                                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                                    return res.status(201).send({ error: false, result: { order: order } });
                                });
                            }
                        });
                    }

                    if (req.body.entity === 'Content') {
                        await Library.findById(req.body.entityId, async (err, content) => {
                            if (err) return res.status(500).send({ error: true, message: [err.message] });
                            if (!content) return res.status(400).send({ error: true, message: ['Invalid entityId for content'] });

                            data.company = req.user;
                            data.entity = 'Content';

                            data.entityDetails.entityId = req.body.entityId;
                            data.entityDetails.amount = content.price * 100;
                            data.entityDetails.serviceFee = 10;
                            data.entityDetails.tax = 18;

                            data.orderAmount = orderUtil.calculateTotal(
                                data.entityDetails.amount,
                                data.entityDetails.serviceFee,
                                data.entityDetails.tax
                            );
                            data.currency = req.body.currency;
                            await Order.countDocuments({}, (err, count) => {
                                if (err) data.invoiceId = '';
                                data.invoiceId = `receipt_${count + 1}`;
                            });
                            await razorpayInstance.orders.create(
                                { amount: data.orderAmount, currency: data.currency, receipt: data.invoiceId },
                                (err, order) => {
                                    if (err) data.rpOrderId = '';
                                    data.rpOrderId = order.id;
                                }
                            );
                            data.status = 'created';

                            // creating the order object
                            if (Object.keys(data).length === 8) {
                                Order.create(data, (err, order) => {
                                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                                    return res.status(201).send({ error: false, result: { order: order } });
                                });
                            }
                        });
                    }
                } else {
                    return res.status(400).send({ error: true, message: ['Error in entityId or Order with entityId already exists'] });
                }
            } else {
                return res.status(400).send({ error: true, message: ['Invalid currency'] });
            }
        } else {
            return res.status(400).send({ error: true, message: ['Invalid entity'] });
        }
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

// function to get all the orders of a specific user
middlewares.getOrders = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // parameters for pagination
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        // performing the query
        Order.aggregate()
            .match({ company: mongoose.Types.ObjectId(req.user) })
            .project({
                entity: true,
                orderAmount: true,
                currency: true,
                rpOrderId: true,
                invoiceId: true,
                status: true,
                createdAt: true,
            })
            .skip(size * (pageNo - 1))
            .limit(size)
            .exec((err, orders) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                return res.status(200).send({ error: false, result: { orders: orders } });
            });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

// function to get a specific order
middlewares.getOrder = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // fetching the order using the given orderId
        Order.aggregate()
            .match({ company: mongoose.Types.ObjectId(req.user), _id: mongoose.Types.ObjectId(req.query.orderId) })
            .project({
                entity: true,
                entityDetails: true,
                orderAmount: true,
                rpOrderId: true,
                currency: true,
                invoiceId: true,
                status: true,
                createdAt: true,
            })
            .limit(1)
            .exec((err, order) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                return res.status(200).send({ error: false, result: { order: order } });
            });
    } else {
        return res.status(400).send({ error: true, message: [err.message] });
    }
};

module.exports = middlewares;
