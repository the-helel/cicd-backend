const { validationResult } = require('express-validator');
const crypto = require('crypto');

const Order = require('../../models/company/order');
const Campaign = require('../../models/campaign/cash-campaign/cash-campaign');

const config = require('../../config');
const { razorpayInstance } = require('../../../payment/razorpay-payment.js');

const Library = require('../../models/library');

const constants = require('../../constants');

const middlewares = {};

// function to verify if the details for transaction is valid or not
middlewares.verifyPayment = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // checking if the receipt id is provided or not
        if (req.params.receiptId) {
            // fetching the order object using given receipt id
            Order.findOne({ invoiceId: req.params.receiptId }, (err, order) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                // saving the payment details in the object if the object ids match
                if (order.rpOrderId === req.body.razorpay_order_id) {
                    order.paymentDetails.razorpay_payment_id = req.body.razorpay_payment_id;
                    order.paymentDetails.razorpay_order_id = req.body.razorpay_order_id;
                    order.paymentDetails.razorpay_signature = req.body.razorpay_signature;
                    order.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        // creating the hmac digest done using sha256
                        let data = `${order.rpOrderId}|${order.paymentDetails.razorpay_payment_id}`;

                        const hmac = crypto.createHmac('sha256', config.RAZORPAY_KEY_SECRET);
                        const digest = hmac.update(data);

                        let generated_signature = digest.digest('hex');

                        // comparing the signature with the recieved signature
                        if (generated_signature === order.paymentDetails.razorpay_signature) {
                            // updating the order status
                            order.status = 'authorized';
                            // capturing the payment
                            razorpayInstance.payments
                                .capture(order.paymentDetails.razorpay_payment_id, order.orderAmount, order.currency)
                                .then((response) => {
                                    // if capturing is successfull, update the
                                    // status of the order
                                    if (response.status === 'captured') {
                                        order.status = 'captured';
                                        order.save((err) => {
                                            if (err) return res.status(500).send({ error: true, message: [err.message] });

                                            // update the entity with the stauts as active or paid
                                            if (order.entity === 'Campaign') {
                                                Campaign.findById(order.entityDetails.entityId, (err, campaign) => {
                                                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                                                    campaign.status = constants.ACTIVE;
                                                    campaign.save((err) => {
                                                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                                                        return res.status(200).send({
                                                            error: false,
                                                            result: { message: constants.SUCCESS, entity: 'Campaign' },
                                                        });
                                                    });
                                                });
                                            }

                                            if (order.entity === 'Content') {
                                                Library.findById(order.entityDetails.entityId, (err, content) => {
                                                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                                                    if (req.query.paidPartnershipAllowed)
                                                        content.paidPartnershipAllowed = req.query.paidPartnershipAllowed;
                                                    content.paid = true;
                                                    content.paidBy = req.user;
                                                    content.save((err) => {
                                                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                                                        return res.status(200).send({
                                                            error: false,
                                                            result: { message: constants.SUCCESS, entity: 'Content' },
                                                        });
                                                    });
                                                });
                                            }
                                        });
                                    } else {
                                        return res.status(400).send({ error: true, message: ['Payment capture failed'] });
                                    }
                                })
                                .catch((err) => console.log(err));
                        } else {
                            return res.status(400).send({ error: true, message: ['detected tampering with the order. Cannot process'] });
                        }
                    });
                } else {
                    return res.status(400).send({ error: true, message: ['detected tampering with the order. Cannot process'] });
                }
            });
        } else return res.status(400).send({ error: true, message: ['Invalid url'] });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
