const util = {};

// function to calculate the total value of the order
util.calculateTotal = (base, serviceFee, tax) => {
    const total = base * (1 + serviceFee / 100) * (1 + tax / 100);
    return parseInt(total);
};

module.exports = util;
