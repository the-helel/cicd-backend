const { validationResult } = require('express-validator');

const Notification = require('../../models/notification/notification');
const Subscription = require('../../models/notification/subscriber');

const mongoose = require('mongoose');

const fcm = require('../../firebase/fcm');

const constants = require('../../constants');

const middlewares = {};

middlewares.createNotification = async (title, body, image, userId, type, subType = null, parameter = null) => {
    const notification = new Notification();
    notification.user = userId;
    notification.title = title;
    notification.body = body;
    if (image != null) notification.image = image;
    notification.type = type;
    notification.subType = subType;
    notification.parameter = parameter;

    return await notification
        .save()
        .then(async () => {
            return constants.SUCCESS;
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject([error.message, null]);
        });
};

// function to get all the notifications
middlewares.getNotifications = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // pagination parameters
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        const findFilter = {};
        findFilter['user'] = req.user;
        if (req.query.readStatus) findFilter['read'] = req.query.readStatus;

        // querying the db with the given parameters
        Notification.find(findFilter)
            .skip(size * (pageNo - 1))
            .limit(size)
            .sort('-createdAt')
            .lean()
            .exec((err, notifications) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                return res.status(200).send({ error: false, result: { notifications: notifications } });
            });
    } else {
        return res.status(400).send({ error: true, message: constants.VALIDATION_ERROR });
    }
};

// function to mark the notification as read
middlewares.markReadNotification = (req, res, next) => {
    // checking for notification id parameter
    if (req.query.notificationId) {
        // making the query
        Notification.findById(req.query.notificationId, (err, notification) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });

            // if the user is the owner, change the read status
            if (notification.user === req.user) {
                notification.read = true;
                // saving the notification object
                notification.save((err) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(200).send({ error: true, message: [constants.SUCCESS] });
                });
            } else {
                return res.status(401).send({ error: true, message: ['Not authorized to perform this operation'] });
            }
        });
    } else {
        return res.status(400).send({ error: true, message: ['Invalid url'] });
    }
};

// function to subscribe a device for notifications
middlewares.subscribe = async (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // checking the subscription with the userId exists or not
        if (await Subscription.exists({ userId: req.user })) {
            // updating the existing subscription
            Subscription.findOne({ userId: mongoose.Types.ObjectId(req.user) }).exec((err, subscription) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!subscription)
                    return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Subscription')] });

                subscription.fcmToken = req.body.fcmToken;
                subscription.save((err, subscription) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(200).send({
                        error: false,
                        result: {
                            message: 'subscription updated',
                            subscription: subscription,
                        },
                    });
                });
            });
        } else {
            // creating new subscription in case it doesnt exists already
            Subscription.create(
                {
                    userId: req.user,
                    userType: req.userType,
                    fcmToken: req.body.fcmToken,
                },
                (err, subscription) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(201).send({ error: false, result: { message: 'subscription created', subscription: subscription } });
                }
            );
        }
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

// Function to send notification to influencers by their IDs
middlewares.sendNotification = async (title, body, image, userId, type, subType = null, parameter = null) => {
    // checking for the required parameters
    if (title && body && userId) {
        // find the subscription object for the given userId
        return await Subscription.findOne({ userId: userId })
            .then(async (subscription) => {
                if (!subscription) return Promise.reject('Invalid userId given');

                const notificationOptions = {
                    priority: 'high',
                    timeToLive: 60 * 60 * 24,
                };

                // Sending the notification using fcm
                return await middlewares
                    .createNotification(title, body, image, userId, type, subType, parameter)
                    .then(async (message) => {
                        const data = {
                            type: type,
                            subType: subType,
                            click_action: 'FLUTTER_NOTIFICATION_CLICK',
                        };
                        for (const param in parameter) {
                            data[param] = parameter[param].toString();
                        }

                        const notificationObject = { title: title, body: body };
                        if (image != null) {
                            notificationObject.image = image;
                        }

                        return await fcm
                            .messaging()
                            .sendToDevice(
                                subscription.fcmToken,
                                {
                                    notification: notificationObject,
                                    data: data,
                                },
                                notificationOptions
                            )
                            .then((response) => {
                                console.log(response.results[0]);
                                return [null, { message: constants.SUCCESS, response: response }];
                            })
                            .catch((err) => {
                                console.log(err);
                                return Promise.reject(err);
                            });
                    })
                    .catch((error) => {
                        console.log(error);
                        return Promise.reject(error.message);
                    });
            })
            .catch((error) => {
                console.log(error);
                return Promise.reject(error.message);
            });
    } else {
        return Promise.reject('Required parameters are not there');
    }
};

// function to send the push notifications to the device
middlewares.sendPushNotification = async (title, body, userId, userType, type, subType = null, parameter = null) => {
    // checking for the required parameters
    if (title && body && userId) {
        // find the subscription object for the given userId
        return await Subscription.findOne({ userId: userId })
            .then(async (subscription) => {
                if (!subscription) return Promise.reject(['Invalid userId given', null]);

                const notificationOptions = {
                    priority: 'high',
                    timeToLive: 60 * 60 * 24,
                };

                const data = {
                    type: type,
                    subType: subType,
                    click_action: 'FLUTTER_NOTIFICATION_CLICK',
                };
                for (const param in parameter) {
                    data[param] = parameter[param].toString();
                }
                return await fcm
                    .messaging()
                    .sendToDevice(
                        subscription.fcmToken,
                        {
                            notification: { title: title, body: body },
                            data: data,
                        },
                        notificationOptions
                    )
                    .then((response) => {
                        console.log(response.results[0]);
                        return;
                    })
                    .catch((err) => {
                        console.log(err);
                        return Promise.reject(err.message);
                    });
            })
            .catch((error) => {
                console.log(error);
                return Promise.reject(error.message);
            });
    } else {
        return Promise.reject('Required parameters are not there');
    }
};

middlewares.test = (req, res, next) => {
    middlewares
        .sendNotification(
            'Socksoho',
            'Socksoho has published a new campaign.',
            null,
            '607b56b4a16ce625320470d7',
            constants.CASH_CAMPAIGN,
            'published-campaign',
            {
                campaign: '6051f344cdd93b0514bc14b8',
            }
        )
        // .sendPushNotification(
        //     'Refer and earn',
        //     'Refer your friend and earn famstar points.',
        //     null,
        //     '605883f170803e4b0899a3cb',
        //     constants.INFLUENCER,
        //     'earn',
        //     'referral',
        //     {}
        // )
        .then(() => {
            // console.log(response);
            return res.status(200).send('Test passed.');
        })
        .catch((err) => {
            console.log(err);
        });
};

module.exports = middlewares;
