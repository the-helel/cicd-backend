const { axiosInstance } = require('../../../../payment/razorpay-payment.js');

const middlewares = {};

middlewares.createPayout = (accountNumber, fundAccountId, amount, currency, mode, purpose, queued) => {
    console.log('Creating a payout');
    return axiosInstance
        .post('/payouts', {
            account_number: accountNumber,
            fund_account_id: fundAccountId,
            amount: amount * 100,
            currency: currency,
            mode: mode,
            purpose: purpose,
            queue_if_low_balance: queued,
        })
        .then((response) => {
            console.log(response.data);
            return response.data;
        })
        .catch((error) => {
            console.log('Error creating payout', error.response.data);
            return Promise.reject(error);
        });
};

middlewares.getPayout = (payoutId) => {
    return axiosInstance
        .get(`/payouts/${payoutId}`)
        .then((response) => {
            console.log(`PayoutId: ${payoutId} -> ${response.data}`);

            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = middlewares;
