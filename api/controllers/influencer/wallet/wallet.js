const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const Influencer = require('../../../models/influencer/influencer');
const Wallet = require('../../../models/wallet/wallet');
const KYC = require('../../../models/wallet/kyc');
const Transaction = require('../../../models/wallet/transaction');
const CashCampaign = require('../../../models/campaign/cash-campaign/cash-campaign');
const Brand = require('../../../models/company/brand');
const Product = require('../../../models/reward/product');
const VerifyBankAccount = require('../../../models/wallet/verify-bank-account');

const createContact = require('./create-contact');
const createFundsAccountUsingBankAccount = require('./create-funds-account-using-bank-account');
const createFundsAccountUsingUPIAccount = require('./create-funds-account-using-upi-account');
const { createPayout, getPayout } = require('./payout');
const InfluencerService = require('../../../services/influencer');
const config = require('../../../config');
const { RAZORPAY_ACCOUNT } = require('../../../razorpay/razorpay.js');

const constants = require('../../../constants');
const { RESOURCE_NOT_FOUND_ERROR } = require('../../../constants');
const { sendPushNotification } = require('../../notification/notification');

const middlewares = {};

const setupWallet = async (influencer, fundsAccount, type, photo = null) => {
    return await Wallet.findOne({ influencer: influencer })
        .then(async (wallet) => {
            if (wallet) {
                if (type == constants.BANK_ACCOUNT) {
                    if (!Object.keys(wallet.toObject()).includes('bankAccounts')) {
                        wallet.bankAccounts = {};
                    }

                    if (!Object.keys(wallet.bankAccounts).includes(fundsAccount.id)) {
                        wallet.bankAccounts[fundsAccount.id] = fundsAccount;
                        wallet.bankAccounts[fundsAccount.id].status = constants.PENDING;

                        const verifyBankAccount = new VerifyBankAccount();
                        verifyBankAccount.influencer = influencer;
                        verifyBankAccount.status = constants.PENDING;
                        verifyBankAccount.fundsAccountId = fundsAccount.id;
                        verifyBankAccount.photo = photo;

                        await verifyBankAccount
                            .save()
                            .then(() => {
                                return;
                            })
                            .catch((error) => {
                                return Promise.reject(error);
                            });
                    } else {
                        return Promise.reject('This bank account is already added.');
                    }
                    wallet.markModified('bankAccounts');
                } else if (type == constants.UPI) {
                    if (!Object.keys(wallet.toObject()).includes('upiAccounts')) {
                        wallet.upiAccounts = {};
                    }

                    if (!Object.keys(wallet.upiAccounts).includes(fundsAccount.id)) {
                        wallet.upiAccounts[fundsAccount.id] = fundsAccount;
                    } else {
                        return Promise.reject('This upi account is already added.');
                    }
                    wallet.markModified('upiAccounts');
                }

                if (!Object.keys(wallet.toObject()).includes('defaultAccount')) {
                    wallet.defaultAccount = fundsAccount.id;
                }

                return await wallet.save();
            } else {
                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Wallet'));
            }
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

middlewares.getWallet = (req, res, next) => {
    Wallet.findOne({ influencer: req.user })
        .lean()
        .then((wallet) => {
            if (wallet) {
                res.status(200).json({ error: false, result: wallet });
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Wallet')] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error] });
        });
};

/**
 * When user performs a create bank account operation. We first create a contact object
 * in razorpay using its api.
 *
 * See also {@link createContact}(create-contact.js)
 *
 *
 */
middlewares.addBankAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Influencer.findById(req.user).then((influencer) => {
            const contact = Number(influencer.phoneNumber.slice(3));
            createContact(influencer.name, influencer.email, contact, constants.INFLUENCER)
                .then((contact) => {
                    createFundsAccountUsingBankAccount(contact.id, req.body.accountHolderName, req.body.ifsc, req.body.accountNumber)
                        .then((fundsAccount) => {
                            setupWallet(req.user, fundsAccount, constants.BANK_ACCOUNT, req.file.location)
                                .then(() => {
                                    res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error] });
                                });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error] });
                        });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error] });
                });
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.removeBankAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Wallet.findOne({ influencer: req.user })
            .then((wallet) => {
                if (wallet != null) {
                    if (!Object.keys(wallet.toObject()).includes('bankAccounts')) {
                        res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Bank Accounts')] });
                    } else {
                        if (!Object.keys(wallet.bankAccounts).includes(req.query.id)) {
                            res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Account')] });
                        } else {
                            delete wallet.bankAccounts[req.query.id];
                            wallet.markModified('bankAccounts');

                            wallet
                                .save()
                                .then(() => {
                                    res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error] });
                                });
                        }
                    }
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

/**
 * When user performs a create VPA account operation. We first create a contact object
 * in razorpay using its api. We take the name and VPA ID of user.
 *
 * See also {@link createContact}(create-contact.js)
 * See also {@link createFundsAccountUsingUPIAccount}(create-funds-account-using-upi-account.js)
 *
 */
middlewares.addUPIAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Influencer.findById(req.user).then((influencer) => {
            const contact = Number(influencer.phoneNumber.slice(3));
            createContact(influencer.name, influencer.email, contact, constants.INFLUENCER)
                .then((contact) => {
                    createFundsAccountUsingUPIAccount(contact.id, req.body.upiId)
                        .then((fundsAccount) => {
                            fundsAccount.name = req.body.name;
                            setupWallet(req.user, fundsAccount, constants.UPI)
                                .then(() => {
                                    res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error] });
                                });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error] });
                        });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error] });
                });
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.removeUPIAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Wallet.findOne({ influencer: req.user })
            .then((wallet) => {
                if (wallet != null) {
                    if (!Object.keys(wallet.toObject()).includes('upiAccounts')) {
                        res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('UPI Accounts')] });
                    } else {
                        if (!Object.keys(wallet.upiAccounts).includes(req.query.id)) {
                            res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Account')] });
                        } else {
                            delete wallet.upiAccounts[req.query.id];
                            wallet.markModified('upiAccounts');

                            wallet
                                .save()
                                .then(() => {
                                    res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error] });
                                });
                        }
                    }
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.addMoneyToWallet = async (influencer, amount, entityType, entityId, session) => {
    return await Wallet.findOne({ influencer: influencer }).then(async (wallet) => {
        if (wallet) {
            wallet.totalEarnings += amount;
            wallet.moneyToBeWithdrawn += amount;
            return await wallet
                .save({ session })
                .then(async () => {
                    await middlewares
                        .createTransaction(
                            influencer,
                            constants.CREDIT,
                            amount,
                            entityType,
                            entityId,
                            null,
                            null,
                            null,
                            null,
                            null,
                            session
                        )
                        .then(() => {
                            return;
                        })
                        .catch((error) => {
                            return Promise.reject(error);
                        });
                })
                .catch((error) => {
                    return Promise.reject(error);
                });
        }
    });
};

middlewares.refundMoneyToWallet = async (influencer, amount, entityId, status, session) => {
    return await Wallet.findOne({ influencer: influencer })
        .then(async (wallet) => {
            if (wallet) {
                wallet.moneyToBeWithdrawn += amount;
                return await wallet
                    .save({ session })
                    .then(async () => {
                        await middlewares
                            .createTransaction(
                                influencer,
                                constants.CREDIT,
                                amount,
                                'refund',
                                entityId,
                                null,
                                null,
                                null,
                                null,
                                status,
                                session
                            )
                            .then(() => {
                                return;
                            })
                            .catch((error) => {
                                return Promise.reject(error);
                            });
                    })
                    .catch((error) => {
                        return Promise.reject(error);
                    });
            }
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

middlewares.createTransaction = async (
    influencer,
    type,
    amount,
    entityType,
    entityId,
    payoutId,
    fundsAccountId,
    utr,
    mode,
    status,
    session,
    payoutAmount = null
) => {
    const transaction = new Transaction();
    transaction.influencer = influencer;
    transaction.type = type;
    transaction.amount = amount;

    transaction.metadata = {};
    if (entityType) {
        if (entityType === constants.SCRATCHCARD) transaction.metadata.description = 'The amount has been added for using Famstar points';
        transaction.metadata.entityType = entityType;
    }
    if (entityId) transaction.metadata.entityId = entityId;
    if (payoutId) transaction.metadata.payoutId = payoutId;
    if (fundsAccountId) transaction.metadata.fundsAccountId = fundsAccountId;
    if (utr) transaction.metadata.utr = utr;
    if (mode) transaction.metadata.mode = mode;
    if (status) {
        transaction.status = status;
        transaction.metadata.description = 'Amount will be transferred in 2-3 working days.';
    }

    if ([constants.CANCELLED, constants.REVERSED, constants.FAILED].includes(transaction.status)) {
        transaction.metadata.description = 'We were unable to process your withdraw request right now.';
    }

    if (payoutAmount != null) transaction.metadata.payoutAmount = payoutAmount;

    return await transaction
        .save({ session })
        .then(() => {
            return;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

middlewares.calculateTDS = (wallet, amount) => {
    const totalMoneyWithdrawn = wallet.totalEarnings - wallet.moneyToBeWithdrawn;
    let deductibleAmount = 0;

    if (totalMoneyWithdrawn > constants.CUMULATIVE_WITHDRAW_AMOUNT) {
        deductibleAmount = amount;
    } else if (totalMoneyWithdrawn + amount <= constants.CUMULATIVE_WITHDRAW_AMOUNT) {
        deductibleAmount = 0;
    } else if (totalMoneyWithdrawn + amount > constants.CUMULATIVE_WITHDRAW_AMOUNT) {
        deductibleAmount = totalMoneyWithdrawn + amount;
    }

    const deductionAmount = Math.round(deductibleAmount * 10) / 100.0;
    const amountAfterDeduction = Math.round((amount - deductionAmount) * 100) / 100.0;

    return {
        deductibleAmount: deductibleAmount,
        deductionAmount: deductionAmount,
        amountAfterDeduction: amountAfterDeduction,
    };
};

middlewares.getTDS = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const amount = parseInt(req.query.amount);

        await Wallet.findOne({ influencer: req.user })
            .lean()
            .then(async (wallet) => {
                if (wallet) {
                    if (wallet.kyc) {
                        KYC.findById(wallet.kyc)
                            .lean()
                            .then(async (kyc) => {
                                if (kyc) {
                                    if (kyc.status == constants.VERIFIED) {
                                        if (amount > 0) {
                                            if (amount <= wallet.moneyToBeWithdrawn) {
                                                res.status(200).json({ error: false, result: middlewares.calculateTDS(wallet, amount) });
                                            } else {
                                                res.status(400).json({ error: true, message: ['Not enough wallet balance'] });
                                            }
                                        } else {
                                            res.status(400).json({ error: true, message: ['Invalid redeem amount'] });
                                        }
                                    } else {
                                        res.status(400).json({ error: true, message: ['KYC not verified.'] });
                                    }
                                } else {
                                    res.status(400).json({ error: true, message: [RESOURCE_NOT_FOUND_ERROR('KYC')] });
                                }
                            })
                            .catch(async (error) => {
                                res.status(500).json({ error: true, message: [error] });
                            });
                    } else {
                        res.status(400).json({ error: true, message: ['Please complete the KYC.'] });
                    }
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Wallet')] });
                }
            })
            .catch(async (error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.redeemWallet = async (req, res, next) => {
    const errors = validationResult(req);
    const session = await mongoose.startSession();
    console.log(req.user);

    if (errors.isEmpty()) {
        const amount = parseFloat(req.body.amount);
        const accountId = req.body.accountId;
        const type = req.body.type;
        await Wallet.findOne({ influencer: req.user })
            .then(async (wallet) => {
                if (wallet) {
                    if (wallet.kyc) {
                        KYC.findById(wallet.kyc)
                            .then(async (kyc) => {
                                console.log('KYC found: ', kyc);
                                if (kyc) {
                                    if (kyc.status === constants.VERIFIED) {
                                        if (amount > 0) {
                                            if (amount <= wallet.moneyToBeWithdrawn) {
                                                const { amountAfterDeduction } = middlewares.calculateTDS(wallet, amount);
                                                if (amountAfterDeduction > 1.0) {
                                                    let fundsAccount = null;
                                                    console.log('Account id ', accountId);
                                                    if (
                                                        type === constants.UPI &&
                                                        Object.keys(wallet.toObject()).includes('upiAccounts') &&
                                                        Object.keys(wallet.upiAccounts).includes(accountId)
                                                    ) {
                                                        fundsAccount = wallet.upiAccounts[accountId];
                                                    } else if (
                                                        type === constants.BANK_ACCOUNT &&
                                                        Object.keys(wallet.toObject()).includes('bankAccounts') &&
                                                        Object.keys(wallet.bankAccounts).includes(accountId)
                                                    ) {
                                                        fundsAccount = wallet.bankAccounts[accountId];
                                                    } else {
                                                        res.status(400).json({ error: true, message: ['Account not verified.'] });
                                                        return session.endSession();
                                                    }
                                                    await session.startTransaction();
                                                    wallet.moneyToBeWithdrawn -= amount;
                                                    wallet
                                                        .save({ session })
                                                        .then(async () => {
                                                            const mode =
                                                                fundsAccount.account_type === 'vpa' ? constants.UPI : constants.IMPS;
                                                            await createPayout(
                                                                // TODO: Change this with test virtual account when development
                                                                RAZORPAY_ACCOUNT,
                                                                fundsAccount.id,
                                                                amountAfterDeduction,
                                                                constants.INR,
                                                                mode,
                                                                constants.PAYOUT,
                                                                true
                                                            )
                                                                .then(async (response) => {
                                                                    await middlewares
                                                                        .createTransaction(
                                                                            req.user,
                                                                            constants.REDEEM,
                                                                            amount,
                                                                            null,
                                                                            null,
                                                                            response.id,
                                                                            response.fund_account_id,
                                                                            response.utr,
                                                                            response.mode,
                                                                            response.status,
                                                                            session,
                                                                            amountAfterDeduction
                                                                        )
                                                                        .then(async () => {
                                                                            res.status(200).json({
                                                                                error: false,
                                                                                message: [constants.SUCCESS],
                                                                            });
                                                                            await session.commitTransaction();
                                                                            await session.endSession();
                                                                        })
                                                                        .catch(async (error) => {
                                                                            res.status(500).json({ error: true, message: [error] });
                                                                            await session.abortTransaction();
                                                                            await session.endSession();
                                                                        });
                                                                })
                                                                .catch(async (error) => {
                                                                    res.status(500).json({ error: true, message: [error] });
                                                                    await session.abortTransaction();
                                                                    await session.endSession();
                                                                });
                                                        })
                                                        .catch(async (error) => {
                                                            res.status(500).json({ error: true, message: [error] });
                                                            await session.endSession();
                                                        });
                                                } else {
                                                    res.status(400).json({
                                                        error: true,
                                                        message: ['Amount after deduction is less than 1.'],
                                                    });
                                                    await session.endSession();
                                                }
                                            } else {
                                                res.status(400).json({ error: true, message: ['Not enough wallet balance'] });
                                                await session.endSession();
                                            }
                                        } else {
                                            res.status(400).json({ error: true, message: ['Invalid redeem amount'] });
                                            await session.endSession();
                                        }
                                    } else {
                                        res.status(400).json({ error: true, message: ['KYC not verified.'] });
                                        await session.endSession();
                                    }
                                } else {
                                    res.status(400).json({ error: true, message: [RESOURCE_NOT_FOUND_ERROR('KYC')] });
                                    await session.endSession();
                                }
                            })
                            .catch(async (error) => {
                                res.status(500).json({ error: true, message: [error] });
                                await session.endSession();
                            });
                    } else {
                        res.status(400).json({ error: true, message: ['Please complete the KYC.'] });
                        await session.endSession();
                    }
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Wallet')] });
                    await session.endSession();
                }
            })
            .catch(async (error) => {
                res.status(500).json({ error: true, message: [error] });
                await session.endSession();
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
        await session.endSession();
    }
};

middlewares.getTransactions = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const size = req.query.size ? parseInt(req.query.size) : 10;
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;

        Transaction.find({ influencer: req.user })
            .skip(size * (pageNo - 1))
            .limit(size)
            .sort({ createdAt: -1 })
            .lean()
            .then(async (transactions) => {
                for (let index in transactions) {
                    const transaction = transactions[index];
                    if (transaction.type == constants.CREDIT) {
                        if (transaction.metadata.entityType == constants.CASH_CAMPAIGN) {
                            await CashCampaign.findById(transaction.metadata.entityId)
                                .lean()
                                .then((campaign) => {
                                    if (campaign != null) {
                                        Brand.findById(campaign.brand)
                                            .lean()
                                            .then((brand) => {
                                                if (brand != null) {
                                                    transactions[index].metadata.campaignTitle = campaign.brief.title;
                                                    transactions[index].metadata.campaignCoverPhoto = campaign.brief.coverPhoto.url;
                                                    transactions[index].metadata.brandName = brand.name;
                                                    transactions[index].metadata.brandLogo = brand.logo.url;
                                                }
                                            })
                                            .catch((error) => {
                                                console.log(error);
                                            });
                                    }
                                })
                                .catch((error) => {
                                    console.log(error);
                                });
                        } else if (transaction.metadata.entityType == constants.CASH) {
                            await Product.findById(transaction.metadata.entityId)
                                .lean()
                                .then((product) => {
                                    if (product != null) {
                                        transactions[index].metadata.productType = product.productType;
                                        transactions[index].metadata.productRedeemPoints = product.productRedeemPoints;
                                    }
                                })
                                .catch((error) => {
                                    console.log(error);
                                });
                        }
                    }
                }
                res.status(200).json({ error: false, result: transactions });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

// New wallet controllers
middlewares.getWalletHistory = async (req, res, next) => {
    const { page } = req.query;

    await InfluencerService.FetchInfluencerTransactionHistory(req.user, page, (err, result) => {
        if (err) {
            return res.status(400).json({
                error: true,
                message: err,
            });
        } else {
            return res.status(200).json({
                error: false,
                data: result,
            });
        }
    });
};

middlewares.processTransitionalTransactions = async () => {
    Transaction.find({ type: constants.REDEEM, status: constants.PROCESSING })
        .then(async (transactions) => {
            console.log('Running job to process transactions');
            for (let transaction of transactions) {
                await getPayout(transaction.metadata.payoutId)
                    .then(async (data) => {
                        if (transaction.status !== data.status) {
                            const session = await mongoose.startSession();
                            await session.startTransaction();
                            transaction.status = data.status;
                            if (data.status === constants.PROCESSED) {
                                transaction.metadata.description = 'Amount successfully transferred to your account.';
                            }

                            return await transaction
                                .save({ session })
                                .then(async () => {
                                    if ([constants.CANCELLED, constants.REVERSED, constants.FAILED].includes(transaction.status)) {
                                        return await middlewares
                                            .refundMoneyToWallet(
                                                transaction.influencer,
                                                transaction.amount,
                                                transaction._id,
                                                data.status,
                                                session
                                            )
                                            .then(async () => {
                                                await session.commitTransaction();
                                                session.endSession();
                                            })
                                            .catch(async (error) => {
                                                console.log(error);
                                                await session.abortTransaction();
                                                session.endSession();
                                            });
                                    } else {
                                        await session.commitTransaction();
                                        session.endSession();
                                    }
                                })
                                .catch(async (error) => {
                                    console.log(error);
                                    await session.abortTransaction();
                                });
                        }
                    })
                    .catch(async (error) => {
                        console.log(error);
                    });
            }
        })
        .catch((error) => {
            console.log(error);
        });
};

module.exports = middlewares;
