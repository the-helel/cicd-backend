const Wallet = require('../../../models/wallet/wallet');
const KYC = require('../../../models/wallet/kyc');

const constants = require('../../../constants');

const middlewares = {};

/**
 * Returns the status of KYC application.
 */
middlewares.getKYCStatus = (req, res, next) => {
    Wallet.findOne({ influencer: req.user })
        .lean()
        .then((wallet) => {
            if (wallet != null) {
                if (wallet.kyc == null) {
                    res.status(200).json({ error: false, status: constants.PENDING });
                } else {
                    KYC.findById(wallet.kyc)
                        .select({ status: 1 })
                        .lean()
                        .then((kyc) => {
                            if (kyc != null) {
                                res.status(200).json({ error: false, status: kyc.status });
                            } else {
                                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('KYC')] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                }
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Wallet')] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

/**
 * User uploads all the required documents and apply for KYC.
 */
middlewares.addKYC = (req, res, next) => {
    Wallet.findOne({ influencer: req.user })
        .then((wallet) => {
            if (wallet != null) {
                const kyc = new KYC();
                kyc.name = req.body.documentName;
                kyc.uid = req.body.uid;
                kyc.influencer = req.user;
                kyc.document = [];

                for (let file of req.files.documents) {
                    kyc.document.push(file.location);
                }
                kyc.panNumber = req.body.panNumber;
                kyc.panCard = [];
                for (let file of req.files.panCard) {
                    kyc.panCard.push(file.location);
                }
                kyc.save()
                    .then((kycRecord) => {
                        wallet.kyc = kycRecord._id;
                        wallet
                            .save()
                            .then(() => {
                                res.status(200).json({ error: false, message: [constants.SUCCESS] });
                            })
                            .catch((error) => {
                                res.status(500).json({ error: true, message: [error.message] });
                            });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            }
        })
        .catch((error) => {
            console.log(req.body);
            console.log(error.message);
            res.status(500).json({ error: true, message: [error.message] });
        });
};

module.exports = middlewares;
