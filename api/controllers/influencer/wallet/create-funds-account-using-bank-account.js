const { axiosInstance } = require('../../../../payment/razorpay-payment.js');

/**
 * Find more about API on: {@url https://razorpay.com/docs/razorpayx/api/fund-accounts/}
 * 
 * @param {*} contactId The contact id after creating the Razorpay contact using api.
 * @param {*} name The account holder name
 * @param {*} ifsc Bank IFSC code
 * @param {*} accountNumber Bank account number
 * @returns 
 * 
 * {
    "id" : "fa_00000000000001",
    "entity": "fund_account",
    "contact_id" : "cont_00000000000001",
    "account_type": "bank_account",
    "bank_account": {
        "ifsc": "HDFC0000053",
        "bank_name": "HDFC Bank",
        "name": "Gaurav Kumar",
        "account_number": "765432123456789",
        "notes": []
    },
    "active": true,
    "batch_id": null,
    "created_at": 1543650891
    }
 * 
 */
const createFundsAccountUsingBankAccount = (contactId, name, ifsc, accountNumber) => {
    return axiosInstance
        .post('/fund_accounts', {
            contact_id: contactId,
            account_type: 'bank_account',
            bank_account: {
                name: name,
                ifsc: ifsc,
                account_number: accountNumber,
            },
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error.response.data.error.description);
        });
};

module.exports = createFundsAccountUsingBankAccount;
