const { axiosInstance } = require('../../../../payment/razorpay-payment.js');

/**
 * Find more about API on: {@url https://razorpay.com/docs/razorpayx/api/fund-accounts/#vpa-upi-id}
 * 
 * @param {*} contactId The contact id after creating the Razorpay contact using api.
 * @param {*} upiId The account holder name
 * @returns 
 * 
 * 
 {
    "id": "fa_00000000000002",
    "entity": "fund_account",
    "contact_id": "cont_00000000000001",
    "account_type": "vpa",
    "vpa": {
        "username": "gaurav.kumar",
        "handle": "exampleupi",
        "address": "gaurav.kumar@exampleupi"
    },
    "active": true,
    "batch_id": null,
    "created_at": 1545223741
 }
 * 
 */
const createFundsAccountUsingUPIAccount = (contactId, upiId) => {
    return axiosInstance
        .post('/fund_accounts', {
            contact_id: contactId,
            account_type: 'vpa',
            vpa: {
                address: upiId,
            },
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error.response.data.error.description);
        });
};

module.exports = createFundsAccountUsingUPIAccount;
