const { validationResult } = require('express-validator');

const Video = require('../../models/learn/video');
const BookmarkVideo = require('../../models/learn/bookmark-video');

const constants = require('../../constants');

const middlewares = {};

middlewares.getVideos = async (req, res, next) => {
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;
    let sortBy = {};
    if (req.query.sortBy) {
        sortBy[req.query.sortBy] = -1;
    } else {
        sortBy['createdAt'] = -1;
    }

    let bookmarkedVideosList = [];
    await BookmarkVideo.findOne({ influencer: req.user })
        .lean()
        .then((bookmarkedVideos) => {
            if (bookmarkedVideos != null) bookmarkedVideosList = bookmarkedVideos.videos;
        });

    for (let i = 0; i < bookmarkedVideosList.length; ++i) {
        bookmarkedVideosList[i] = '' + bookmarkedVideosList[i];
    }

    Video.find()
        .skip(size * (pageNo - 1))
        .limit(size)
        .sort(sortBy)
        .lean()
        .then((videos) => {
            for (let index in videos) {
                videos[index].isBookmarked = bookmarkedVideosList.includes('' + videos[index]._id);
            }
            res.status(200).json({ error: false, result: videos });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getBookmarkedVideos = (req, res, next) => {
    const bookmarkedVideosList = [];
    BookmarkVideo.findOne({ influencer: req.user })
        .lean()
        .then(async (bookmarkedVideos) => {
            if (bookmarkedVideos != null) {
                for (let videoId of bookmarkedVideos.videos) {
                    await Video.findById(videoId)
                        .then((video) => {
                            if (video != null) {
                                bookmarkedVideosList.push(video);
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            return;
                        });
                }
                res.status(200).json({ error: false, result: bookmarkedVideosList });
            } else {
                res.status(200).json({ error: false, result: [] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.bookmarkVideo = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Video.findById(req.body.id).then((video) => {
            if (video != null) {
                BookmarkVideo.findOne({ influencer: req.user })
                    .then((bookmarkedVideos) => {
                        let bookmarkedVideosList = null;
                        if (bookmarkedVideos != null) {
                            bookmarkedVideosList = bookmarkedVideos;
                        } else {
                            bookmarkedVideosList = new BookmarkVideo();
                            bookmarkedVideosList.influencer = req.user;
                            bookmarkedVideosList.videos = [];
                        }
                        bookmarkedVideosList.videos.push(req.body.id);
                        bookmarkedVideosList
                            .save()
                            .then(() => {
                                res.status(200).json({ error: false, message: [constants.SUCCESS] });
                            })
                            .catch((error) => {
                                res.status(500).json({ error: true, message: [error.message] });
                            });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            } else {
                res.status(400).json({ error: true, message: ['You cannot bookmark this video.'] });
            }
        });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.removeBookmarkedVideo = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        BookmarkVideo.findOne({ influencer: req.user })
            .then((bookmarkedVideos) => {
                if (bookmarkedVideos != null) {
                    const videoIndex = bookmarkedVideos.videos.indexOf(req.query.id);
                    bookmarkedVideos.videos.splice(videoIndex, 1);
                    bookmarkedVideos
                        .save()
                        .then(() => {
                            res.status(200).json({ error: false, message: [constants.SUCCESS] });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Bookmarked videos')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
