const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const CashApplicant = require('../../../models/campaign/cash-campaign/cash-campaign-applicant');
const SavedCashCampaigns = require('../../../models/campaign/cash-campaign/saved-cash-campaigns');
const SavedProductCampaigns = require('../../../models/campaign/product-campaign/saved-product-campaigns');
const { fetchCampaign } = require('../../campaign/fetch-campaign');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getCampaignsByStatus = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        let applicantsList = [];
        await CashApplicant.aggregate([
            { $match: { influencer: mongoose.Types.ObjectId(req.user), status: req.query.status } },
            { $addFields: { type: constants.CASH_CAMPAIGN } },
            {
                $unionWith: {
                    coll: 'product-campaign-applicants',
                    pipeline: [
                        { $match: { influencer: mongoose.Types.ObjectId(req.user), status: req.query.status } },
                        { $addFields: { type: constants.PRODUCT_CAMPAIGN } },
                    ],
                },
            },
        ])
            .then((applicants) => {
                applicantsList = applicants;
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });

        let savedCashCampaigns = [];
        let savedProductCampaigns = [];
        await SavedCashCampaigns.find({ influencer: req.user })
            .lean()
            .then(async (savedCampaigns) => {
                if (savedCampaigns) savedCashCampaigns = savedCampaigns;
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
        await SavedProductCampaigns.find({ influencer: req.user })
            .lean()
            .then(async (savedCampaigns) => {
                if (savedCampaigns) savedProductCampaigns = savedCampaigns;
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });

        const campaigns = [];
        for (let applicant of applicantsList) {
            await fetchCampaign(
                applicant.type,
                applicant.campaign,
                applicant.type == constants.CASH_CAMPAIGN
                    ? savedCashCampaigns.includes(applicant.campaign)
                    : savedProductCampaigns.includes(applicant.campaign)
            )
                .then((result) => {
                    campaigns.push(result);
                    return;
                })
                .catch((error) => {
                    return;
                });
        }
        res.status(200).json({ error: false, result: campaigns });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
