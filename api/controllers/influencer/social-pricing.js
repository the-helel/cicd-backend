/**
 * Middleware for setting up social platform pricing.
 */

const { validationResult } = require('express-validator');

const SocialPricing = require('../../models/social/social-pricing');
const Influencer = require('../../models/influencer/influencer');
const constants = require('../../constants');

const middlewares = {};

/**
 * Set social pricing
 */

middlewares.setSocialPricing = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        try {
            let user = await Influencer.findById(req.user);

            if (user === null) {
                return res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')] });
            }

            let socialPricing = await SocialPricing.findOne({ influencer: req.user });

            if (socialPricing === null) {
                socialPricing = new SocialPricing({ influencer: req.user, ...req.body });
            } else {
                let instagramPrices = socialPricing.instagram;
                socialPricing.instagram = { ...instagramPrices, ...req.body.instagram };
                // TODO: Add more social profiles here
            }

            await socialPricing.save();

            return res.status(200).json({ error: false, message: [constants.SUCCESS] });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    } else {
        return res.status(400).json({ error: true, message: errors.array() });
    }
};

/**
 * Get Social Pricing of user
 */

middlewares.getSocialPricing = async (req, res, next) => {
    try {
        let socialPricing = await SocialPricing.findOne({ influencer: req.user }).lean();

        if (socialPricing === null) {
            return res.status(404).json({ error: true, message: ['Pricing have not been saved yet'] });
        }

        return res.status(200).json({ error: true, result: socialPricing });
    } catch (err) {
        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
    }
};

module.exports = middlewares;
