const { validationResult } = require('express-validator');

const { fetchCampaign } = require('../../campaign/fetch-campaign');

const SavedCampaigns = require('../../../models/campaign/cash-campaign/saved-cash-campaigns');
const Applicant = require('../../../models/campaign/cash-campaign/cash-campaign-applicant');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getInfluencerCampaigns = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        SavedCampaigns.findOne({ influencer: req.user })
            .lean()
            .then(async (savedCampaigns) => {
                if (savedCampaigns != null) {
                    for (let campaignId of influencer.campaigns) {
                        const campaigns = [];
                        await Applicant.findOne({ campaign: campaignId, influencer: req.user })
                            .lean()
                            .then(async (applicant) => {
                                if (applicant != null) {
                                    if (applicant.status == req.query.status) {
                                        await fetchCampaign(
                                            constants.CASH_CAMPAIGN,
                                            campaignId,
                                            savedCampaigns.savedCampaigns.includes(campaignId)
                                        )
                                            .then((result) => {
                                                campaigns.push(result);
                                                return;
                                            })
                                            .catch((error) => {
                                                console.log(error);
                                                return;
                                            });
                                    }
                                } else {
                                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                                }
                            })
                            .catch((error) => {
                                res.status(500).json({ error: true, message: [error.message] });
                            });
                        res.status(200).json({ error: false, result: campaigns });
                    }
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR(constants.INFLUENCER)] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
