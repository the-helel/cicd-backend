const { validationResult } = require('express-validator');

const { fetchCampaign } = require('../../../campaign/fetch-campaign');

const ProductCampaign = require('../../../../models/campaign/product-campaign/product-campaign');
const SavedProductCampaigns = require('../../../../models/campaign/product-campaign/saved-product-campaigns');

const constants = require('../../../../constants');

const middlewares = {};

middlewares.getCampaignById = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        ProductCampaign.findById(req.query.campaign)
            .lean()
            .then((campaign) => {
                if (campaign != null) {
                    SavedProductCampaigns.findOne({ influencer: req.user })
                        .lean()
                        .then((savedCampaigns) => {
                            let savedCampaignsList = [];
                            if (savedCampaigns) {
                                savedCampaignsList = savedCampaigns.savedCampaigns;
                            }
                            fetchCampaign(constants.PRODUCT_CAMPAIGN, campaign._id, savedCampaignsList.includes(campaign._id))
                                .then((result) => {
                                    res.status(200).json({ error: false, result: result });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: error[0] });
                                });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Campaign')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
