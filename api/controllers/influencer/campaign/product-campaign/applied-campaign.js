const { validationResult } = require('express-validator');

const Applicant = require('../../../../models/campaign/product-campaign/product-campaign-applicant');

const constants = require('../../../../constants');

const middlewares = {};

middlewares.isAppliedCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Applicant.findOne({ influencer: req.user, campaign: req.query.campaign })
            .select({ _id: 1 })
            .lean()
            .then((applicant) => {
                if (applicant != null) {
                    res.status(200).json({ error: false, result: true });
                } else {
                    res.status(200).json({ error: false, result: false });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.getApplicant = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Applicant.findOne({ influencer: req.user, campaign: req.query.campaign })
            .lean()
            .then((applicant) => {
                if (applicant != null) {
                    res.status(200).json({ error: false, result: applicant });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Applicant')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
