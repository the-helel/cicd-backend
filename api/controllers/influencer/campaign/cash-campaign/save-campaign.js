const { validationResult } = require('express-validator');

const SavedCampaigns = require('../../../../models/campaign/cash-campaign/saved-cash-campaigns');
const Campaign = require('../../../../models/campaign/cash-campaign/cash-campaign');

const constants = require('../../../../constants');

const middlewares = {};

middlewares.saveCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Campaign.findById(req.body.id)
            .lean()
            .then((campaign) => {
                if (campaign.status === constants.ACTIVE) {
                    SavedCampaigns.findOne({ influencer: req.user })
                        .then((savedCampaigns) => {
                            let savedCampaignsList = null;
                            if (savedCampaigns != null) {
                                savedCampaignsList = savedCampaigns;
                            } else {
                                savedCampaignsList = new SavedCampaigns();
                                savedCampaignsList.influencer = req.user;
                                savedCampaignsList.savedCampaigns = [];
                            }
                            savedCampaignsList.savedCampaigns.push(req.body.id);
                            savedCampaignsList
                                .save()
                                .then(() => {
                                    res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error.message] });
                                });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: ['You cannot save this campaign.'] });
                }
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.removeSavedCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        SavedCampaigns.findOne({ influencer: req.user })
            .then((savedCampaigns) => {
                if (savedCampaigns != null) {
                    const campaignIndex = savedCampaigns.savedCampaigns.indexOf(req.query.id);
                    savedCampaigns.savedCampaigns.splice(campaignIndex, 1);
                    savedCampaigns
                        .save()
                        .then(() => {
                            res.status(200).json({ error: false, message: [constants.SUCCESS] });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Saved campaigns')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
