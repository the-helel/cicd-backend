const CashCampaign = require('../../../models/campaign/cash-campaign/cash-campaign');
const SavedCampaigns = require('../../../models/campaign/cash-campaign/saved-cash-campaigns');
const Brand = require('../../../models/company/brand');

const { fetchCampaignData } = require('../../campaign/fetch-campaign');
const Company = require('../../../models/company/company');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getSubscribedBrandsCampaigns = (req, res, next) => {
    SavedCampaigns.findOne({ influencer: req.user })
        .lean()
        .then((savedCampaigns) => {
            Brand.find({ subscribedBy: req.user })
                .lean()
                .then(async (brands) => {
                    const subscribedBrandsCampaigns = [];
                    for (const brand of brands) {
                        for (const campaignId of brand.campaigns) {
                            await CashCampaign.findById(campaignId)
                                .lean()
                                .then(async (campaign) => {
                                    if (campaign.status === constants.ACTIVE) {
                                        await Company.findById(campaign.company)
                                            .lean()
                                            .then(async (company) => {
                                                if (savedCampaigns.savedCampaigns.includes(campaignId)) {
                                                    subscribedBrandsCampaigns.push(await fetchCampaignData(campaign, brand, company, true));
                                                } else {
                                                    subscribedBrandsCampaigns.push(
                                                        await fetchCampaignData(campaign, brand, company, false)
                                                    );
                                                }
                                            })
                                            .catch((error) => {
                                                console.log(error.message);
                                            });
                                    }
                                })
                                .catch((error) => {
                                    console.log(error.message);
                                });
                        }
                    }
                    res.status(200).json({ error: false, result: subscribedBrandsCampaigns });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

module.exports = middlewares;
