const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const { fetchCampaign } = require('../../campaign/fetch-campaign');

const Category = require('../../../models/category');
const Brand = require('../../../models/company/brand');
const CashCampaign = require('../../../models/campaign/cash-campaign/cash-campaign');
const SavedCashCampaigns = require('../../../models/campaign/cash-campaign/saved-cash-campaigns');
const CashApplicant = require('../../../models/campaign/cash-campaign/cash-campaign-applicant');
const ProductCampaign = require('../../../models/campaign/product-campaign/product-campaign');
const SavedProductCampaigns = require('../../../models/campaign/product-campaign/saved-product-campaigns');
const ProductApplicant = require('../../../models/campaign/product-campaign/product-campaign-applicant');

const Campaigns = require('../../../models/campaign/campaign');
const Influencers = require('../../../models/influencer/influencer');
const Applicants = require('../../../models/campaign/applicant');

const constants = require('../../../constants');
const { setApplicantFields } = require('../../../services/campaign/utils.js');
const { AddToBookmarkCampaign, ListCampaigns } = require('../../../services/campaign');

const middlewares = {};

// function to fetch campaigns for influencers with filtering, pagination and sorting features
middlewares.getAllCampaigns = async (req, res, next) => {
    // getting all the parameters
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;
    let sortBy = {};
    if (req.query.sortBy) {
        sortBy[req.query.sortBy] = -1;
    } else {
        sortBy['startDate'] = -1;
    }

    let categories = req.query.categories ? req.query.categories : [];
    for (let id in categories) {
        categories[id] = mongoose.Types.ObjectId(categories[id]);
    }

    if (pageNo <= 0)
        return res.status(400).send({
            error: true,
            message: ['Invalid page number, should start with 1'],
        });

    const appliedCashCampaigns = [];
    const appliedProductCampaigns = [];

    /// Get all the campaigns applied by user and filter them from the campaign list.
    await CashApplicant.find({
        influencer: req.user,
    })
        .lean()
        .then((applicants) => {
            for (const applicant of applicants) {
                appliedCashCampaigns.push(mongoose.Types.ObjectId(applicant.campaign));
            }
        })
        .catch((error) => {
            console.log(error);
        });

    await ProductApplicant.find({
        influencer: req.user,
    })
        .lean()
        .then((applicants) => {
            for (const applicant of applicants) {
                appliedProductCampaigns.push(mongoose.Types.ObjectId(applicant.campaign));
            }
        })
        .catch((error) => {
            console.log(error);
        });

    let cashQueryPublic;
    let productQueryPublic;
    let cashQueryPrivate;
    let productQueryPrivate;
    if (categories.length == 0) {
        cashQueryPublic = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedCashCampaigns,
            },
            inviteOnly: false,
        };
        cashQueryPrivate = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedCashCampaigns,
            },
            inviteOnly: true,
            influencers: {
                $in: [req.user],
            },
        };
        productQueryPublic = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedProductCampaigns,
            },
            inviteOnly: false,
        };
        productQueryPrivate = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedProductCampaigns,
            },
            inviteOnly: true,
            influencers: {
                $in: [req.user],
            },
        };
    } else {
        cashQueryPublic = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedCashCampaigns,
            },
            inviteOnly: false,
            'criteria.categories': {
                $in: categories,
            },
        };
        cashQueryPrivate = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedCashCampaigns,
            },
            inviteOnly: true,
            influencers: {
                $in: [req.user],
            },
            'criteria.categories': {
                $in: categories,
            },
        };
        productQueryPublic = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedProductCampaigns,
            },
            inviteOnly: false,
            'criteria.categories': {
                $in: categories,
            },
        };
        productQueryPrivate = {
            status: constants.ACTIVE,
            _id: {
                $nin: appliedProductCampaigns,
            },
            inviteOnly: true,
            influencers: {
                $in: [req.user],
            },
            'criteria.categories': {
                $in: categories,
            },
        };
    }

    let cashQueryPipeline = [
        {
            $match: cashQueryPublic,
        },
        {
            $lookup: {
                from: 'cash-campaign-applicants',
                localField: '_id',
                foreignField: 'campaign',
                as: 'applicants',
            },
        },
        {
            $unionWith: {
                coll: 'cash-campaigns',
                pipeline: [
                    {
                        $match: cashQueryPrivate,
                    },
                    {
                        $lookup: {
                            from: 'cash-campaign-applicants',
                            localField: '_id',
                            foreignField: 'campaign',
                            as: 'applicants',
                        },
                    },
                ],
            },
        },
        {
            $lookup: {
                from: 'saved-cash-campaigns',
                let: {
                    influencer_id: mongoose.Types.ObjectId(req.user),
                    campaign_id: '$_id',
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    {
                                        $eq: ['$influencer', '$$influencer_id'],
                                    },
                                    {
                                        $in: ['$$campaign_id', '$savedCampaigns'],
                                    },
                                ],
                            },
                        },
                    },
                    {
                        $project: {
                            influencer: 1,
                            _id: 0,
                        },
                    },
                ],
                as: 'savedCampaigns',
            },
        },
        {
            $addFields: {
                type: constants.CASH_CAMPAIGN,
                applicantCount: {
                    $size: '$applicants',
                },
            },
        },
        {
            $project: {
                _id: 1,
                type: 1,
                applicantCount: 1,
                createdAt: 1,
                updatedAt: 1,
                startDate: 1,
                endDate: 1,
                isSaved: {
                    $in: [
                        {
                            influencer: mongoose.Types.ObjectId(req.user),
                        },
                        '$savedCampaigns',
                    ],
                },
            },
        },
    ];

    let productQueryPipeline = [
        {
            $match: productQueryPublic,
        },
        {
            $lookup: {
                from: 'product-campaign-applicants',
                localField: '_id',
                foreignField: 'campaign',
                as: 'applicants',
            },
        },
        {
            $unionWith: {
                coll: 'product-campaigns',
                pipeline: [
                    {
                        $match: productQueryPrivate,
                    },
                    {
                        $lookup: {
                            from: 'product-campaign-applicants',
                            localField: '_id',
                            foreignField: 'campaign',
                            as: 'applicants',
                        },
                    },
                ],
            },
        },
        {
            $lookup: {
                from: 'saved-product-campaigns',
                let: {
                    influencer_id: mongoose.Types.ObjectId(req.user),
                    campaign_id: '$_id',
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    {
                                        $eq: ['$influencer', '$$influencer_id'],
                                    },
                                    {
                                        $in: ['$$campaign_id', '$savedCampaigns'],
                                    },
                                ],
                            },
                        },
                    },
                    {
                        $project: {
                            influencer: 1,
                            _id: 0,
                        },
                    },
                ],
                as: 'savedCampaigns',
            },
        },
        {
            $addFields: {
                type: constants.PRODUCT_CAMPAIGN,
                applicantCount: {
                    $size: '$applicants',
                },
            },
        },
        {
            $project: {
                _id: 1,
                type: 1,
                applicantCount: 1,
                createdAt: 1,
                updatedAt: 1,
                startDate: 1,
                endDate: 1,
                isSaved: {
                    $in: [
                        {
                            influencer: mongoose.Types.ObjectId(req.user),
                        },
                        '$savedCampaigns',
                    ],
                },
            },
        },
    ];

    let query = [
        ...cashQueryPipeline,
        {
            $unionWith: {
                coll: 'product-campaigns',
                pipeline: [...productQueryPipeline],
            },
        },
    ];
    let CampaignModel = CashCampaign;

    if (req.query.campaignType == constants.CASH_CAMPAIGN) {
        query = [...cashQueryPipeline];
    } else if (req.query.campaignType == constants.PRODUCT_CAMPAIGN) {
        query = [...productQueryPipeline];
        CampaignModel = ProductCampaign;
    }

    await CampaignModel.aggregate(query)
        .skip(size * (pageNo - 1))
        .limit(size)
        .sort(sortBy)
        .exec(async (err, campaigns) => {
            if (err)
                return res.status(400).send({
                    error: true,
                    message: [err.message],
                });
            else {
                const resultCampaigns = [];
                for (const campaign of campaigns) {
                    await fetchCampaign(campaign.type, campaign._id, campaign.isSaved)
                        .then((result) => {
                            resultCampaigns.push(result);
                            return;
                        })
                        .catch((error) => {
                            console.log(error[0]);
                            return;
                        });
                }
                return res.status(200).send({
                    error: false,
                    result: resultCampaigns,
                });
            }
        });
};

// function to search for campaigns
middlewares.searchCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;
        const categoryPage = req.query.categorypage ? parseInt(req.query.categorypage) : 1;
        const categorySize = req.query.categorysize ? parseInt(req.query.categorysize) : 10;
        const brandPage = req.query.brandpage ? parseInt(req.query.brandpage) : 1;
        const brandSize = req.query.brandsize ? parseInt(req.query.brandsize) : 20;

        if (pageNo <= 0)
            return res.status(400).send({
                error: true,
                message: ['Invalid page number, should start with 1'],
            });

        Category.find({
            name: {
                $regex: req.query.query,
                $options: 'i',
            },
        })
            .select({
                _id: 1,
                name: 1,
            })
            .skip(categorySize * (categoryPage - 1))
            .limit(categorySize)
            .exec((err, validCategories) => {
                if (err)
                    return res.status(500).send({
                        error: true,
                        message: [err.message],
                    });
                const validCategoriesList = validCategories.map((category) => category._id);

                Brand.find({
                    name: {
                        $regex: req.query.query,
                        $options: 'i',
                    },
                })
                    .select({
                        _id: 1,
                        name: 1,
                    })
                    .skip(brandSize * (brandPage - 1))
                    .limit(brandSize)
                    .exec((err, validBrands) => {
                        if (err)
                            return res.status(500).send({
                                error: true,
                                message: [err.message],
                            });
                        const validBrandsList = validBrands.map((brand) => brand._id);

                        CashCampaign.find({
                            status: constants.ACTIVE,
                            $or: [
                                {
                                    'brief.title': {
                                        $regex: req.query.query,
                                        $options: 'i',
                                    },
                                },
                                {
                                    brand: {
                                        $in: validBrandsList,
                                    },
                                },
                                {
                                    'criteria.categories': {
                                        $all: validCategoriesList,
                                    },
                                },
                            ],
                        })
                            .limit(size)
                            .exec((err, campaigns) => {
                                if (err)
                                    return res.status(500).send({
                                        error: true,
                                        message: [err.message],
                                    });

                                return res.status(200).send({
                                    error: false,
                                    result: {
                                        campaigns: campaigns,
                                    },
                                });
                            });
                    });
            });
    } else {
        return res.status(400).send({
            error: true,
            message: errors.array(),
        });
    }
};

// functions to get all the campaigns of a specific brand of the user
middlewares.getCampaignsByBrandId = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = constants.PAGE_SIZE;
        const sortBy = req.query.sortBy ? req.query.sortBy : 'startedAt';

        let savedCashCampaignsList = [];
        let savedProductCampaignsList = [];
        await SavedCashCampaigns.findOne({
            influencer: req.user,
        })
            .lean()
            .then(async (savedCampaigns) => {
                if (savedCampaigns) {
                    savedCashCampaignsList = savedCampaigns.savedCampaigns;
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error.message],
                });
            });
        await SavedProductCampaigns.findOne({
            influencer: req.user,
        })
            .lean()
            .then(async (savedCampaigns) => {
                if (savedCampaigns) {
                    savedProductCampaignsList = savedCampaigns.savedCampaigns;
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error.message],
                });
            });

        await CashCampaign.aggregate([
            {
                $match: {
                    brand: mongoose.Types.ObjectId(req.query.id),
                    status: constants.ACTIVE,
                },
            },
            {
                $addFields: {
                    type: constants.CASH_CAMPAIGN,
                },
            },
            {
                $project: {
                    _id: 1,
                    type: 1,
                },
            },
            {
                $unionWith: {
                    coll: 'product-campaigns',
                    pipeline: [
                        {
                            $match: {
                                brand: mongoose.Types.ObjectId(req.query.id),
                                status: constants.ACTIVE,
                            },
                        },
                        {
                            $addFields: {
                                type: constants.PRODUCT_CAMPAIGN,
                            },
                        },
                        {
                            $project: {
                                _id: 1,
                                type: 1,
                            },
                        },
                    ],
                },
            },
        ])
            .skip(size * (pageNo - 1))
            .limit(size)
            .sort(sortBy)
            .then(async (campaigns) => {
                let campaignsList = [];
                for (const campaign of campaigns) {
                    await fetchCampaign(
                        campaign.type,
                        campaign._id,
                        campaign.type == constants.PRODUCT_CAMPAIGN
                            ? savedProductCampaignsList.includes(campaign._id)
                            : savedCashCampaignsList.includes(campaign._id)
                    )
                        .then((result) => {
                            campaignsList.push(result);
                            return;
                        })
                        .catch((error) => {
                            console.log(error[0]);
                            return;
                        });
                }
                res.status(200).json({
                    error: false,
                    result: campaignsList,
                });
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error.message],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.getSavedCampaigns = async (req, res, next) => {
    const savedCampaignsList = [];

    let error = await SavedCashCampaigns.findOne({
        influencer: req.user,
    })
        .lean()
        .then(async (savedCampaigns) => {
            if (savedCampaigns != null) {
                for (let campaignId of savedCampaigns.savedCampaigns) {
                    await fetchCampaign(constants.CASH_CAMPAIGN, campaignId, true)
                        .then((result) => {
                            savedCampaignsList.push(result);
                            return null;
                        })
                        .catch((error) => {
                            console.log(error[0]);
                            return Promise.reject(error);
                        });
                }
            }
        })
        .catch((error) => {
            return Promise.reject(error);
        });
    if (error != null) {
        return res.status(500).json({
            error: true,
            message: [error],
        });
    }

    error = await SavedProductCampaigns.findOne({
        influencer: req.user,
    })
        .lean()
        .then(async (savedCampaigns) => {
            if (savedCampaigns != null) {
                for (let campaignId of savedCampaigns.savedCampaigns) {
                    await fetchCampaign(constants.PRODUCT_CAMPAIGN, campaignId, true)
                        .then((result) => {
                            savedCampaignsList.push(result);
                            return null;
                        })
                        .catch((error) => {
                            console.log(error[0]);
                            return Promise.reject(error);
                        });
                }
            }
        })
        .catch((error) => {
            return Promise.reject(error);
        });
    if (error != null) {
        return res.status(500).json({
            error: true,
            message: [error],
        });
    }

    return res.status(200).json({
        error: false,
        result: savedCampaignsList,
    });
};

//New controllers method
/**
 * List all campaigns for influencers
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
middlewares.getAllCompanyCampaigns = async (req, res, next) => {
    try {
        await ListCampaigns(req.query, req.params, req.user, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            } else {
                return res.status(200).json(result);
            }
        });
    } catch (e) {}
};

/**
 * Bookmark the campaign by ID. The campaignId is passed in query params.
 *
 * @param req
 * @returns {Promise<void>}
 */
middlewares.bookmarkCampaign = async (req, res, next) => {
    try {
        await AddToBookmarkCampaign(req.body.campaignId, req.user, req.body.isBookmarked, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            }
            return res.status(200).json(result);
        });
    } catch (e) {
        res.status(400).json({
            error: true,
            message: constants.SERVER_ERR,
        });
    }
};

middlewares.getBookmarkedCampaigns = async (req, res, next) => {
    try {
        const { page } = req.query;
        await GetBookmarkedCampaign(req.user, page, (err, result) => {
            if (err) {
                return res.status(400).json(err);
            }
            return res.status(200).json(result);
        });
    } catch (e) {
        return res.status(400).json({
            error: true,
            message: constants.SERVER_ERR,
        });
    }
};

middlewares.getCompanyCampaignById = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    } else {
        // checking if the user is the owner of the request campaign
        Influencers.findById(req.user, (err, user) => {
            if (err)
                return res.status(500).send({
                    error: true,
                    message: [err.message],
                });
            if (!user)
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to perform this operation'],
                });

            // checking if the campaign id exists and returns campaign object if it exists
            if (req.params.campaignId) {
                Campaigns.findById(req.params.campaignId)
                    .populate('brief.categories')
                    .then((campaign) => {
                        if (campaign == null) {
                            return res.status(400).send({
                                error: true,
                                message: ['Campaign not found'],
                            });
                        }
                        return res.status(200).json({
                            error: false,
                            message: constants.SUCCESS,
                            data: campaign,
                        });
                    })
                    .catch((err) => {
                        return res.status(500).send({
                            error: true,
                            message: [err.message],
                        });
                    });
            }
            // Campaigns.findById(req.params.campaignId, (err, campaign) => {
            //     if (err)
            //         return res.status(500).send({
            //             error: true,
            //             message: [err.message],
            //         });
            //     if (!campaign)
            //         return res.status(400).send({
            //             error: true,
            //             message: [constants.RESOURCE_NOT_FOUND_ERROR('campaign')],
            //         });
            //     return res.status(200).json({
            //         error: false,
            //         message: 'success',
            //         data: campaign,
            //     });
            // });
            else {
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to access this campaign'],
                });
            }
        });
    }
};

/**
 * Get campaign applicant by influencer id and applicant id
 */
middlewares.getCampaignApplicantById = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    } else {
        // checking if the user is the owner of the request campaign
        Influencers.findById(req.user, (err, user) => {
            if (err)
                return res.status(500).send({
                    error: true,
                    message: [err.message],
                });
            if (!user)
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to perform this operation'],
                });

            // checking if the campaign id exists and returns campaign object if it exists
            if (req.params.applicantId) {
                Applicants.findById(req.params.applicantId, (err, applicant) => {
                    if (err)
                        return res.status(500).send({
                            error: true,
                            message: [err.message],
                        });
                    if (!applicant)
                        return res.status(400).send({
                            error: true,
                            message: [constants.RESOURCE_NOT_FOUND_ERROR('applicant')],
                        });

                    return res.status(200).json({
                        error: false,
                        message: 'success',
                        data: applicant,
                    });
                });
            } else {
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to access this campaign'],
                });
            }
        });
    }
};

middlewares.getApplicantByCampaignId = async (req, res, next) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
        res.status(400).json({
            error: true,
            message: error.array(),
        });
    } else {
        const { campaignId } = req.params;
        // checking if the user is the owner of the request campaign
        Influencers.findById(req.user, async (err, user) => {
            if (err)
                return res.status(500).send({
                    error: true,
                    message: [err.message],
                });
            if (!user)
                return res.status(400).send({
                    error: true,
                    message: ['Not authorized to perform this operation'],
                });

            let deliverables = 0;
            // Get campaign tasks
            await Campaigns.findById(campaignId, (err, campaign) => {
                if (campaign !== null) {
                    if (campaign.brief.campaignPlatform === constants.INSTAGRAM) {
                        Object.keys(campaign.criteria.task.instagram).forEach((key) => {
                            deliverables += campaign.criteria.task.instagram[key];
                        });
                    }
                }
            });

            // checking if the campaign id exists and returns campaign object if it exists
            if (req.params.campaignId) {
                Applicants.findOne({
                    influencerId: req.user,
                    campaignId: campaignId,
                })
                    .populate({
                        path: 'link',
                        select: [
                            'media',
                            'like_count',
                            'comment_count',
                            'engagement',
                            'video_view_count',
                            'impressions',
                            'post_link',
                            'short_code',
                            'actual_reach',
                            'engagement_rate_by_impression',
                            'engagement_rate_by_followers',
                            'engagement_rate_by_reach',
                        ],
                    })
                    .exec(async (err, applicant) => {
                        if (err)
                            return res.status(500).send({
                                error: true,
                                message: [err.message],
                            });
                        if (!applicant)
                            return res.status(400).send({
                                error: true,
                                message: constants.RESOURCE_NOT_FOUND_ERROR('Applicant'),
                            });
                        applicant = await setApplicantFields(applicant, deliverables);

                        return res.status(200).json({
                            error: false,
                            message: 'success',
                            data: applicant,
                        });
                    });
            } else {
                return res.status(400).send({
                    error: true,
                    message: 'Not authorized to access this campaign',
                });
            }
        });
    }
};

middlewares.getCampaignByApplicantStatus = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        console.log(req.query.pageNo);
        let pageNo = req.query.pageNo || 1;
        const size = constants.PAGE_SIZE;

        let applicantsList = [];
        await Applicants.aggregate([
            {
                $match: {
                    influencerId: mongoose.Types.ObjectId(req.user),
                    status: req.query.status,
                },
            },
        ])
            .then((applicants) => {
                applicantsList = applicants;
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error.message],
                });
            });

        const campaigns = [];
        for (let applicant of applicantsList) {
            await Campaigns.findById(applicant.campaignId)
                .skip(size * (pageNo - 1))
                .limit(size)
                .populate({ path: 'brief.categories' })
                .lean()
                .then((campaign) => {
                    if (campaign != null) {
                        campaigns.push(campaign);
                    }
                });
        }
        res.status(200).json({
            error: false,
            result: campaigns,
        });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

module.exports = middlewares;
