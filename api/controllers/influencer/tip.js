const { validationResult } = require('express-validator');
const seedrandom = require('seedrandom');

const Tip = require('../../models/tip');
const TipClaps = require('../../models/tip-claps');

const constants = require('../../constants');

const middlewares = {};

middlewares.getTip = async (req, res, next) => {
    let count = null;

    await Tip.countDocuments()
        .then((c) => {
            count = c;
        })
        .catch((error) => {
            console.log(error);
        });

    if (count == null) {
        res.status(500).json({ error: true, result: [constants.SERVER_ERR] });
        return null;
    }

    if (count == 0) {
        res.status(400).json({ error: true, result: [constants.RESOURCE_NOT_FOUND_ERROR('Tip')] });
        return;
    }

    const rn = seedrandom(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()));
    const num = rn();
    const index = Math.ceil(num * (count - 1) + 1);

    Tip.find()
        .skip(index - 1)
        .limit(1)
        .lean()
        .then(async (tips) => {
            if (tips.length != 0) {
                await TipClaps.aggregate([
                    { $match: { tip: tips[0]._id } },
                    {
                        $group: {
                            _id: null,
                            claps: { $sum: '$claps' },
                        },
                    },
                ])
                    .then((group) => {
                        if (group.length != 0) res.status(200).json({ error: false, result: { tip: tips[0], claps: group[0].claps } });
                        else res.status(200).json({ error: false, result: { tip: tips[0], claps: 0 } });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, result: [error.message] });
                    });
            } else {
                res.status(400).json({ error: true, result: [constants.RESOURCE_NOT_FOUND_ERROR('Tip')] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, result: [error.message] });
        });
};

middlewares.addClaps = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const claps = parseInt(req.body.claps);
        if (claps > 0) {
            TipClaps.findOne({ influencer: req.user, tip: req.body.tip })
                .then((tipClaps) => {
                    let tipClapsObject = null;

                    if (tipClaps) {
                        tipClapsObject = tipClaps;
                    } else {
                        tipClapsObject = new TipClaps();
                        tipClapsObject.tip = req.body.tip;
                        tipClapsObject.influencer = req.user;
                        tipClapsObject.claps = 0;
                    }

                    tipClapsObject.claps += claps;

                    tipClapsObject
                        .save()
                        .then(() => {
                            res.status(200).json({ error: false, message: [constants.SUCCESS] });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: false, message: [error.message] });
                        });
                })
                .catch((error) => {
                    res.status(500).json({ error: false, message: [error.message] });
                });
        } else {
            res.status(400).json({ error: true, message: ['Claps must be greater than 0.'] });
        }
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
