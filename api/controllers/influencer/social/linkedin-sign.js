const express = require('express');
const config = require('../../../config');
const { validationResult } = require('express-validator');
const constants = require('../../../constants');
const axios = require('axios');

const scopes = 'r_liteprofile%20r_emailaddress%20w_member_social%20r_1st_connections_size';
const responseType = 'code';

const middleware = {};

/*
    Request - [GET] influencer/connect/linkedin
*/
middleware.linkedinSignIn = (req, res, next) => {
    // Perform a GET request to perform authorization on-behalf of the user
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        res.redirect(
            `https://www.linkedin.com/oauth/v2/authorization?response_type=${responseType}&client_id=${config.LINKEDIN_CLIENT_ID}&redirect_uri=${config.LINKEDIN_REDIRECT_URI}&scope=${scopes}`
        );
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

const exchangeLinkedinToken = (code) => {
    const url = `https://api.instagram.com/oauth/access_token`;

    const data = `client_id=${config.APP_ID}&client_secret=${config.APP_SECRET}&grant_type=authorization_code&redirect_uri=${config.REDIRECT_URI}&code=${code}`;

    return axios
        .post(url, data)
        .then((response) => {
            return getLongLivedAccessToken(response.data.access_token)
                .then((object) => {
                    object.userId = response.data.user_id;
                    return object;
                })
                .catch((error) => {
                    return Promise.reject(error);
                });
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = middleware;
