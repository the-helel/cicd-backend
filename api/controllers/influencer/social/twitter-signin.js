const express = require('express');
const config = require('../../../config');
const { validationResult } = require('express-validator');
const constants = require('../../../constants');
const axios = require('axios');
const Twitter = require('../../../models/social/twitter');

const middlewares = {};

/*
  Find more about this api here:  
  https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference/get-users-by-username-username
*/

const tweetFields = 'author_id,organic_metrics';
const userFields =
    'created_at,description,entities,id,location,name,pinned_tweet_id,profile_image_url,protected,public_metrics,url,username,verified,withheld';

const getTwitterUserData = (username) => {
    const url = `https://api.twitter.com/2/users/by/username/${username}?tweet.fields=${tweetFields}&user.fields=${userFields}`;
    return axios
        .get(url, {
            headers: {
                Authorization: `Bearer ${config.TWITTER_BEARER_TOKEN}`,
            },
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

middlewares.addTwitter = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        getTwitterUserData(req.body.username)
            .then((data) => {
                const userData = data.data;
                Twitter.findOne({
                    twitterId: userData.id,
                })
                    .then((instance) => {
                        if (instance) {
                            res.status(400).json({
                                error: true,
                                message: ['Account already in use.'],
                            });
                        } else {
                            Twitter.create({
                                influencer: req.user,
                                twitterId: userData.id,
                                twitterName: userData.username,
                                twitterScreenName: userData.name,
                                description: userData.description,
                                location: userData.location,
                                profilePicture: userData.profile_image_url,
                                url: userData.url,
                                isVerified: userData.verified,
                                followersCount: userData.public_metrics.followers_count,
                                followingsCount: userData.public_metrics.following_count,
                                tweetsCount: userData.public_metrics.tweet_count,
                            }).then(() => {
                                res.status(200).json({
                                    error: false,
                                    message: ['Account added successfully.'],
                                });
                            });
                        }
                    })
                    .catch((error) => {
                        res.status(500).json({
                            error: true,
                            message: [error.message],
                        });
                    });
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    } else {
        return res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.removeTwitter = async (req, res) => {
    try {
        Twitter.findOneAndDelete(
            {
                influencer: req.user,
            },
            (error, deleteRecord) => {
                if (error)
                    return res.status(500).json({
                        error: true,
                        message: [error],
                    });

                return res.status(200).json({
                    error: false,
                    message: [constants.SUCCESS],
                });
            }
        );
    } catch (error) {
        return res.status(500).json({
            error: true,
            message: [error],
        });
    }
};

module.exports = middlewares;
