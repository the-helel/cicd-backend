const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const Influencer = require('../../../models/influencer/influencer');
const Instagram = require('../../../models/social/instagram');

const exchangeInstagramToken = require('../../auth/instagram/exchange-instagram-token');
const getUsername = require('../../auth/instagram/get-username');

const config = require('../../../config');

const { setupInfluencer } = require('./create-influencer');

const constants = require('../../../constants');

const middlewares = {};

const assignInstagram = (instagramInstance, apiObject, username) => {
    instagramInstance.accessToken = apiObject.accessToken;
    instagramInstance.accessTokenExpiry = apiObject.expiry;
    instagramInstance.username = username;
};

middlewares.signinInstagram = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        exchangeInstagramToken(req.body.code)
            .then((instagramObject) => {
                // getProfile(instagram_object.userId, instagram_object.accessToken)
                //     .then((profile) => {
                getUsername(instagramObject.accessToken)
                    .then(async (username) => {
                        await Instagram.findOne({ userId: instagramObject.userId })
                            .then(async (instagram) => {
                                let registeredInfluencer = null;
                                let instagramInstance = instagram;

                                if (instagram) {
                                    await Influencer.findById(instagram.influencer)
                                        .then((influencer) => {
                                            if (influencer) {
                                                registeredInfluencer = influencer;
                                            } else {
                                                res.status(400).json({
                                                    error: true,
                                                    message: constants.RESOURCE_NOT_FOUND_ERROR('Influencer'),
                                                });
                                            }
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: true, message: [error.message] });
                                        });
                                } else {
                                    const influencer = new Influencer();

                                    instagramInstance = new Instagram();
                                    instagramInstance.influencer = influencer._id;
                                    instagramInstance.userId = req.body.userId;

                                    await setupInfluencer(influencer._id, req.body.referee)
                                        .then(() => {
                                            registeredInfluencer = influencer;
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: true, message: [error] });
                                        });
                                }

                                if (registeredInfluencer) {
                                    assignInstagram(instagramInstance, instagramObject, username);
                                    await registeredInfluencer
                                        .save()
                                        .then(async (influencer) => {
                                            await instagramInstance
                                                .save()
                                                .then(() => {
                                                    const jwtToken = jwt.sign(
                                                        {
                                                            userId: influencer._id,
                                                            userType: constants.INFLUENCER,
                                                        },
                                                        config.JWT_SECRET
                                                    );
                                                    res.status(200).json({
                                                        error: false,
                                                        result: { auth: true, token: jwtToken, userId: influencer._id },
                                                    });
                                                })
                                                .catch((error) => {
                                                    res.status(500).json({ error: true, message: [error.message] });
                                                });
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: true, message: [error.message] });
                                        });
                                }
                            })
                            .catch((error) => {
                                res.status(500).json({ error: true, message: [error.message] });
                            });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.addInstagram = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        exchangeInstagramToken(req.body.code)
            .then((instagramObject) => {
                // getProfile(instagram_object.userId, instagram_object.accessToken)
                //     .then((profile) => {
                getUsername(instagramObject.accessToken)
                    .then((username) => {
                        Instagram.findOne({ userId: instagramObject.userId })
                            .then((instance) => {
                                if (instance) {
                                    res.status(400).json({ error: true, message: ['Account already in use.'] });
                                } else {
                                    Instagram.findOne({ influencer: req.user })
                                        .then((instagram) => {
                                            let instagramInstance = null;

                                            if (instagram) {
                                                instagramInstance = instagram;
                                            } else {
                                                instagramInstance = new Instagram();
                                                instagramInstance.influencer = req.user;
                                            }

                                            instagramInstance.userId = instagramObject.userId;
                                            assignInstagram(instagramInstance, instagramObject, username);

                                            instagramInstance
                                                .save()
                                                .then(() => {
                                                    res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                                })
                                                .catch((error) => {
                                                    res.status(500).json({ error: true, message: [error.message] });
                                                });
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: true, message: [error.message] });
                                        });
                                }
                            })
                            .catch((error) => {
                                res.status(500).json({ error: true, message: [error.message] });
                            });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
