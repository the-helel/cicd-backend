const mongoose = require('mongoose');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const randomstring = require('randomstring');
const seedrandom = require('seedrandom');

const Wallet = require('../../../models/wallet/wallet');
const RewardsWallet = require('../../../models/reward/rewards-wallet');
const Facebook = require('../../../models/social/facebook');
const Apple = require('../../../models/social/apple');
const EmailAuth = require('../../../models/influencer/email-auth');
const Influencer = require('../../../models/influencer/influencer');
const SocialPricing = require('../../../models/social/social-pricing');
const Constant = require('../../../models/constant');

const createTransaction = require('../reward/create-transaction');

const { addReferral } = require('../reward/add-points');

const constants = require('../../../constants');
const config = require('../../../config');
const sendEmail = require('../../util/email');
const templates = require('../../../templates');

const { createNotification } = require('../../notification/notification');
const { default: axios } = require('axios');

const middlewares = {};

middlewares.assignInfluencer = async (influencer, profile, email) => {
    influencer.email = email;
    influencer.name = profile.name;
    influencer.dob = profile.dob;
    influencer.gender = profile.gender;
    influencer.city = profile.city;
    influencer.phoneNumber = profile.phoneNumber;
    influencer.categories = profile.categoryIds;

    let referralCode = '';
    if (influencer.name.length >= 3) {
        referralCode =
            influencer.name.toLowerCase().substring(0, 3) +
            randomstring.generate({
                length: 2,
                charset: 'alphabetic',
                capitalization: 'lowercase',
            });
    } else {
        referralCode =
            influencer.name.toLowerCase() +
            randomstring.generate({
                length: 5 - influencer.name.length,
                charset: 'alphabetic',
                capitalization: 'lowercase',
            });
    }

    const rn = seedrandom(new Date());
    referralCode += String(Math.floor(rn() * 900) + 100);
    influencer.referralCode = referralCode;

    let avatar = null;
    if (influencer.gender === 'male') avatar = 'maleAvatar';
    else avatar = 'femaleAvatar';

    let response = await axios.post(`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${config.FIREBASE_WEB_API}`, {
        longDynamicLink: `https://famstar.page.link/?link=https://www.famstar.page.link/invite/?uid=${referralCode}&ofl=https://famstar.in&apn=com.famstar.android&afl=https://play.google.com/store/apps/details?id=com.famstar.android&ibi=com.famstar.ios&ifl=https://apps.apple.com/in/app/famstar/id1561400242&si=https://famstar.s3.ap-south-1.amazonaws.com/iconBg.png&sd=Join me and other Indian influencers on Famstar to collaborate with top brands and get paid in 24 hours. Earn up to 100 Fam Points in your account wallet when signing up using this link.&st=Refer Influencers and Earn`,
        suffix: {
            option: 'SHORT',
        },
    });

    if (response.status === 200) {
        influencer.referralLink = response.data.shortLink;
        return await Constant.findOne({ name: avatar })
            .then((avatar) => {
                influencer.profilePicture = avatar.value;
                return;
            })
            .catch((error) => {
                console.log(error);
                return Promise.reject(error);
            });
    } else {
        return await Constant.findOne({ name: avatar })
            .then((avatar) => {
                influencer.profilePicture = avatar.value;
                return;
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }
};

// create influencer based on authentication type
middlewares.createInfluencer = async (req, res, next) => {
    const errors = validationResult(req);

    // TODO: Refactor create user profile role
    if (errors.isEmpty()) {
        if (req.body.authType === 'email') {
            EmailAuth.findById(req.body.authId, async (err, doc) => {
                if (err) {
                    console.log(err);
                    return res.status(500).json({ error: true, message: [err] });
                }

                if (doc === null) {
                    return res.status(400).send({ error: true, message: ['Email not found'] });
                }

                const influencer = new Influencer();
                await middlewares.assignInfluencer(influencer, req.body, req.body.email);

                const session = await mongoose.startSession();
                await session.startTransaction();

                influencer
                    .save({ session })
                    .then(async (influencerInstance) => {
                        doc.userId = influencerInstance._id;
                        doc.save({ session })
                            .then(async () => {
                                await middlewares
                                    .setupInfluencer(influencerInstance._id, req.body.referee, session)
                                    .then(async () => {
                                        await session.commitTransaction();
                                        await session.endSession();

                                        const jwtToken = jwt.sign(
                                            {
                                                userId: influencer._id,
                                                userType: constants.INFLUENCER,
                                            },
                                            config.JWT_SECRET
                                        );

                                        sendEmail(influencerInstance.email, {
                                            subject: 'Welcome to Famstar 👋🏻',
                                            body: templates.INFLUENCER_WELCOME_EMAIL(influencerInstance.name),
                                        })
                                            .then(async () => {
                                                const firstName = influencerInstance.name.split(' ')[0];
                                                await createNotification(
                                                    'Famstar',
                                                    `Hey, ${firstName}! Welcome. The fun times begin now. Happy Collaborating!`,
                                                    null,
                                                    influencerInstance._id,
                                                    'Welcome'
                                                );

                                                return res.status(200).json({
                                                    error: false,
                                                    result: {
                                                        auth: true,
                                                        token: jwtToken,
                                                        userId: influencer._id,
                                                        entryBonus: constants.ENTRY_BONUS_POINTS,
                                                    },
                                                });
                                            })
                                            .catch(async (error) => {
                                                console.log(error);
                                                res.status(500).json({ error: true, message: [error] });
                                            });
                                    })
                                    .catch(async (error) => {
                                        console.log(error);
                                        await session.abortTransaction();
                                        await session.endSession();
                                        res.status(500).json({ error: true, message: [error] });
                                    });
                            })
                            .catch(async (error) => {
                                console.log(error);
                                await session.abortTransaction();
                                await session.endSession();
                                res.status(500).json({ error: true, message: [error] });
                            });
                    })
                    .catch(async (error) => {
                        console.log(error);
                        await session.abortTransaction();
                        await session.endSession();
                        res.status(500).json({ error: true, message: [error] });
                    });
                // .finally(() => session.endSession());
            }).catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
        } else if (req.body.authType === 'facebook') {
            Facebook.findById(req.body.authId)
                .then(async (facebook) => {
                    if (facebook == null) {
                        res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Facebook')] });
                    } else {
                        if (facebook.influencer == null) {
                            const influencer = new Influencer();
                            await middlewares.assignInfluencer(influencer, req.body, facebook.email);
                            const session = await mongoose.startSession();
                            await session.startTransaction();

                            influencer
                                .save({ session })
                                .then((influencerInstance) => {
                                    facebook.influencer = influencerInstance._id;
                                    facebook
                                        .save({ session })
                                        .then(() => {
                                            middlewares
                                                .setupInfluencer(influencerInstance._id, req.body.referee, session)
                                                .then(async () => {
                                                    await session.commitTransaction();
                                                    await session.endSession();

                                                    const jwtToken = jwt.sign(
                                                        {
                                                            userId: influencerInstance._id,
                                                            userType: constants.INFLUENCER,
                                                        },
                                                        config.JWT_SECRET
                                                    );
                                                    await sendEmail(influencerInstance.email, {
                                                        subject: 'Welcome to Famstar 👋🏻',
                                                        body: templates.INFLUENCER_WELCOME_EMAIL(influencerInstance.name),
                                                    })
                                                        .then(async () => {
                                                            console.log('email sent on sign up');

                                                            const firstName = influencerInstance.name.split(' ')[0];
                                                            await createNotification(
                                                                'Famstar',
                                                                `Hey, ${firstName}! Welcome. The fun times begin now. Happy Collaborating!`,
                                                                null,
                                                                influencerInstance._id,
                                                                'Welcome'
                                                            );

                                                            res.status(200).json({
                                                                error: false,
                                                                result: {
                                                                    auth: true,
                                                                    token: jwtToken,
                                                                    userId: influencerInstance._id,
                                                                    entryBonus: constants.ENTRY_BONUS_POINTS,
                                                                },
                                                            });
                                                        })
                                                        .catch(async (error) => {
                                                            res.status(500).json({ error: true, message: [error] });
                                                        });
                                                })
                                                .catch(async (error) => {
                                                    await session.abortTransaction();
                                                    await session.endSession();
                                                    res.status(500).json({ error: true, message: [error] });
                                                });
                                        })
                                        .catch(async (error) => {
                                            await session.abortTransaction();
                                            await session.endSession();
                                            res.status(500).json({ error: true, message: [error] });
                                        });
                                })
                                .catch(async (error) => {
                                    await session.abortTransaction();
                                    await session.endSession();
                                    res.status(500).json({ error: true, message: [error] });
                                });
                        } else {
                            res.status(400).json({ error: true, message: ['Influencer already exists.'] });
                        }
                    }
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error] });
                });
        } else if (req.body.authType === 'apple') {
            Apple.findById(req.body.authId)
                .then(async (apple) => {
                    if (apple == null) {
                        res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Apple')] });
                    } else {
                        if (apple.influencer === null) {
                            const influencer = new Influencer();
                            await middlewares.assignInfluencer(influencer, req.body, apple.email);
                            const session = await mongoose.startSession();
                            await session.startTransaction();

                            influencer
                                .save({ session })
                                .then((influencerInstance) => {
                                    apple.influencer = influencerInstance._id;
                                    apple
                                        .save({ session })
                                        .then(() => {
                                            middlewares
                                                .setupInfluencer(influencerInstance._id, req.body.referee, session)
                                                .then(async () => {
                                                    await session.commitTransaction();
                                                    await session.endSession();

                                                    const jwtToken = jwt.sign(
                                                        {
                                                            userId: influencerInstance._id,
                                                            userType: constants.INFLUENCER,
                                                        },
                                                        config.JWT_SECRET
                                                    );
                                                    await sendEmail(influencerInstance.email, {
                                                        subject: 'Welcome to Famstar 👋🏻',
                                                        body: templates.INFLUENCER_WELCOME_EMAIL(influencerInstance.name),
                                                    })
                                                        .then(async () => {
                                                            const firstName = influencerInstance.name.split(' ')[0];
                                                            await createNotification(
                                                                'Famstar',
                                                                `Hey, ${firstName}! Welcome. The fun times begin now. Happy Collaborating!`,
                                                                null,
                                                                influencerInstance._id,
                                                                'Welcome'
                                                            );

                                                            return res.status(200).json({
                                                                error: false,
                                                                result: {
                                                                    auth: true,
                                                                    token: jwtToken,
                                                                    userId: influencerInstance._id,
                                                                    entryBonus: constants.ENTRY_BONUS_POINTS,
                                                                },
                                                            });
                                                        })
                                                        .catch(async (error) => {
                                                            return res.status(500).json({ error: true, message: [error] });
                                                        });
                                                })
                                                .catch(async (error) => {
                                                    await session.abortTransaction();
                                                    await session.endSession();
                                                    return res.status(500).json({ error: true, message: [error] });
                                                });
                                        })
                                        .catch(async (error) => {
                                            await session.abortTransaction();
                                            await session.endSession();
                                            return res.status(500).json({ error: true, message: [error] });
                                        });
                                })
                                .catch(async (error) => {
                                    await session.abortTransaction();
                                    await session.endSession();
                                    return res.status(500).json({ error: true, message: [error] });
                                });
                        } else {
                            return res.status(400).json({ error: true, message: ['Influencer already exists.'] });
                        }
                    }
                })
                .catch((error) => {
                    return res.status(500).json({ error: true, message: [error] });
                });
        }
    } else {
        return res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.setupInfluencer = async (influencer, referee, session) => {
    const wallet = new Wallet();
    wallet.influencer = influencer;

    return await wallet
        .save({ session })
        .then(async () => {
            const rewardsWallet = new RewardsWallet();
            rewardsWallet.influencer = influencer;
            rewardsWallet.points = 100;

            // setting up social pricing
            try {
                let socialPricing = new SocialPricing();
                socialPricing.influencer = influencer;

                await socialPricing.save({ session });
            } catch (error) {
                return Promise.reject(error);
            }
            return await rewardsWallet
                .save({ session })
                .then(async () => {
                    return await createTransaction(
                        constants.CREDIT,
                        influencer,
                        constants.ENTRY_BONUS_POINTS,
                        { source: constants.ENTRY_BONUS, sourceId: influencer },
                        session
                    )
                        .then(async () => {
                            if (String(referee) != 'null') {
                                return await addReferral(referee, influencer);
                            }
                        })
                        .catch(async (error) => {
                            return Promise.reject(error);
                        });
                })
                .catch((error) => {
                    return Promise.reject(error);
                });
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = middlewares;
