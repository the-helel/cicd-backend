const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const { jwtDecode } = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const constants = require('../../../constants');
const config = require('../../../config');

const EmailAuth = require('../../../models/influencer/email-auth');
const Influencer = require('../../../models/influencer/influencer');
const Facebook = require('../../../models/social/facebook');

const sendEmail = require('../../util/email');

const templates = require('../../../templates');

const middlewares = {};

// Check if email already exist, then refresh the temporary access token
// else generate new document with access token
middlewares.generateTemporaryAccessToken = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const email = req.body.email;
        const password = req.body.password;
        const name = req.body.name;

        // check if email is taken or not
        Influencer.findOne(
            {
                email: email,
            },
            (err, doc) => {
                if (err)
                    return res.status(500).send({
                        error: true,
                        message: [constants.SERVER_ERR],
                    });

                if (doc !== null)
                    return res.status(400).send({
                        error: true,
                        message: ['Email already taken'],
                    });
                else {
                    EmailAuth.findOne(
                        {
                            email: email,
                        },
                        async (err, doc) => {
                            if (err)
                                return res.status(500).send({
                                    error: true,
                                    message: [constants.SERVER_ERR],
                                });

                            if (doc === null) {
                                const newEmailAuth = new EmailAuth();
                                await bcrypt
                                    .genSalt(10)
                                    .then(async (salt) => {
                                        await bcrypt
                                            .hash(password, salt)
                                            .then(async (hash) => {
                                                newEmailAuth.password = hash;
                                            })
                                            .catch((error) => {
                                                return res.status(500).send({
                                                    error: true,
                                                    message: [error],
                                                });
                                            });
                                    })
                                    .catch((error) => {
                                        return res.status(500).send({
                                            error: true,
                                            message: [error],
                                        });
                                    });

                                newEmailAuth.email = email;
                                newEmailAuth.name = name;

                                newEmailAuth
                                    .save()
                                    .then(async (emailAuthInstance) => {
                                        let accessToken = jwt.sign(
                                            {
                                                userId: emailAuthInstance._id,
                                                userType: constants.INFLUENCER,
                                            },
                                            config.JWT_SECRET,
                                            {
                                                expiresIn: constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES,
                                            }
                                        );
                                        await sendEmail(email, {
                                            subject: 'Please verify your Famstar Account',
                                            body: templates.INFLUENCER_EMAIL_VERIFICATION_MAIL(name, accessToken),
                                        })
                                            .then(() => {
                                                console.log('email sent');
                                                return res.status(200).send({
                                                    error: false,
                                                    message: [constants.SUCCESS],
                                                });
                                            })
                                            .catch(() => {
                                                return res.status(500).send({
                                                    error: true,
                                                    message: [constants.SERVER_ERR],
                                                });
                                            });
                                    })
                                    .catch(() => {
                                        return res.status(500).send({
                                            error: true,
                                            message: [constants.SERVER_ERR],
                                        });
                                    });
                            } else {
                                // if email is already verified
                                if (doc.isVerified) {
                                    return res.status(200).send({
                                        error: false,
                                        message: ['Email already verified! Please Login'],
                                    });
                                } else {
                                    // just refresh the access token
                                    let accessToken = jwt.sign(
                                        {
                                            userId: doc._id,
                                            userType: constants.INFLUENCER,
                                        },
                                        config.JWT_SECRET,
                                        {
                                            expiresIn: constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES,
                                        }
                                    );

                                    sendEmail(doc.email, {
                                        subject: 'Please verify your Famstar Account',
                                        body: templates.INFLUENCER_EMAIL_VERIFICATION_MAIL(name, accessToken),
                                    })
                                        .then(() => {
                                            return res.status(200).send({
                                                error: false,
                                                message: [constants.SUCCESS],
                                            });
                                        })
                                        .catch(() => {
                                            return res.status(500).send({
                                                error: true,
                                                message: [constants.SERVER_ERR],
                                            });
                                        });
                                }
                            }
                        }
                    );
                }
            }
        );
    } else {
        return res.status(400).send({
            error: true,
            message: errors.array(),
        });
    }
};

// Confirm email id
middlewares.confirmEmail = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const token = req.params.token;

        try {
            jwt.verify(token, config.JWT_SECRET);

            const decodedToken = jwt.decode(token);

            let emailAuthInstance = await EmailAuth.findById(decodedToken.userId);

            console.log(emailAuthInstance);

            if (emailAuthInstance === null) {
                return res.send('<h1>Seems like you have not signed up yet!! </h1>');
            }

            emailAuthInstance.isVerified = true;

            await emailAuthInstance.save();

            return res.render('verified');
        } catch (error) {
            // if jwt has expired / or invalid
            console.log(error);
            return res.render('expired');
        }
    } else {
        return res.status(400).send({
            error: true,
            message: errors.array(),
        });
    }
};

// generate forgot password link
middlewares.forgotpassword = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const email = req.body.email;

        EmailAuth.findOne(
            {
                email: email,
            },
            (err, doc) => {
                if (err)
                    return res.status(500).send({
                        error: true,
                        message: [constants.SERVER_ERR],
                    });

                if (doc === null)
                    return res.status(404).send({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')],
                    });

                const token = jwt.sign(
                    {
                        userId: doc._id,
                        userType: constants.INFLUENCER,
                        email: email,
                    },
                    config.JWT_SECRET,
                    {
                        expiresIn: constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES,
                    }
                );

                sendEmail(doc.email, {
                    subject: 'Reset Password | Famstar ',
                    body: templates.INFLUENCER_PWD_RESET(doc.name, token),
                })
                    .then(() => {
                        return res.status(200).send({
                            error: false,
                            message: ['Reset link sent to email'],
                        });
                    })
                    .catch((err) => {
                        return res.status(500).send({
                            error: true,
                            message: [constants.SERVER_ERR],
                        });
                    });
            }
        );
    } else {
        return res.status(400).send({
            error: true,
            message: errors.array(),
        });
    }
};

// reset email password
middlewares.resetPassword = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const { token, password } = req.body;
        const decodedToken = jwt.decode(token);

        await EmailAuth.findOne({
            email: decodedToken.email,
        })
            .then(async (emailAuth) => {
                if (emailAuth === null)
                    return res.status(404).send({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')],
                    });

                try {
                    jwt.verify(token, config.JWT_SECRET);

                    await bcrypt
                        .genSalt(10)
                        .then(async (salt) => {
                            await bcrypt
                                .hash(password, salt)
                                .then((hash) => {
                                    emailAuth.password = hash;
                                })
                                .catch((err) => {
                                    return res.status(500).send({
                                        error: true,
                                        message: [constants.SERVER_ERR],
                                    });
                                });
                        })
                        .catch((err) => {
                            return res.status(500).send({
                                error: true,
                                message: [constants.SERVER_ERR],
                            });
                        });

                    emailAuth
                        .save()
                        .then(() => {
                            return res.status(200).send({
                                error: true,
                                message: [constants.SUCCESS],
                            });
                        })
                        .catch(() => {
                            return res.status(500).send({
                                error: true,
                                message: [constants.SERVER_ERR],
                            });
                        });
                } catch (err) {
                    console.log(err);
                    return res.render('expired');
                }
            })
            .catch((err) => {
                return res.status(500).send({
                    error: true,
                    message: [constants.SERVER_ERR],
                });
            });
    } else {
        return res.status(400).send({
            error: true,
            message: errors.array(),
        });
    }
};

// email password based login
middlewares.login = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const email = req.body.email;
        const password = req.body.password;

        EmailAuth.findOne(
            {
                email: email,
            },
            (err, doc) => {
                if (err)
                    return res.status(500).send({
                        error: true,
                        message: [constants.SERVER_ERR],
                    });

                if (doc === null)
                    return res.status(404).send({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Influencer')],
                    });

                if (doc.isVerified === false)
                    return res.status(400).send({
                        error: true,
                        message: ['Email not verified!'],
                    });

                bcrypt.compare(password, doc.password, (err, result) => {
                    if (result) {
                        let token = jwt.sign(
                            {
                                userId: doc.userId,
                                userType: constants.INFLUENCER,
                            },
                            config.JWT_SECRET
                        );

                        if (doc.userId === null) {
                            return res.status(200).send({
                                error: false,
                                result: {
                                    auth: true,
                                    token: token,
                                    authId: doc._id,
                                    name: doc.name,
                                },
                            });
                        } else {
                            return res.status(200).send({
                                error: false,
                                result: {
                                    auth: true,
                                    token: token,
                                    userId: doc.userId,
                                    name: doc.name,
                                },
                            });
                        }
                    } else {
                        return res.status(401).send({
                            error: true,
                            message: ['Invalid Credentials'],
                        });
                    }
                });
            }
        );
    } else {
        return res.status(400).send({
            error: true,
            message: errors.array(),
        });
    }
};

module.exports = middlewares;
