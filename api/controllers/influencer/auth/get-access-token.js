const jwt = require('jsonwebtoken');

const config = require('../../../config');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getAccessToken = (req, res, next) => {
    const jwtToken = jwt.sign(
        {
            userId: req.query.influencer,
            userType: constants.INFLUENCER,
        },
        config.JWT_SECRET
    );
    res.status(200).json({ error: false, result: { token: jwtToken } });
};

module.exports = middlewares;
