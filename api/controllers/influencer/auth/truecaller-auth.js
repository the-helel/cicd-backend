const truecaller = require('@vyng/truecaller-node');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const verifyNonTruecallerProfile = require('../../auth/truecaller/verify-non-truecaller-profile');

const Wallet = require('../../../models/wallet/wallet');
const Influencer = require('../../../models/influencer/influencer');
const Constant = require('../../../models/constant');

const config = require('../../../config');

const addCredits = require('../reward/add-points');

const constants = require('../../../constants');

const middlewares = {};

let options = {
    url: 'https://api4.truecaller.com/v1/key',
    ttl: 1000 * 60 * 10,
    publicKeys: undefined,
};

middlewares.signinTruecaller = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const profile = JSON.parse(req.body.profile);
        let phoneNumber = null;
        if (req.body.isTruecaller) {
            const result = await truecaller.verifyProfile(
                {
                    payload: profile.payload,
                    signature: profile.signature,
                    signatureAlgorithm: profile.signatureAlgorithm,
                },
                options
            );
            if (result.verifiedSignature) {
                const tempNumber = String(profile.phoneNumber);
                phoneNumber = '+91' + tempNumber.substr(tempNumber.length - 10, 10);
            } else {
                res.status(400).json({ error: true, message: ['Truecaller bad payload'] });
            }
        } else {
            await verifyNonTruecallerProfile(profile.accessToken)
                .then((response) => {
                    const tempNumber = String(response.phoneNumber);
                    phoneNumber = '+91' + tempNumber.substr(tempNumber.length - 10, 10);
                })
                .catch((error) => {
                    res.status(400).json({ error: true, message: [error.message] });
                });
        }
        if (phoneNumber != null) {
            Influencer.findOne({ phoneNumber: phoneNumber })
                .then(async (registeredInfluencer) => {
                    let result;
                    if (registeredInfluencer == null) {
                        const influencer = new Influencer();
                        influencer.phoneNumber = phoneNumber;
                        influencer.firstName = profile.firstName;
                        influencer.lastName = profile.lastName;
                        const wallet = new Wallet();
                        await wallet.save().then((walletRecord) => {
                            influencer.walletId = walletRecord._id;
                        });
                        if (String(profile.email) != 'null') {
                            influencer.email = profile.email;
                        }
                        if (String(req.body.referee) != 'null') {
                            result = await addCredits(req.body.referee, influencer)
                                .then(async () => {
                                    return await Constant.findOne({ name: 'referralAmountForReferred' })
                                        .then((constant) => {
                                            influencer.referee = mongoose.Types.ObjectId(req.body.referee);
                                            influencer.credits += parseInt(constant.value);
                                            return null;
                                        })
                                        .catch((error) => {
                                            return Promise.reject(error);
                                        });
                                })
                                .catch((error) => {
                                    return Promise.reject(error);
                                });
                        }
                        if (result == null) {
                            await influencer
                                .save()
                                .then((record) => {
                                    registeredInfluencer = record;
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error.message] });
                                });
                        }
                    }
                    if (result != null) {
                        res.status(500).json({ error: true, message: ['Error caused for referral.'] });
                    } else {
                        const jwtToken = jwt.sign(
                            {
                                userId: registeredInfluencer._id,
                                userType: constants.INFLUENCER,
                            },
                            config.JWT_SECRET
                        );
                        res.status(200).json({ error: false, result: { auth: true, token: jwtToken, userId: registeredInfluencer._id } });
                    }
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        }
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
