const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const Influencer = require('../../../models/influencer/influencer');

const sendOtp = require('../../auth/phone-number/send-otp');
const verifyOtp = require('../../auth/phone-number/verify-otp');

const config = require('../../../config');

const constants = require('../../../constants');

const middlewares = {};

middlewares.signinPhoneNumber = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        Influencer.findOne({ phoneNumber: req.body.phoneNumber })
            .then((registeredInfluencer) => {
                let influencer = null;
                if (registeredInfluencer != null) {
                    influencer = registeredInfluencer;
                } else {
                    influencer = new Influencer();
                    influencer.phoneNumber = req.body.phoneNumber;
                }
                sendOtp(influencer)
                    .then(() => {
                        influencer
                            .save()
                            .then((record) => {
                                res.status(202).json({ error: false, message: [constants.SUCCESS] });
                            })
                            .catch((error) => {
                                res.status(500).json({ error: true, mesaage: [error.message] });
                            });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    }
};

middlewares.verifyPhoneNumber = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: errors.array() });
    } else {
        Influencer.findOne({ phoneNumber: req.body.phoneNumber })
            .then((registeredInfluencer) => {
                if (registeredInfluencer != null) {
                    const [verdict, error] = verifyOtp(registeredInfluencer, req.body.otpToken);
                    if (verdict) {
                        const jwtToken = jwt.sign(
                            {
                                userId: registeredInfluencer._id,
                                userType: constants.INFLUENCER,
                            },
                            config.JWT_SECRET
                        );
                        res.status(200).json({ error: false, result: { auth: true, token: jwtToken, userId: registeredInfluencer._id } });
                    } else {
                        res.status(400).json({ error: true, message: [error.message] });
                    }
                } else {
                    res.status(400).json({ error: true, message: ['Phone number not registered.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    }
};

module.exports = middlewares;
