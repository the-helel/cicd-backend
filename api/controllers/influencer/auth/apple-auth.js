const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const Influencer = require('../../../models/influencer/influencer');
const Apple = require('../../../models/social/apple');
const config = require('../../../config');
const constants = require('../../../constants');

const middlewares = {};

middlewares.assignApple = (appleInstance, body) => {
    appleInstance.name = body.name;
    appleInstance.email = body.email;
    appleInstance.authorizationCode = body.authorizationCode;
    appleInstance.userId = body.userId;
};

middlewares.signInApple = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Apple.findOne({ userId: req.body.userId })
            .then((apple) => {
                if (apple == null) {
                    Influencer.findOne({ email: req.body.email })
                        .then((influencer) => {
                            if (influencer != null) {
                                return res.status(400).json({ error: true, message: ['Already account is registered with this email'] });
                            } else if (!req.body.email) {
                                // No email is associated with fb account.
                                return res.status(400).json({
                                    error: true,
                                    message: ['Email not connected to facebook account. Try signing up with email.'],
                                });
                            } else {
                                let apple = new Apple();
                                middlewares.assignApple(apple, req.body);

                                apple
                                    .save()
                                    .then((instance) => {
                                        return res.status(201).json({
                                            error: false,
                                            result: { apple: instance._id },
                                            message: [constants.SUCCESS],
                                        });
                                    })
                                    .catch((error) => {
                                        return res.status(500).json({ error: true, message: [error] });
                                    });
                            }
                        })
                        .catch((error) => {
                            return res.status(500).json({ error: true, message: [error] });
                        });
                } else {
                    // Account found but not completed the profile
                    if (apple.influencer === null) {
                        return res.status(202).json({ error: false, result: { apple: apple._id }, message: [constants.SUCCESS] });
                    } else {
                        // Successful sign in
                        const jwtToken = jwt.sign(
                            {
                                userId: apple.influencer,
                                userType: constants.INFLUENCER,
                            },
                            config.JWT_SECRET
                        );

                        return res.status(200).json({
                            error: false,
                            result: {
                                auth: true,
                                token: jwtToken,
                                userId: apple.influencer,
                            },
                        });
                    }
                }
            })
            .catch((error) => {
                return res.status(500).json({ error: true, message: [error] });
            });
    } else {
        return res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
