const { validationResult } = require('express-validator');
const { default: axios } = require('axios');

const Facebook = require('../../../models/social/facebook');
const Instagram = require('../../../models/social/instagram');
const Youtube = require('../../../models/social/youtube');
const Influencer = require('../../../models/influencer/influencer');
const Constant = require('../../../models/constant');

const getInfluencerPages = require('../../auth/facebook/get-influencer-pages');
const getInfluencerIgAccount = require('../../auth/facebook/get-influencer-ig-accounts');
const getInstagramProfile = require('../../auth/facebook/get-instagram-profile');

const constants = require('../../../constants');
const config = require('../../../config');
const { addInstagramConnect } = require('../reward/add-points');
const { uploadUsingFaceBookUserId, uploadUsingUrl } = require('../../util/s3');

// setting up s3
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const s3 = new AWS.S3({
    secretAccessKey: config.S3_SECRET_ACCESS_KEY,
    accessKeyId: config.S3_ACCESS_KEY_ID,
    region: config.S3_REGION,
});

const middlewares = {};

// connect facebook account
middlewares.connectFacebook = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Facebook.findOne({
            userId: req.body.userId,
        })
            .lean()
            .then((facebook) => {
                if (facebook !== null) {
                    // a record is found in facebook collection with given credentials.
                    res.status(400).json({
                        error: true,
                        message: ['Account already in use.'],
                    });
                } else {
                    Facebook.findOne({
                        influencer: req.user,
                    })
                        .then(async (facebook) => {
                            if (facebook === null) {
                                // if email not associated with fb account then set email null
                                let email = req.body.email ? req.body.email : null;

                                // creating a new facebook instance ( if no fb account connected )
                                const facebook = new Facebook();
                                facebook.influencer = req.user;
                                facebook.userId = req.body.userId;
                                facebook.accessToken = req.body.accessToken;
                                facebook.accessTokenExpiry = req.body.accessTokenExpiry;
                                facebook.name = req.body.name;
                                facebook.email = email;
                                if (req.body.profilePicture) {
                                    facebook.profilePicture = req.body.profilePicture;
                                    await Influencer.findById(req.user).then(async (influencer) => {
                                        let [url, err] = await uploadUsingFaceBookUserId(req.body.userId);

                                        if (err) {
                                            influencer.profilePicture = facebook.profilePicture;
                                        } else {
                                            influencer.profilePicture = url;
                                        }

                                        await influencer.save();
                                    });
                                } else {
                                    await Constant.findOne({
                                        name: 'avatar',
                                    }).then(async (constant) => {
                                        facebook.profilePicture = constant.value;
                                    });
                                }

                                facebook
                                    .save()
                                    .then(() => {
                                        return res.status(200).json({
                                            error: false,
                                            message: [constants.SUCCESS],
                                        });
                                    })
                                    .catch((error) => {
                                        res.status(500).json({
                                            error: true,
                                            message: [error],
                                        });
                                    });
                            } else {
                                // facebook account already connected
                                res.status(400).json({
                                    error: true,
                                    message: ['Facebook account already connected.'],
                                });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({
                                error: true,
                                message: [error],
                            });
                        });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.checkTokenExpired = async (req, res, next) => {
    try {
        let { accessToken } = await Facebook.findOne(
            {
                influencer: req.user,
            },
            {
                _id: 0,
                accessToken: 1,
            }
        );

        if (!accessToken)
            return res.status(400).json({
                error: true,
                message: [constants.RESOURCE_NOT_FOUND_ERROR('Token')],
            });

        // this api tells if token is expired or not
        axios
            .get(
                `${config.FACEBOOK_GRAPH}/oauth/access_token?grant_type=fb_attenuate_token&client_id=498820597950659&fb_exchange_token=${accessToken}`
            )
            .then((response) => {
                if (response.status === 400) {
                    return res.status(400).json({
                        error: true,
                        message: ['Token expired'],
                    });
                }

                if (response.status === 200)
                    return res.status(200).json({
                        error: false,
                        message: [constants.SUCCESS],
                    });
            })
            .catch((error) => {
                return res.status(error.response.status).json({
                    error: true,
                    message: ['Token expired'],
                });
            });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            error: true,
            message: [constants.SERVER_ERR],
        });
    }
};

middlewares.refreshFacebookTokenAndInstagramPages = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        try {
            let facebookInstance = await Facebook.findOne({
                userId: req.body.userId,
                influencer: req.user,
            });

            if (facebookInstance === null) {
                return res.status(400).json({
                    error: true,
                    message: ['Facebook error'],
                });
            }

            facebookInstance.accessToken = req.body.accessToken;
            facebookInstance.accessTokenExpiry = req.body.expiry;
            facebookInstance.name = req.body.name;

            await facebookInstance.save();

            await Instagram.find({
                influencer: req.user,
            }).then(async (instagramProfiles) => {
                for (let instagramProfile of instagramProfiles) {
                    await Facebook.find({
                        influencer: instagramProfile.influencer,
                    })
                        .then(async (facebookProfiles) => {
                            if (facebookProfiles != null) {
                                for (let facebookProfile of facebookProfiles) {
                                    await getInstagramProfile(instagramProfile.userId, facebookProfile.accessToken)
                                        .then(async (instagramData) => {
                                            instagramProfile.igId = instagramData.ig_id;
                                            instagramProfile.username = instagramData.username;
                                            instagramProfile.website = instagramData.website;
                                            instagramProfile.numberOfFollowers = instagramData.followers_count;
                                            instagramProfile.numberOfFollowing = instagramData.follows_count;
                                            instagramProfile.fullName = instagramData.name;
                                            instagramProfile.biography = instagramData.biography;

                                            if (instagramData.profile_picture_url) {
                                                let [url, err] = await uploadUsingUrl(instagramData.profile_picture_url);

                                                if (err) {
                                                    console.log(err);
                                                    instagramProfile.profilePicture = instagramData.profile_picture_url;
                                                } else {
                                                    instagramProfile.profilePicture = url;
                                                }
                                            }

                                            await instagramProfile.save().then(() => console.log('IG Updated'));
                                        })
                                        .catch((error) => {
                                            console.log(error);
                                        });
                                }
                            } else {
                                console.log('Facebook not found.');
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                }
            });

            return res.status(200).json({
                error: false,
                message: ['Account refreshed'],
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json({
                error: true,
                message: [constants.SERVER_ERR],
            });
        }
    } else {
        res.status(400).json({
            error: true,
            message: [constants.VALIDATION_ERROR],
        });
    }
};

middlewares.getFacebookPages = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        getInfluencerPages(req.query.accessToken)
            .then((pages) => {
                Facebook.findOne({
                    userId: req.query.userId,
                    influencer: req.user,
                }).then((facebook) => {
                    if (facebook) {
                        facebook.accessToken = req.query.accessToken;
                        facebook.accessTokenExpiry = req.query.accessTokenExpiry;
                        facebook.pageAccessToken = pages.data.length == 0 ? null : pages.data[0].access_token;
                        facebook
                            .save()
                            .then(async () => {
                                const pagesList = [];
                                for (let index in pages.data) {
                                    await getInfluencerIgAccount(pages.data[index].id, facebook.accessToken)
                                        .then(async (data) => {
                                            if (Object.getOwnPropertyNames(data).includes('instagram_business_account')) {
                                                await getInstagramProfile(data.instagram_business_account.id, facebook.accessToken)
                                                    .then(async (instaData) => {
                                                        const instagram = {};
                                                        instagram.influencer = req.user;
                                                        instagram.userId = instaData.id;
                                                        instagram.igId = instaData.ig_id;
                                                        instagram.username = instaData.username;
                                                        instagram.website = instaData.website;
                                                        instagram.numberOfFollowers = instaData.followers_count;
                                                        instagram.numberOfFollowing = instaData.follows_count;
                                                        instagram.fullName = instaData.name;
                                                        instagram.biography = instaData.biography;
                                                        instagram.data = data;
                                                        if (instaData.profile_picture_url) {
                                                            let [url, err] = await uploadUsingUrl(instaData.profile_picture_url);

                                                            if (err) {
                                                                instagram.profilePicture = instaData.profile_picture_url;
                                                            } else {
                                                                instagram.profilePicture = url;
                                                            }
                                                        }
                                                        pages.data[index].instagramData = instagram;
                                                        pagesList.push(pages.data[index]);
                                                    })
                                                    .catch((error) => {
                                                        return Promise.reject(error);
                                                    });
                                            }
                                        })
                                        .catch((error) => {
                                            console.log(error);
                                        });
                                }
                                res.status(200).json({
                                    error: false,
                                    result: pagesList,
                                });
                            })
                            .catch((error) => {
                                res.status(500).json({
                                    error: true,
                                    message: [error.message],
                                });
                            });
                    } else {
                        res.status(400).json({
                            error: true,
                            message: ['You are logged in as a different facebook user.'],
                        });
                    }
                });
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.connectIgAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Facebook.findOne({
            userId: req.body.userId,
            influencer: req.user,
        })
            .lean()
            .then((facebook) => {
                if (facebook) {
                    //getInfluencerIgAccount(req.body.pageId, facebook.accessToken)
                    getInfluencerIgAccount(req.body.pageId, facebook.pageAccessToken)
                        .then((data) => {
                            if (Object.getOwnPropertyNames(data).includes('instagram_business_account')) {
                                Instagram.findOne({
                                    userId: data.instagram_business_account.id,
                                })
                                    .then(async (instagram) => {
                                        if (instagram) {
                                            if (instagram.influencer == req.user) {
                                                res.status(400).json({
                                                    error: true,
                                                    message: ['Account already connected'],
                                                });
                                            } else {
                                                res.status(400).json({
                                                    error: true,
                                                    message: ['Account already in use'],
                                                });
                                            }
                                        } else {
                                            await getInstagramProfile(data.instagram_business_account.id, facebook.accessToken)
                                                .then(async (instaData) => {
                                                    const instagram = new Instagram();
                                                    instagram.influencer = req.user;
                                                    instagram.userId = instaData.id;
                                                    instagram.igId = instaData.ig_id;
                                                    instagram.username = instaData.username;
                                                    instagram.website = instaData.website;
                                                    instagram.numberOfFollowers = instaData.followers_count;
                                                    instagram.numberOfFollowing = instaData.follows_count;
                                                    instagram.fullName = instaData.name;
                                                    instagram.biography = instaData.biography;

                                                    if (instaData.profile_picture_url) {
                                                        let [url, err] = await uploadUsingUrl(instaData.profile_picture_url);

                                                        if (err) {
                                                            instagram.profilePicture = instaData.profile_picture_url;
                                                        } else {
                                                            instagram.profilePicture = url;
                                                        }

                                                        await Influencer.findById(req.user).then(async (influencer) => {
                                                            influencer.profilePicture = instagram.profilePicture;

                                                            await influencer.save();
                                                        });
                                                    } else {
                                                        await Constant.findOne({
                                                            name: 'avatar',
                                                        }).then(async (constant) => {
                                                            instagram.profilePicture = constant.value;
                                                        });
                                                    }

                                                    instagram
                                                        .save()
                                                        .then(async () => {
                                                            await addInstagramConnect(req.user, instaData.followers_count);
                                                            return res.status(200).json({
                                                                error: false,
                                                                message: [constants.SUCCESS],
                                                            });
                                                        })
                                                        .catch((error) => {
                                                            res.status(500).json({
                                                                error: false,
                                                                message: [error],
                                                            });
                                                        });
                                                })
                                                .catch((error) => {
                                                    res.status(500).json({
                                                        error: false,
                                                        message: [error],
                                                    });
                                                });
                                        }
                                    })
                                    .catch((error) => {
                                        res.status(500).json({
                                            error: false,
                                            message: [error.message],
                                        });
                                    });
                            } else {
                                res.status(400).json({
                                    error: true,
                                    message: ['Instagram account is not connected'],
                                });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({
                                error: true,
                                message: [error],
                            });
                        });
                } else {
                    res.status(400).json({
                        error: false,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Facebook')],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.disconnectIgAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Instagram.findOneAndDelete({
            userId: req.body.userId,
            influencer: req.user,
        })
            .then((instagram) => {
                if (instagram != null) {
                    res.status(200).json({
                        error: false,
                        message: [constants.SUCCESS],
                    });
                } else {
                    res.status(400).json({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR('Instagram')],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.connectYoutube = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const bearerToken = req.body.token;

        axios({
            method: 'GET',
            url: constants.YT_DATA_API_URL,
            headers: {
                Authorization: bearerToken,
            },
        })
            .then((response) => {
                Youtube.findOne({
                    channelId: response.data.items[0].id,
                })
                    .lean()
                    .then(async (youtubeInstance) => {
                        if (youtubeInstance != null) {
                            try {
                                assignYoutube(youtubeInstance, response, req);
                                return res.status(200).json({
                                    error: false,
                                    message: ['Account info refreshed'],
                                });
                            } catch (error) {
                                return res.status(500).send({
                                    error: false,
                                    message: [constants.SERVER_ERR],
                                });
                            }
                        } else {
                            Youtube.findOne({
                                influencer: req.user,
                            })
                                .lean()
                                .then(async (youtubeInstance) => {
                                    if (youtubeInstance === null) {
                                        let youtube = new Youtube();

                                        try {
                                            assignYoutube(youtube, response, req);
                                            youtube
                                                .save()
                                                .then(() => {
                                                    return res.status(201).send({
                                                        error: false,
                                                        message: [constants.SUCCESS],
                                                    });
                                                })
                                                .catch((err) => {
                                                    console.log(err);
                                                    return res.status(500).send({
                                                        error: false,
                                                        message: [constants.SERVER_ERR],
                                                    });
                                                });
                                        } catch (err) {
                                            console.log(err);
                                            return res.status(500).send({
                                                error: false,
                                                message: [constants.SERVER_ERR],
                                            });
                                        }
                                    } else {
                                        return res.status(200).send({
                                            error: false,
                                            message: ['Youtube already connected'],
                                        });
                                    }
                                })
                                .catch(async (error) => {
                                    return res.status(500).json({
                                        error: true,
                                        message: [constants.SERVER_ERR],
                                    });
                                });
                        }
                    })
                    .catch(async (error) => {
                        return res.status(500).json({
                            error: true,
                            message: [constants.SERVER_ERR],
                        });
                    });
            })
            .catch(async (error) => {
                return res.status(500).json({
                    error: true,
                    message: [constants.SERVER_ERR],
                });
            });
    } else {
        return res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

middlewares.disconnectYoutubeAccount = async (req, res) => {
    try {
        Youtube.findOneAndDelete(
            {
                influencer: req.user,
            },
            (error, deletedRecord) => {
                if (error)
                    return res.status(500).json({
                        error: true,
                        message: [error],
                    });

                return res.status(200).json({
                    error: false,
                    message: [constants.SUCCESS],
                });
            }
        );
    } catch (error) {
        return res.status(500).json({
            error: true,
            message: [error],
        });
    }
};

async function assignYoutube(youtube, response, req) {
    try {
        youtube.influencer = req.user;
        youtube.channelId = response.data.items[0].id;

        youtube.statistics = {};
        youtube.statistics.viewCount = response.data.items[0].statistics.viewCount;
        youtube.statistics.subscriberCount = response.data.items[0].statistics.subscriberCount;
        youtube.statistics.videoCount = response.data.items[0].statistics.videoCount;

        youtube.channelData = {};
        youtube.channelData.name = response.data.items[0].snippet.title;
        youtube.channelData.description = response.data.items[0].snippet.description;
        youtube.channelData.dateChannelCreated = response.data.items[0].snippet.publishedAt;
        youtube.channelData.country = response.data.items[0].snippet.country;
        youtube.channelData.privacyStatus = response.data.items[0].status.privacyStatus;

        if (response.data.items[0].snippet.thumbnails.default.url) {
            youtube.channelData.image = response.data.items[0].snippet.thumbnails.default.url;

            await Influencer.findById(req.user).then(async (influencer) => {
                influencer.profilePicture = youtube.channelData.image;

                return await influencer.save();
            });
        } else {
            await Constant.findOne({
                name: 'avatar',
            }).then(async (constant) => {
                youtube.channelData.image = constant.value;
            });
        }

        // saving topic ids in topics
        // to get topic name go here https://developers.google.com/youtube/v3/docs/channels#
        youtube.channelData.topics = [];
        if (response.data.items[0].topicDetails) {
            response.data.items[0].topicDetails.topicIds.map((topicId) => youtube.channelData.topics.push(topicId));
        }
    } catch (error) {
        throw new Error(error);
    }
}

module.exports = middlewares;
