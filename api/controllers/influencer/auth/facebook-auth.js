const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const Influencer = require('../../../models/influencer/influencer');
const Facebook = require('../../../models/social/facebook');

const config = require('../../../config');

const constants = require('../../../constants');

const middlewares = {};

middlewares.assignFacebook = (facebookInstance, body) => {
    facebookInstance.accessToken = body.accessToken;
    facebookInstance.accessTokenExpiry = body.expiry;
    facebookInstance.name = body.name;
    facebookInstance.email = body.email;
    facebookInstance.profilePicture = body.profilePicture;
    facebookInstance.userId = body.userId;
};

middlewares.signinFacebook = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Facebook.findOne({ userId: req.body.userId })
            .then((facebook) => {
                if (facebook == null) {
                    Influencer.findOne({ email: req.body.email })
                        .then((influencer) => {
                            if (influencer != null) {
                                res.status(400).json({ error: true, message: ['Connected Email is already in use.'] });
                            } else if (!req.body.email) {
                                // No email is associated with fb account.
                                res.status(400).json({
                                    error: true,
                                    message: ['Email not connected to facebook account. Try signing up with email.'],
                                });
                            } else {
                                facebook = new Facebook();
                                middlewares.assignFacebook(facebook, req.body);

                                facebook
                                    .save()
                                    .then((facebookInstance) => {
                                        res.status(201).json({
                                            error: false,
                                            result: { facebook: facebookInstance._id },
                                            message: [constants.SUCCESS],
                                        });
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: true, message: [error] });
                                    });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error] });
                        });
                } else {
                    // facebook account found but profile is not complete
                    if (facebook.influencer === null) {
                        res.status(202).json({ error: false, result: { facebook: facebook._id }, message: [constants.SUCCESS] });
                    } else {
                        // login with facebook account successfull
                        const jwtToken = jwt.sign(
                            {
                                userId: facebook.influencer,
                                userType: constants.INFLUENCER,
                            },
                            config.JWT_SECRET
                        );

                        res.status(200).json({
                            error: false,
                            result: {
                                auth: true,
                                token: jwtToken,
                                userId: facebook.influencer,
                            },
                        });
                    }
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
