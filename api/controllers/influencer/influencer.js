const { validationResult } = require('express-validator');
const _ = require('lodash');

const Influencer = require('../../models/influencer/influencer');

const Instagram = require('../../models/social/instagram');
const Facebook = require('../../models/social/facebook');
const Youtube = require('../../models/social/youtube');
const SocialPricing = require('../../models/social/social-pricing');
const Twitter = require('../../models/social/twitter');

const Category = require('../../models/category');
const Wallet = require('../../models/wallet/wallet');
const NewsletterSubscription = require('../../models/newsletter-subscription');
const KYC = require('../../models/wallet/kyc');

const populateCategories = require('../util/populate-categories');

const constants = require('../../constants');

const middlewares = {};

const fetchProfile = async (influencer, instagrams, facebook, youtube, twitter, wallet, newsletter, socialPricing) => {
    let profileCompletePercentage = 0;

    if (socialPricing !== null) {
        if (socialPricing.instagram) {
            if (
                socialPricing.instagram.staticPost ||
                socialPricing.instagram.staticStory ||
                socialPricing.instagram.videoPost ||
                socialPricing.instagram.videoStory ||
                socialPricing.instagram.reelVideo ||
                socialPricing.instagram.carouselPost
            )
                profileCompletePercentage += 25;
        }
    }

    let kyc = await KYC.findOne({
        influencer: influencer._id,
    });

    if (kyc !== null) profileCompletePercentage += 25;

    if (instagrams.length > 0 && facebook !== null) profileCompletePercentage += 25;

    if (!_.isEmpty(wallet.bankAccounts) || !_.isEmpty(wallet.upiAccounts)) profileCompletePercentage += 25;
    console.log(twitter);
    return {
        _id: influencer._id,
        name: influencer.name,
        dob: influencer.dob,
        gender: influencer.gender,
        city: influencer.city,
        email: influencer.email,
        phoneNumber: influencer.phoneNumber,
        socials: {
            instagrams: instagrams,
            facebook: facebook,
            youtube: youtube,
            pricing: socialPricing,
            twitter: twitter,
        },
        wallet: wallet,
        categories: await populateCategories(influencer.categories),
        profilePicture: influencer.profilePicture,
        referralCode: influencer.referralCode,
        referralLink: influencer.referralLink,
        newsletter: newsletter,
        profileCompletePercentage,
        createdAt: influencer.createdAt,
        updatedAt: influencer.updatedAt,
    };
};

middlewares.fetchInfluencer = async (influencerId) => {
    return await Influencer.findById(influencerId)
        .then(async (influencer) => {
            if (influencer != null) {
                return await Instagram.find({
                    influencer: influencerId,
                })
                    .then(async (instagrams) => {
                        return await Facebook.findOne({
                            influencer: influencerId,
                        })
                            .then(async (facebook) => {
                                return await Youtube.findOne({
                                    influencer: influencerId,
                                })
                                    .then(async (youtube) => {
                                        return await Wallet.findOne({
                                            influencer: influencerId,
                                        })
                                            .then(async (wallet) => {
                                                if (wallet != null) {
                                                    return await NewsletterSubscription.findOne({
                                                        id: influencerId,
                                                    }).then(async (subscription) => {
                                                        return await SocialPricing.findOne({
                                                            influencer: influencerId,
                                                        })
                                                            .then(async (socialPricing) => {
                                                                console.log(influencerId);
                                                                return await Twitter.findOne({
                                                                    influencer: influencerId,
                                                                })
                                                                    .then(async (twitter) => {
                                                                        console.log(`Twitter: ${twitter}`);
                                                                        return await fetchProfile(
                                                                            influencer,
                                                                            instagrams,
                                                                            facebook,
                                                                            youtube,
                                                                            twitter,
                                                                            wallet,
                                                                            subscription != null,
                                                                            socialPricing
                                                                        );
                                                                    })
                                                                    .catch((err) => {
                                                                        return Promise.reject(err);
                                                                    });
                                                            })
                                                            .catch((error) => {
                                                                return Promise.reject(error);
                                                            });
                                                    });
                                                } else {
                                                    return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Wallet'));
                                                }
                                            })
                                            .catch((error) => {
                                                return Promise.reject(error);
                                            });
                                    })
                                    .catch((error) => {
                                        return Promise.reject(error);
                                    });
                            })
                            .catch((error) => {
                                return Promise.reject(error);
                            });
                    })
                    .catch((error) => {
                        return Promise.reject(error);
                    });
            } else {
                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR(constants.INFLUENCER));
            }
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

middlewares.assignProfile = async (influencer, profile) => {
    influencer.name = profile.name;
    influencer.dob = profile.dob;
    influencer.gender = profile.gender;
    influencer.city = profile.city;
    influencer.phoneNumber = profile.phoneNumber;
};

middlewares.getProfile = (req, res, next) => {
    middlewares
        .fetchInfluencer(req.user)
        .then((result) => {
            res.status(200).json({
                error: false,
                result: result,
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: true,
                message: [error],
            });
        });
};

middlewares.saveProfile = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    } else {
        Influencer.findById(req.user)
            .then((influencer) => {
                if (influencer != null) {
                    middlewares.assignProfile(influencer, req.body);
                    influencer
                        .save()
                        .then(() => {
                            res.status(201).json({
                                error: false,
                                message: [constants.SUCCESS],
                            });
                        })
                        .catch((error) => {
                            res.status(500).json({
                                error: true,
                                message: [error.message],
                            });
                        });
                } else {
                    res.status(400).json({
                        error: true,
                        message: [constants.RESOURCE_NOT_FOUND_ERROR(constants.INFLUENCER)],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    error: true,
                    message: [error.message],
                });
            });
    }
};

middlewares.getCategories = (req, res, next) => {
    Category.find()
        .lean()
        .then(async (categories) => {
            const finalCategories = [];
            for (let category of categories) {
                const influencerCategory = {
                    id: category._id,
                    name: category.name,
                    description: category.description,
                    icon: category.icon,
                };
                finalCategories.push(influencerCategory);
            }
            res.status(200).json({
                error: false,
                result: finalCategories,
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: true,
                message: [error.message],
            });
        });
};

middlewares.getInfluencerCategories = (req, res, next) => {
    Category.find()
        .then(async (categories) => {
            Influencer.findById(req.user)
                .then(async (influencer) => {
                    if (influencer != null) {
                        const finalCategories = [];
                        for (let category of categories) {
                            const influencerCategory = {
                                id: category._id,
                                name: category.name,
                                description: category.description,
                                icon: category.icon,
                            };
                            if (influencer.categories.includes(category._id) && req.query.selected !== 'false') {
                                influencerCategory.selected = true;
                                finalCategories.push(influencerCategory);
                            }
                            if (!influencer.categories.includes(category._id) && req.query.selected !== 'true') {
                                influencerCategory.selected = false;
                                finalCategories.push(influencerCategory);
                            }
                        }
                        res.status(200).json({
                            error: false,
                            result: finalCategories,
                        });
                    } else {
                        res.status(400).json({
                            error: true,
                            message: [constants.RESOURCE_NOT_FOUND_ERROR(constants.INFLUENCER)],
                        });
                    }
                })
                .catch((error) => {
                    res.status(500).json({
                        error: true,
                        message: [error.message],
                    });
                });
        })
        .catch((error) => {
            res.status(500).json({
                error: true,
                message: [error.message],
            });
        });
};

middlewares.saveCategories = (req, res, next) => {
    Influencer.findById(req.user)
        .then((influencer) => {
            if (influencer != null) {
                influencer.categories = req.body.categoryIds;
                influencer
                    .save()
                    .then((record) => {
                        res.status(201).json({
                            error: false,
                            message: [constants.SUCCESS],
                        });
                    })
                    .catch((error) => {
                        res.status(500).json({
                            error: true,
                            message: [error.message],
                        });
                    });
            } else {
                res.status(400).json({
                    error: true,
                    message: [constants.RESOURCE_NOT_FOUND_ERROR(constants.INFLUENCER)],
                });
            }
        })
        .catch((error) => {
            res.status(500).json({
                error: true,
                message: [error.message],
            });
        });
};

middlewares.subscribe = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const subscription = new NewsletterSubscription();
        subscription.id = req.user;
        subscription.type = constants.INFLUENCER;
        subscription.email = req.body.email;

        subscription
            .save()
            .then(() => {
                res.status(200).json({
                    error: false,
                    message: [constants.SUCCESS],
                });
            })
            .catch((error) => {
                res.status(400).json({
                    error: true,
                    message: [error.message],
                });
            });
    } else {
        res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
};

module.exports = middlewares;
