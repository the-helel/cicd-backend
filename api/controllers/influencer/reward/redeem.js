/**
 * Controllers related to influencer and products.
 * Influencer Can
 *  - View All Products
 *  - View Product by ID
 *  - Claim Products
 *
 *  @author Rahul Bera <rbasu611@gmail.com>
 */

const mongoose = require('mongoose');
const { validationResult } = require('express-validator');
const seedrandom = require('seedrandom');

const constants = require('../../../constants');

const Products = require('../../../models/reward/product');
const PointsTransactions = require('../../../models/reward/points-transaction');
const createTransaction = require('./create-transaction');
const RewardsWallet = require('../../../models/reward/rewards-wallet');
const Order = require('../../../models/reward/order');

const { addMoneyToWallet } = require('../wallet/wallet');

const middlewares = {};

middlewares.getCashScratchCards = (req, res, next) => {
    Products.find({ productType: constants.CASH })
        .lean()
        .then((products) => {
            res.status(200).json({ error: false, result: products });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error] });
        });
};

middlewares.getAllProducts = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        Products.find((err, docs) => {
            if (err) return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });

            if (docs.length === 0) return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Products')] });

            return res.status(200).send({ error: false, result: { content: docs } });
        });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.getProductById = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const productId = req.params.id;

        Products.findById(productId, (err, doc) => {
            if (err) return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });

            if (doc.length === 0) return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Product')] });

            return res.status(200).send({ error: false, result: { content: doc } });
        });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.claimScratchCard = async (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Products.findById(req.body.productId)
            .then((product) => {
                if (product != null) {
                    if (product.productType === constants.CASH) {
                        const rn = seedrandom(new Date());
                        console.log('Random: ', rn);
                        const num = rn();
                        console.log('Num:', num);
                        const percent = Math.floor(5 + num * 6);
                        console.log('Percent: ', percent);
                        const cashReward = Math.floor((product.productRedeemPoints * percent) / 100);
                        console.log('Cash reward: ', cashReward);

                        RewardsWallet.findOne({ influencer: req.user })
                            .then(async (rewardsWallet) => {
                                if (rewardsWallet != null) {
                                    if (rewardsWallet.points >= product.productRedeemPoints) {
                                        const session = await mongoose.startSession();
                                        await session.startTransaction();

                                        rewardsWallet.points -= parseInt(product.productRedeemPoints);
                                        rewardsWallet
                                            .save({ session })
                                            .then(async () => {
                                                const order = new Order();
                                                order.productId = product._id;
                                                order.userId = req.user;

                                                order
                                                    .save({ session })
                                                    .then(async () => {
                                                        const metadata = {
                                                            source: constants.CASH,
                                                            sourceId: product._id,
                                                        };
                                                        await createTransaction(
                                                            constants.REDEEM,
                                                            req.user,
                                                            product.productRedeemPoints,
                                                            metadata,
                                                            session
                                                        )
                                                            .then(() => {
                                                                addMoneyToWallet(
                                                                    req.user,
                                                                    cashReward,
                                                                    constants.SCRATCHCARD,
                                                                    product._id,
                                                                    session
                                                                )
                                                                    .then(async () => {
                                                                        res.status(200).json({
                                                                            error: false,
                                                                            result: [constants.SUCCESS],
                                                                            reward: cashReward,
                                                                        });
                                                                        await session.commitTransaction();
                                                                        session.endSession();
                                                                    })
                                                                    .catch(async (error) => {
                                                                        res.status(500).json({ error: true, message: [error] });
                                                                        await session.abortTransaction();
                                                                        session.endSession();
                                                                    });
                                                            })
                                                            .catch(async (error) => {
                                                                res.status(500).json({ error: true, message: [error] });
                                                                await session.abortTransaction();
                                                                session.endSession();
                                                            });
                                                    })
                                                    .catch(async (error) => {
                                                        res.status(500).json({ error: true, message: [error] });
                                                        await session.abortTransaction();
                                                        session.endSession();
                                                    });
                                            })
                                            .catch(async (error) => {
                                                await session.abortTransaction();
                                                session.endSession();
                                                return res.status(500).json({ error: true, message: [error] });
                                            });
                                    } else {
                                        return res.status(400).json({ error: true, message: ["You don't have enough points."] });
                                    }
                                } else {
                                    return res
                                        .status(400)
                                        .json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Rewards wallet')] });
                                }
                            })
                            .catch((error) => {
                                return res.status(500).json({ error: true, message: [error] });
                            });
                    } else {
                        return res.status(400).json({ error: true, message: ['Cannot claim this product'] });
                    }
                } else {
                    return res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Product')] });
                }
            })
            .catch((error) => {
                return res.status(500).json({ error: true, message: [error] });
            });
    } else {
        return res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.claimProduct = async (req, res, next) => {
    const errors = validationResult(req);

    let userId = req.user;
    if (errors.isEmpty()) {
        const { productId, address, pinCode, phoneNumber, city, state } = req.body;

        Products.findById(productId, (err, doc) => {
            if (err) return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });

            if (doc.length === 0) return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Product')] });

            if (doc.quantity > 0) {
                const totalPointsrequired = parseInt(doc.productRedeemPoints);

                RewardsWallet.findOne({ influencer: req.user }, async (err, doc) => {
                    if (err) {
                        console.log(err);
                        return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                    }

                    if (doc.length === 0)
                        return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('User')] });

                    let pointsHave = doc.points;

                    if (totalPointsrequired > pointsHave) {
                        return res.status(400).send({ error: true, message: ['Points not sufficient'] });
                    } else {
                        /**
                         * Decrese quantity
                         * remove points
                         * transaction log
                         *
                         */

                        // Transaction to perform the operation completely or end ttransaction
                        const session = await mongoose.startSession();
                        await session.startTransaction();

                        try {
                            const opts = { session, new: true };

                            Products.findByIdAndUpdate(productId, { $inc: { quantity: -1 } }, opts, async (err, doc) => {
                                if (err) {
                                    await session.abortTransaction();
                                    session.endSession();

                                    console.log(err);
                                    console.log('Finding product failed');

                                    return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                                }

                                RewardsWallet.findOneAndUpdate(
                                    { influencer: req.user },
                                    { $inc: { points: parseInt(totalPointsrequired) * -1 } },
                                    opts,
                                    async (err, doc) => {
                                        if (err) {
                                            await session.abortTransaction();
                                            session.endSession();

                                            console.log(err);
                                            console.log('Increasing point failed');

                                            return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                                        }

                                        const newOrder = new Order({
                                            productId,
                                            userId,
                                            shippingDetails: {
                                                address,
                                                pinCode,
                                                phoneNumber,
                                                city,
                                                state,
                                            },
                                        });

                                        const newPointsTransaction = new PointsTransactions({
                                            transactionType: constants.REDEEM,
                                            userId: req.user,
                                            points: parseInt(totalPointsrequired) * -1,
                                        });

                                        newOrder
                                            .save()
                                            .then(async (order) => {
                                                newPointsTransaction.metadata.sourceId = order._id;
                                                // commiting the transaction
                                                newPointsTransaction
                                                    .save()
                                                    .then(async () => {
                                                        await session.commitTransaction();
                                                        session.endSession();

                                                        return res.status(200).send({ error: false, message: [constants.SUCCESS] });
                                                    })
                                                    .catch(async (err) => {
                                                        await session.abortTransaction();
                                                        session.endSession();

                                                        return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                                                    });
                                            })
                                            .catch(async (err) => {
                                                await session.abortTransaction();
                                                session.endSession();

                                                console.log(err);
                                                console.log('saving transaction failed');

                                                return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                                            });
                                    }
                                );
                            });
                        } catch (err) {
                            await session.abortTransaction();
                            session.endSession();

                            console.log(err);
                            console.log('Transaction failed');

                            return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                        }
                    }
                });
            } else {
                return res.status(400).send({ error: true, message: ['Not enough product'] });
            }
        });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
