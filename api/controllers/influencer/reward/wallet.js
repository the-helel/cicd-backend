const RewardsWallet = require('../../../models/reward/rewards-wallet');

const constants = require('../../../constants');
const pointsTransaction = require('../../../models/reward/points-transaction');

const middlewares = {};

middlewares.getRewardsWallet = (req, res, next) => {
    RewardsWallet.findOne({ influencer: req.user })
        .lean()
        .then((rewardsWallet) => {
            if (rewardsWallet) {
                res.status(200).json({ error: false, result: rewardsWallet });
            } else {
                res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Rewards Wallet')] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getPointsTransactionHistory = async (req, res, next) => {
    let { pageNo, size } = req.query;

    if (!pageNo) pageNo = 1;
    if (!size) size = 10;

    if (pageNo <= 0) pageNo = 1;

    let skip = (pageNo - 1) * size;

    try {
        let transactions = await pointsTransaction
            .find({ userId: req.user })
            .sort({ createdAt: -1 })
            .skip(skip)
            .limit(parseInt(size))
            .lean();

        if (transactions.length === 0) return res.status(200).send({ error: false, result: [] });

        return res.status(200).send({ error: false, result: transactions });
    } catch (err) {
        return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
    }
};

module.exports = middlewares;
