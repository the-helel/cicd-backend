const PointsTransaction = require('../../../models/reward/points-transaction');

const createTransaction = async (transactionType, userId, points, metadata, session) => {
    const transaction = new PointsTransaction();
    transaction.transactionType = transactionType;
    transaction.userId = userId;
    transaction.points = points;
    transaction.metadata = metadata;

    return await transaction
        .save()
        .then((t) => {
            console.log('Transaction created', t);
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = createTransaction;
