const { validationResult } = require('express-validator');

const UIDGenerator = require('uid-generator');
const uidGen = new UIDGenerator();

const ShippingDetails = require('../../../models/reward/shipping-detail');

const constants = require('../../../constants');

const middlewares = {};

middlewares.getShippingDetails = (req, res, next) => {
    ShippingDetails.findOne({ userId: req.user })
        .lean()
        .then((shippingDetails) => {
            if (shippingDetails) {
                res.status(200).json({ error: false, result: shippingDetails });
            } else {
                res.status(400).json({ error: true, message: ['Shipping details not found.'] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.addShippingDetails = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        ShippingDetails.findOne({ userId: req.user }).then(async (shippingDetails) => {
            let shippingDetailsObject = shippingDetails;
            if (!shippingDetails) {
                shippingDetailsObject = new ShippingDetails();
                shippingDetailsObject.userId = req.user;
                shippingDetailsObject.shippingDetails = {};
            }
            const uid = await uidGen.generate();
            shippingDetailsObject.shippingDetails[uid] = {};
            shippingDetailsObject.shippingDetails[uid].name = req.body.name;
            shippingDetailsObject.shippingDetails[uid].email = req.body.email;
            shippingDetailsObject.shippingDetails[uid].address = req.body.address;
            shippingDetailsObject.shippingDetails[uid].pinCode = req.body.pinCode;
            shippingDetailsObject.shippingDetails[uid].phoneNumber = req.body.phoneNumber;
            shippingDetailsObject.shippingDetails[uid].city = req.body.city;
            shippingDetailsObject.shippingDetails[uid].state = req.body.state;

            shippingDetailsObject.markModified('shippingDetails');

            shippingDetailsObject
                .save()
                .then(() => {
                    res.status(200).json({ error: false, result: constants.SUCCESS });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.editShippingDetails = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        ShippingDetails.findOne({ userId: req.user }).then((shippingDetails) => {
            if (!shippingDetails) {
                return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Shipping details')] });
            } else {
                const uid = req.body.uid;
                if (Object.keys(shippingDetails.shippingDetails).includes(uid)) {
                    shippingDetails.shippingDetails[uid].name = req.body.name;
                    shippingDetails.shippingDetails[uid].email = req.body.email;
                    shippingDetails.shippingDetails[uid].address = req.body.address;
                    shippingDetails.shippingDetails[uid].pinCode = req.body.pinCode;
                    shippingDetails.shippingDetails[uid].phoneNumber = req.body.phoneNumber;
                    shippingDetails.shippingDetails[uid].city = req.body.city;
                    shippingDetails.shippingDetails[uid].state = req.body.state;

                    shippingDetails.markModified('shippingDetails');

                    shippingDetails
                        .save()
                        .then(() => {
                            res.status(200).json({ error: false, result: constants.SUCCESS });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Item')] });
                }
            }
        });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.removeShippingDetails = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const index = parseInt(req.query.index);
        ShippingDetails.findOne({ userId: req.user }).then((shippingDetails) => {
            if (shippingDetails) {
                const uid = req.query.uid;
                if (Object.keys(shippingDetails.shippingDetails).includes(uid)) {
                    delete shippingDetails.shippingDetails[uid];

                    shippingDetails.markModified('shippingDetails');

                    shippingDetails
                        .save()
                        .then(() => {
                            res.status(200).json({ error: false, result: constants.SUCCESS });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Item')] });
                }
            } else {
                return res.status(400).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Shipping details')] });
            }
        });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
