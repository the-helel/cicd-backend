const mongoose = require('mongoose');

const RewardsWallet = require('../../../models/reward/rewards-wallet');
const Referral = require('../../../models/activity/referral');
const Constant = require('../../../models/constant');
const Influencer = require('../../../models/influencer/influencer');

const createTransaction = require('./create-transaction');

const constants = require('../../../constants');

const middlewares = {};

middlewares.addReferral = async (referee, referred) => {
    Influencer.findOne({ referralCode: referee })
        .then(async (influencer) => {
            if (influencer != null) {
                const session = await mongoose.startSession();
                session.startTransaction();

                const referral = new Referral();

                referral.influencer = mongoose.Types.ObjectId(referred);
                referral.referee = mongoose.Types.ObjectId(influencer._id);

                return await referral
                    .save({ session })
                    .then(async () => {
                        await session.commitTransaction();
                        await session.endSession();
                        return;
                    })
                    .catch(async (error) => {
                        await session.abortTransaction();
                        await session.endSession();
                        // return Promise.reject(error.message);
                    });
            } else {
                // return Promise.reject('Referee influencer does not exists.');
            }
        })
        .catch((error) => {
            // return Promise.reject(error);
        });
};

middlewares.addInstagramConnect = async (influencer, followersCount) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    return await Referral.findOne({ influencer: influencer })
        .then(async (referral) => {
            if (referral && followersCount >= 5000) {
                let key = null;
                if (followersCount < 10000) {
                    key = 'div1Insta';
                } else if (followersCount < 50000) {
                    key = 'div2Insta';
                } else if (followersCount < 100000) {
                    key = 'div3Insta';
                } else if (followersCount < 500000) {
                    key = 'div4Insta';
                } else {
                    key = 'div5Insta';
                }

                // Adding reward to user who is connecting instagram
                return await RewardsWallet.findOne({ influencer: influencer })
                    .then(async (refereeWallet) => {
                        if (refereeWallet != null) {
                            return await Constant.findOne({ name: 'instaConnect' })
                                .then(async (constant) => {
                                    refereeWallet.points += parseInt(constant.value);
                                    return await refereeWallet
                                        .save({ session })
                                        .then(async () => {
                                            // creating a transaction record in points-trasaction collection
                                            return await createTransaction(
                                                constants.CREDIT,
                                                influencer,
                                                constant.value,
                                                {
                                                    source: constants.INSTAGRAM_CONNECT,
                                                    sourceId: null,
                                                },
                                                session
                                            )
                                                .then(async () => {
                                                    // add reward to user who have refferd the user (referee)
                                                    return await RewardsWallet.findOne({ influencer: referral.referee })
                                                        .then(async (referredWallet) => {
                                                            if (referredWallet != null) {
                                                                return await Constant.findOne({ name: key })
                                                                    .then(async (constant) => {
                                                                        referredWallet.points += parseInt(constant.value);
                                                                        return await referredWallet
                                                                            .save({ session })
                                                                            .then(async () => {
                                                                                // retriving name of user who have been referred
                                                                                let { name } = await Influencer.findById(influencer, {
                                                                                    name: 1,
                                                                                    _id: 0,
                                                                                }).lean();
                                                                                return await createTransaction(
                                                                                    constants.CREDIT,
                                                                                    referral.referee,
                                                                                    constant.value,
                                                                                    {
                                                                                        source: constants.INSTAGRAM_CONNECT,
                                                                                        sourceId: influencer,
                                                                                        name: name,
                                                                                    },
                                                                                    session
                                                                                )
                                                                                    .then(async () => {
                                                                                        await session.commitTransaction();
                                                                                        session.endSession();
                                                                                        return;
                                                                                    })
                                                                                    .catch(async (error) => {
                                                                                        return Promise.reject(error);
                                                                                    });
                                                                            })
                                                                            .catch(async (error) => {
                                                                                return Promise.reject(error);
                                                                            });
                                                                    })
                                                                    .catch(async (error) => {
                                                                        return Promise.reject(error.message);
                                                                    });
                                                            } else {
                                                                return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Referee'));
                                                            }
                                                        })
                                                        .catch(async (error) => {
                                                            return Promise.reject(error);
                                                        });
                                                })
                                                .catch(async (error) => {
                                                    return Promise.reject(error);
                                                });
                                        })
                                        .catch(async (error) => {
                                            return Promise.reject(error);
                                        });
                                })
                                .catch(async (error) => {
                                    return Promise.reject(error);
                                });
                        } else {
                            return Promise.reject(constants.RESOURCE_NOT_FOUND_ERROR('Referee'));
                        }
                    })
                    .catch(async (error) => {
                        return Promise.reject(error);
                    });
            } else {
                await session.abortTransaction();
                session.endSession();
                return;
            }
        })
        .catch(async (error) => {
            await session.abortTransaction();
            session.endSession();
            return Promise.reject(error);
        });
};

module.exports = middlewares;
