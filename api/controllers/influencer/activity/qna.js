/**
 * Middlewares for QnA feature on famstar
 *
 */

const { validationResult } = require('express-validator');
const mongoose = require('mongoose');
const Filter = require('bad-words');
const filter = new Filter();

const Question = require('../../../models/activity/QnA/question');
const Answer = require('../../../models/activity/QnA/answer');
const LikeQnA = require('../../../models/activity/QnA/like-QnA');
const Influencer = require('../../../models/influencer/influencer');

const constants = require('../../../constants');
const { sendNotificationById } = require('../../util/notification');

const middlewares = {};

// add question
middlewares.postQuestion = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        if (filter.isProfane(req.body.description)) {
            return res.status(400).json({ error: true, message: ['Profane words detected!! Cannot post'] });
        }
        const question = new Question();
        question.media = [];
        question.askedBy = req.user;
        question.description = req.body.description;

        if (req.files !== undefined) {
            for (let file of req.files) {
                question.media.push(file.location);
            }
        }

        question
            .save()
            .then(() => {
                return res.status(200).json({ error: false, message: [constants.SUCCESS] });
            })
            .catch(() => {
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            });
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

// like a question
middlewares.likeQuestion = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const question = req.params.id;
        const influencer = req.user;

        LikeQnA.findOne({ influencer: influencer, question: question })
            .then(async (likeInstance) => {
                if (likeInstance === null) {
                    likeInstance = new LikeQnA({ influencer, question: question, isLiked: true });
                } else if (likeInstance.isLiked) {
                    return res.status(200).json({ error: false, message: ['Already liked'] });
                } else {
                    likeInstance.isLiked = true;
                }
                likeInstance
                    .save()
                    .then(async () => {
                        return res.status(200).json({ error: false, message: [constants.SUCCESS] });
                    })
                    .catch(async (err) => {
                        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
                    });
            })
            .catch(async (err) => {
                console.log(err);
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            });
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

// unlike question
middlewares.unlikeQuestion = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const question = req.params.id;
        const influencer = req.user;

        LikeQnA.findOne({ question: question, influencer: influencer, isLiked: true })
            .then((likeInstance) => {
                if (likeInstance === null) return res.status(400).json({ error: true, message: ['Already Unliked'] });

                likeInstance.isLiked = false;
                likeInstance
                    .save()
                    .then(() => {
                        return res.status(200).json({ error: false, message: [constants.SUCCESS] });
                    })
                    .catch(async (err) => {
                        return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
                    });
            })
            .catch(async (err) => {
                return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
            });
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

// answer a question
middlewares.answerQuestion = async (req, res, next) => {
    const errors = validationResult(req);

    console.log(errors.array());

    if (errors.isEmpty()) {
        const answeredBy = req.user;
        const answerBody = req.body.answerBody;
        const question = req.body.questionId;

        if (filter.isProfane(answerBody)) {
            return res.status(400).json({ error: true, message: ['Profane words detected !!'] });
        }

        const answer = new Answer({ answerBody, answeredBy, question });

        try {
            await answer.save();

            let questionInstance = await Question.findById(question);

            if (questionInstance === null) {
                return res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Question')] });
            }

            if (answeredBy !== '' + questionInstance['askedBy']) {
                let { name } = await Influencer.findById(req.user, { name: 1, _id: 0 }).lean();

                // sending notification to the user, who asked this question
                let [success, err] = await sendNotificationById(
                    questionInstance.askedBy,
                    'Famstar Forum',
                    `${name} commented on, ${
                        questionInstance.description.length > 30
                            ? questionInstance.description.substring(0, 30) + '...'
                            : questionInstance.description
                    }`,
                    null,
                    'FAMSTAR_FORUM',
                    '',
                    {
                        questionId: '' + questionInstance['_id'],
                    }
                );
            }

            return res.status(200).json({ error: false, message: [constants.SUCCESS] });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

// send list of questions
middlewares.getQuestions = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const page = req.query.page ? parseInt(req.query.page) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 10;
        let sortFilter = { _id: -1 };

        Question.aggregate([
            {
                $sort: sortFilter,
            },
            {
                $skip: size * (page - 1),
            },
            {
                $limit: size,
            },
            {
                $lookup: {
                    from: 'like-qnas',
                    let: { question_id: '$_id' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [{ $eq: ['$question', '$$question_id'] }, { $eq: ['$isLiked', true] }],
                                },
                            },
                        },
                        { $project: { influencer: 1, _id: 0 } },
                    ],
                    as: 'likes',
                },
            },
            {
                $lookup: {
                    from: 'answers',
                    localField: '_id',
                    foreignField: 'question',
                    as: 'answers',
                },
            },
            {
                $lookup: {
                    from: 'influencers',
                    localField: 'askedBy',
                    foreignField: '_id',
                    as: 'influencer',
                },
            },
            {
                $project: {
                    'influencer.profilePicture': 1,
                    'influencer.name': 1,
                    description: 1,
                    media: 1,
                    createdAt: 1,
                    likesCount: { $size: '$likes' },
                    commentsCount: { $size: '$answers' },
                    isLiked: { $in: [{ influencer: mongoose.Types.ObjectId(req.user) }, '$likes'] },
                },
            },
        ])
            .then((questions) => {
                return res.status(200).json({ error: false, result: questions });
            })
            .catch((err) => {
                throw new Error(err);
            });
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

// get answers of question whose id is passed in param
middlewares.getAnswers = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        let questionId = req.params.id;
        const page = req.query.page > 0 ? parseInt(req.query.page) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 10;

        try {
            let answers = await Answer.aggregate([
                {
                    $match: {
                        question: mongoose.Types.ObjectId(questionId),
                    },
                },
                {
                    $skip: size * (page - 1),
                },
                {
                    $limit: size,
                },
                {
                    $sort: {
                        createdAt: -1,
                    },
                },
                {
                    $lookup: {
                        from: 'influencers',
                        localField: 'answeredBy',
                        foreignField: '_id',
                        as: 'influencer',
                    },
                },
                {
                    $project: {
                        'influencer.profilePicture': 1,
                        'influencer.name': 1,
                        answerBody: 1,
                        createdAt: 1,
                    },
                },
            ]);

            return res.status(200).json({ error: true, result: answers });
        } catch (err) {
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

// get question by id
middlewares.getQuestionById = async (req, res, next) => {
    let { pageNo, size, id } = req.query;

    if (!pageNo) {
        pageNo = 1;
    }
    if (!size) {
        size = 10;
    }

    let skip = (pageNo - 1) * size;
    let limit = parseInt(size);

    const errors = validationResult(req);

    if (errors.isEmpty()) {
        try {
            let question = await Question.aggregate([
                {
                    $match: {
                        _id: mongoose.Types.ObjectId(id),
                    },
                },
                {
                    $lookup: {
                        from: 'influencers',
                        localField: 'askedBy',
                        foreignField: '_id',
                        as: 'influencer',
                    },
                },
                {
                    $lookup: {
                        from: 'answers',
                        localField: '_id',
                        foreignField: 'question',
                        as: 'answers',
                    },
                },
                {
                    $lookup: {
                        from: 'like-qnas',
                        let: { question_id: '$_id' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [{ $eq: ['$question', '$$question_id'] }, { $eq: ['$isLiked', true] }],
                                    },
                                },
                            },
                            { $project: { influencer: 1, _id: 0 } },
                        ],
                        as: 'likes',
                    },
                },
                {
                    $project: {
                        'influencer.profilePicture': 1,
                        'influencer.name': 1,
                        description: 1,
                        media: 1,
                        createdAt: 1,
                        likesCount: { $size: '$likes' },
                        commentsCount: { $size: '$answers' },
                        isLiked: { $in: [{ influencer: mongoose.Types.ObjectId(req.user) }, '$likes'] },
                    },
                },
            ])
                .skip(skip)
                .limit(limit);

            if (question === null) {
                return res.status(404).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Question')] });
            }

            return res.status(200).json({ error: false, result: question });
        } catch (err) {
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

// get questions asked by user
middlewares.getMyQuestions = async (req, res, next) => {
    let { pageNo, size } = req.query;

    if (!pageNo) {
        pageNo = 1;
    }
    if (!size) {
        size = 10;
    }

    let skip = (pageNo - 1) * size;
    let limit = parseInt(size);

    const errors = validationResult(req);

    if (errors.isEmpty()) {
        try {
            let questionsByMe = await Question.aggregate([
                {
                    $match: {
                        askedBy: mongoose.Types.ObjectId(req.user),
                    },
                },
                {
                    $lookup: {
                        from: 'influencers',
                        localField: 'askedBy',
                        foreignField: '_id',
                        as: 'influencer',
                    },
                },
                {
                    $lookup: {
                        from: 'answers',
                        localField: '_id',
                        foreignField: 'question',
                        as: 'answers',
                    },
                },
                {
                    $lookup: {
                        from: 'like-qnas',
                        let: { question_id: '$_id' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [{ $eq: ['$question', '$$question_id'] }, { $eq: ['$isLiked', true] }],
                                    },
                                },
                            },
                            { $project: { influencer: 1, _id: 0 } },
                        ],
                        as: 'likes',
                    },
                },
                {
                    $project: {
                        'influencer.profilePicture': 1,
                        'influencer.name': 1,
                        description: 1,
                        media: 1,
                        createdAt: 1,
                        likesCount: { $size: '$likes' },
                        commentsCount: { $size: '$answers' },
                        isLiked: { $in: [{ influencer: mongoose.Types.ObjectId(req.user) }, '$likes'] },
                    },
                },
            ])
                .skip(skip)
                .limit(limit);

            return res.status(200).json({ error: false, result: questionsByMe });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ error: true, message: [constants.SERVER_ERR] });
        }
    } else {
        return res.status(400).json({ error: true, message: [constants.VALIDATION_ERROR] });
    }
};

module.exports = middlewares;
