const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const Journey = require('../../../models/activity/journey');
const Influencer = require('../../../models/influencer/influencer');
const BookmarkJourney = require('../../../models/activity/bookmark-journey');

const constants = require('../../../constants');
const config = require('../../../config');
const GhostContentAPI = require('@tryghost/content-api');

const ghost = new GhostContentAPI({
    url: 'https://www.famstar.in/blog',
    key: `${config.GHOST_API_KEY}`,
    version: 'v3',
});

const middlewares = {};

middlewares.getAllJournies = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const size = req.query.size ? parseInt(req.query.size) : 10;
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;

        BookmarkJourney.findOne({ influencer: req.user })
            .lean()
            .then((bookmarkedJourneys) => {
                let bookmarkedJourneysList, havebookmarkedJourneys;

                if (bookmarkedJourneys === null) {
                    bookmarkedJourneys = new BookmarkJourney();
                    bookmarkedJourneys.influencer = req.user;
                    bookmarkedJourneys.journeys = [];
                }

                bookmarkedJourneysList = bookmarkedJourneys.journeys;

                if (bookmarkedJourneysList.length === 0) havebookmarkedJourneys = false;
                else havebookmarkedJourneys = true;

                ghost.posts
                    .browse({ page: pageNo, limit: size, filter: 'tag:influencer-story' })
                    .then((journeys) => {
                        const journeysList = [];

                        journeys.map((journey) => {
                            const journeyObject = {};

                            journeyObject.title = journey.title;
                            journeyObject.image = journey.feature_image;
                            journeyObject.url = journey.url;
                            journeyObject.ghostId = journey.id;
                            journeyObject.readingTime = journey.reading_time;
                            journeyObject.publishedAt = journey.published_at;

                            if (havebookmarkedJourneys === false) {
                                journeyObject.isBookmarked = false;
                            } else {
                                bookmarkedJourneysList.filter((journey) => journey.ghostId === journeyObject.ghostId).length > 0
                                    ? (journeyObject.isBookmarked = true)
                                    : (journeyObject.isBookmarked = false);
                            }

                            journeysList.push(journeyObject);
                        });
                        return res.status(200).send({ error: false, result: { content: journeysList } });
                    })
                    .catch((err) => {
                        return res.status(500).status({ error: true, message: [constants.SERVER_ERR] });
                    });
            });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.getBookmarkedJourneys = (req, res, next) => {
    const bookmarkedJourneysList = [];
    BookmarkJourney.findOne({ influencer: req.user })
        .lean()
        .then((bookmarkedJourneys) => {
            if (bookmarkedJourneys != null) {
                res.status(200).json({ error: false, result: bookmarkedJourneys.journeys });
            } else {
                res.status(200).json({ error: false, result: [] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.bookmarkJourney = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const ghostId = req.body.ghostId;
        let found = false;

        ghost.posts
            .read({ id: ghostId })
            .then((post) => {
                if (post) {
                    BookmarkJourney.findOne({ influencer: req.user })
                        .then((bookmarkedJourneys) => {
                            let bookmarkedJourneysList = null;
                            if (bookmarkedJourneys != null) {
                                bookmarkedJourneysList = bookmarkedJourneys;
                                bookmarkedJourneysList.journeys.filter((journey) => {
                                    if (journey.ghostId === ghostId) found = true;
                                });
                            } else {
                                bookmarkedJourneysList = new BookmarkJourney();
                                bookmarkedJourneysList.influencer = req.user;
                                bookmarkedJourneysList.journeys = [];
                            }

                            if (found === false) {
                                bookmarkedJourneysList.journeys.push({
                                    ghostId,
                                    title: post.title,
                                    url: post.url,
                                    image: post.feature_image,
                                    readingTime: post.reading_time,
                                    publishedAt: post.published_at,
                                });

                                bookmarkedJourneysList
                                    .save()
                                    .then(() => {
                                        res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                    })
                                    .catch((error) => {
                                        console.log(err);
                                        res.status(500).json({ error: true, message: [error.message] });
                                    });
                            } else {
                                res.status(200).json({ error: false, message: ['Journey Already Bookmarked'] });
                            }
                        })
                        .catch((err) => {
                            console.log(err, 148);
                            res.status(500).json({ error: true, message: [err.message] });
                        });
                } else res.status(404).json({ error: true, message: constants.RESOURCE_NOT_FOUND_ERROR('Journey') });
            })
            .catch((err) => {
                console.log(err);
                res.status(500).json({ error: true, message: [err.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.removeBookmarkedJourney = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        BookmarkJourney.findOne({ influencer: req.user })
            .then((bookmarkedJourneys) => {
                if (bookmarkedJourneys != null) {
                    bookmarkedJourneys.journeys = bookmarkedJourneys.journeys.filter((journey) => {
                        return journey.ghostId != req.query.id;
                    });
                    bookmarkedJourneys
                        .save()
                        .then(() => {
                            res.status(200).json({ error: false, message: [constants.SUCCESS] });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Bookmarked journeys')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
