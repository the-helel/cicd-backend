/**
 * Middleware for blog and earn
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 */

const { validationResult } = require('express-validator');

const Blog = require('../../../models/activity/blog');
const Influencer = require('../../../models/influencer/influencer');
const BookmarkBlog = require('../../../models/activity/bookmark-blog');

const constants = require('../../../constants');
const config = require('../../../config');
const GhostContentAPI = require('@tryghost/content-api');

const ghost = new GhostContentAPI({
    url: 'https://www.famstar.in/blog',
    key: `${config.GHOST_API_KEY}`,
    version: 'v3',
});

const middlewares = {};

middlewares.postBlogLink = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        Influencer.findById(req.user)
            .lean()
            .then((user) => {
                if (user == null) {
                    return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('User')] });
                }

                // Every validation is passed and new document is pushed to Blog Collection
                const newBlogEntry = new Blog({
                    postedBy: req.user,
                    blogURL: req.body.blogURL,
                    blogTitle: req.body.blogTitle,
                    blogDescription: req.body.blogDescription,
                });

                newBlogEntry
                    .save()
                    .then(() => {
                        return res.status(201).send({ error: false, result: { content: newBlogEntry } });
                    })
                    .catch((err) => {
                        return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
                    });
            })
            .catch((err) => {
                return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
            });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.getBlogInfoAndStatus = async (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        Blog.findById(req.query.id)
            .lean()
            .then((blog) => {
                if (blog == null) {
                    return res.status(404).send({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Blog')] });
                }
                return res.status(200).send({ error: false, result: { content: blog } });
            })
            .catch((err) => {
                return res.status(500).send({ error: true, message: [constants.SERVER_ERR] });
            });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.getAllBlogs = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        const size = req.query.size ? parseInt(req.query.size) : 10;
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;

        BookmarkBlog.findOne({ influencer: req.user })
            .lean()
            .then((bookmarkedBlogs) => {
                let bookmarkedBlogsList, haveBookmarkedBlogs;

                if (bookmarkedBlogs === null) {
                    bookmarkedBlogs = new BookmarkBlog();
                    bookmarkedBlogs.influencer = req.user;
                    bookmarkedBlogs.blogs = [];
                }

                bookmarkedBlogsList = bookmarkedBlogs.blogs;

                if (bookmarkedBlogsList.length === 0) haveBookmarkedBlogs = false;
                else haveBookmarkedBlogs = true;

                ghost.posts
                    .browse({ page: pageNo, limit: size, filter: 'tag:influencer-marketing' })
                    .then((blogs) => {
                        const blogsList = [];

                        blogs.map((blog) => {
                            const blogObject = {};

                            blogObject.title = blog.title;
                            blogObject.image = blog.feature_image;
                            blogObject.url = blog.url;
                            blogObject.ghostId = blog.id;
                            blogObject.readingTime = blog.reading_time;
                            blogObject.publishedAt = blog.published_at;

                            if (haveBookmarkedBlogs === false) {
                                blogObject.isBookmarked = false;
                            } else {
                                bookmarkedBlogsList.filter((blog) => blog.ghostId === blogObject.ghostId).length > 0
                                    ? (blogObject.isBookmarked = true)
                                    : (blogObject.isBookmarked = false);
                            }

                            blogsList.push(blogObject);
                        });
                        return res.status(200).send({ error: false, result: { content: blogsList } });
                    })
                    .catch((err) => {
                        return res.status(500).status({ error: true, message: [constants.SERVER_ERR] });
                    });
            });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

middlewares.getBookmarkedBlogs = (req, res, next) => {
    const bookmarkedBlogsList = [];
    BookmarkBlog.findOne({ influencer: req.user })
        .lean()
        .then((bookmarkedBlogs) => {
            if (bookmarkedBlogs != null) {
                res.status(200).json({ error: false, result: bookmarkedBlogs.blogs });
            } else {
                res.status(200).json({ error: false, result: [] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.bookmarkBlog = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const ghostId = req.body.ghostId;
        let found = false;

        ghost.posts
            .read({ id: ghostId })
            .then((post) => {
                if (post) {
                    BookmarkBlog.findOne({ influencer: req.user })
                        .then((bookmarkedBlogs) => {
                            let bookmarkedBlogsList = null;
                            if (bookmarkedBlogs != null) {
                                bookmarkedBlogsList = bookmarkedBlogs;
                                bookmarkedBlogsList.blogs.filter((blog) => {
                                    if (blog.ghostId === ghostId) found = true;
                                });
                            } else {
                                bookmarkedBlogsList = new BookmarkBlog();
                                bookmarkedBlogsList.influencer = req.user;
                                bookmarkedBlogsList.blogs = [];
                            }

                            if (!found) {
                                bookmarkedBlogsList.blogs.push({
                                    ghostId,
                                    title: post.title,
                                    url: post.url,
                                    image: post.feature_image,
                                    readingTime: post.reading_time,
                                    publishedAt: post.published_at,
                                });
                                bookmarkedBlogsList
                                    .save()
                                    .then(() => {
                                        res.status(200).json({ error: false, message: [constants.SUCCESS] });
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: true, message: [error.message] });
                                    });
                            } else {
                                res.status(200).json({ error: false, message: ['Blog Already Bookmarked'] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else res.status(404).json({ error: true, message: constants.RESOURCE_NOT_FOUND_ERROR('Blog') });
            })
            .catch((err) => res.status(500).json({ error: true, message: [err.message] }));
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

middlewares.removeBookmarkedBlog = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        BookmarkBlog.findOne({ influencer: req.user })
            .then((bookmarkedBlogs) => {
                if (bookmarkedBlogs != null && bookmarkedBlogs.blogs.length !== 0) {
                    let newBookmarkedBlogs = [];
                    bookmarkedBlogs.blogs.map((blog) => {
                        if (blog.ghostId !== req.query.ghostId) newBookmarkedBlogs.push(blog);
                    });

                    bookmarkedBlogs.blogs = newBookmarkedBlogs;
                    bookmarkedBlogs
                        .save()
                        .then(() => {
                            res.status(200).json({ error: false, message: [constants.SUCCESS] });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: [constants.RESOURCE_NOT_FOUND_ERROR('Bookmarked blogs')] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: errors.array() });
    }
};

module.exports = middlewares;
