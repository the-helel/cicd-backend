const router = require('express').Router();

router.get('/test', async (req, res, next) => {
    return res.status(200).json({
        message: 'V2',
    });
});

module.exports = router;
