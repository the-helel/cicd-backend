const router = require('express').Router();
const { query, param } = require('express-validator');
const { body } = require('express-validator');

// const campaignMiddlewares = require('../../controllers/campaign');

const authMiddleware = require('../../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../../controllers/auth/access-middleware');
const constants = require('../../../constants');
const config = require('../../../config');
const campaignMiddlewares = require('../../../controllers/company/campaign/campaign');
const applicantMiddlewares = require('../../../controllers/company/campaign/applicant');
const campaign = require('../../../controllers/campaign');
const { uploadPublicImage } = require('../../../../aws/aws-s3');

/*
- New @routes for campaign which can be referenced to influencers, company as well as admin
- here access is always granted for above the provided roles by default i.e if a api is accessible to
*/
router.post(
    '/campaigns/create',
    accessMiddleware(constants.COMPANY, 30),
    body('companyId').isMongoId().trim(),
    body('brandId').isMongoId(),
    body('campaignType').isString(),
    campaignMiddlewares.createCampaign
);

router.put(
    '/campaigns/:campaignId/edit/status',
    accessMiddleware(constants.COMPANY, 30),
    [body('brandId').isMongoId(), body('status').isString(), body('campaignId').isString()],
    campaignMiddlewares.editCampaignStatus
);

router.put(
    '/campaigns/:campaignId/edit',
    accessMiddleware(constants.COMPANY, 30),
    uploadPublicImage.single('coverImage'),
    uploadPublicImage.array('moodBoard', 5),
    [body('brandId').isMongoId(), body('campaignType').isString(), body('campaignId').isString()],
    campaignMiddlewares.editCampaign
);

router.post('/campaigns/live', accessMiddleware(constants.COMPANY, 30), campaignMiddlewares.liveCampaign);

// Add note for applicant
router
    .post(
        '/campaigns/applicant/note',
        accessMiddleware(constants.COMPANY, 20),
        [body('applicantId').isMongoId(), body('note').isString().trim().isLength({ min: 1, max: 5000 })],
        applicantMiddlewares.addNote
    )
    .get('/campaigns/applicant/:applicantId/note', accessMiddleware(constants.COMPANY, 20), applicantMiddlewares.getNotes)
    .put(
        '/campaigns/applicant/:noteId/note',
        accessMiddleware(constants.COMPANY, 20),
        body('note').isString().trim(),
        applicantMiddlewares.updateNote
    )
    .delete(
        '/campaigns/applicant/:noteId/note',
        accessMiddleware(constants.COMPANY, 20),
        param('noteId').isMongoId().trim(),
        applicantMiddlewares.deleteNote
    );

router.post(
    '/campaigns/:campaignId/applicants/cross-bid',
    accessMiddleware(constants.COMPANY, 30),
    [body('campaignId').isMongoId(), body('applicantId').isMongoId(), body('amount').isInt(), body('status').isString()],
    campaign.companyCrossBid
);

router.post(
    '/campaigns/:campaignId/copy',
    accessMiddleware(constants.COMPANY, 30),
    [body('companyId').isMongoId().notEmpty(), body('brandId').isMongoId(), body('campaignType').isString(), body('campaignId').isString()],
    campaignMiddlewares.copyCampaign
);
router.post('/campaigns/:campaignId/apply', accessMiddleware(constants.COMPANY, 30), applicantMiddlewares.createCampaignApplicant); // need to check
router.get('/campaigns', accessMiddleware(constants.MEMBER, 20), campaignMiddlewares.getCampaigns);
router.get(
    '/campaigns/:campaignId',
    accessMiddleware(constants.COMPANY, 0),
    param('campaignId').isMongoId().trim(),
    campaignMiddlewares.getCampaignById
);
router.delete('/campaigns/:campaignId', accessMiddleware(constants.COMPANY, 30), campaignMiddlewares.deleteCampaignById);

router.get('/campaigns/:campaignId/applicants', accessMiddleware(constants.COMPANY, 30), applicantMiddlewares.getCampaignApplicant);

router.put(
    '/campaigns/:campaignId/applicants/:applicantId/status',
    accessMiddleware(constants.COMPANY, 30),
    [body('status').isString().notEmpty()],
    applicantMiddlewares.updateApplicantStatus
);
router.put(
    '/campaigns/:campaignId/applicants/:applicantId/bid',
    accessMiddleware(constants.COMPANY, 30),
    [body('bidAmount').isString().notEmpty()],
    applicantMiddlewares.updateApplicantbid
);

router.get('/campaigns/:campaignId/posts', accessMiddleware(constants.COMPANY, 30), applicantMiddlewares.getCampaignPosts);
router.post('/campaigns/:campaignId/posts', accessMiddleware(constants.COMPANY, 30), applicantMiddlewares.createCampaignPost);

router.get('/campaigns/:campaignId/report', accessMiddleware(constants.COMPANY, 30), campaign.getCampaignReport);

// Deprecated routes
// router.get('/get-campaigns-by-status/:status', authMiddleware(constants.COMPANY), campaignMiddlewares.getCampaignsByStatus);

// router.get('/get-campaigns', authMiddleware(constants.COMPANY), campaignMiddlewares.getCampaigns);

// router.get('/brands/:id', [param('id').isMongoId()], authMiddleware(constants.COMPANY), campaignMiddlewares.getCampaignsByBrandId);

// router.get('/:id', authMiddleware(constants.COMPANY), [param('id').isMongoId()], campaignMiddlewares.getCampaign);

// router.delete(
//     '/delete',
//     authMiddleware(constants.COMPANY),
//     [query('brandId').isMongoId(), query('campaignId').isMongoId()],
//     campaignMiddlewares.deleteCampaign
// );

module.exports = router;
