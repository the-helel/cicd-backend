const router = require('express').Router();

const campaignMiddlewares = require('../../../../controllers/campaign/cash-campaign/upsert-campaign');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.post('/', authMiddleware(constants.COMPANY), campaignMiddlewares.upsertCampaign);

module.exports = router;
