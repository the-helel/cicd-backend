const router = require('express').Router();

const applyCampaignRoutes = require('./apply-campaign');
const upsertCampaignRoutes = require('./upsert-campaign');

router.use(applyCampaignRoutes);
router.use(upsertCampaignRoutes);

module.exports = router;
