const router = require('express').Router();
const { body } = require('express-validator');

const { uploadPublicImage } = require('../../../../aws/aws-s3');

const applyCampaignMiddlewares = require('../../../../controllers/campaign/cash-campaign/apply-campaign');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.post(
    '/apply-campaign',
    authMiddleware(constants.INFLUENCER),
    uploadPublicImage.array('files', 100),
    [body('campaignId').isMongoId(), body('social').isMongoId(), body('biddingAmount').isInt(), body('pitch').isString()],
    applyCampaignMiddlewares.applyCampaign
);

router.post(
    '/influencer-cross-bid',
    authMiddleware(constants.INFLUENCER),
    [body('campaign').isMongoId(), body('biddingAmount').isInt(), body('status').isString()],
    applyCampaignMiddlewares.influencerCrossBid
);

router.post(
    '/company-cross-bid',
    authMiddleware(constants.COMPANY),
    [body('campaign').isMongoId(), body('applicant').isMongoId(), body('biddingAmount').isInt(), body('status').isString()],
    applyCampaignMiddlewares.companyCrossBid
);

router.put(
    '/link',
    authMiddleware(constants.INFLUENCER),
    [body('postLink').isString(), body('campaignId').isMongoId()],
    applyCampaignMiddlewares.updateLink
);

module.exports = router;
