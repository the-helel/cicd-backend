const router = require('express').Router();
const { body } = require('express-validator');

const { uploadPublicImage } = require('../../../../aws/aws-s3');

const applyCampaignMiddlewares = require('../../../../controllers/campaign/product-campaign/apply-campaign');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.post(
    '/apply-campaign',
    authMiddleware(constants.INFLUENCER),
    uploadPublicImage.array('files', 100),
    [body('campaignId').isMongoId(), body('social').isMongoId(), body('pitch').isString()],
    applyCampaignMiddlewares.applyCampaign
);

router.post(
    '/applicant-status',
    authMiddleware(constants.COMPANY),
    [body('campaign').isMongoId(), body('applicant').isMongoId(), body('status').isString()],
    applyCampaignMiddlewares.updateApplicantStatus
);

router.put(
    '/link',
    authMiddleware(constants.INFLUENCER),
    [body('postLink').isString(), body('campaignId').isMongoId()],
    applyCampaignMiddlewares.updateLink
);

module.exports = router;
