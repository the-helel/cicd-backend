const router = require('express').Router();
const { body } = require('express-validator');

const NewsletterSubscriptionMiddlewares = require('../../controllers/newsletter-subscription');

const constants = require('../../../constants');

router.post(
    '/subscribe',
    [body('type').isString().isIn([constants.BRAND, constants.INFLUENCER]), body('email').isString().isEmail()],
    NewsletterSubscriptionMiddlewares.addNewsletterSubscription
);

module.exports = router;
