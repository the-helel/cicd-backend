const router = require('express').Router();
const influencerRoutes = require('./influencer');

const companyAuthRoutes = require('./company/auth');
const companyProfileRoutes = require('./company/profile');
const companyPasswordResetRoutes = require('./company/password-reset');
const companySearchRoutes = require('./company/search');
const companyDiscoverRoutes = require('./company/discover_refactor');
const companyBookmarkRoutes = require('./company/bookmark');
const brandRoutes = require('./company/brand');
const companyGetInfluencersRoutes = require('./company/get-influencers');
const companyCampaignRoutes = require('./company/campaign');

const campaignRoutes = require('./campaign');

const campaignApplicantsRoutes = require('./company/campaign-applicants');
const cashCampaignRoutes = require('./campaign/cash-campaign');
const productCampaignRoutes = require('./campaign/product-campaign');

const influencerLibraryRoutes = require('./library/influencer');
const companyLibraryRoutes = require('./library/company');

const adminRoutes = require('./admin');

const utilRoutes = require('./util/image-upload');
const getLocationsRoute = require('./util/get-locations');
const getCategoriesRoute = require('./util/get-categories');

const orderRoutes = require('./orders/order');
const paymentRoutes = require('./orders/payment');

const notificationRoutes = require('./notification/notification');

const contactMessageRoutes = require('./contact-message');
const newsletterSubscriptionRoutes = require('./newsletter-subscription');

/* 
- Need to enhance more on making only one api for company
- New routes changes lists
*/
router.use('/admin', adminRoutes);
router.use('/influencer/', influencerRoutes);

router.use('/company', companyCampaignRoutes); // Routes for company level campaigns functionality
router.use('/company', companyDiscoverRoutes); // Routes for Discover section

//Previous routes
router.use('/company', companyAuthRoutes);
router.use('/company', companyProfileRoutes);
router.use('/company', companyPasswordResetRoutes);
router.use('/company', companySearchRoutes);
router.use('/company', companyBookmarkRoutes);
router.use('/company', brandRoutes);
router.use('/company', companyGetInfluencersRoutes);

router.use('/campaign', campaignRoutes);

router.use('/library', influencerLibraryRoutes);
router.use('/library', companyLibraryRoutes);

router.use('/order', orderRoutes);
router.use('/payment', paymentRoutes);

router.use('/notification', notificationRoutes);

router.use(utilRoutes);
router.use('/util', getLocationsRoute);
router.use('/util', getCategoriesRoute);

router.use(contactMessageRoutes);
router.use(newsletterSubscriptionRoutes);

// Depricated
router.use('/campaign/applicant', campaignApplicantsRoutes);
router.use('/cash-campaign/', cashCampaignRoutes);
router.use('/product-campaign/', productCampaignRoutes);

module.exports = router;
