const router = require('express').Router();

const getLocationsMiddleware = require('../../../controllers/util/get-locations');

router.get('/get-locations', getLocationsMiddleware.getLocations);

module.exports = router;
