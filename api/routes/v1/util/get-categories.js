const router = require('express').Router();

const getCategoriesMiddleware = require('../../../controllers/influencer/influencer');

router.get('/categories', getCategoriesMiddleware.getCategories);

module.exports = router;
