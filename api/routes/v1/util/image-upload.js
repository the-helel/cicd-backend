const router = require('express').Router();
const { uploadPublicImage } = require('../../../aws/aws-s3');

const uploadFile = uploadPublicImage.single('file');
const uploadFiles = uploadPublicImage.array('files', 100);

// function to upload an image to s3 bucket
router.post('/image', (req, res) => {
    uploadFile(req, res, (err) => {
        if (err) return res.status(500).send({ error: true, message: err.message });
        return res.status(200).send({ error: false, data: { url: req.file.location } });
    });
});

// function to upload an image to s3 bucket
router.post('/images', (req, res) => {
    uploadFiles(req, res, (err) => {
        if (err) return res.status(500).send({ error: true, message: err.message });
        return res.status(200).send({ error: false, data: { url: req.files } });
    });
});

module.exports = router;
