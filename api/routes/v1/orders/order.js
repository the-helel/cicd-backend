const router = require('express').Router();
const { body, query } = require('express-validator');

const orderMiddlewares = require('../../../controllers/orders/orders');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.post(
    '/create-order',
    authMiddleware(constants.COMPANY),
    [body('entity').trim().isString(), body('entityId').isMongoId(), body('currency').trim().isLength({ max: 3, min: 3 })],
    orderMiddlewares.createOrder
);

router.get('/get-orders', authMiddleware(constants.COMPANY), orderMiddlewares.getOrders);

router.get('/get-order', authMiddleware(constants.COMPANY), [query('orderId').isMongoId()], orderMiddlewares.getOrder);

router.get(
    '/check-order',
    authMiddleware(constants.COMPANY),
    [query('entityType').isString(), query('entityId').isMongoId()],
    orderMiddlewares.checkOrder
);

module.exports = router;
