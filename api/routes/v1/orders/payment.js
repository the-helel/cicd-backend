const router = require('express').Router();
const { body, query } = require('express-validator');

const paymentMiddlewares = require('../../../controllers/orders/payments');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.post('/success/:receiptId', paymentMiddlewares.verifyPayment);

module.exports = router;
