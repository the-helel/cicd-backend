const router = require('express').Router();
const { body } = require('express-validator');

const ContactMessageMiddlewares = require('../../controllers/contact-message');
const authMiddleware = require('../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../controllers/auth/access-middleware');

const constants = require('../../constants');

router.post(
    '/contact',
    [
        body('type').isString().isIn([constants.BRAND, constants.INFLUENCER]),
        body('name').isString().trim(),
        body('phoneNumber').isString().isMobilePhone(),
        body('email').isString().isEmail(),
        body('body').isString(),
    ],
    ContactMessageMiddlewares.addContactMessage
);

router.get('/admin/contact', accessMiddleware(constants.ADMIN, 50), ContactMessageMiddlewares.readContact);

module.exports = router;
