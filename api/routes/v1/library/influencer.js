const router = require('express').Router();
const { body, param } = require('express-validator');

const { uploadPublicImage } = require('../../../aws/aws-s3');

const influencerLibraryMiddlewares = require('../../../controllers/library/influencer');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.post(
    '/create-content',
    authMiddleware(constants.INFLUENCER),
    uploadPublicImage.single('file'),
    [body('price').trim().isNumeric(), body('caption').trim().isString()],
    influencerLibraryMiddlewares.createContent
);

router.put(
    '/edit-content',
    authMiddleware(constants.INFLUENCER),
    [body('price').trim().isNumeric(), body('caption').trim().isString()],
    influencerLibraryMiddlewares.editContent
);

router.get('/view-content/:id', authMiddleware(constants.INFLUENCER), [param('id').isMongoId()], influencerLibraryMiddlewares.viewContent);

router.get('/view-contents', authMiddleware(constants.INFLUENCER), influencerLibraryMiddlewares.viewContents);

router.delete('/delete-content', authMiddleware(constants.INFLUENCER), influencerLibraryMiddlewares.deleteContent);

module.exports = router;
