const router = require('express').Router();
const { body, param, query } = require('express-validator');

const companyLibraryMiddlewares = require('../../../controllers/library/company');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get('/get-contents/:tag', authMiddleware(constants.COMPANY), companyLibraryMiddlewares.viewAllContents);

router.put(
    '/bookmark-content',
    authMiddleware(constants.COMPANY),
    [body('contentId').isMongoId()],
    companyLibraryMiddlewares.bookmarkContent
);

router.put('/remove-bookmark', authMiddleware(constants.COMPANY), [
    body('contentId').isMongoId(),
    companyLibraryMiddlewares.deleteBookmarkContent,
]);

module.exports = router;
