const router = require('express').Router();
const { body } = require('express-validator');

const notificationMiddlewares = require('../../../controllers/notification/notification');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get('/get-notifications', authMiddleware(constants.INFLUENCER), notificationMiddlewares.getNotifications);

router.put('/mark-read', authMiddleware(constants.INFLUENCER), notificationMiddlewares.markReadNotification);

router.post(
    '/subscribe',
    authMiddleware(constants.INFLUENCER),
    [body('fcmToken').trim().isString().notEmpty()],
    notificationMiddlewares.subscribe
);

router.get('/test', authMiddleware(null), notificationMiddlewares.test);

module.exports = router;
