const router = require('express').Router();
const { body } = require('express-validator');

const tipMiddlewares = require('../../../controllers/admin/tip');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const upload = require('../../../multer/parse-csv');

const constants = require('../../../constants');

router.get('/tips', authMiddleware(constants.ADMIN), tipMiddlewares.getTips);

router.post('/tip', authMiddleware(constants.ADMIN), [body('tip').trim().isString()], tipMiddlewares.addTip);

router.post('/tips', authMiddleware(constants.ADMIN), upload.single('file'), tipMiddlewares.addTips);

router.put(
    '/tip',
    authMiddleware(constants.ADMIN),
    [body('tip').trim().isString(), body('tipId').trim().isMongoId()],
    tipMiddlewares.editTip
);

module.exports = router;
