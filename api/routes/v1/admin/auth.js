const express = require('express');
const { body } = require('express-validator');
const router = express.Router();

const authMiddlewares = require('../../../controllers/admin/auth');

router.post(
    '/register',
    [body('email').isEmail().normalizeEmail().isLength({ min: 8 }), body('password').trim().isLength({ min: 8 })],
    authMiddlewares.createUser
);

router.post('/login', [body('email').isEmail().normalizeEmail(), body('password').trim().isLength({ min: 8 })], authMiddlewares.login);

module.exports = router;
