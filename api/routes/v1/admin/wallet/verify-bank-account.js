const router = require('express').Router();
const { body } = require('express-validator');

const verifyBankAccountMiddlewares = require('../../../../controllers/admin/wallet/verify-bank-account');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/verify-bank-account-requests', authMiddleware(constants.ADMIN), verifyBankAccountMiddlewares.getVerifyBankAccountRequests);

router.post(
    '/verify-bank-account',
    authMiddleware(constants.ADMIN),
    [body('accountId').isMongoId(), body('status').isString().trim(), body('feedback').isString().trim()],
    verifyBankAccountMiddlewares.verifyBankAccount
);

module.exports = router;
