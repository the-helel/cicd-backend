const router = require('express').Router();

const kycRoutes = require('./kyc');
const verifyBankAccountRoutes = require('./verify-bank-account');

router.use(kycRoutes);
router.use(verifyBankAccountRoutes);

module.exports = router;
