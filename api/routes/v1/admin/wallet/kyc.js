const router = require('express').Router();
const { body } = require('express-validator');

const kycMiddlewares = require('../../../../controllers/admin/wallet/kyc');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../../../controllers/auth/access-middleware.js');
const config = require('../../../../config');

const constants = require('../../../../constants');

router.get('/kyc-requests', accessMiddleware(config.ROLES.Admin, 50), kycMiddlewares.getKYCRequests);

router.post(
    '/kyc',
    accessMiddleware(constants.ADMIN, 50),
    [body('kycId').isMongoId(), body('status').isString().trim(), body('feedback').isString().trim()],
    kycMiddlewares.verifyKYC
);

module.exports = router;
