const router = require('express').Router();
const { body } = require('express-validator');

const brandMiddlewares = require('../../../../controllers/admin/company/brand');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.post(
    '/create',
    authMiddleware(constants.ADMIN),
    [
        body('company').trim().isMongoId(),
        body('name').trim().isLength({ min: 4 }),
        body('pageUrl').trim().isURL(),
        body('logo').trim().isURL(),
        body('about').trim().isLength({ min: 8 }),
        body('coverUrl').trim().isURL(),
    ],
    brandMiddlewares.createBrand
);

router.put(
    '/edit',
    authMiddleware(constants.ADMIN),
    [
        body('company').trim().isMongoId(),
        body('id').trim().isMongoId(),
        body('name').trim().isString(),
        body('pageUrl').trim().isURL(),
        body('logo').trim().isURL(),
        body('about').trim().isString(),
        body('coverUrl').trim().isString(),
    ],
    brandMiddlewares.editBrand
);

router.get('/', authMiddleware(constants.ADMIN), brandMiddlewares.viewBrands);

router.get('/:id', authMiddleware(constants.ADMIN), brandMiddlewares.viewBrand);

module.exports = router;
