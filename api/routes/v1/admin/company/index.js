const router = require('express').Router();

const brandRoutes = require('./brand');
const companyRoutes = require('./company');

router.use('/brand/', brandRoutes);
router.use('/company/', companyRoutes);

module.exports = router;
