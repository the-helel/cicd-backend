const express = require('express');
const { body, query, param } = require('express-validator');
const router = express.Router();

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const companyMiddlewares = require('../../../../controllers/admin/company/company');

const constants = require('../../../../constants');

router.post(
    '/',
    authMiddleware(constants.ADMIN),
    [
        body('email').isEmail().normalizeEmail().isLength({ min: 8 }),
        body('password').trim().isLength({ min: 8 }),
        body('name').trim().isLength({ min: 4 }),
        body('mobile').trim().isMobilePhone(),
        body('accountHolderName').trim().isLength({ min: 3 }),
    ],
    companyMiddlewares.createCompany
);

// Edit Company
router.put(
    '/edit',
    authMiddleware(constants.ADMIN),
    [
        query('company').isMongoId(),
        body('email').isEmail().normalizeEmail().isLength({ min: 8 }),
        body('name').trim().isLength({ min: 4 }),
        body('mobile').trim().isMobilePhone(),
        body('accountHolderName').trim().isLength({ min: 3 }),
    ],
    companyMiddlewares.updateCompany
);

router.get('/all', authMiddleware(constants.ADMIN), companyMiddlewares.getAllCompanies);

router.get('/:id/brands', authMiddleware(constants.ADMIN), companyMiddlewares.getCompanyBrands);

// Get Company Info
router.get('/:id', authMiddleware(constants.ADMIN), [param('id').isMongoId()], companyMiddlewares.getCompany);

module.exports = router;
