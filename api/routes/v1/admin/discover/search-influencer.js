const router = require('express').Router();
const { body } = require('express-validator');
const constants = require('../../../../constants');
const accessMiddleware = require('../../../../controllers/auth/access-middleware');
const adminDiscoverMiddleware = require('../../../../controllers/admin/discover/search-influencer');

router.post(
    '/search-influencer',
    accessMiddleware(constants.ADMIN, 50),
    body('platform').trim().isString(),
    body('username').trim().isString(),
    adminDiscoverMiddleware.searchInfluencer
);

module.exports = router;
