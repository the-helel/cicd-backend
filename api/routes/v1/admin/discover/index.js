const router = require('express').Router();
const adminDiscoverRoutes = require('./search-influencer');

router.use('/discover/', adminDiscoverRoutes);

module.exports = router;
