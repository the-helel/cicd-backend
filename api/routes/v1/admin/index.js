const router = require('express').Router();

const activityRoutes = require('./activity');
const campaignRoutes = require('./campaign');
const companyRoutes = require('./company');
const rewardRoutes = require('./reward');
const walletRoutes = require('./wallet');
const authRoutes = require('./auth');
const categoryRoutes = require('./category');
const tipRoutes = require('./tip');
const videoRoutes = require('./video');
const notificationRoutes = require('./notification/notification');
const analyticsRoutes = require('./analytics');
const adminDiscoverRoutes = require('./discover');

router.use(activityRoutes);
router.use(campaignRoutes);
router.use(companyRoutes);
router.use(rewardRoutes);
router.use(walletRoutes);
router.use(authRoutes);
router.use(categoryRoutes);
router.use(tipRoutes);
router.use(videoRoutes);
router.use(notificationRoutes);
router.use(analyticsRoutes);
router.use(adminDiscoverRoutes);

module.exports = router;
