const router = require('express').Router();
const { body, query } = require('express-validator');

const categoryMiddlewares = require('../../../controllers/admin/category');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get('/categories', authMiddleware(constants.ADMIN), categoryMiddlewares.getAllCategories);

router.post(
    '/category',
    authMiddleware(constants.ADMIN),
    [body('name').isString().trim().isAlpha(), body('icon').isString().trim().isURL()],
    categoryMiddlewares.addCategory
);

router.put(
    '/category',
    authMiddleware(constants.ADMIN),
    [
        body('id').isMongoId(),
        body('name').isString().trim().isAlpha(),
        body('description').isString().trim(),
        body('icon').isString().trim().isURL(),
    ],
    categoryMiddlewares.updateCategory
);

router.delete('/remove-category', [query('categoryId').isMongoId()], categoryMiddlewares.removeCategory);

module.exports = router;
