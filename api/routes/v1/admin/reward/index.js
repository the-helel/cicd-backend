const router = require('express').Router();

const addInventoryRoutes = require('./add-inventory');

router.use(addInventoryRoutes);

module.exports = router;
