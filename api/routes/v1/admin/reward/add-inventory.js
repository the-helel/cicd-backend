const router = require('express').Router();

// controller
const inventoryMiddleware = require('../../../../controllers/admin/reward/purchase-inventory');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

// adds inventory and creates new product
router.post('/inventory/new', authMiddleware(constants.ADMIN), inventoryMiddleware.addPurchase);

// adds inventory and updates quantity of existing product
router.post('/inventory/existing', authMiddleware(constants.ADMIN), inventoryMiddleware.addPurchaseOfExistingProduct);

module.exports = router;
