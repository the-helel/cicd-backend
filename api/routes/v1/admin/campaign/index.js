const router = require('express').Router();

const cashCampaignRoutes = require('./cash-campaign');
const productCampaignRoutes = require('./product-campaign');
const getCompanyCampaignsRoutes = require('./get-campaigns');
const campaignRoutes = require('../../campaign');
const campaignPaymentRoutes = require('./campaign-payment');

router.use('/cash-campaign/', cashCampaignRoutes);
router.use('/product-campaign/', productCampaignRoutes);
router.use('/company', getCompanyCampaignsRoutes);
router.use(campaignPaymentRoutes);
// router.use(campaignRoutes);

module.exports = router;
