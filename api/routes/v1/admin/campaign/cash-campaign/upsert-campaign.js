const router = require('express').Router();

const campaignMiddlewares = require('../../../../../controllers/admin/campaign/cash-campaign/upsert-campaign');

const authMiddleware = require('../../../../../controllers/auth/auth-middleware');

const constants = require('../../../../../constants');
const { query } = require('express-validator');

router.post('/', authMiddleware(constants.ADMIN), campaignMiddlewares.upsertCampaign);
router.get('/', authMiddleware(constants.ADMIN), [query('id').isMongoId()], campaignMiddlewares.getCampaignById);

module.exports = router;
