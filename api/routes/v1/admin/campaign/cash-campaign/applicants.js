const router = require('express').Router();
const { query, body } = require('express-validator');

const authMiddleware = require('../../../../../controllers/auth/auth-middleware');

const constants = require('../../../../../constants');

const applicantMiddlewares = require('../../../../../controllers/admin/campaign/cash-campaign/applicants');

router.get('/applicants', authMiddleware(constants.ADMIN), [query('campaign').isMongoId()], applicantMiddlewares.getApplicants);

router.post(
    '/cross-bid',
    authMiddleware(constants.ADMIN),
    [body('campaign').isMongoId(), body('applicant').isMongoId(), body('biddingAmount').isInt(), body('status').isString()],
    applicantMiddlewares.adminCrossBid
);

module.exports = router;
