const router = require('express').Router();
const { body } = require('express-validator');

const authMiddleware = require('../../../../../controllers/auth/auth-middleware');

const constants = require('../../../../../constants');

const submittedLinkMiddlewares = require('../../../../../controllers/admin/campaign/cash-campaign/submitted-link');

router.post(
    '/link-status',
    authMiddleware(constants.ADMIN),
    [body('status').isString().isIn([constants.COMPLETED]), body('applicant').isMongoId()],
    submittedLinkMiddlewares.updateLinkStatus
);

module.exports = router;
