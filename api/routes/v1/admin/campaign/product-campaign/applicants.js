const router = require('express').Router();
const { query, body } = require('express-validator');

const authMiddleware = require('../../../../../controllers/auth/auth-middleware');

const constants = require('../../../../../constants');

const applicantMiddlewares = require('../../../../../controllers/admin/campaign/product-campaign/applicants');

router.get('/applicants', authMiddleware(constants.ADMIN), [query('campaign').isMongoId()], applicantMiddlewares.getApplicants);

router.post(
    '/applicant-status',
    authMiddleware(constants.ADMIN),
    [body('campaign').isMongoId(), body('applicant').isMongoId(), body('status').isString()],
    applicantMiddlewares.updateApplicantStatus
);

module.exports = router;
