const router = require('express').Router();

const applicantRoutes = require('./applicants');
const submittedLinkRoutes = require('./submitted-link');
const upsertCampaignRoutes = require('./upsert-campaign');

router.use(applicantRoutes);
router.use(submittedLinkRoutes);
router.use(upsertCampaignRoutes);

module.exports = router;
