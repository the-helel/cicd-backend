const router = require('express').Router();

const authMiddleware = require('../../../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../../../controllers/auth/access-middleware');

const constants = require('../../../../constants');
const config = require('../../../../config');

const getCampaignsMiddlewares = require('../../../../controllers/admin/campaign/get-campaigns');

// router.get('/campaigns', authMiddleware(constants.ADMIN), getCampaignsMiddlewares.getCampaigns);

// new routes for admin//note need to change company to admin
/**
 * Get campaign route
 */
router.get('/campaigns', accessMiddleware(config.ROLES.Admin, 50), getCampaignsMiddlewares.getAllCampaigns);
// router.get('/:companyId/campaigns', authMiddleware(constants.ADMIN), getCampaignsMiddlewares.getCompanyCampaigns);

module.exports = router;
