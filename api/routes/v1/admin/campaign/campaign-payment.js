const router = require('express').Router();

const authMiddleware = require('../../../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../../../controllers/auth/access-middleware');

const constants = require('../../../../constants');

const getCampaignsMiddlewares = require('../../../../controllers/admin/campaign/get-campaigns');

router.get('/campaigns/payout', accessMiddleware(constants.ADMIN, 50), getCampaignsMiddlewares.listCompletedCampaigns);
router.get(
    '/campaigns/payout/influencers',
    accessMiddleware(constants.ADMIN, 50),
    getCampaignsMiddlewares.listCompletedCampaignInfluencers
);

router.post('/campaigns/payment', accessMiddleware(constants.ADMIN, 50), getCampaignsMiddlewares.markCampaignCompleteInfluencer);

router.put('/campaign/:campaignId/status/edit', accessMiddleware(constants.ADMIN, 50), getCampaignsMiddlewares.updateCampaignStatus);

module.exports = router;
