const router = require('express').Router();
const { body } = require('express-validator');

const videoMiddlewares = require('../../../controllers/admin/video');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get('/videos', authMiddleware(constants.ADMIN), videoMiddlewares.getVideos);

router.post(
    '/video',
    authMiddleware(constants.ADMIN),
    [
        body('videoCode').trim().isString(),
        body('title').trim().isString(),
        body('description').trim().isString(),
        body('category').trim().isString(),
        body('keyMoments').isArray(),
    ],
    videoMiddlewares.addVideo
);

router.put(
    '/video',
    authMiddleware(constants.ADMIN),
    [
        body('videoId').trim().isMongoId(),
        body('videoCode').trim().isString(),
        body('title').trim().isString(),
        body('description').trim().isString(),
        body('category').trim().isString(),
        body('keyMoments').isArray(),
    ],
    videoMiddlewares.editVideo
);

module.exports = router;
