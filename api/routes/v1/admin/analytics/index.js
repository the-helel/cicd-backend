const router = require('express').Router();

const AnalyticsRoutes = require('./analytics');

router.use(AnalyticsRoutes);

module.exports = router;
