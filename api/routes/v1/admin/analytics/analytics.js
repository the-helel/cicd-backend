/**
 * Routes for base analytics for admin panel
 *
 */

const router = require('express').Router();
const authMiddleware = require('../../../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../../../controllers/auth/access-middleware');
const AnalyticsMiddlewares = require('../../../../controllers/admin/analytics/analytics');
const constants = require('../../../../constants');

router.get('/analytics', accessMiddleware(constants.ADMIN, 50), AnalyticsMiddlewares.baseAnalytics);
router.get('/analytics/users', accessMiddleware(constants.ADMIN, 50), AnalyticsMiddlewares.usersInfo);
router.get('/analytics/referrals', accessMiddleware(constants.ADMIN, 50), AnalyticsMiddlewares.referralsInfo);

module.exports = router;
