const express = require('express');
const { body } = require('express-validator');
const constants = require('../../../../constants');
const router = express.Router();

const notificationMiddlewares = require('../../../../controllers/admin/notifications/notifications');
const authMiddleware = require('../../../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../../../controllers/auth/access-middleware');

router.post(
    '/send-to-all-influencer',
    authMiddleware(constants.ADMIN),
    [body('title').trim().isString(), body('body').trim().isString(), body('type').trim().isString(), body('subType').trim().isString()],
    notificationMiddlewares.sendToAllInfluencers
);

/**
 * New routes
 */
router.post(
    '/send-notification-influencers',
    accessMiddleware(constants.ADMIN, 50),
    [body('title').trim().isString(), body('body').trim().isString()],
    notificationMiddlewares.sendNotificationInfluencer
);

module.exports = router;
