const router = require('express').Router();
const { body } = require('express-validator');

// Controller
const journeyAdminMiddleware = require('../../../../controllers/admin/activity/journey');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/journies', authMiddleware(constants.ADMIN), journeyAdminMiddleware.getAllSubmittedJourney);

router.put(
    '/journey',
    authMiddleware(constants.ADMIN),
    [body('Id').trim().isMongoId(), body('status').trim().isString(), body('feedback').trim().isString()],
    journeyAdminMiddleware.updateStatus
);

module.exports = router;
