/**
 * Admin Routes for Blog and earn
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 */

const router = require('express').Router();
const { body, query } = require('express-validator');

// Controller
const blogAndEarnAdminMiddleware = require('../../../../controllers/admin/activity/blog');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/blogs', authMiddleware(constants.ADMIN), blogAndEarnAdminMiddleware.getAllSubmittedBlogs);

router.put(
    '/blog',
    authMiddleware(constants.ADMIN),
    [body('blogId').trim().isMongoId(), body('status').trim().isString(), body('feedback').trim().isString()],
    blogAndEarnAdminMiddleware.updateStatus
);

module.exports = router;
