const router = require('express').Router();
const { body } = require('express-validator');

const referralMiddlewares = require('../../../../controllers/admin/activity/referral');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/referral-amount-for-referrer', authMiddleware(constants.ADMIN), referralMiddlewares.getReferralAmountForReferrer);

router.get('/referral-amount-for-referred', authMiddleware(constants.ADMIN), referralMiddlewares.getReferralAmountForReferred);

router.get('/referral-percentage', authMiddleware(constants.ADMIN), referralMiddlewares.getReferralPercentage);

router.get('/referral-max-amount', authMiddleware(constants.ADMIN), referralMiddlewares.getReferralMaxAmount);

router.put(
    '/referral-amount-for-referrer',
    authMiddleware(constants.ADMIN),
    [body('referralAmountForReferrer').isInt()],
    referralMiddlewares.saveReferralAmountForReferrer
);

router.put(
    '/referral-amount-for-referred',
    authMiddleware(constants.ADMIN),
    [body('referralAmountForReferred').isInt()],
    referralMiddlewares.saveReferralAmountForReferred
);

router.put(
    '/referral-percentage',
    authMiddleware(constants.ADMIN),
    [body('referralPercentage').isFloat()],
    referralMiddlewares.saveReferralPercentage
);

router.put(
    '/referral-max-amount',
    authMiddleware(constants.ADMIN),
    [body('referralMaxAmount').isInt()],
    referralMiddlewares.saveReferralMaxAmount
);

router.get('/division-1', authMiddleware(constants.ADMIN), referralMiddlewares.getDivision1Amount);

router.get('/division-2', authMiddleware(constants.ADMIN), referralMiddlewares.getDivision2Amount);

router.get('/division-3', authMiddleware(constants.ADMIN), referralMiddlewares.getDivision3Amount);

router.get('/division-4', authMiddleware(constants.ADMIN), referralMiddlewares.getDivision4Amount);

router.get('/division-5', authMiddleware(constants.ADMIN), referralMiddlewares.getDivision5Amount);

router.get('/instagram-connect', authMiddleware(constants.ADMIN), referralMiddlewares.getInstagramConnectAmount);

router.put('/division-1', authMiddleware(constants.ADMIN), referralMiddlewares.saveDivision1Amount);

router.put('/division-2', authMiddleware(constants.ADMIN), referralMiddlewares.saveDivision2Amount);

router.put('/division-3', authMiddleware(constants.ADMIN), referralMiddlewares.saveDivision3Amount);

router.put('/division-4', authMiddleware(constants.ADMIN), referralMiddlewares.saveDivision4Amount);

router.put('/division-5', authMiddleware(constants.ADMIN), referralMiddlewares.saveDivision5Amount);

router.put('/instagram-connect', authMiddleware(constants.ADMIN), referralMiddlewares.saveInstagramConnectAmount);

module.exports = router;
