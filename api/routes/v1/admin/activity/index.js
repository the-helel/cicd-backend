const router = require('express').Router();

const blogRoutes = require('./blog');
const referralRoutes = require('./referral');
const journeyRoutes = require('./journey');

router.use(blogRoutes);
router.use(referralRoutes);
router.use(journeyRoutes);

module.exports = router;
