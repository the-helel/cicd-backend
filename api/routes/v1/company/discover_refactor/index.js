const router = require('express').Router();
const { body } = require('express-validator');
const searchMethods = require('../../../../controllers/company/discover_refactor/search-methods');

router.post(
    '/discover/search',
    [body('query').isString().notEmpty(), body('size').isInt(), body('page').isInt()],
    searchMethods.getInstagramInfluencerList
);

/**
 * Suggestion API for discover
 */
router.post(
    '/discover/suggestions',
    [body('query').isString().notEmpty(), body('size').isInt(), body('page').isInt()],
    searchMethods.getSuggestions
);

/**
 * It will list all the influencer based on category and other filters.
 */
router.post('/discover/profile', [body('id').isString().notEmpty()], searchMethods.getInstagramInfluencerProfile);

module.exports = router;
