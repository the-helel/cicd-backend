const assignCampaignType = require('../../../../controllers/company/campaign');

async function createCampaign(req, res) {
    let campaignPayload = req.body;

    assignCampaignType(campaignPayload);

    res.send('hi');
}

module.exports = {
    createCampaign,
};
