const { body } = require('express-validator');
const { createCampaign } = require('./create-campaign');
const router = require('express').Router();
const constants = require('../../../../constants.js');
const authMiddleware = require('../../../../controllers/auth/auth-middleware');
const campaign = require('../../../../controllers/campaign');
const campaignMiddlewares = require('../../../../controllers/company/campaign/campaign');
const applicantMiddlewares = require('../../../../controllers/company/campaign/applicant');
const campaignRoutes = require('../../campaign');
// router.route('/create-campaign').post(createCampaign)

// router.post(
//     '/campaigns/create',
//     authMiddleware(constants.COMPANY),
//     [body('companyId').isMongoId().notEmpty(), body('brandId').isMongoId(), body('campaignType').isString()],
//     campaignMiddlewares.createCampaign
// );

// router.put(
//     '/campaigns/:campaignId/edit/status',
//     authMiddleware(constants.COMPANY),
//     [body('brandId').isMongoId(), body('status').isString(), body('campaignId').isString()],
//     campaignMiddlewares.editCampaignStatus
// );

// router.put(
//     '/campaigns/:campaignId/edit',
//     authMiddleware(constants.COMPANY),
//     [body('brandId').isMongoId(), body('campaignType').isString(), body('campaignId').isString()],
//     campaignMiddlewares.editCampaign
// );

// router.post(
//     '/campaigns/:campaignId/applicants/cross-bid',
//     authMiddleware(constants.COMPANY),
//     [body('campaignId').isMongoId(), body('applicantId').isMongoId(), body('amount').isInt(), body('status').isString()],
//     campaign.companyCrossBid
// );

// router.post(
//     '/campaigns/:campaignId/copy',
//     authMiddleware(constants.COMPANY),
//     [body('companyId').isMongoId().notEmpty(), body('brandId').isMongoId(), body('campaignType').isString(), body('campaignId').isString()],
//     campaignMiddlewares.copyCampaign
// );
// router.post('/campaigns/:campaignId/apply', authMiddleware(constants.COMPANY), applicantMiddlewares.createCampaignApplicant);// need to check
// router.get('/campaigns', authMiddleware(constants.COMPANY), campaignMiddlewares.getCampaigns);
// router.get('/campaigns/:campaignId', authMiddleware(constants.COMPANY), campaignMiddlewares.getCampaignById);
// router.delete('/campaigns/:campaignId', authMiddleware(constants.COMPANY), campaignMiddlewares.deleteCampaignById);

// router.get('/campaigns/:campaignId/applicants', authMiddleware(constants.COMPANY), applicantMiddlewares.getCampaignApplicant);
// router.put(
//     '/campaigns/:campaignId/applicants/:applicantId/status',
//     authMiddleware(constants.COMPANY),
//     [body('status').isString().notEmpty()],
//     applicantMiddlewares.updateApplicantStatus
// );
// router.put(
//     '/campaigns/:campaignId/applicants/:applicantId/bid',
//     authMiddleware(constants.COMPANY),
//     [body('bidAmount').isString().notEmpty()],
//     applicantMiddlewares.updateApplicantbid
// );

// router.get('/campaigns/:campaignId/posts', authMiddleware(constants.COMPANY), applicantMiddlewares.getCampaignPosts);
// router.post('/campaigns/:campaignId/posts', authMiddleware(constants.COMPANY), applicantMiddlewares.createCampaignPost);

/**
 * New Way of company routes
 */
router.use(campaignRoutes);

module.exports = router;
