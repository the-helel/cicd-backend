const express = require('express');
const { body, query } = require('express-validator');
const router = express.Router();
const accessMiddleware = require('../../../controllers/auth/access-middleware');

const brandAuthMiddlewares = require('../../../controllers/company/auth');
const constants = require('../../../constants');

router.post(
    '/register',
    [
        body('email').isEmail().normalizeEmail().isLength({ min: 8 }),
        body('password').trim().isLength({ min: 8 }),
        body('name').trim().isLength({ min: 3 }),
        body('mobile').trim().isMobilePhone(),
        body('accountHolderName').trim().isLength({ min: 3 }),
    ],
    brandAuthMiddlewares.createCompany
);

//need to handle the forgot password templates
router.get('/register/verify/:token', brandAuthMiddlewares.verifyUser);

router.post('/login', [body('email').isEmail().normalizeEmail(), body('password').trim().isLength({ min: 8 })], brandAuthMiddlewares.login);

// Password reset render view
router.get('/reset', [query('token').trim().isJWT()], (req, res) => {
    return res.render('reset_company_password');
});

/**
 * Routes to create organization members
 */
router.post(
    '/member/invite',
    accessMiddleware(constants.COMPANY, 30),
    body('email').isEmail().normalizeEmail(),
    body('role').isString().trim(),
    body('accountHolderName').isString().trim(),
    brandAuthMiddlewares.createMember
);

router.post(
    '/invite/verify',
    [body('token').trim().isJWT(), body('id').isMongoId().trim(), body('password').isString().trim()],
    brandAuthMiddlewares.verifyMember
);

router.get('/members', accessMiddleware(constants.COMPANY, 30), brandAuthMiddlewares.getMembers);

/**
 * Render template for reset password
 */
router.get('/invite/', [query('token').isJWT(), query('id').isMongoId()], (req, res) => {
    return res.render('member_invite_verify');
});

module.exports = router;
