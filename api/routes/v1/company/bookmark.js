const router = require('express').Router();
const { body } = require('express-validator');

const brandBookmarkMiddlewares = require('../../../controllers/company/bookmark-influencer');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.put(
    '/bookmarks/add',
    authMiddleware(constants.COMPANY),
    [body('influencerId').isMongoId()],
    brandBookmarkMiddlewares.bookmarkInfluencer
);

module.exports = router;
