const router = require('express').Router();
const { body } = require('express-validator');

const brandPasswordResetMiddlewares = require('../../../controllers/company/password-reset');

router.post(
    '/forgot-password',
    [body('email').isEmail().withMessage('Enter a valid email address')],
    brandPasswordResetMiddlewares.sendResetPasswordLink
);

router.post(
    '/reset-password',
    [body('password').trim().isString(), body('token').trim().isJWT()],
    brandPasswordResetMiddlewares.resetPassword
);

module.exports = router;
