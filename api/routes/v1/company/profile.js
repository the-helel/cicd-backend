const router = require('express').Router();
const { body } = require('express-validator');

const brandProfileMiddlewares = require('../../../controllers/company/profile');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');
const accessMiddleWare = require('../../../controllers/auth/access-middleware.js');

router.get('/get-profile', accessMiddleWare(constants.COMPANY, 30), brandProfileMiddlewares.getProfile);

router.put('/edit-profile', accessMiddleWare(constants.COMPANY, 30), brandProfileMiddlewares.editProfile);

module.exports = router;
