const router = require('express').Router();
const { body } = require('express-validator');

const brandMiddlewares = require('../../../controllers/company/brand');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');
const accessMiddleWare = require('../../../controllers/auth/access-middleware.js');

router.post(
    '/brand/create',
    accessMiddleWare(constants.COMPANY, 30),
    [body('name').isLength({ min: 4 }).trim(), body('pageUrl').isURL().trim(), body('logo').isURL().trim()],
    brandMiddlewares.createBrand
);

router.put(
    '/brand/edit',
    accessMiddleWare(constants.COMPANY, 30),
    [
        body('id').trim(),
        body('name').isString().trim(),
        body('pageUrl').isURL().trim(),
        body('logo').isURL().trim(),
        body('about').isString().trim(),
    ],
    brandMiddlewares.editBrand
);

router.get('/brands', accessMiddleWare(constants.COMPANY, 20), brandMiddlewares.viewBrands);

router.get('/brand/:id', accessMiddleWare(constants.COMPANY, 30), brandMiddlewares.viewBrand);

router.delete('/brand/:id', accessMiddleWare(constants.COMPANY, 30), brandMiddlewares.deleteBrand);

module.exports = router;
