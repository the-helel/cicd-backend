const router = require('express').Router();
const { query } = require('express-validator');

const getInfluencersMiddlewares = require('../../../controllers/company/get-influencers');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get(
    '/get-influencer',
    authMiddleware(constants.COMPANY),
    [query(constants.INFLUENCER).isMongoId()],
    getInfluencersMiddlewares.getInfluencer
);

router.get('/get-influencers', authMiddleware(constants.COMPANY), getInfluencersMiddlewares.getInfluencers);

module.exports = router;
