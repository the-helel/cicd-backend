const router = require('express').Router();
const { body, query } = require('express-validator');

const applicantsMiddlewares = require('../../../controllers/company/campaign-applicants');

const authMiddleware = require('../../../controllers/auth/auth-middleware');
const accessMiddleware = require('../../../controllers/auth/access-middleware');

const constants = require('../../../constants');

router.get(
    '/status/:status',
    accessMiddleware(constants.COMPANY, 20),
    [query('campaignId').isMongoId()],
    applicantsMiddlewares.getCampaignApplicants
);

router.post(
    '/',
    accessMiddleware(constants.COMPANY, 20),
    [body('applicantId').isMongoId(), body('updateStatus').trim().isLength({ min: 6, max: 9 })],
    applicantsMiddlewares.updateStatus
);

module.exports = router;
