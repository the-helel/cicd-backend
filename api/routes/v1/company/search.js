const router = require('express').Router();

const searchMiddlewares = require('../../../controllers/company/search');

router.get('/search', searchMiddlewares.search);

module.exports = router;
