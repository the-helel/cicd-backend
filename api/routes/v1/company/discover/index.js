const router = require('express').Router();
const { query, param } = require('express-validator');
const { searchAll, searchByPlatformAndUsername, suggestInfluencers } = require('./search-and-filter');

router.route('/discover/search').post(searchAll);

/**
 * Suggestion API for discover
 */
router.route('/discover/suggest/profile').post(suggestInfluencers);

/**
 * It will list all the influencer based on category and other filters.
 */
router.route('/discover/list/profile').get(suggestInfluencers);

module.exports = router;
