const config = require('../../../../config');
const searchAndFilterController = require('../../../../controllers/company/discover_refactor/search-and-filter');

// Useful constants
const FILTER_OPTIONS = {
    instagram_followers: {
        range: {
            field: 'instagram_followers',
            ranges: [
                { from: 0.0, to: 10000.0, key: '0 - 10000' },
                { from: 10001.0, to: 100000.0, key: '10001 - 100000' },
                { from: 100001.0, to: 500000.0, key: '100001 - 500000' },
                { from: 500001.0, to: 1000000.0, key: '500001 - 1000000' },
                { from: 1000001.0, to: 5000000.0, key: '1000001 - 5000000' },
                { from: 5000001.0, to: 10000000.0, key: '5000001 - 10000000' },
                { from: 10000001.0, key: '10000001+' },
            ],
        },
    },
    famstar_score: {
        range: {
            field: 'famstar_score',
            ranges: [
                { from: 1.0, to: 3.0, key: 'nano' },
                { from: 3.0, to: 5.0, key: 'micro' },
                { from: 5.0, to: 8.0, key: 'macro' },
                { from: 8.0, to: 10.0, key: 'toppers' },
            ],
        },
    },
    gender: {
        term: {
            field: 'gender',
            terms: ['male', 'female', 'others'],
        },
    },
    insta_engagement_rate: {
        range: {
            field: 'instagram.insta_engagement_rate',
            ranges: [{ from: 1.0, to: 10.0 }],
        },
    },
    avg_likes: {
        range: {
            field: 'instagram.avg_likes',
            ranges: [
                { from: 0.0, to: 10000.0, key: '0 - 10000' },
                { from: 10001.0, to: 100000.0, key: '10001 - 100000' },
                { from: 100001.0, to: 500000.0, key: '100001 - 500000' },
                { from: 500001.0, to: 1000000.0, key: '500001 - 1000000' },
                { from: 1000001.0, to: 5000000.0, key: '1000001 - 5000000' },
                { from: 5000001.0, to: 10000000.0, key: '5000001 - 10000000' },
                { from: 10000001.0, key: '10000001+' },
            ],
        },
    },
    post_count: {
        range: {
            field: 'instagram.post_count',
            ranges: [
                { from: 0.0, to: 10000.0, key: '0 - 10000' },
                { from: 10001.0, to: 100000.0, key: '10001 - 100000' },
                { from: 100001.0, to: 500000.0, key: '100001 - 500000' },
                { from: 500001.0, to: 1000000.0, key: '500001 - 1000000' },
                { from: 1000001.0, to: 5000000.0, key: '1000001 - 5000000' },
                { from: 5000001.0, to: 10000000.0, key: '5000001 - 10000000' },
                { from: 10000001.0, key: '10000001+' },
            ],
        },
    },
};

function searchAll(req, res) {
    try {
        const current = req.body.page || 1;
        const resultsPerPage = config.RESULTS_PER_PAGE;
        const searchTerm = req.body.search || '';
        const sortBody = req.body.sort;

        console.log(req.body);
        const { page, filters, search, sort, include } = req.body;

        searchAndFilterController.searchAll(page, resultsPerPage, search, filters, sort, include, (err, response) => {
            if (!err && response) {
                const data = response.body.hits.hits.map((influencer) => {
                    return {
                        id: influencer._id,
                        data: influencer._source,
                    };
                });
                const result = {
                    total: response.body.hits.total.value,
                    data: data,
                    agg: response.body.aggregations,
                    filterOptions: FILTER_OPTIONS,
                };

                return res.json({ status_code: 200, success: true, data: result, message: 'Influencers data successfully fetched!' });
            } else {
                console.log('Error found while fetching data from elastic server ', err);
                return res.json({ status_code: 400, success: false, data: [], message: err.message });
            }
        });
    } catch (err) {
        console.log('Unexpected error occurred while querying to elastic server in controller ', err);
        res.json({
            status_code: 500,
            success: false,
            data: [],
            message: 'Unexpected error occured while fetching search data. Please try later!',
        });
    }
}

function searchByPlatformAndUsername(req, res) {
    try {
        const resultsPerPage = config.RESULTS_PER_PAGE;
        const { page, filters, search, sort, include } = req.body;
        console.log(req.query);

        searchAndFilterController.searchAll(page, resultsPerPage, search, filters, sort, include, (err, response) => {
            if (!err && response) {
                const data = response.body.hits.hits.map((influencer) => {
                    const result = influencer._source;
                    return {
                        id: influencer._id,
                        name: result.name,
                        image: result.profile_image,
                    };
                });
                const result = {
                    total: response.body.hits.total.value,
                    data: data,
                    agg: response.body.aggregations,
                    filterOptions: FILTER_OPTIONS,
                };

                return res.json({ status_code: 200, success: true, data: result, message: 'Influencers data successfully fetched!' });
            } else {
                console.log('Error found while fetching data from elastic server ', err);
                return res.json({ status_code: 400, success: false, data: [], message: err.message });
            }
        });
    } catch (err) {
        console.log('Unexpected error occurred while querying to elastic server in controller ', err);
        res.json({
            status_code: 500,
            success: false,
            data: [],
            message: 'Unexpected error occured while fetching search data. Please try later!',
        });
    }
}

function suggestInfluencers(req, res) {
    try {
        const { page, platform, username } = req.body;

        searchAndFilterController.suggestInfluencers(page, platform, username, (err, response) => {
            if (!err && response) {
                const data = response.body.hits.hits.map((influencer) => {
                    const result = influencer._source;
                    return {
                        id: influencer._id,
                        influencer: result,
                    };
                });
                const result = {
                    total: response.body.hits.total.value,
                    data: data,
                    agg: response.body.aggregations,
                    filterOptions: FILTER_OPTIONS,
                };

                return res.json({ status_code: 200, success: true, data: result, message: 'Influencers data successfully fetched!' });
            } else {
                console.log('Error found while fetching data from elastic server ', err);
                return res.json({ status_code: 400, success: false, data: [], message: err.message });
            }
        });
    } catch (e) {
        res.json({
            status_code: 500,
            success: false,
            data: [],
            message: 'Unexpected error occured while fetching search data. Please try later!',
        });
    }
}

module.exports = {
    searchAll,
    searchByPlatformAndUsername,
    suggestInfluencers,
};
