const router = require('express').Router();

const getAccessTokenMiddlewares = require('../../../../controllers/influencer/auth/get-access-token');

router.get('/access-token', getAccessTokenMiddlewares.getAccessToken);

module.exports = router;
