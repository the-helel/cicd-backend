const router = require('express').Router();
const { body } = require('express-validator');

const influencerAuthMiddlewares = require('../../../../controllers/influencer/auth/instagram-auth.js');
const dataDeletionMiddleware = require('../../../../controllers/auth/instagram/data-deletion');
const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.post('/auth/instagram', [body('code').trim().isLength({ min: 1, max: undefined })], influencerAuthMiddlewares.signinInstagram);

router.post(
    '/connect/instagram',
    authMiddleware(constants.INFLUENCER),
    [body('code').trim().isLength({ min: 1, max: undefined })],
    influencerAuthMiddlewares.addInstagram
);

router.delete('/data/instagram', dataDeletionMiddleware);

module.exports = router;
