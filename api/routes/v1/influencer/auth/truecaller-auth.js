const router = require('express').Router();
const { body } = require('express-validator');

const influencerAuthMiddlewares = require('../../../../controllers/influencer/auth/truecaller-auth.js');

router.post('/auth/truecaller', [body('profile').isJSON(), body('isTruecaller').isBoolean()], influencerAuthMiddlewares.signinTruecaller);

module.exports = router;
