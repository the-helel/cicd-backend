const router = require('express').Router();

const connect = require('./connect');
const facebookAuthRoutes = require('./social-auth');
const getAccessTokenRoutes = require('./get-access-token');
const instagramAuthRoutes = require('./instagram-auth');
const phoneNumberAuthRoutes = require('./phone-number-auth');
const truecallerAuthRoutes = require('./truecaller-auth');
const emailAuthRoutes = require('./email-auth');

router.use(connect);
router.use(facebookAuthRoutes);
router.use(getAccessTokenRoutes);
router.use(instagramAuthRoutes);
router.use(phoneNumberAuthRoutes);
router.use(truecallerAuthRoutes);
router.use(emailAuthRoutes);

module.exports = router;
