const router = require('express').Router();
const { body, query } = require('express-validator');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const influencerAuthMiddlewares = require('../../../../controllers/influencer/auth/connect');

const constants = require('../../../../constants');

router.post(
    '/connect-facebook',
    authMiddleware(constants.INFLUENCER),
    [body('userId').isString(), body('accessToken').isString(), body('accessTokenExpiry').isString(), body('name').isString()],
    influencerAuthMiddlewares.connectFacebook
);

router.get(
    '/facebook/pages',
    authMiddleware(constants.INFLUENCER),
    [
        query('userId').trim().isString().notEmpty(),
        query('accessToken').trim().isString().notEmpty(),
        query('accessTokenExpiry').trim().notEmpty().isString(),
    ],
    influencerAuthMiddlewares.getFacebookPages
);

router.post(
    '/connect-instagram',
    authMiddleware(constants.INFLUENCER),
    [body('userId').trim().isString().notEmpty(), body('pageId').trim().isString().notEmpty()],
    influencerAuthMiddlewares.connectIgAccount
);

router.post(
    '/disconnect-instagram',
    authMiddleware(constants.INFLUENCER),
    [body('userId').trim().isString().notEmpty()],
    influencerAuthMiddlewares.disconnectIgAccount
);

router.post(
    '/connect-youtube',
    authMiddleware(constants.INFLUENCER),
    [body('token').trim().isString()],
    influencerAuthMiddlewares.connectYoutube
);

router.delete('/disconnect-youtube', authMiddleware(constants.INFLUENCER), influencerAuthMiddlewares.disconnectYoutubeAccount);

// Check if token expired
router.get('/check-token', authMiddleware(constants.INFLUENCER), influencerAuthMiddlewares.checkTokenExpired);

// refresh facebook token
router.post(
    '/refresh-facebook',
    authMiddleware(constants.INFLUENCER),
    [body('userId').isString(), body('accessToken').isString(), body('accessTokenExpiry').isString(), body('name').isString()],
    influencerAuthMiddlewares.refreshFacebookTokenAndInstagramPages
);

module.exports = router;
