const router = require('express').Router();
const { body, param, query } = require('express-validator');
const constants = require('../../../../constants');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const emailAuthMiddlewares = require('../../../../controllers/influencer/auth/email-auth');

router.post(
    '/auth/email/token',
    [body('email').trim().isEmail(), body('password').trim().isString(), body('name').trim().isString()],
    emailAuthMiddlewares.generateTemporaryAccessToken
); // generate and send email with verification link
router.get('/auth/email/confirm/:token', [param('token').trim().isJWT()], emailAuthMiddlewares.confirmEmail); // confirm email
router.post('/auth/email/login', [body('email').trim().isEmail(), body('password').trim().isString()], emailAuthMiddlewares.login); // login with email password

router.post('/auth/email/forgot-password', [body('email').trim().isEmail()], emailAuthMiddlewares.forgotpassword);

router.get('/auth/email/reset-password', [query('token').trim().isJWT()], (req, res) => {
    return res.render('reset');
});
router.post(
    '/auth/email/reset-password',
    [body('password').trim().isString(), body('token').trim().isJWT()],
    emailAuthMiddlewares.resetPassword
);

module.exports = router;
