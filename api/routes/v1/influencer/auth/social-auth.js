const router = require('express').Router();
const { body } = require('express-validator');

const facebookMiddleware = require('../../../../controllers/influencer/auth/facebook-auth');
const appleMiddleware = require('../../../../controllers/influencer/auth/apple-auth');

// Facebook sign in route
router.post(
    '/auth/facebook',
    [body('userId').isString(), body('accessToken').isString(), body('accessTokenExpiry').isString(), body('name').isString()],
    facebookMiddleware.signinFacebook
);

// Apple sign in route
router.post(
    '/auth/apple',
    [
        body('userId').isString(),
        body('name').isString(),
        body('email').isString(),
        body('authorizationCode').isString(),
        body('identityToken').isString(),
    ],
    appleMiddleware.signInApple
);

module.exports = router;
