const router = require('express').Router();
const { body, query } = require('express-validator');

const videoMiddlewares = require('../../../controllers/influencer/video');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get('/videos', authMiddleware(constants.INFLUENCER), videoMiddlewares.getVideos);

router.get('/bookmarked-videos', authMiddleware(constants.INFLUENCER), videoMiddlewares.getBookmarkedVideos);

router.post('/bookmark-video', authMiddleware(constants.INFLUENCER), [body('id').isMongoId()], videoMiddlewares.bookmarkVideo);

router.delete('/bookmark-video', authMiddleware(constants.INFLUENCER), [query('id').isMongoId()], videoMiddlewares.removeBookmarkedVideo);

module.exports = router;
