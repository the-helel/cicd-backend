const router = require('express').Router();
const authMiddleware = require('../../../../controllers/auth/auth-middleware');
const { body } = require('express-validator');
const constants = require('../../../../constants');
const twitterMiddleware = require('../../../../controllers/influencer/social/twitter-signin');

router.post(
    '/connect/twitter',
    authMiddleware(constants.INFLUENCER),
    [
        body('username').trim().isLength({
            min: 1,
            max: undefined,
        }),
    ],
    twitterMiddleware.addTwitter
);

router.delete('/disconnect-twitter', authMiddleware(constants.INFLUENCER), twitterMiddleware.removeTwitter);

module.exports = router;
