const router = require('express').Router();
const { body, query } = require('express-validator');

const influencerWalletMiddlewares = require('../../../../controllers/influencer/wallet/wallet');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const { uploadPublicImage } = require('../../../../aws/aws-s3');

const constants = require('../../../../constants');

router.get('/', authMiddleware(constants.INFLUENCER), influencerWalletMiddlewares.getWallet);

router.post(
    '/bank-account',
    authMiddleware(constants.INFLUENCER),
    uploadPublicImage.single('file'),
    [body('accountHolderName').trim().isString(), body('ifsc').trim().isString(), body('accountNumber').trim().isString()],
    influencerWalletMiddlewares.addBankAccount
);

router.delete(
    '/bank-account',
    authMiddleware(constants.INFLUENCER),
    [query('id').trim().isString()],
    influencerWalletMiddlewares.removeBankAccount
);

router.post(
    '/upi-account',
    authMiddleware(constants.INFLUENCER),
    [body('name').trim().isString(), body('upiId').trim().isString()],
    influencerWalletMiddlewares.addUPIAccount
);

router.delete(
    '/upi-account',
    authMiddleware(constants.INFLUENCER),
    [body('id').trim().isString()],
    influencerWalletMiddlewares.removeUPIAccount
);

router.get('/tds-deductions', authMiddleware(constants.INFLUENCER), [query('amount').trim().isInt()], influencerWalletMiddlewares.getTDS);

router.post(
    '/redeem',
    authMiddleware(constants.INFLUENCER),
    [body('amount').trim().isInt(), body('accountId').trim().isString(), body('type').isIn([constants.UPI, constants.BANK_ACCOUNT])],
    influencerWalletMiddlewares.redeemWallet
);

router.get(
    '/transactions',
    authMiddleware(constants.INFLUENCER),
    [query('size').trim().isInt(), query('pageNo').trim().isInt()],
    influencerWalletMiddlewares.getTransactions
);

router.get('/transaction', authMiddleware(constants.INFLUENCER), influencerWalletMiddlewares.getWalletHistory);

module.exports = router;
