const router = require('express').Router();

const kycRoutes = require('./kyc');
const walletRoutes = require('./wallet');

router.use(kycRoutes);
router.use(walletRoutes);

module.exports = router;
