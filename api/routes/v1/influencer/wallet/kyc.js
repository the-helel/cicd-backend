const router = require('express').Router();
const { body } = require('express-validator');

const { uploadPublicImage } = require('../../../../aws/aws-s3');

const KYCMiddlewares = require('../../../../controllers/influencer/wallet/kyc');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/kyc', authMiddleware(constants.INFLUENCER), KYCMiddlewares.getKYCStatus);

router.post(
    '/kyc',
    authMiddleware(constants.INFLUENCER),
    uploadPublicImage.fields([
        {
            name: 'documents',
            maxCount: 2,
        },
        {
            name: 'panCard',
            maxCount: 2,
        },
    ]),
    [body('documentName').isString().trim(), body('uid').isString().trim()],
    KYCMiddlewares.addKYC
);

module.exports = router;
