const router = require('express').Router();

const applicantRoutes = require('./applicant');
const authRoutes = require('./auth');
const brandRoutes = require('./brand');
const campaignRoutes = require('./campaign');
const rewardsRoutes = require('./rewards');
const walletRoutes = require('./wallet');
const influencerRoutes = require('./influencer');
const searchRoutes = require('./search');
const tipRoutes = require('./tip');
const videoRoutes = require('./video');
const qnaRoutes = require('./qna');
const pricingRoutes = require('./social-pricing');
const socialLoginRoutes = require('./social/social-signin');

router.use(applicantRoutes);
router.use(authRoutes);
router.use(brandRoutes);
router.use(campaignRoutes);
router.use(rewardsRoutes);
router.use('/wallet/', walletRoutes);
router.use(influencerRoutes);
router.use(searchRoutes);
router.use('/tip/', tipRoutes);
router.use(videoRoutes);
router.use(qnaRoutes);
router.use(pricingRoutes);
router.use(socialLoginRoutes);

router.get('/verified', (req, res) => {
    return res.render('verified');
});
module.exports = router;
