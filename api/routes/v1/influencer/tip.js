const router = require('express').Router();
const { body } = require('express-validator');

const tipMiddlewares = require('../../../controllers/influencer/tip');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get('/', authMiddleware(constants.INFLUENCER), tipMiddlewares.getTip);

router.post(
    '/claps',
    authMiddleware(constants.INFLUENCER),
    [body('tip').trim().isMongoId(), body('claps').isNumeric()],
    tipMiddlewares.addClaps
);

module.exports = router;
