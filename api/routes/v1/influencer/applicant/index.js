const router = require('express').Router();

const getCampaignsByStatusRoutes = require('./get-campaigns-by-status');

router.use(getCampaignsByStatusRoutes);

module.exports = router;
