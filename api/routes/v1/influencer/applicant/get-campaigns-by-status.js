const router = require('express').Router();
const { query } = require('express-validator');

const influencerGetCampaignsByStatusMiddlewares = require('../../../../controllers/influencer/applicant/get-campaigns-by-status');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get(
    '/applicant-campaigns',
    authMiddleware(constants.INFLUENCER),
    query('status').isString(),
    influencerGetCampaignsByStatusMiddlewares.getCampaignsByStatus
);

module.exports = router;
