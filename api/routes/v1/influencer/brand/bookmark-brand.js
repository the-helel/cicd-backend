const router = require('express').Router();
const { body, query } = require('express-validator');

const influencerBookmarkBrandMiddlewares = require('../../../../controllers/influencer/brand/bookmark-brand');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/bookmarked-brands', authMiddleware(constants.INFLUENCER), influencerBookmarkBrandMiddlewares.getBookmarkedBrands);

router.post(
    '/bookmark-brand',
    authMiddleware(constants.INFLUENCER),
    [body('id').isMongoId()],
    influencerBookmarkBrandMiddlewares.bookmarkBrand
);

router.delete(
    '/bookmarked-brand',
    authMiddleware(constants.INFLUENCER),
    [query('id').isMongoId()],
    influencerBookmarkBrandMiddlewares.removeBookmarkedBrand
);

module.exports = router;
