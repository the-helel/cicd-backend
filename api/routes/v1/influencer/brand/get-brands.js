const router = require('express').Router();

const influencerGetBrandsMiddlewares = require('../../../../controllers/influencer/brand/get-brand');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/brands', authMiddleware(constants.INFLUENCER), influencerGetBrandsMiddlewares.getAllBrands);

module.exports = router;
