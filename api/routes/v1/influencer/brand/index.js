const router = require('express').Router();

const bookmarkBrandRoutes = require('./bookmark-brand');
const getBrandsRoutes = require('./get-brands');

router.use(bookmarkBrandRoutes);
router.use(getBrandsRoutes);

module.exports = router;
