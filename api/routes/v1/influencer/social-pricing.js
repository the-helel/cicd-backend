/**
 * Routes to set Social pricing
 */

const socialPricingMiddlewares = require('../../../controllers/influencer/social-pricing');
const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

const router = require('express').Router();

router.post('/pricing', authMiddleware(constants.INFLUENCER), socialPricingMiddlewares.setSocialPricing);
router.get('/pricing', authMiddleware(constants.INFLUENCER), socialPricingMiddlewares.getSocialPricing);

module.exports = router;
