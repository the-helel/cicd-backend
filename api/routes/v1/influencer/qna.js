/**
 * Routes for QnA feature on famstar
 */

const router = require('express').Router();
const { body, param, query } = require('express-validator');

const { uploadPublicImage } = require('../../../aws/aws-s3');
const qnaMiddlewares = require('../../../controllers/influencer/activity/qna');
const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

// post a new question
router.post(
    '/qna/question',
    authMiddleware(constants.INFLUENCER),
    uploadPublicImage.array('media', 10),
    [body('description').isString()],
    qnaMiddlewares.postQuestion
);

// get question by id
router.get('/qna/question/', [query('id').isMongoId()], authMiddleware(constants.INFLUENCER), qnaMiddlewares.getQuestionById);

// like and unlike question
router.patch('/qna/question/like/:id', [param('id').not().isEmpty()], authMiddleware(constants.INFLUENCER), qnaMiddlewares.likeQuestion);

router.patch(
    '/qna/question/unlike/:id',
    [param('id').not().isEmpty()],
    authMiddleware(constants.INFLUENCER),
    qnaMiddlewares.unlikeQuestion
);

// get all question
router.get('/qna/questions', authMiddleware(constants.INFLUENCER), qnaMiddlewares.getQuestions);

// post answer to a question
router.post(
    '/qna/answer/',
    [body('answerBody').isString(), body('questionId').isMongoId()],
    authMiddleware(constants.INFLUENCER),
    qnaMiddlewares.answerQuestion
);

// get answers to specific question
router.get('/qna/:id', [param('id').not().isEmpty().isMongoId()], authMiddleware(constants.INFLUENCER), qnaMiddlewares.getAnswers);

// get questions by the signedin user
router.get('/qna/questions/me', authMiddleware(constants.INFLUENCER), qnaMiddlewares.getMyQuestions);

module.exports = router;
