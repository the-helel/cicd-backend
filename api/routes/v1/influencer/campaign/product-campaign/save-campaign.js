const router = require('express').Router();
const { body, query } = require('express-validator');

const influencerSaveCampaignMiddlewares = require('../../../../../controllers/influencer/campaign/product-campaign/save-campaign');

const authMiddleware = require('../../../../../controllers/auth/auth-middleware');

const constants = require('../../../../../constants');

router.post(
    '/saved-campaign',
    authMiddleware(constants.INFLUENCER),
    [body('id').isMongoId()],
    influencerSaveCampaignMiddlewares.saveCampaign
);

router.delete(
    '/saved-campaign',
    authMiddleware(constants.INFLUENCER),
    [query('id').isMongoId()],
    influencerSaveCampaignMiddlewares.removeSavedCampaign
);

module.exports = router;
