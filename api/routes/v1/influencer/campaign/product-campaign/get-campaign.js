const router = require('express').Router();
const { query } = require('express-validator');

const getCampaignMiddlewares = require('../../../../../controllers/influencer/campaign/product-campaign/get-campaign');

const authMiddleware = require('../../../../../controllers/auth/auth-middleware');

const constants = require('../../../../../constants');

router.get('/', authMiddleware(constants.INFLUENCER), [query('campaign').isMongoId()], getCampaignMiddlewares.getCampaignById);

module.exports = router;
