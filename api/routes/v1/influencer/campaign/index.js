const router = require('express').Router();

const cashCampaignRoutes = require('./cash-campaign');
const productCampaignRoutes = require('./product-campaign');
const getCampaignsRoutes = require('./get-campaigns');
const getSubscribedBrandsCampaignsRoutes = require('./get-subscribed-brands-campaigns');
const campaignsRoutes = require('./campaigns-routes');

//Deprecated routes
router.use('/cash-campaign/', cashCampaignRoutes);
router.use('/product-campaign/', productCampaignRoutes);
router.use(getCampaignsRoutes);
router.use(getSubscribedBrandsCampaignsRoutes);

//New campaigns route
router.use('/campaign', campaignsRoutes);

module.exports = router;
