const router = require('express').Router();

const appliedCampaignRoutes = require('./applied-campaign');
const getCampaignRoutes = require('./get-campaign');
const saveCampaignRoutes = require('./save-campaign');

router.use(appliedCampaignRoutes);
router.use(getCampaignRoutes);
router.use(saveCampaignRoutes);

module.exports = router;
