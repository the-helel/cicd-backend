const router = require('express').Router();
const { query } = require('express-validator');

const influencerAppliedCampaignMiddlewares = require('../../../../../controllers/influencer/campaign/cash-campaign/applied-campaign');

const authMiddleware = require('../../../../../controllers/auth/auth-middleware');

const constants = require('../../../../../constants');

router.get(
    '/is-applied',
    authMiddleware(constants.INFLUENCER),
    [query('campaign').isMongoId()],
    influencerAppliedCampaignMiddlewares.isAppliedCampaign
);

router.get(
    '/applicant',
    authMiddleware(constants.INFLUENCER),
    [query('campaign').isMongoId()],
    influencerAppliedCampaignMiddlewares.getApplicant
);

module.exports = router;
