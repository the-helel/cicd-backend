const router = require('express').Router();

const influencerGetSubscribedBrandsCampaignsMiddlewares = require('../../../../controllers/influencer/campaign/get-subscribed-brands-campaigns');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get(
    '/subscribed-brands-campaigns',
    authMiddleware(constants.INFLUENCER),
    influencerGetSubscribedBrandsCampaignsMiddlewares.getSubscribedBrandsCampaigns
);

module.exports = router;
