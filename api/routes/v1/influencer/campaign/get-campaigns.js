const router = require('express').Router();
const { query } = require('express-validator');

const influencerGetCampaignsMiddlewares = require('../../../../controllers/influencer/campaign/get-influencer-campaigns');
const influencerCampaignMiddlewares = require('../../../../controllers/influencer/campaign/get-campaigns');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get(
    '/influencer-campaigns',
    authMiddleware(constants.INFLUENCER),
    [query('status').isIn([constants.APPLIED, 'Rejected', 'Approved'])],
    influencerGetCampaignsMiddlewares.getInfluencerCampaigns
);

router.get('/influencer-campaigns/filter', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.getAllCampaigns);

router.get(
    '/campaigns/brand',
    authMiddleware(constants.INFLUENCER),
    [query('id').isMongoId()],
    influencerCampaignMiddlewares.getCampaignsByBrandId
);

router.get('/saved-campaigns', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.getSavedCampaigns);

module.exports = router;
