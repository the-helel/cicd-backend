const router = require('express').Router();
const { query, body } = require('express-validator');

const influencerGetCampaignsMiddlewares = require('../../../../controllers/influencer/campaign/get-influencer-campaigns');
const influencerCampaignMiddlewares = require('../../../../controllers/influencer/campaign/get-campaigns');
const campaignMiddleware = require('../../../../controllers/campaign');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');
const { uploadPublicImage } = require('../../../../aws/aws-s3');

router.get(
    '/influencer-campaigns',
    authMiddleware(constants.INFLUENCER),
    [query('status').isIn([constants.APPLIED, 'Rejected', 'Approved'])],
    influencerGetCampaignsMiddlewares.getInfluencerCampaigns
);
router.get(
    '/campaigns/brand',
    authMiddleware(constants.INFLUENCER),
    [query('id').isMongoId()],
    influencerCampaignMiddlewares.getCampaignsByBrandId
);
router.get('/saved-campaigns', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.getSavedCampaigns);

// New Routes for influencers campaign
router.get('/all', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.getAllCompanyCampaigns);

router.get(
    '/applicant/check-status',
    authMiddleware(constants.INFLUENCER),
    query('status').isString(),
    influencerCampaignMiddlewares.getCampaignByApplicantStatus
);

router.post(
    '/applicant/cross-bid',
    authMiddleware(constants.INFLUENCER),
    [body('campaignId').isMongoId(), body('amount').isInt(), body('status').isString()],
    campaignMiddleware.influencerCrossBid
);

router.get('/:campaignId', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.getCompanyCampaignById);

// Influencer uploads sample works images which will be uploaded to S3 by middleware.
router.post(
    '/:campaignId/apply',
    authMiddleware(constants.INFLUENCER),
    uploadPublicImage.array('files', 5),
    campaignMiddleware.applyCompanyCampaign
);
router.get(
    '/:campaignId/applicant/:applicantId',
    authMiddleware(constants.INFLUENCER),
    influencerCampaignMiddlewares.getCampaignApplicantById
);

router.post('/bookmark', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.bookmarkCampaign);

router.get('/applicant/:campaignId', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.getApplicantByCampaignId);

router.put('/:campaignId/applicant/:applicantId/link', authMiddleware(constants.INFLUENCER), campaignMiddleware.updateApplicantPostLink);

router.get('/brand/:id', authMiddleware(constants.INFLUENCER), influencerCampaignMiddlewares.getAllCompanyCampaigns);

module.exports = router;
