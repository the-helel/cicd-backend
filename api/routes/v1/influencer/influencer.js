const router = require('express').Router();
const { body } = require('express-validator');

const influencerProfileMiddlewares = require('../../../controllers/influencer/influencer');
const createInfluencerMiddlewares = require('../../../controllers/influencer/auth/create-influencer');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

const constants = require('../../../constants');

router.get('/', authMiddleware(constants.INFLUENCER), influencerProfileMiddlewares.getProfile);

router.post(
    '/',
    [
        body('authType').trim().isString().isIn(['email', 'facebook', 'apple']),
        body('authId').trim().isMongoId(),
        body('name').trim().isString(),
        body('dob').isDate(),
        body('gender').trim().isIn(['male', 'female', 'other']),
        body('city').trim(),
        body('phoneNumber').trim().isMobilePhone(),
        body('categoryIds').isArray(),
    ],
    createInfluencerMiddlewares.createInfluencer
);

router.put(
    '/',
    authMiddleware(constants.INFLUENCER),
    [
        body('name').trim().isString(),
        body('dob').isDate(),
        body('gender').trim().isIn(['male', 'female', 'other']),
        body('city').trim(),
        body('phoneNumber').trim().isMobilePhone(),
    ],
    influencerProfileMiddlewares.saveProfile
);

router.get('/categories', authMiddleware(constants.INFLUENCER), influencerProfileMiddlewares.getInfluencerCategories);

router.put(
    '/categories',
    authMiddleware(constants.INFLUENCER),
    [body('categoryIds').isArray()],
    influencerProfileMiddlewares.saveCategories
);

router.post('/subscribe', [body('email').trim().isEmail()], influencerProfileMiddlewares.subscribe);

module.exports = router;
