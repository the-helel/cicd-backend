/**
 * Routes for story and earn
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 */

const router = require('express').Router();
const { body, query } = require('express-validator');

const journeyMiddlewares = require('../../../../controllers/influencer/activity/journey');
const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/journeys', authMiddleware(constants.INFLUENCER), journeyMiddlewares.getAllJournies);

router.get('/bookmarked-journeys', authMiddleware(constants.INFLUENCER), journeyMiddlewares.getBookmarkedJourneys);

router.post('/bookmark-journey', authMiddleware(constants.INFLUENCER), [body('ghostId').isMongoId()], journeyMiddlewares.bookmarkJourney);

router.delete(
    '/bookmark-journey',
    authMiddleware(constants.INFLUENCER),
    [query('id').isMongoId()],
    journeyMiddlewares.removeBookmarkedJourney
);

module.exports = router;
