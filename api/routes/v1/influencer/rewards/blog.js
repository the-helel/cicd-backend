const router = require('express').Router();
const { body, query } = require('express-validator');

const blogAndEarnMiddlewares = require('../../../../controllers/influencer/activity/blog');
const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/blog', authMiddleware(constants.INFLUENCER), [query('id').trim().isMongoId()], blogAndEarnMiddlewares.getBlogInfoAndStatus);

router.post('/blog', authMiddleware(constants.INFLUENCER), [body('blogURL').trim().isURL()], blogAndEarnMiddlewares.postBlogLink);

router.get('/blogs', authMiddleware(constants.INFLUENCER), blogAndEarnMiddlewares.getAllBlogs);

router.get('/bookmarked-blogs', authMiddleware(constants.INFLUENCER), blogAndEarnMiddlewares.getBookmarkedBlogs);

router.post(
    '/bookmark-blog',
    authMiddleware(constants.INFLUENCER),
    [
        body('ghostId').isMongoId(),
        // body('title').trim().isString(),
        // body('image').trim().isURL(),
        // body('url').trim().isURL(),
        // body('readingTime').trim().isNumeric(),
        // body('publishedAt').trim().isString(),
    ],
    blogAndEarnMiddlewares.bookmarkBlog
);

router.delete(
    '/bookmark-blog/',
    authMiddleware(constants.INFLUENCER),
    [query('ghostId').isMongoId()],
    blogAndEarnMiddlewares.removeBookmarkedBlog
);

module.exports = router;
