const router = require('express').Router();

const blogAndEarnRoutes = require('./blog');
const journeyRoutes = require('./journey');
const redeemRoutes = require('./redeem');
const shippingDetailsRoutes = require('./shipping-details');
const walletRoutes = require('./wallet');

router.use(blogAndEarnRoutes);
router.use(journeyRoutes);
router.use(redeemRoutes);
router.use(shippingDetailsRoutes);
router.use('/rewards/wallet/', walletRoutes);

module.exports = router;
