const router = require('express').Router();
const { body, query } = require('express-validator');

const shippingDetailsMiddlewares = require('../../../../controllers/influencer/reward/shipping-details');
const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/shipping-details', authMiddleware(constants.INFLUENCER), shippingDetailsMiddlewares.getShippingDetails);

router.post(
    '/shipping-details',
    authMiddleware(constants.INFLUENCER),
    [
        body('name').trim().isString(),
        body('email').trim().isEmail(),
        body('address').trim().isString(),
        body('pinCode').trim().isNumeric(),
        body('phoneNumber').trim().isMobilePhone(),
        body('city').trim().isString(),
        body('state').trim().isString(),
    ],
    shippingDetailsMiddlewares.addShippingDetails
);

router.put(
    '/shipping-details',
    authMiddleware(constants.INFLUENCER),
    [
        body('uid').trim().isBase58(),
        body('name').trim().isString(),
        body('email').trim().isString().isEmail(),
        body('address').trim().isString(),
        body('pinCode').trim().isNumeric(),
        body('phoneNumber').trim().isMobilePhone(),
        body('city').trim().isString(),
        body('state').trim().isString(),
    ],
    shippingDetailsMiddlewares.editShippingDetails
);

router.delete(
    '/shipping-details',
    authMiddleware(constants.INFLUENCER),
    [query('uid').isBase58()],
    shippingDetailsMiddlewares.removeShippingDetails
);

module.exports = router;
