const router = require('express').Router();
const { body } = require('express-validator');

const redeemMiddleware = require('../../../../controllers/influencer/reward/redeem');
const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/scratch-cards', authMiddleware(constants.INFLUENCER), redeemMiddleware.getCashScratchCards);

router.post(
    '/claim-scratch-card',
    authMiddleware(constants.INFLUENCER),
    [body('productId').isMongoId()],
    redeemMiddleware.claimScratchCard
);

router.get('/products', authMiddleware(constants.INFLUENCER), redeemMiddleware.getAllProducts);

router.get('/product/:id', authMiddleware(constants.INFLUENCER), redeemMiddleware.getProductById);

router.post('/claim', authMiddleware(constants.INFLUENCER), redeemMiddleware.claimProduct);

module.exports = router;
