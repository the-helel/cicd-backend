const router = require('express').Router();

const influencerRewardsWalletMiddlewares = require('../../../../controllers/influencer/reward/wallet');

const authMiddleware = require('../../../../controllers/auth/auth-middleware');

const constants = require('../../../../constants');

router.get('/', authMiddleware(constants.INFLUENCER), influencerRewardsWalletMiddlewares.getRewardsWallet);
router.get('/points/transactions/', authMiddleware(constants.INFLUENCER), influencerRewardsWalletMiddlewares.getPointsTransactionHistory);

module.exports = router;
