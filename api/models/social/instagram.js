const mongoose = require('mongoose');
require('mongoose-double')(mongoose);

const SchemaTypes = mongoose.Schema.Types;

const InstagramSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        userId: String,
        igId: String,
        username: String,
        website: String,
        numberOfFollowers: Number,
        numberOfFollowing: Number,
        fullName: String,
        profilePicture: String,
        engagementRate: {
            type: Number,
            default: 0,
        },
        biography: String,
    },
    { timestamps: true }
);

const Instagram = mongoose.model('instagram', InstagramSchema);

module.exports = Instagram;
