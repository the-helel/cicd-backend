const mongoose = require('mongoose');

const YoutubeSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
        },
        channelId: String,
        channelData: {
            name: String,
            description: String,
            topics: [],
            country: String,
            privacyStatus: {
                type: String,
                enum: ['public', 'private', 'unlisted'],
            },
            dateChannelCreated: Date,
            image: {
                type: String,
            },
        },
        statistics: {
            viewCount: Number,
            subscriberCount: Number,
            videoCount: Number,
        },
    },
    { timestamps: true }
);

const Youtube = mongoose.model('youtube', YoutubeSchema);

module.exports = Youtube;
