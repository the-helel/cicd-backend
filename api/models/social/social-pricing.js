/**
 * Schema to store social platform pricing of user
 */

const mongoose = require('mongoose');

const SocialPricingSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        instagram: {
            staticPost: {
                type: Number,
                default: null,
            },
            staticStory: {
                type: Number,
                default: null,
            },
            videoPost: {
                type: Number,
                default: null,
            },
            videoStory: {
                type: Number,
                default: null,
            },
            reelVideo: {
                type: Number,
                default: null,
            },
            carouselPost: {
                type: Number,
                default: null,
            },
        },
    },
    { timestamps: true }
);

const SocialPricing = mongoose.model('socialPricing', SocialPricingSchema);

module.exports = SocialPricing;
