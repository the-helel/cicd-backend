const mongoose = require('mongoose');

const FacebookSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencer',
            default: null,
        },
        userId: String,
        accessToken: String,
        accessTokenExpiry: Date,
        pageAccessToken: String,
        fullName: String,
        email: String,
        profilePicture: String,
    },
    { timestamps: true }
);

const Facebook = mongoose.model('facebook', FacebookSchema);

module.exports = Facebook;
