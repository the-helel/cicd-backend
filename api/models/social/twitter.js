const mongoose = require('mongoose');
require('mongoose-double')(mongoose);

const TwitterSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        twitterId: String,
        twitterName: String,
        twitterScreenName: String,
        description: String,
        profilePicture: String,
        isVerified: Boolean,
        profileImage: String,
        followersCount: Number,
        followingsCount: Number,
        tweetsCount: Number,
        url: String,
    },
    {
        timestamps: true,
    }
);

const Twitter = mongoose.model('twitter', TwitterSchema);
module.exports = Twitter;
