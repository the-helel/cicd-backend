const mongoose = require('mongoose');

const AppleSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencer',
            default: null,
        },
        userId: String,
        identityToken: String,
        name: String,
        email: String,
        authorizationCode: String,
    },
    { timestamps: true }
);

const Apple = mongoose.model('apple-sign-in-auth', AppleSchema);

module.exports = Apple;
