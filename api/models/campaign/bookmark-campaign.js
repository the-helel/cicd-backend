const mongoose = require('mongoose');

const BookmarkedCampaignSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        campaign: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaign',
        },
    },
    { timestamps: true }
);

const SavedCashCampaigns = mongoose.model('bookmarked-campaign', BookmarkedCampaignSchema);

module.exports = SavedCashCampaigns;
