const mongoose = require('mongoose');

const constants = require('../../constants.js');

const CampaignSchema = new mongoose.Schema(
    {
        brandId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'brand',
            default: null,
        },
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'company',
            default: null,
        },
        campaignType: {
            type: String,
            enum: [constants.CASH, constants.PRODUCT],
            default: constants.CASH,
        },
        status: {
            type: String,
            enum: [
                constants.DRAFT,
                constants.SUBMITTED,
                constants.ACTIVE,
                constants.PAUSE,
                constants.EXPIRED,
                constants.ARCHIVE,
                constants.COMPLETED,
            ],
            default: constants.DRAFT,
        },
        title: {
            type: String,
            default: null,
        },
        ratings: {
            type: String,
            default: '',
        },
        aboutBrand: String,
        brandName: String,
        brandLogo: String,
        caption: {
            type: String,
            default: '',
        },
        coverImage: {
            type: String,
            default: '',
        },
        brief: {
            moodboard: {
                type: Array,
                default: [],
            },
            description: {
                type: String,
                default: '',
            },
            dos: {
                type: Array,
                default: [
                    'The image should be visually appealing.',
                    'Make sure the content is high quality, well lit and not heavily edited.',
                ],
            },
            donts: {
                type: Array,
                default: [
                    'Any sort of nudity and profanity shall not be entertained.',
                    'Do not post without approval.',
                    'Do not share product images available publicly on the internet.',
                ],
            },
            hashtags: {
                type: Array,
                default: [],
            },
            campaignPlatform: {
                type: String,
                default: '',
            },
            campaignUrl: {
                type: String,
                default: '',
            },
            categories: [
                {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'category',
                    default: null,
                },
            ],
            taggedProfiles: {
                type: Array,
                default: [],
            },
            startDate: {
                type: Date,
                default: null,
            },
            endDate: {
                type: Date,
                default: null,
            },
        },
        product: {
            isProvidedByBrand: {
                type: Boolean,
                default: false,
            },
            optionalDetails: {
                type: String,
                default: '',
            },
            valueOfProduct: {
                type: String,
                default: '',
            },
        },
        criteria: {
            gender: {
                type: String,
                enum: ['Male', 'Female', 'Both'],
                default: 'Male',
            },
            city: {
                type: String,
                default: '',
            },
            age: {
                min: {
                    type: Number,
                    default: 15,
                },
                max: {
                    type: Number,
                    default: 60,
                },
            },
            followers: {
                min: {
                    type: Number,
                    default: 1000,
                },
                max: {
                    type: Number,
                    default: 50000,
                },
            },
            task: {
                instagram: {
                    stories: {
                        type: Number,
                        default: 0,
                    },
                    reels: {
                        type: Number,
                        default: 0,
                    },
                    posts: {
                        type: Number,
                        default: 0,
                    },
                    carousels: {
                        type: Number,
                        default: 0,
                    },
                },
                posts: {
                    type: Number,
                    default: 0,
                },
                videos: {
                    type: Number,
                    default: 0,
                },
            },
        },
        payment: {
            budget: {
                type: Number,
                default: '',
            },
            productCount: {
                type: Number,
                default: 0,
            },
            method: {
                type: String,
                default: '',
            },
            remarks: {
                type: String,
                default: '',
            },
            utrno: {
                type: String,
                default: '',
            },
            contact: {
                companyName: {
                    type: String,
                    default: '',
                },
                gstno: {
                    type: String,
                    default: '',
                },
                phoneno: {
                    type: Number,
                    default: '',
                },
                pincode: {
                    type: Number,
                    default: '',
                },
                locality: {
                    type: String,
                    default: '',
                },
                landmark: {
                    type: String,
                    default: '',
                },
                address: {
                    type: String,
                    default: '',
                },
                city: {
                    type: String,
                    default: '',
                },
                state: {
                    type: String,
                    default: '',
                },
                country: {
                    type: String,
                    default: '',
                },
            },
        },
        report: {
            summary: {
                influencer_count: {
                    total: {
                        type: Number,
                        default: 0,
                    },
                    rejected: {
                        type: Number,
                        default: 0,
                    },
                    selected: {
                        type: Number,
                        default: 0,
                    },
                },
                post_count: {
                    type: Number,
                    default: 0,
                },
                estimated_reach: {
                    type: Number,
                    default: 0,
                },
                actual_reach: {
                    type: Number,
                    default: 0,
                },
            },
            progress: {
                post: {
                    total_posts: {
                        type: Number,
                        default: 0,
                    },
                    total_live_posts: {
                        type: Number,
                        default: 0,
                    },
                    progress: {
                        type: Number,
                        default: 0,
                    },
                },
                budget: {
                    total_cost: {
                        type: Number,
                        default: 0,
                    },
                    cost_spent: {
                        type: Number,
                        default: 0,
                    },
                    progress: {
                        type: Number,
                        default: 0,
                    },
                },
                engagement: {
                    post_count: {
                        type: Number,
                        default: 0,
                    },
                    likes: {
                        type: Number,
                        default: 0,
                    },
                    comments: {
                        type: Number,
                        default: 0,
                    },
                    engagement_rate: {
                        type: Number,
                        default: 0,
                    },
                    video_views: {
                        type: Number,
                        default: 0,
                    },
                },
            },
        },
    },
    {
        timestamps: true,
    }
);

const Campaign = mongoose.model('campaign', CampaignSchema);

module.exports = Campaign;
