const mongoose = require('mongoose');

const constants = require('../../../constants.js');

const cashCampaignSchema = new mongoose.Schema(
    {
        brand: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'brands',
            default: null,
        },
        inHouse: {
            type: Boolean,
            default: false,
        },
        inviteOnly: {
            type: Boolean,
            default: false,
        },
        influencers: {
            type: Array,
            default: [],
        },
        status: {
            type: String,
            enum: [constants.DRAFT, constants.ACTIVE, constants.EXPIRED],
            default: constants.DRAFT,
        },
        socialType: {
            type: String,
            enum: [constants.INSTAGRAM, constants.FACEBOOK, constants.YOUTUBE, constants.TWITTER, constants.LINKEDIN, constants.SNAPCHAT],
            default: null,
        },
        brief: {
            objective: {
                type: String,
                default: '',
            },
            title: {
                type: String,
                default: '',
            },
            brief: {
                type: String,
                default: '',
            },
            tasks: {
                instagram: {
                    type: {
                        stories: {
                            type: Number,
                            default: 0,
                        },
                        posts: {
                            type: Number,
                            default: 0,
                        },
                    },
                },
            },
            pageUrl: {
                type: String,
                default: '',
            },
            caption: {
                type: String,
                default: '',
            },
            coverPhoto: {
                url: {
                    type: String,
                    default: '',
                },
            },
            product: {
                isProvidedByBrand: {
                    type: Boolean,
                    default: false,
                },
                optionalDetails: {
                    type: String,
                    default: '',
                },
            },
        },
        criteria: {
            gender: {
                type: String,
                enum: ['Male', 'Female', 'Both'],
                default: 'Male',
            },
            city: {
                type: String,
                default: '',
            },
            age: {
                min: {
                    type: Number,
                    default: null,
                },
                max: {
                    type: Number,
                    default: null,
                },
            },
            reachCount: {
                min: {
                    type: Number,
                    default: null,
                },
                max: {
                    type: Number,
                    default: null,
                },
            },
            categories: [
                {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'categories',
                    default: null,
                },
            ],
            moodboard: {
                type: Array,
                default: [],
            },
            description: {
                type: String,
                default: '',
            },
            dos: {
                type: Array,
                default: [],
            },
            donts: {
                type: Array,
                default: [],
            },
            hashtags: {
                type: Array,
                default: [],
            },
            taggedProfiles: {
                type: Array,
                default: [],
            },
            caption: {
                type: String,
                default: '',
            },
        },
        budget: {
            type: Number,
            default: '',
        },
        startDate: {
            type: Date,
            default: null,
        },
        endDate: {
            type: Date,
            default: null,
        },
    },
    { timestamps: true }
);

const CashCampaign = mongoose.model('cash-campaign', cashCampaignSchema);

module.exports = CashCampaign;
