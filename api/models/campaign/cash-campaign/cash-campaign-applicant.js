const mongoose = require('mongoose');

const constants = require('../../../constants');

const CashCampaignApplicantSchema = new mongoose.Schema(
    {
        status: {
            type: String,
            enum: [
                constants.APPLIED,
                constants.SHORTLISTED,
                constants.REJECTED,
                constants.APPROVED,
                constants.SUBMITTED,
                constants.COMPLETED,
            ],
            default: constants.APPLIED,
        },
        pitch: {
            type: String,
            default: '',
        },
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        campaign: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaigns',
            required: true,
        },
        bids: {
            type: Array,
            default: [],
        },
        workSample: {
            type: Array,
            default: [],
        },
        link: {
            type: String,
            default: '',
        },
        social: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
    },
    { timestamps: true }
);

const CashCampaignApplicant = mongoose.model('cash-campaign-applicant', CashCampaignApplicantSchema);

module.exports = CashCampaignApplicant;
