const mongoose = require('mongoose');

const SavedCashCampaignsSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        savedCampaigns: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'cash-campaigns',
                default: [],
            },
        ],
    },
    { timestamps: true }
);

const SavedCashCampaigns = mongoose.model('saved-cash-campaign', SavedCashCampaignsSchema);

module.exports = SavedCashCampaigns;
