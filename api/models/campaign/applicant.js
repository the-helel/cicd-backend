const mongoose = require('mongoose');

const constants = require('../../constants.js');

const CampaignApplicantSchema = new mongoose.Schema(
    {
        campaignId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaign',
            required: true,
        },
        influencerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencer',
            required: true,
        },
        status: {
            type: String,
            enum: [
                constants.APPLIED,
                constants.SHORTLISTED,
                constants.REJECTED,
                constants.APPROVED,
                constants.SUBMITTED,
                constants.COMPLETED,
            ],
            default: constants.APPLIED,
        },
        social: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        pitch: {
            type: String,
            default: '',
        },
        bids: {
            type: Array,
            default: [],
        },
        workSample: {
            type: Array,
            default: [],
        },
        amount: {
            type: Number,
            default: null,
        },
        crossBidAmount: {
            type: Number,
            default: null,
        },
        finalBidAmount: {
            type: Number,
            default: null,
        },
        link: [
            {
                type: mongoose.Schema.ObjectId,
                ref: 'campaign-post',
            },
        ],
        applied_platform: {
            type: String,
            default: '',
        },
        name: {
            type: String,
            default: '',
        },
        followers: {
            type: Number,
            default: 0,
        },
        post_count: {
            type: Number,
            default: 0,
        },
        engagement: {
            type: Number,
            default: 0,
        },
        video_views: {
            type: Number,
            default: 0,
        },
        like_count: {
            type: Number,
            default: 0,
        },
        location: {
            type: String,
            default: '',
        },
        category: {
            type: Array,
            default: [],
        },
        platforms: {
            type: Array,
            default: [],
        },
        posts: {
            type: Array,
            default: [],
        },
    },
    { timestamps: true }
);

// Instance Methods
CampaignApplicantSchema.methods.addLinkMeta = function (metadata) {
    this.link.push(metadata);
    this.markmodified('link');
};

const CampaignApplicant = mongoose.model('campaign-applicant', CampaignApplicantSchema);

module.exports = CampaignApplicant;
