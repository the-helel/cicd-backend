const mongoose = require('mongoose');
const constants = require('../../constants.js');
const { getTimeSince } = require('../../../utils/formatter');

const CampaignPostSchema = new mongoose.Schema(
    {
        campaignId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaign',
            required: true,
        },
        applicantId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaign-applicant',
        },
        platform: {
            type: String,
        },
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencer',
        },
        short_code: {
            type: String,
            required: true,
        },
        post_link: {
            type: String,
            required: true,
        },
        posted_at: {
            type: Date,
            default: '',
        },
        status: {
            type: String,
            enum: [constants.ACTIVE, constants.EXPIRED],
            default: constants.ACTIVE,
        },
        media: {
            type: {
                type: String,
                default: '',
            },
            id: {
                type: String,
                default: null,
            },
            display_url: {
                type: String,
                default: '',
            },
        },
        caption: {
            type: String,
            default: '',
        },
        like_count: {
            type: Number,
            default: 0,
        },
        comment_count: {
            type: Number,
            default: 0,
        },
        engagement: {
            type: Number,
            default: 0,
        },
        video_view_count: {
            type: Number,
            default: 0,
        },
        impressions: {
            type: Number,
            default: 0,
        },
        engagement_rate_by_impression: {
            type: Number,
            default: 0,
        },
        engagement_rate_by_reach: {
            type: Number,
            default: 0,
        },
        engagement_rate_by_followers: {
            type: Number,
            default: 0,
        },
        estimated_reach: {
            type: Number,
            default: 0,
        },
        actual_reach: {
            type: Number,
            default: 0,
        },
        timeline: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'timeline-post',
        },
    },
    { timestamps: true }
);

CampaignPostSchema.virtual('since').get(function () {
    console.log('Get since posted content');
    return getTimeSince(this.posted_at);
});

const CampaignPost = mongoose.model('campaign-post', CampaignPostSchema);

module.exports = CampaignPost;
