const mongoose = require('mongoose');

const constants = require('../../../constants');

const ProductCampaignApplicantSchema = new mongoose.Schema(
    {
        status: {
            type: String,
            enum: [
                constants.APPLIED,
                constants.SHORTLISTED,
                constants.REJECTED,
                constants.APPROVED,
                constants.SUBMITTED,
                constants.COMPLETED,
            ],
            default: constants.APPLIED,
        },
        pitch: {
            type: String,
            default: '',
        },
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        campaign: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaigns',
            required: true,
        },
        workSample: {
            type: Array,
            default: [],
        },
        link: {
            type: String,
            default: '',
        },
        social: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
    },
    { timestamps: true }
);

const ProductCampaignApplicant = mongoose.model('product-campaign-applicant', ProductCampaignApplicantSchema);

module.exports = ProductCampaignApplicant;
