const mongoose = require('mongoose');

const SavedProductCampaignsSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        savedCampaigns: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'product-campaigns',
                default: [],
            },
        ],
    },
    { timestamps: true }
);

const SavedProductCampaigns = mongoose.model('saved-product-campaign', SavedProductCampaignsSchema);

module.exports = SavedProductCampaigns;
