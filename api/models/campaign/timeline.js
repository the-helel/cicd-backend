const mongoose = require('mongoose');

const timelineDataSchema = new mongoose.Schema({
    timestamp: {
        type: Date,
    },
    value: Number,
});

const TimelinePostSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        applicant: {
            type: mongoose.Types.ObjectId,
            ref: 'campaign-post',
            required: true,
        },
        post: {
            type: mongoose.Types.ObjectId,
            ref: 'campaign-applicant',
            required: true,
        },
        campaign: {
            type: mongoose.Types.ObjectId,
            ref: 'campaign',
        },
        impressions: {
            type: Array,
        },
        likes: {
            type: Array,
        },
        engagements: {
            type: Array,
        },
        video_views: {
            type: Array,
        },
    },
    { timestamps: true }
);

const PostTimeline = mongoose.model('timeline-post', TimelinePostSchema);

module.exports = PostTimeline;
