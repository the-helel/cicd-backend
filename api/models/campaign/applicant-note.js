const mongoose = require('mongoose');
const constants = require('../../constants.js');

const ApplicantNoteSchema = new mongoose.Schema(
    {
        applicantId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaign-applicants',
            required: true,
        },
        note: {
            type: String,
            default: '',
            required: true,
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companies',
            required: true,
        },
    },
    { timestamps: true }
);

const ApplicantNote = mongoose.model('applicant-notes', ApplicantNoteSchema);

module.exports = ApplicantNote;
