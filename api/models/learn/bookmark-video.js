const mongoose = require('mongoose');

const BookmarkVideoSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        videos: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'videos',
                default: [],
            },
        ],
    },
    { timestamps: true }
);

const BookmarkVideo = mongoose.model('bookmark-video', BookmarkVideoSchema);

module.exports = BookmarkVideo;
