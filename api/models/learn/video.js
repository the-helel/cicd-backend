const mongoose = require('mongoose');

const VideoSchema = new mongoose.Schema(
    {
        videoCode: {
            type: String,
            required: true,
        },
        title: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        category: {
            type: String,
            required: true,
        },
        keyMoments: {
            type: Array,
            default: [],
        },
    },
    { timestamps: true }
);

const Video = mongoose.model('video', VideoSchema);

module.exports = Video;
