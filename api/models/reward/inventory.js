/**
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 * Schema for Purchase inventory where each and every purchase
 * record would be stored.
 *
 */

const mongoose = require('mongoose');

const inventorySchema = new mongoose.Schema(
    {
        productId: {
            type: mongoose.Schema.Types.ObjectId,
        },
        quantityPurchased: {
            type: Number,
        },
        unitPrice: {
            type: Number,
        },
        totalCosting: {
            type: Number,
        },
        issuer: {
            type: String,
        },
        dateOfPurchase: {
            type: Date,
            default: Date.now,
        },
        billImage: {
            type: String,
        },
    },
    { timestamps: true }
);

const Inventory = mongoose.model('inventory', inventorySchema);

module.exports = Inventory;
