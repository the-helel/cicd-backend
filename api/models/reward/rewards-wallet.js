const mongoose = require('mongoose');

const rewardsWalletSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        points: {
            type: Number,
            default: 0,
        },
    },
    { timestamps: true }
);

const RewardsWallet = mongoose.model('rewards-wallet', rewardsWalletSchema);

module.exports = RewardsWallet;
