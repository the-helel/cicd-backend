const mongoose = require('mongoose');

const shippingDetailsSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        shippingDetails: {
            type: Object,
        },
    },
    { timestamps: true }
);

const shippingDetails = mongoose.model('shipping-details', shippingDetailsSchema);

module.exports = shippingDetails;
