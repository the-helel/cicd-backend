/**
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 * Schema for Products that can be reedem using points
 */

const mongoose = require('mongoose');
const constants = require('../../constants');

const productSchema = new mongoose.Schema(
    {
        productName: {
            type: String,
        },
        productImages: {
            type: Array,
        },
        productType: {
            type: String,
            enum: [constants.VOUCHER, constants.COUPON, constants.CASH],
            default: constants.CASH,
        },
        productDescription: {
            type: String,
        },
        productRedeemPoints: {
            type: Number,
            required: true,
        },
        quantity: {
            type: Number,
        },
        brandName: {
            type: String,
        },
        brandDescription: {
            type: String,
        },
    },
    { timestamps: true }
);

const Products = mongoose.model('product', productSchema);

module.exports = Products;
