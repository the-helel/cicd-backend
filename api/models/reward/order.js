const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema(
    {
        productId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'products',
            required: true,
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        shippingDetails: {
            address: {
                type: String,
            },
            pinCode: {
                type: Number,
            },
            phoneNumber: {
                type: String,
            },
            city: {
                type: String,
            },
            state: {
                type: String,
            },
        },
    },
    { timestamps: true }
);

const productRedeemOrder = mongoose.model('product-order', orderSchema);

module.exports = productRedeemOrder;
