/**
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 * Schema for transactions made by influencer to add or redeem points
 */

const mongoose = require('mongoose');

const constants = require('../../constants');

const pointsTransactionSchema = new mongoose.Schema(
    {
        transactionType: {
            type: String,
            enum: [constants.CREDIT, constants.REDEEM],
            required: true,
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        points: {
            type: Number,
            required: true,
        },
        metadata: {
            source: {
                type: String,
                enum: [
                    constants.STORY,
                    constants.BLOG,
                    constants.REFERRAL,
                    constants.INSTAGRAM_CONNECT,
                    constants.VOUCHER,
                    constants.COUPON,
                    constants.CASH,
                ],
            },
            sourceId: {
                type: mongoose.Schema.Types.ObjectId,
            },
            name: {
                type: String,
                required: false,
            },
        },
    },
    { timestamps: true }
);

const PointsTransaction = mongoose.model('points-transaction', pointsTransactionSchema);

module.exports = PointsTransaction;
