const mongoose = require('mongoose');
const constants = require('../../constants');
require('mongoose-double')(mongoose);

const SchemaTypes = mongoose.Schema.Types;

const transactionSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
        },
        type: {
            type: String,
            enum: [constants.CREDIT, constants.REDEEM],
            default: constants.CREDIT,
        },
        amount: {
            type: Number,
        },
        metadata: {
            description: {
                type: String,
                required: false,
            },
            entityType: {
                type: String,
            },
            entityId: {
                type: mongoose.Schema.Types.ObjectId,
            },
            payoutId: {
                type: String,
            },
            fundsAccountId: {
                type: String,
            },
            utr: {
                type: String,
            },
            mode: {
                type: String,
                enum: [constants.UPI, constants.IMPS],
            },
            payoutAmount: {
                type: SchemaTypes.Double,
            },
        },
        status: {
            type: String,
            enum: [constants.QUEUED, constants.PROCESSING, constants.PROCESSED, constants.CANCELLED, constants.REVERSED, constants.FAILED],
        },
    },
    { timestamps: true }
);

const Transaction = mongoose.model('transaction', transactionSchema);

module.exports = Transaction;
