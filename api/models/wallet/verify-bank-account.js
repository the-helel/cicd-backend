const mongoose = require('mongoose');

const constants = require('../../constants');

const VerifyBankAccountSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        fundsAccountId: {
            type: String,
            required: true,
        },
        photo: {
            type: String,
            required: true,
        },
        status: {
            type: String,
            enum: [constants.REJECTED, constants.PENDING, constants.VERIFIED],
            required: true,
        },
        feedback: {
            type: String,
            default: '',
        },
    },
    { timestamps: true }
);

const VerifyBankAccount = mongoose.model('verify-bank-account', VerifyBankAccountSchema);

module.exports = VerifyBankAccount;
