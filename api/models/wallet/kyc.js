const mongoose = require('mongoose');

const constants = require('../../constants');

const KYCSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
        },
        status: {
            type: String,
            enum: [constants.VERIFYING, constants.VERIFIED, constants.REJECTED],
            default: constants.VERIFYING,
        },
        feedback: {
            type: String,
            default: '',
        },
        uid: {
            type: String,
            required: true,
        },
        name: {
            type: String,
            enum: [constants.AADHAAR_CARD, constants.PASSPORT, constants.DRIVING_LICENSE],
            required: true,
        },
        document: {
            type: Array,
            required: true,
        },
        panNumber: {
            type: String,
            required: true,
        },
        panCard: {
            type: Array,
            required: true,
        },
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
        },
    },
    { timestamps: true }
);

const KYC = mongoose.model('kyc', KYCSchema);

module.exports = KYC;
