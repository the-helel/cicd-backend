const mongoose = require('mongoose');

const WalletSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        totalEarnings: {
            type: Number,
            default: 0,
        },
        moneyToBeWithdrawn: {
            type: Number,
            default: 0,
        },
        bankAccounts: {
            type: Object,
        },
        upiAccounts: {
            type: Object,
        },
        defaultAccount: {
            type: String,
        },
        kyc: {
            type: mongoose.Types.ObjectId,
            ref: 'kycs',
            default: null,
        },
    },
    { timestamps: true }
);

const Wallet = mongoose.model('wallet', WalletSchema);

module.exports = Wallet;
