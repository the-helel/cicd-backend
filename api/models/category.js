const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema(
    {
        name: String,
        description: {
            type: String,
            default: '',
        },
        icon: String,
    },
    { timestamps: true }
);

const Category = mongoose.model('category', CategorySchema);

module.exports = Category;
