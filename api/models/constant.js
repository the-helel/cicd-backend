const mongoose = require('mongoose');

const ConstantSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        value: {
            type: String,
            required: true,
        },
    },
    { timestamps: true }
);

const Constant = mongoose.model('constant', ConstantSchema);

module.exports = Constant;
