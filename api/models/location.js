const mongoose = require('mongoose');

const LocationSchema = new mongoose.Schema({
    city: String,
    state: String,
});

const Location = mongoose.model('location', LocationSchema);

module.exports = Location;
