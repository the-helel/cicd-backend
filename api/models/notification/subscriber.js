const mongoose = require('mongoose');
const constants = require('../../constants');

const subscriberSchema = new mongoose.Schema(
    {
        fcmToken: {
            type: String,
            required: true,
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        userType: {
            type: String,
            enum: [constants.INFLUENCER, constants.COMPANY],
            required: [true, 'User Type is required'],
        },
    },
    { timestamps: true }
);

const Subscription = mongoose.model('subscription', subscriberSchema);

module.exports = Subscription;
