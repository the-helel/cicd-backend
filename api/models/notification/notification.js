const mongoose = require('mongoose');
const constants = require('../../constants');

const notificationSchema = new mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Notification User is required'],
        },
        title: {
            type: String,
            required: [true, 'Notification Title is required'],
        },
        body: {
            type: String,
            required: [true, 'Notification Description is required'],
        },
        image: {
            type: String,
            default: null,
        },
        type: {
            type: String,
            required: [true, 'Notification Type is required'],
        },
        subType: {
            type: String,
            default: null,
        },
        parameter: {
            type: Object,
            default: null,
        },
        read: {
            type: Boolean,
            default: false,
        },
    },
    { timestamps: true }
);

const Notification = mongoose.model('notification', notificationSchema);

module.exports = Notification;
