const mongoose = require('mongoose');

const constants = require('../constants');

const ContactMessageSchema = new mongoose.Schema(
    {
        type: {
            type: String,
            enum: [constants.BRAND, constants.INFLUENCER],
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        phoneNumber: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        body: {
            type: String,
            required: true,
        },
    },
    { timestamps: true }
);

const ContactMessage = mongoose.model('contact-message', ContactMessageSchema);

module.exports = ContactMessage;
