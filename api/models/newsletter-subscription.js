const mongoose = require('mongoose');

const constants = require('../constants.js');

const NewsletterSubscriptionSchema = new mongoose.Schema(
    {
        id: mongoose.Schema.Types.ObjectId,
        type: {
            type: String,
            enum: [constants.BRAND, constants.INFLUENCER],
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
    },
    { timestamps: true }
);

const NewsletterSubscription = mongoose.model('newsletter-subscription', NewsletterSubscriptionSchema);

module.exports = NewsletterSubscription;
