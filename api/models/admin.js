const mongoose = require('mongoose');

const AdminSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: [true, 'Email is required'],
        },
        password: {
            type: String,
            required: [true, 'Password is required'],
        },
    },
    { timestamps: true }
);

const Admin = mongoose.model('admin', AdminSchema);

module.exports = Admin;
