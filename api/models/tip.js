const mongoose = require('mongoose');

const TipSchema = new mongoose.Schema(
    {
        tip: {
            type: String,
            required: true,
        },
    },
    { timestamps: true }
);

const Tip = mongoose.model('tip', TipSchema);

module.exports = Tip;
