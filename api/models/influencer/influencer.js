const mongoose = require('mongoose');

const InfluencerSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        dob: Date,
        gender: {
            type: String,
            enum: ['male', 'female', 'other'],
            default: 'male',
        },
        city: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        categories: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'categories',
            },
        ],
        phoneNumber: {
            type: String,
            required: true,
        },
        profilePicture: {
            type: String,
            required: true,
        },
        referralCode: {
            type: String,
            required: true,
        },
        referralLink: {
            type: String,
        },
    },
    { timestamps: true }
);

const Influencer = mongoose.model('influencer', InfluencerSchema);

module.exports = Influencer;
