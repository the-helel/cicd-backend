/**
 * Model holding temporary access token to verify email
 * during user sign up
 */

const mongoose = require('mongoose');

const emailAuthSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            unique: true,
        },
        password: {
            type: String,
        },
        userId: {
            type: mongoose.Types.ObjectId,
            default: null,
        },
        isVerified: {
            type: Boolean,
            enum: [true, false],
            default: false,
        },
        name: {
            type: String,
            required: true,
        },
    },
    { timestamps: true }
);

const emailAuth = mongoose.model('email-auth', emailAuthSchema);

module.exports = emailAuth;
