/**
 * Schema to hold like status of a question by an influencer.
 * Mapping between influencer with question like status
 */

const mongoose = require('mongoose');

const likeQnASchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        question: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'questions',
            required: true,
        },
        isLiked: {
            type: Boolean,
            default: false,
        },
    },
    { timestamps: true }
);

const LikeQnA = mongoose.model('like-qna', likeQnASchema);

module.exports = LikeQnA;
