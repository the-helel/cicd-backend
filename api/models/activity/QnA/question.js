/**
 * Schema for questions of QnA feature on famstar
 *
 */

const mongoose = require('mongoose');

const questionSchema = new mongoose.Schema(
    {
        description: {
            // extra text describing the question
            type: String,
            required: true,
        },
        media: {
            // media uploaded with question like image or video
            type: Array,
            default: [],
        },
        askedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
    },
    { timestamps: true }
);

const Question = mongoose.model('question', questionSchema);

module.exports = Question;
