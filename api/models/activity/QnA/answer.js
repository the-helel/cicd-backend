/**
 * Schema for answers of QnA feature on famstar
 *
 */

const mongoose = require('mongoose');

const answerSchema = new mongoose.Schema(
    {
        answerBody: {
            type: String,
            required: true,
        },
        answeredBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        question: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'questions',
            required: true,
        },
    },
    { timestamps: true }
);

const Answer = mongoose.model('answer', answerSchema);

module.exports = Answer;
