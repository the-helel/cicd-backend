/**
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 * Schema for blog posted by an influencer
 */

const mongoose = require('mongoose');

const constants = require('../../constants');

const blogSchema = new mongoose.Schema(
    {
        status: {
            type: String,
            enum: [constants.SUBMITTED, constants.APPROVED, constants.REJECTED],
            default: constants.SUBMITTED,
        },
        blogURL: {
            type: String,
            default: '',
        },
        postedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        feedback: {
            type: String,
            default: '',
        },
        approvedBlogURL: {
            type: String,
            default: '',
        },
        blogTitle: {
            type: String,
            default: '',
        },
        blogDescription: {
            type: String,
            default: '',
        },
    },
    { timestamps: true }
);

const Blog = mongoose.model('blog', blogSchema);

module.exports = Blog;
