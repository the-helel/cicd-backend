const mongoose = require('mongoose');

const BookmarkBlogSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        blogs: [
            {
                ghostId: String,
                title: String,
                url: String,
                readingTime: Number,
                publishedAt: Date,
                image: String,
            },
        ],
    },
    { timestamps: true }
);

const BookmarkBlog = mongoose.model('bookmark-blog', BookmarkBlogSchema);

module.exports = BookmarkBlog;
