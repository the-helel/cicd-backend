const mongoose = require('mongoose');

const referralSchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        referee: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        isVerified: {
            type: Boolean,
            default: false,
        },
    },
    { timestamps: true }
);

const Referral = mongoose.model('referral', referralSchema);

module.exports = Referral;
