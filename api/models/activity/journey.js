/**
 * Model for story and earn
 *
 * @author Rahul Bera <rbasu611@gmail.com>
 *
 */
const mongoose = require('mongoose');

const constants = require('../../constants');

const journeySchema = new mongoose.Schema(
    {
        status: {
            type: String,
            enum: [constants.SUBMITTED, constants.APPROVED, constants.REJECTED],
            default: constants.SUBMITTED,
        },
        journeyURL: {
            type: String,
            default: '',
        },
        postedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        feedback: {
            type: String,
            default: '',
        },
        approvedJourneyURL: {
            type: String,
            default: '',
        },
        influencerImageURL: {
            type: String,
            default: '',
        },
        influencerName: {
            type: String,
            default: '',
        },
        title: {
            type: String,
        },
        coverPhotoURL: {
            type: String,
        },
    },
    { timestamps: true }
);

const journey = mongoose.model('journey', journeySchema);

module.exports = journey;
