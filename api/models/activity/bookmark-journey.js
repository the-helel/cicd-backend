const mongoose = require('mongoose');

const BookmarkJourneySchema = new mongoose.Schema(
    {
        influencer: {
            type: mongoose.Types.ObjectId,
            ref: 'influencers',
            required: true,
        },
        journeys: [
            {
                ghostId: String,
                title: String,
                url: String,
                readingTime: Number,
                publishedAt: Date,
                image: String,
            },
        ],
    },
    { timestamps: true }
);

const BookmarkJourney = mongoose.model('bookmark-journey', BookmarkJourneySchema);

module.exports = BookmarkJourney;
