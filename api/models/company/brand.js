const mongoose = require('mongoose');

const BrandSchema = new mongoose.Schema(
    {
        company: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companies',
            required: true,
        },
        name: {
            type: String,
            default: '',
            required: [true, 'Name is required'],
        },
        pageUrl: {
            type: String,
            default: '',
            // required: [true, 'Page Url is required'],
        },
        logo: {
            url: {
                type: String,
                default: '',
                required: [true, 'Logo is required'],
            },
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        coverUrl: {
            type: String,
            default: '',
            // required: [true, 'Cover Url is required'],
        },
        about: {
            type: String,
            default: '',
            // required: [true, 'Description is required'],
        },
        rating: {
            type: Number,
            default: 0,
        },
        subscribedBy: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'influencers',
            },
        ],
    },
    { timestamps: true }
);

const Brand = mongoose.model('brand', BrandSchema);

module.exports = Brand;
