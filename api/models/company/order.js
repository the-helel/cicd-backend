const mongoose = require('mongoose');

const OrderSchema = new mongoose.Schema(
    {
        company: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companies',
        },
        entity: {
            type: String,
            required: [true, 'Entity is required'],
        },
        entityDetails: {
            entityId: {
                type: mongoose.Schema.Types.ObjectId,
                required: [true, 'EntityId is required'],
            },
            amount: {
                type: Number,
                required: [true, 'Amount of entity is required'],
            },
            serviceFee: {
                type: Number,
                required: [true, 'Service fee is required (%)'],
            },
            tax: {
                type: Number,
                required: [true, 'Tax(%) is required'],
            },
        },
        orderAmount: {
            type: Number,
            required: [true, 'Amount is required (in paise)'],
        },
        currency: {
            type: String,
            required: [true, 'Currency code is required'],
        },
        invoiceId: {
            type: String,
            required: [true, 'reciept number is required'],
        },
        rpOrderId: {
            type: String,
            required: [true, 'razorpay order Id is required'],
        },
        status: {
            type: String,
            required: [true, 'status is required'],
        },
        paymentDetails: {
            razorpay_payment_id: {
                type: String,
            },
            razorpay_order_id: {
                type: String,
            },
            razorpay_signature: {
                type: String,
            },
        },
    },
    { timestamps: true }
);

const Order = mongoose.model('order', OrderSchema);

module.exports = Order;
