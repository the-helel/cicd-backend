const mongoose = require('mongoose');
const constants = require('../../constants.js');

const CompanySchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: [true, 'Email is required'],
        },
        password: {
            type: String,
            default: null,
        },
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            unique: true,
            default: null,
        },
        name: {
            type: String,
            required: [true, 'Company name is required'],
        },
        role: {
            type: String,
            enum: [constants.ADMIN, constants.ORGANIZATION, constants.MEMBER],
            default: constants.ORGANIZATION,
        },
        mobile: {
            type: String,
            default: null,
        },
        accountHolderName: {
            type: String,
            required: [true, "Account Holder's name is required"],
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        details: {
            logo: {
                type: String,
                default: '',
            },
            uid: {
                type: String,
                default: '',
            },
            regNo: {
                type: String,
                default: '',
            },
            gst: {
                type: String,
                default: '',
            },
            pan: {
                type: String,
                default: '',
            },
        },
        address: {
            available: {
                type: Boolean,
                default: false,
            },
            address: {
                base: {
                    type: String,
                    default: '',
                },
                city: {
                    type: String,
                    default: '',
                },
                state: {
                    type: String,
                    default: '',
                },
                pincode: {
                    type: Number,
                    default: 0,
                },
            },
        },
        verificationStatus: {
            type: Boolean,
            default: false,
            required: true,
        },
        verificationToken: {
            type: String,
            required: false,
        },
        resetPasswordToken: {
            type: String,
            required: false,
        },
        resetPasswordExpires: {
            type: Date,
            required: false,
        },
        brands: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'brands',
            },
        ],
        favInfluencers: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'influencers',
            },
        ],
    },
    { timestamps: true }
);

const Company = mongoose.model('company', CompanySchema);

module.exports = Company;
