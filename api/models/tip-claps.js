const mongoose = require('mongoose');

const TipClapsSchema = new mongoose.Schema(
    {
        tip: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        claps: {
            type: Number,
            required: true,
        },
    },
    { timestamps: true }
);

const TipClaps = mongoose.model('tip-clap', TipClapsSchema);

module.exports = TipClaps;
