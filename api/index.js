const router = require('express').Router();
const v1Routes = require('./routes/v1/index');
const v2Routes = require('./routes/v2/index');

// API versioning routes
router.use(v1Routes);
router.use('/v2', v2Routes);

module.exports = router;
