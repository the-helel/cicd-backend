const admin = require('firebase-admin');

const serviceAccount = require('./fcm-config.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://famstar-app.firebaseio.com',
});

module.exports = admin;
