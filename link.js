const Influencer = require('./models/influencer/influencer');
const FaceBook = require('./models/social/facebook');
const mongoose = require('mongoose');
const config = require('./config');
const s3Middlewares = require('./controllers/util/s3');
const { default: axios } = require('axios');

(async () => {
    try {
        await mongoose.connect(config.DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
        });
        console.log('connected');
    } catch (err) {
        console.log(err);
    }
})();

// Influencer.find({})
//     .then((influencers) => {
//         influencers.forEach(async (influencer) => {
//             let code = influencer.referralCode;

//             let response = await axios.post(`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${config.FIREBASE_WEB_API}`, {
//                 longDynamicLink: `https://famstar.page.link/?link=https://www.famstar.page.link/invite/?uid=${code}&ofl=https://famstar.in&apn=com.famstar.android&afl=https://play.google.com/store/apps/details?id=com.famstar.android&ibi=com.famstar.ios&ifl=https://apps.apple.com/in/app/famstar/id1561400242`,
//                 suffix: {
//                     option: 'SHORT',
//                 },
//             });

//             if (response.status === 200) {
//                 influencer.referralLink = response.data.shortLink;
//                 try {
//                     await influencer.save();
//                 } catch (err) {
//                     console.log(err);
//                 }
//             }

//             console.log(`${code}  ${response.data.shortLink} ${influencer.name}`);
//         });

//         console.log('done');
//     })
//     .catch((err) => console.log(err));

FaceBook.find({}).then(async (facebooks) => {
    facebooks.forEach(async (facebook) => {
        let influencerId = facebook.influencer;
        let userId = facebook.userId;

        try {
            let influencer = await Influencer.findOne(influencerId);
            let [url, err] = await s3Middlewares.uploadUsingFaceBookUserId(userId);

            if (err) {
                console.log(err);
                console.log(facebook, influencer);
            }

            influencer.profilePicture = url;

            await influencer.save();
            console.log(`Saved for ${influencer.name} ${url}`);
        } catch (err) {
            console.log(err);
            console.log(facebook);
        }
    });
});
