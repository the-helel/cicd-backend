const fs = require('fs');
const client = require('./elasticsearch');
// const data = JSON.parse(fs.readFileSync(__dirname + '/2010-3015-users.json')); //need to import with correct path
const data = {};
const config = require('./config');
const es_index_schema = require('./Infulencer.model');

const index = config.ELASTICSEARCH_INDEX;
const type = config.ELASTICSEARCH_TYPE;
const bulk_data = [];

// This methods are for testing purpose Pls try to remove if data is getting populated by some trusted sorce
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomDate(from, to) {
    const fromTime = from.getTime();
    const toTime = to.getTime();
    return new Date(fromTime + Math.random() * (toTime - fromTime));
}

function generateRandomDOB() {
    const random = getRandomDate(new Date('1985-02-12T01:57:45.271Z'), new Date('2010-02-12T01:57:45.271Z'));
    return random.toISOString();
}

// Later we can add followers and get label according to it
function getLabel() {
    const label = ['Celeb', 'Micro Influencer', 'Nano Influencer', 'Macro Influencer'];
    let randomInt = getRandomInt(0, 3);
    return label[randomInt];
}

function convertSymbolToNum(symbolValue) {
    try {
        if (symbolValue) {
            return getRandomInt(0, 10000000);
        }
        let stringValue = symbolValue.slice(-1);
        if (stringValue === 'k' || stringValue === 'K') {
            return +symbolValue.replace(/\k|K$/, '') * 1000;
        } else if (stringValue === 'm' || stringValue === 'M') {
            return +symbolValue.replace(/\m|M$/, '') * 1000000;
        } else if ((stringValue === 'b') | (stringValue === 'B')) {
            return +symbolValue.replace(/b|B$/, '') * 1000000000;
        }
    } catch (error) {
        return getRandomInt(0, 10000000);
    }
}
// End

// Need to omprove code to fetch the data in bulk
function writeBulkDataToEsIndex(es_index, es_type, qurozdatalist) {
    try {
        for (const current in qurozdatalist) {
            let qurozData = {
                id: qurozdatalist[current].id,
                name: qurozdatalist[current].name,
                email: qurozdatalist[current].email || null,
                profile_image: qurozdatalist[current].profile_image,
                city: qurozdatalist[current].location || null,
                gender: qurozdatalist[current].gender || getRandomInt(0, 1) ? 'male' : 'female',
                dob: qurozdatalist[current].dob || generateRandomDOB(),
                famstar_score: qurozdatalist[current].score || getRandomInt(0, 10),
                famstar_verified: qurozdatalist[current].verified || getRandomInt(0, 1) ? true : false,
                categories: qurozdatalist[current].categories,
                created_at: qurozdatalist[current].created_at || generateRandomDOB(),
                updated_at: qurozdatalist[current].updated_at || generateRandomDOB(),
                label: qurozdatalist[current].label || getLabel(),
                phone: qurozdatalist[current].phone || null,
                source: qurozdatalist[current].source || null,
                barter_ready: qurozdatalist[current].barter_ready || null,
                instagram_followers: convertSymbolToNum(qurozdatalist[current].instagram_followers),
                instagram_handle: qurozdatalist[current].instagram_handle || null,
                instagram_profile_link: qurozdatalist[current].instagram_profile_link || null,
                instagram_id: qurozdatalist[current].instagram_id || null,
                instagram_true_reach: convertSymbolToNum(qurozdatalist[current].instagram_true_reach),
                twitter_handle: qurozdatalist[current].twitter_handle,
                twitter_followers: convertSymbolToNum(qurozdatalist[current].twitter_followers),
                twitter_profile_link: qurozdatalist[current].twitter_profile_link,
                youtube_handle: qurozdatalist[current].youtube_handle,
                youtube_subscribers: convertSymbolToNum(qurozdatalist[current].youtube_subscribers),
                youtube_profile_link: qurozdatalist[current].youtube_profile_link,
                facebook_handle: qurozdatalist[current].facebook_handle,
                facebook_page_likes: convertSymbolToNum(qurozdatalist[current].facebook_page_likes),
                facebook_profile_link: qurozdatalist[current].facebook_profile_link,
                spam_status: qurozdatalist[current].spam || getRandomInt(0, 1) ? true : false,
                instagram: {
                    insta_id: qurozdatalist[current].instagram.id,
                    insta_handle: qurozdatalist[current].instagram.username,
                    insta_url: qurozdatalist[current].instagram.profile_link,
                    bio: qurozdatalist[current].instagram.bio,
                    secondary_email: qurozdatalist[current].instagram.secondary_email,
                    post_count: convertSymbolToNum(qurozdatalist[current].instagram.media_count),
                    insta_followers: convertSymbolToNum(qurozdatalist[current].instagram.followers),
                    insta_following: convertSymbolToNum(qurozdatalist[current].instagram.following),
                    insta_engagement_rate: convertSymbolToNum(qurozdatalist[current].instagram.engagement_rate),
                    avg_likes: [{ date: Date.now, value: convertSymbolToNum(qurozdatalist[current].instagram.average_likes) }],
                    avg_comments: [{ date: Date.now, value: convertSymbolToNum(qurozdatalist[current].instagram.average_comments) }],
                    avg_views: [{ date: Date.now, value: convertSymbolToNum(qurozdatalist[current].instagram.average_views) }],
                    insta_real_reach: convertSymbolToNum(qurozdatalist[current].instagram.true_reach),
                    avg_interactions: convertSymbolToNum(qurozdatalist[current].instagram.average_interaction),
                    insta_verified: qurozdatalist[current].instagram.is_verified,
                    promo_posts_count: getRandomInt(0, 1000),
                    promo_avg_likes: getRandomInt(0, 10000000),
                    promo_avg_comments: getRandomInt(0, 100000),
                    business_category_name: null,
                    category_name: null,
                    ffratio:
                        convertSymbolToNum(qurozdatalist[current].instagram.followers) /
                        convertSymbolToNum(qurozdatalist[current].instagram.following),
                    invite_notes: null,
                    costing: {
                        reels: getRandomInt(0, 10000),
                        post: getRandomInt(0, 1000),
                        story: getRandomInt(0, 5000),
                    },
                    top_mentions: qurozdatalist[current].instagram.top_mentions,
                    top_hashtags: qurozdatalist[current].instagram.top_used_hashtags || null,
                    top_keywords: qurozdatalist[current].instagram.top_used_words,
                    recent_collabs: {
                        brand: null,
                        price: null,
                        ratings: null,
                    },
                    audience: {
                        demographics: null,
                        age: null,
                        city: null,
                    },
                    follower_growth_analysis: {
                        followers_growth_last_30days: convertSymbolToNum(
                            qurozdatalist[current].instagram.follower_growth_analysis.followers_growth_last_30days
                        ),
                        followers_gained_last_30days: convertSymbolToNum(
                            qurozdatalist[current].instagram.follower_growth_analysis.followers_gained_last_30days
                        ),
                        followers_growth_series: qurozdatalist[current].instagram.follower_growth_analysis.followers_growth_series,
                        y_axis_min: qurozdatalist[current].instagram.follower_growth_analysis.y_axis_min,
                        success: qurozdatalist[current].instagram.follower_growth_analysis.success,
                    },
                    engagement_stats: {
                        engagement_rate: qurozdatalist[current].instagram.engagement_analysis.engagement_rate,
                        recent_engagements: qurozdatalist[current].instagram.engagement_analysis.recent_engagements,
                        last_30_days: null,
                        recent_engagement_rate: null,
                    },
                    engagement_split: qurozdatalist[current].instagram.engagement_split,
                },
            };

            //     }
            // );
            client
                .index({
                    index: es_index,
                    type: es_type,
                    id: qurozdatalist[current].id,
                    body: qurozData,
                })
                .then((response) => {
                    console.log('Successfully imported one data into elastic ', qurozdatalist[current].id);
                })
                .catch((error) => {
                    console.error('Failed to import bulk data into elastic', error);
                });
        }

        // console.log('Successfully imported bulk data into elastic ', es_index, es_type, bulk_data.length);
        // fs.writeFile('file.json', JSON.stringify(bulk_data), (error) => {
        //     if (error) throw error;
        //   });

        // client
        //     .bulk({
        //         refresh: true,
        //         index: es_index,
        //         type: es_type,
        //         body: bulk_data,
        //     })
        //     .then((response) => {
        //         console.log('Successfully imported bulk data into elastic ', response);
        //     })
        //     .catch((error) => {
        //         console.error('Failed to import bulk data into elastic', error);
        //     });
    } catch (error) {
        console.log('Unexpected error occured while putting bulk data into elk', error);
    }
}

function createESIndexMapping(es_index, es_type) {
    try {
        const InfluencerSchema = es_index_schema; // schema model for influencer
        client.indices
            .putMapping({ index: es_index, type: es_type, include_type_name: true, body: { properties: InfluencerSchema } })
            .then((response) => {
                console.log('Successfully created a mapping for index ', response);
            })
            .catch((error) => {
                console.error('Failed to put mapping for index', error);
            });
    } catch (error) {
        console.error('Unexpected error while creating index map', error);
    }
}

function intializingES() {
    try {
        if (client.indices.exists({ index: index })) {
            client.indices.delete({ index: index });
        }
        // writeBulkDataToEsIndex(index, type, data);
        const es_client_intialize = client.indices
            .create({ index: index })
            .then((response) => {
                createESIndexMapping(index, type);
                writeBulkDataToEsIndex(index, type, data);
            })
            .catch((error) => {
                console.error('Error accured while initializing the elastic client', error);
            });
    } catch (error) {
        console.error('Unexpected error while initializing elatic client', error);
    }
}

module.exports = {
    intializingES,
};
