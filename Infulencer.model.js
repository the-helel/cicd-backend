// const elastic_index_schema = {
//     id: {
//         type: 'text',
//     },
//     username: {
//         type: 'text',
//     },
//     email: {
//         type: 'text',
//     },
//     name: {
//         type: 'keyword',
//         fields: {
//             analyzed: {
//                 type: 'text',
//                 analyzer: 'eventNameAnalyzer',
//                 search_analyzer: 'eventNameAnalyzer',
//             },
//         },
//     },
//     profile_image: {
//         type: 'text',
//     },
//     url: {
//         type: 'text',
//     },
//     location: {
//         type: 'nested',
//         properties: {
//             city: {
//                 ignore_above: 1000,
//                 type: 'keyword',
//             },
//             state: {
//                 ignore_above: 1000,
//                 type: 'keyword',
//             },
//             country: {
//                 ignore_above: 1000,
//                 type: 'keyword',
//             },
//         },
//     },
//     gender: {
//         type: 'keyword',
//     },
//     core_category: {
//         ignore_above: 100,
//         type: 'keyword',
//     },
//     bio: {
//         type: 'text',
//     },
//     category_enum: {
//         type: 'keyword',
//         fields: {
//             analyzed: {
//                 type: 'text',
//                 analyzer: 'eventNameAnalyzer',
//                 search_analyzer: 'eventNameAnalyzer',
//             },
//         },
//     },
//     created_at: {
//         type: 'date',
//         // "format": "yyyy-MM-dd HH:mm:ss"
//     },
//     updated_at: {
//         type: 'date',
//     },
//     label: {
//         ignore_above: 100,
//         type: 'keyword',
//     },
//     famstar_score: {
//         type: 'long',
//     },
//     birthdate: {
//         type: 'text',
//     },
//     languages: {
//         ignore_above: 100,
//         type: 'keyword',
//     },
//     interests: {
//         ignore_above: 1024,
//         type: 'keyword',
//     },
//     personality_type: {
//         type: 'text',
//     },
//     contact_Info: {
//         type: 'nested',
//         properties: {
//             secondary_email: {
//                 type: 'text',
//             },
//         },
//     },
//     instagram: {
//         type: 'nested',
//         properties: {
//             user_id: {
//                 type: 'text',
//             },
//             profile_id: {
//                 type: 'text',
//             },
//             username: {
//                 type: 'text',
//             },
//             profile_link: {
//                 type: 'text',
//             },
//             full_name: {
//                 type: 'text',
//             },
//             is_verified: {
//                 type: 'boolean',
//             },
//             bio: {
//                 type: 'text',
//             },
//             media_count: {
//                 type: 'integer',
//             },
//             followers: {
//                 type: 'integer',
//             },
//             following: {
//                 type: 'integer',
//             },
//             no_of_post: {
//                 type: 'integer',
//             },
//             average_interaction: {
//                 type: 'long',
//             },
//             average_views: {
//                 type: 'long',
//             },
//             total_comments: {
//                 type: 'integer',
//             },
//             total_likes: {
//                 type: 'integer',
//             },
//             day_engagement_rate: {
//                 type: 'long',
//             },
//             top_used_hashtags: {
//                 ignore_above: 100,
//                 type: 'keyword',
//             },
//             engagement_split: {
//                 type: 'long',
//             },
//             engagement_rate: {
//                 type: 'long',
//             },
//         },
//     },
//     follower_growth_analysis: {
//         type: 'nested',
//         properties: {
//             follower_increase: {
//                 type: 'long',
//             },
//             follower_increase_week: {
//                 type: 'long',
//             },
//             follower_increase_month: {
//                 type: 'long',
//             },
//             followers_growth_series: {
//                 type: 'long',
//             },
//         },
//     },
//     engagement_analysis: {
//         type: 'nested',
//         properties: {
//             avg_engagement_day: {
//                 type: 'long',
//             },
//             avg_engagement_week: {
//                 type: 'long',
//             },
//             avg_engagement_month: {
//                 type: 'long',
//             },
//         },
//     },
//     spam_status: {
//         type: 'text',
//     },
// };

// module.exports = elastic_index_schema;

const elastic_index_schema = {
    id: {
        type: 'integer',
    },
    name: {
        type: 'text',
        // fields: {
        //     analyzed: {
        //         type: 'text',
        //         analyzer: 'eventNameAnalyzer',
        //         search_analyzer: 'eventNameAnalyzer',
        //     },
        // },
    },
    email: {
        type: 'text',
    },
    profile_image: {
        type: 'text',
    },
    city: {
        type: 'keyword',
    },
    gender: {
        type: 'keyword',
    },
    dob: {
        type: 'date',
    },
    famstar_score: {
        type: 'long',
    },
    famstar_verified: {
        type: 'boolean',
    },
    categories: {
        type: 'keyword',

        // fields: {
        //     analyzed: {
        //         type: 'text',
        //         analyzer: 'eventNameAnalyzer',
        //         search_analyzer: 'eventNameAnalyzer',
        //     },
        // },
    },
    created_at: {
        type: 'date',
        // "format": "yyyy-MM-dd HH:mm:ss"
    },
    updated_at: {
        type: 'date',
    },
    label: {
        ignore_above: 100,
        type: 'keyword',
    },
    phone: {
        type: 'long',
    },
    source: {
        type: 'text',
    },
    barter_ready: {
        type: 'text',
    },
    instagram_followers: {
        type: 'long',
    },
    instagram_handle: {
        type: 'text',
    },
    instagram_profile_link: {
        type: 'text',
    },
    instagram_id: {
        type: 'text',
    },
    instagram_true_reach: {
        type: 'long',
    },
    twitter_handle: {
        type: 'text',
    },
    twitter_followers: {
        type: 'long',
    },
    twitter_profile_link: {
        type: 'text',
    },
    youtube_handle: {
        type: 'text',
    },
    youtube_subscribers: {
        type: 'long',
    },
    youtube_profile_link: {
        type: 'text',
    },
    facebook_handle: {
        type: 'text',
    },
    facebook_page_likes: {
        type: 'long',
    },
    facebook_profile_link: {
        type: 'text',
    },
    spam_status: {
        type: 'boolean',
    },
    instagram: {
        type: 'object',
        properties: {
            insta_id: {
                type: 'text',
            },
            insta_handle: {
                type: 'keyword',
            },
            insta_url: {
                type: 'text',
            },
            bio: {
                type: 'text',
            },
            secondary_email: {
                type: 'text',
            },
            post_count: {
                type: 'long',
            },
            insta_followers: {
                type: 'long',
            },
            insta_following: {
                type: 'long',
            },
            insta_engagement_rate: {
                type: 'long',
            },
            avg_likes: {
                type: 'nested',
                properties: {
                    date: {
                        type: 'date',
                    },
                    value: {
                        type: 'long',
                    },
                },
            },
            avg_comments: {
                type: 'nested',
                properties: {
                    date: {
                        type: 'date',
                    },
                    value: {
                        type: 'long',
                    },
                },
            },
            avg_views: {
                type: 'nested',
                properties: {
                    date: {
                        type: 'date',
                    },
                    value: {
                        type: 'long',
                    },
                },
            },
            insta_real_reach: {
                type: 'long',
            },
            avg_interactions: {
                type: 'long',
            },
            insta_verified: {
                type: 'integer',
            },
            promo_posts_count: {
                type: 'long',
            },
            promo_avg_likes: {
                type: 'long',
            },
            promo_avg_comments: {
                type: 'long',
            },
            business_category_name: {
                type: 'keyword',
            },
            category_name: {
                type: 'keyword',
            },
            ffratio: {
                type: 'long',
            },
            invite_notes: {
                type: 'text',
            },
            costing: {
                type: 'object',
                properties: {
                    reels: {
                        type: 'long',
                    },
                    post: {
                        type: 'long',
                    },
                    story: {
                        type: 'long',
                    },
                },
            },
            top_mentions: {
                type: 'nested',
                properties: {
                    mentions: {
                        type: 'text',
                        fields: {
                            keyword: {
                                type: 'keyword',
                            },
                        },
                    },
                },
            },
            top_hashtags: {
                type: 'nested',
                properties: {
                    hastags: {
                        type: 'text',
                        fields: {
                            keyword: {
                                type: 'keyword',
                            },
                        },
                    },
                },
            },
            top_keywords: {
                type: 'nested',
                properties: {
                    keywords: {
                        type: 'text',
                        fields: {
                            keyword: {
                                type: 'keyword',
                            },
                        },
                    },
                },
            },
            recent_collabs: {
                type: 'object',
                properties: {
                    brand: {
                        type: 'keyword',
                    },
                    price: {
                        type: 'long',
                    },
                    ratings: {
                        type: 'text',
                    },
                },
            },
            audience: {
                type: 'object',
                properties: {
                    demographics: {
                        type: 'nested',
                    },
                    age: {
                        type: 'nested',
                    },
                    city: {
                        type: 'nested',
                    },
                },
            },
            follower_growth_analysis: {
                type: 'object',
                properties: {
                    followers_growth_last_30days: {
                        type: 'text',
                    },
                    followers_gained_last_30days: {
                        type: 'text',
                    },
                    followers_growth_series: {
                        type: 'nested',
                        properties: {
                            growth: {
                                type: 'text',
                                fields: {
                                    keyword: {
                                        type: 'keyword',
                                    },
                                },
                            },
                        },
                    },
                    y_axis_min: {
                        type: 'text',
                    },
                    success: {
                        type: 'boolean',
                    },
                },
            },
            engagement_stats: {
                type: 'object',
                properties: {
                    engagement_rate: {
                        type: 'long',
                    },
                    recent_engagements: {
                        type: 'nested',
                        properties: {
                            engagements: {
                                type: 'text',
                                fields: {
                                    keyword: {
                                        type: 'keyword',
                                    },
                                },
                            },
                        },
                    },
                    last_30_days: {
                        type: 'long',
                    },
                    recent_engagement_rate: {
                        type: 'nested',
                    },
                },
            },
            engagement_split: {
                type: 'nested',
            },
        },
    },
};

module.exports = elastic_index_schema;
