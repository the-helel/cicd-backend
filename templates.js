const templates = {};

templates.INFLUENCER_WELCOME_EMAIL = (name) => {
    return `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport"/>
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Poppins', sans-serif;
        }
    </style>
      </head>
      <body>
        
        <!-- **************************************************************************************  -->
        <!--  Header Text Welcome  -->
        <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
        <!-- Fam star Logo   -->
        <div
          style="
            color: #000000;
            font-size: 0px;
            font-style: normal;
            font-variant-ligatures: normal;
            font-variant-caps: normal;
            font-weight: 400;
            letter-spacing: normal;
            text-align: center;
            text-indent: 0px;
            text-transform: none;
            white-space: normal;
            word-spacing: 0px;
            background: 0px 0px transparent;
            text-decoration-style: initial;
            text-decoration-color: initial;
            margin: 0px auto;
            max-width: 600px;
          "
        >
          <table
            style="
              border-collapse: collapse;
              background: 0px 0px transparent;
              width: 590.545px;
            "
            cellspacing="0"
            cellpadding="0"
            border="0"
            align="center"
          >
            <tbody>
              <tr>
                <td
                  style="
                    border-collapse: collapse;
                    direction: ltr;
                    font-size: 0px;
                    padding: 0px;
                    text-align: center;
                  "
                >
                  <div
                    style="
                      max-width: 100%;
                      width: 590.545px;
                      font-size: 0px;
                      text-align: left;
                      direction: ltr;
                      display: inline-block;
                      vertical-align: top;
                    "
                  >
                    <table
                      style="border-collapse: collapse"
                      width="100%"
                      cellspacing="0"
                      cellpadding="0"
                      border="0"
                    >
                      <tbody>
                        <tr>
                          <td
                            style="
                              border-collapse: collapse;
                              background-color: transparent;
                              vertical-align: top;
                              padding: 0px;
                            "
                          >
                            <table
                              style="border-collapse: collapse"
                              width="100%"
                              cellspacing="0"
                              cellpadding="0"
                              border="0"
                            >
                              <tbody>
                                <tr>
                                  <td
                                    style="
                                      border-collapse: collapse;
                                      font-size: 0px;
                                      padding: 0px;
                                      word-break: break-word;
                                    "
                                    align="center"
                                  >
                                    <table
                                      style="
                                        border-collapse: collapse;
                                        border-spacing: 0px;
                                      "
                                      cellspacing="0"
                                      cellpadding="0"
                                      border="0"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            style="
                                              border-collapse: collapse;
                                              width: 100px;
                                            "
                                          >
                                            <img
                                              alt="Famstar Logo"
                                              src="https://famstar.s3.ap-south-1.amazonaws.com/logo.png"
                                              style="
                                                border: 0px;
                                                height: auto;
                                                line-height: 13px;
                                                outline: 0px;
                                                text-decoration: none;
                                                border-radius: 0px;
                                                display: block;
                                                width: 100.364px;
                                                font-size: 13px;
                                                margin-top:  10px;
                                              "
                                              width="100"
                                              height="auto"
                                              class="CToWUd"
                                            />
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!--  Famstar Logo End  -->
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px;text-align:center">
                <div style="max-width:100%;width:590.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background-color:transparent;vertical-align:top;padding:0px">
                     <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                         <div style="height:24px">
                          &nbsp;
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                         <div style="height:0px">
                          &nbsp;
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <div style="font-size:30px;letter-spacing:0px;line-height:1.5;text-align:left;color:#000000">
                          <p style="display:block;margin:0px;text-align:center"><b>Welcome to<span>&nbsp;</span></b><span style="color:#393262"><b>Famstar</b> 🚀</p>
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:8px 0px 24px;word-break:break-word" align="left">
                         <div style="font-size:16px;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                          <p style="display:block;margin:0px;text-align:center">Work with your fav brands & get paid instantly with us</p>
    
                         </div></td>
                       </tr>
                      </tbody>
                     </table></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
        <!--  Header Text welcome end  -->
        <!-- ****************************************************************************   -->
        <!--  Welcome Hero Image start  -->
        <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px !important">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:0px 16px;text-align:center">
                <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background-color:transparent;vertical-align:top;padding:0px">
                     <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                         <table style="border-collapse:collapse;border-spacing:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;width:350px"><img alt="Hello from team" src="https://famstar.s3.ap-south-1.amazonaws.com/Center+Image.png" style="border:0px solid transparent;height:auto;line-height:13px;outline:0px;text-decoration:none;border-radius:0px;display:block;width:350.545px;font-size:13px" width="350" height="auto" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 422.734px; top: 853px;"><div id=":b4" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="akn"><div class="aSK J-J5-Ji aYr"></div></div></div></div></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
        <!--  Welcome Hero Image end  -->
        <!-- ************************************************ -->
    
        <!-- Welcome Message Starts here  -->
    
        <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
                <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                     <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                      <p style="display:block;margin:0px"><b>Hi ${name},</b></p>
                      <p style="display:block;margin:0px">&nbsp;</p>
                      <p style="display:block;margin:0px">You have taken the first step towards a smarter way of reaching and collaborating with brands and we are glad that we can be a part of your journey towards your growth in the creator world.</p>
                     </div></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
    
           <!-- Welcome Message end here  -->
           <!-- ****************************************************************** -->
    
           <!--  Help title  -->
    
           <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:16px;text-align:center">
                <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                     <div style="font-size:24px;letter-spacing:0px;line-height:1.5;text-align:left;color:#000000">
                      <p style="display:block;margin:0px;text-align:center">How we will help you!</p>
                     </div></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
    
           <!-- Help title ends -->
           <!-- ****************************************************   -->
    
           <!-- Three info boxes with text starts here  -->
    
           <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:8px 4px;text-align:center">
                <div style="max-width:33%;width:192.239px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background-color:transparent;vertical-align:top;padding:12px 16px">
                     <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <table style="border-collapse:collapse;border-spacing:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;width:163px"><img alt="Alternate image text" src="https://famstar.s3.ap-south-1.amazonaws.com/Instant+access.png" style="border:0;height:100px;line-height:13px;outline:0px;text-decoration:none;border-radius:12px;display:block;width:140px;font-size:13px" width="140px" height="100px" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 135.422px; top: 1212px;"><div id=":b5" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="akn"><div class="aSK J-J5-Ji aYr"></div></div></div></div></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                         <div style="height:16px">
                          &nbsp;
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 4px;word-break:break-word" align="left">
                         <div style="font-size:14px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#000000">
                          <p style="display:block;margin:0px"><b>Instant access to campaigns</b></p>
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 8px;word-break:break-word" align="left">
                         <div style="font-size:13px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                          <p style="display:block;margin:0px">You can choose your favourite brand and apply to the campaigns that you want to work for.</p>
                         </div></td>
                       </tr>
                       <!-- <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <table style="border-collapse:separate;width:auto;line-height:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;border-radius:4px;height:auto;background:#ffffff" valign="middle" bgcolor="#ffffff" align="center"><a href="http://post.spmailtechnolo.com/f/a/ngVwVcui2ZSTPpOu71smRQ~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUk1TldVMk9XSXlOeUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzYVc1MGQyVmhiSFJvTG0xbFpHbDFiUzVqYjIwdlkyOTJaWEpsWkMxaWIyNWtjeTAwWXpkaVlqWXdZemcwTldVaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" style="display:inline-block;background:#ffffff;color:#23b906;font-size:13px;font-weight:400;line-height:1;letter-spacing:1px;margin:0px;text-decoration:none;text-transform:none;padding:8px 24px 8px 0px;border-radius:4px" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/ngVwVcui2ZSTPpOu71smRQ~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUk1TldVMk9XSXlOeUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzYVc1MGQyVmhiSFJvTG0xbFpHbDFiUzVqYjIwdlkyOTJaWEpsWkMxaWIyNWtjeTAwWXpkaVlqWXdZemcwTldVaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218492000&amp;usg=AFQjCNFNBCHYDIBwtRNC1FD1dU-nAQsc_A">Read More</a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr> -->
                      </tbody>
                     </table></td>
                   </tr>
                  </tbody>
                 </table>
                </div>
                <div style="max-width:33%;width:192.239px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background-color:transparent;vertical-align:top;padding:12px 16px">
                     <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <table style="border-collapse:collapse;border-spacing:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;width:163px"><img alt="Alternate image text" src="https://famstar.s3.ap-south-1.amazonaws.com/customer-feedback.png" style="border:0;height:100px;line-height:13px;outline:0px;text-decoration:none;border-radius:12px;display:block;width:140px;font-size:13px" width="140px" height="100px" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 327.469px; top: 1213px;"><div id=":b6" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="akn"><div class="aSK J-J5-Ji aYr"></div></div></div></div></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                         <div style="height:16px">
                          &nbsp;
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 4px;word-break:break-word" align="left">
                         <div style="font-size:14px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#000000">
                          <p style="display:block;margin:0px"><b>Freedom to Creative pitch to brand</b></p>
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 8px;word-break:break-word" align="left">
                         <div style="font-size:13px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                          <p style="display:block;margin:0px">Being a creator, you have endless ideas  and we give you the freedom to create for brands your own way.</p>
                         </div></td>
                       </tr>
                       <!-- <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <table style="border-collapse:separate;width:auto;line-height:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;border-radius:4px;height:auto;background:#ffffff" valign="middle" bgcolor="#ffffff" align="center"><a href="http://post.spmailtechnolo.com/f/a/pnY7P9vQr4HYx8ccvz4d0A~~/AAQNhwA~/RgRimhYYP4SQAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUolMkJOMlEwTURFd01EUWlMQ0owYjFWU1RDSTZJbWgwZEhCek9pOHZlVzkxZEhVdVltVXZObU5GVDA5dE1ITjBVMk1pZlElM0QlM0RXA3NwY0IKYK4Ykbdgkoy-hlIXZ2F1cmF2c2FpbmkwM0BnbWFpbC5jb21YBAAAAMw~" style="display:inline-block;background:#ffffff;color:#23b906;font-size:14px;font-weight:400;line-height:1;letter-spacing:1px;margin:0px;text-decoration:none;text-transform:none;padding:8px 24px 8px 0px;border-radius:4px" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/pnY7P9vQr4HYx8ccvz4d0A~~/AAQNhwA~/RgRimhYYP4SQAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUolMkJOMlEwTURFd01EUWlMQ0owYjFWU1RDSTZJbWgwZEhCek9pOHZlVzkxZEhVdVltVXZObU5GVDA5dE1ITjBVMk1pZlElM0QlM0RXA3NwY0IKYK4Ykbdgkoy-hlIXZ2F1cmF2c2FpbmkwM0BnbWFpbC5jb21YBAAAAMw~&amp;source=gmail&amp;ust=1623318218492000&amp;usg=AFQjCNGZ7L6C2An0mWFI3MBlQ6xeA7tukw">Play Video</a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr> -->
                      </tbody>
                     </table></td>
                   </tr>
                  </tbody>
                 </table>
                </div>
                <div style="max-width:33%;width:192.239px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background-color:transparent;vertical-align:top;padding:12px 16px">
                     <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <table style="border-collapse:collapse;border-spacing:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;width:163px"><img alt="Alternate image text" src="https://famstar.s3.ap-south-1.amazonaws.com/cashless-payment.png" style="border:0;height:100px;line-height:13px;outline:0px;text-decoration:none;border-radius:12px;display:block;width:140px;font-size:13px" width="140px" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 519.516px; top: 1213px;"><div id=":b8" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="akn"><div class="aSK J-J5-Ji aYr"></div></div></div></div></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                         <div style="height:16px">
                          &nbsp;
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 4px;word-break:break-word" align="left">
                         <div style="font-size:14px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#000000">
                          <p style="display:block;margin:0px"><b>Get paid in 24 hours</b></p>
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 8px;word-break:break-word" align="left">
                         <div style="font-size:13px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                          <p style="display:block;margin:0px">Your hard work should always be paid on time and this is what we make sure of.</p>
                         </div></td>
                       </tr>
                       <!-- <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <table style="border-collapse:separate;width:auto;line-height:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;border-radius:4px;height:auto;background:#ffffff" valign="middle" bgcolor="#ffffff" align="center"><a href="http://post.spmailtechnolo.com/f/a/-4UY_SxYJqYtBOfuPG3pFw~~/AAQNhwA~/RgRimhYYP4TQAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzTURrek1ERTJaQ0lzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzYVc1MGQyVmhiSFJvTG0xbFpHbDFiUzVqYjIwdmFHOTNMWFJ2TFdsdWRtVnpkQzFwYmkxM2FXNTBMWGRsWVd4MGFITXRjSEp2WkhWamRITXRaVEpsWVdZM1pqWTVOR0UxSW4wJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" style="display:inline-block;background:#ffffff;color:#23b906;font-size:14px;font-weight:400;line-height:1;letter-spacing:1px;margin:0px;text-decoration:none;text-transform:none;padding:8px 24px 8px 0px;border-radius:4px" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/-4UY_SxYJqYtBOfuPG3pFw~~/AAQNhwA~/RgRimhYYP4TQAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzTURrek1ERTJaQ0lzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzYVc1MGQyVmhiSFJvTG0xbFpHbDFiUzVqYjIwdmFHOTNMWFJ2TFdsdWRtVnpkQzFwYmkxM2FXNTBMWGRsWVd4MGFITXRjSEp2WkhWamRITXRaVEpsWVdZM1pqWTVOR0UxSW4wJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218492000&amp;usg=AFQjCNES6YXNEPUcnnWnrKHA02_DWgLLuA">Read More</a></td>
                           </tr> -->
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
    
           <!-- Three info boxes with text ends here  -->
           <!-- **************************************************************  -->
    
           <!-- Header Text -->
           <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
                <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                     <div style="font-size:24px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#000000">
                      <p style="display:block;margin:0px;text-align:center"><b>Are you ready to experience a new way of doing influencer Marketing?</b></p>
                     </div></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
           <!-- Header Text ends  -->
           <!-- ************************************************************** -->
    
           <!-- Info about us starts -->
           <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 0px 0px 24px;text-align:center">
                <div style="max-width:50%;width:283.273px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background-color:transparent;vertical-align:top;padding:0px">
                     <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                         <div style="height:24px">
                          &nbsp;
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 10px 5px;word-break:break-word" align="left">
                         <div style="font-size:20px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#393262">
                          <p style="display:block;margin:0px"><b>Few Highlights:</b></p>
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 24px 8px 0px;word-break:break-word" align="left">
                         <div style="font-size:16px;letter-spacing:0px;line-height:1.6;text-align:left;color:#444444">
                          <ul>
                           <li style="text-align:left;color:#393262">Exclusive Android & iOS app</li>
                           <li style="text-align:left;color:#393262">Payment in 24 hours</li>
                           <li style="text-align:left;color:#393262">Specially designed for Indian Creators</li>
                          </ul>
                         </div></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:12px 0px 12px 10px;word-break:break-word" align="left">
                         <table style="border-collapse:separate;width:auto;line-height:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;border-radius:4px;height:auto;background:#FF8F99" valign="middle" bgcolor="#000000" align="center"><a href="https://famstar.page.link/welcome" style="display:inline-block;background:#FF8F99;color:#ffffff;font-size:16px;font-weight:400;line-height:1;letter-spacing:1px;margin:0px;text-decoration:none;text-transform:none;padding:12px 24px;border-radius:4px" rel="noopener" target="_blank" data-saferedirecturl="https://famstar.page.link/welcome"><b>Open App</b></a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                         <table style="border-collapse:separate;width:auto;line-height:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;border-radius:4px;height:auto;background:#ffffff" valign="middle" bgcolor="#ffffff" align="center"><p style="display:inline-block;margin:0px;background:#ffffff;color:#FF8F99;font-size:16px;font-weight:400;line-height:1;letter-spacing:1px;text-decoration:none;text-transform:none;padding:8px 16px 8px 0px;border-radius:4px"><br></p></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table></td>
                   </tr>
                  </tbody>
                 </table>
                </div>
                <div style="max-width:49%;width:277.602px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background-color:transparent;vertical-align:top;padding:0px">
                     <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;font-size:0px;padding:0px 24px 0px 0px;word-break:break-word" align="center">
                         <table style="border-collapse:collapse;border-spacing:0px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;width:258px"><img alt="Alternate image text" src="https://famstar.s3.ap-south-1.amazonaws.com/Hands+-+Camera.png" style="border:0px;line-height:13px;outline:0px;text-decoration:none;border-radius:12px;display:block;font-size:13px" width="230px" height="200px" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 515.328px; top: 1722px;"><div id=":b7" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="akn"><div class="aSK J-J5-Ji aYr"></div></div></div></div></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
        <!-- Info About us ends -->
        <!-- ************************************** -->
    
        <!-- Contact info starts -->
        <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#f8faf6;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#f8faf6;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px 16px 0px;text-align:center">
                <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px 16px 5px;word-break:break-word" align="left">
                     <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                      <p style="display:block;margin:0px">In case you have any queries or feedback, please write to us at <a href="mailto:hello@famstar.in" style="color: #393262;">hello@famstar.in</a></p>
    
                      <p style="display:block;margin:0px">&nbsp;</p>
                      <p style="display:block;margin:0px"><span style="color:#1b2631">We read every email :)</span></p>
                     </div></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
           <!-- contact info ends -->
           <!-- ******************************************* -->
           
           <!-- End text starts  -->
           <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
                <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word" align="left">
                     <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                      <p style="display:block;margin:0px"><span style="color:#1b2631">Happy Influencing,</span></p>
                      <p style="display:block;margin:0px"><span style="color:#393262"><b>Team Famstar.</b></span></p>
                     </div></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
           <!-- End text ends -->
           <!-- *************************************************** -->
           <!-- Social Icons starts here -->
           <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
                <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                     <div style="height:12px">
                      &nbsp;
                     </div></td>
                   </tr>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                     <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                         <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://twitter.com/famstarhq" rel="noopener" target="_blank" data-saferedirecturl="https://twitter.com/famstarhq"><img alt="twitter" src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table>
                     <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                         <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.linkedin.com/company/famstar/" rel="noopener" target="_blank" data-saferedirecturl="https://www.linkedin.com/company/famstar/"><img alt="linkedin" src="https://ci5.googleusercontent.com/proxy/D79Rrzm2yqxpbyaq9obVZRGTSzhvMEgIqLu463HdX9OIutpMOcEoE78JXfpHrslgXMmE87Bv-jTJrIo9ill_sj1nW9WkHBIMjjRjvqcWktcmvBrFpFYwXINwpJvLz6jRYtK35wLfZfrjPsNNeSJyyQKMd6yFX6T9pUxJYm43Sst3-cy0O04F-GCymI1Fqk0KSKc8tLm0yw=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845188311_linkedin.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table> -->
                     <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                         <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218493000&amp;usg=AFQjCNETS6jnN8bYjJe-ymkGNd_Ab1fP_w"><img alt="youtube" src="https://ci4.googleusercontent.com/proxy/W1l9kK-GAECVakgi6Jz4Nxb78e7vl33uaFoSVjDxD_wjLUl3uzCd8_wzD-4AR2AV5Urcvc9lHix0ZhmYwW-Fdph4vKhj49Jqz4cCcfdgIDUJgXJusc20b-X1vQ87TjOT60KwRsJXSuO62ja5H20KCLfnKRcBk_wFZLfqYWyrowYELbPGzl2gwriCHBYqzyNXkmlu3Owb=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845199657_youtube.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table> -->
                     <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                         <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.facebook.com/famstarHQ" rel="noopener" target="_blank" data-saferedirecturl="https://www.facebook.com/famstarHQ"><img alt="facebook" src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table>
                     <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                       <tr>
                        <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                         <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                           <tr>
                            <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.instagram.com/famstarhq/" rel="noopener" target="_blank" data-saferedirecturl="https://www.instagram.com/famstarhq/"><img alt="instagram" src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table></td>
                   </tr>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word" align="left">
                     <div style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                      <p style="display:block;margin-top:10px;text-align:center">You are receiving this mail because you are a member at Famstar</p>
                     </div></td>
                   </tr>
                   <tr>
                    <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                     <div style="height:8px">
                      &nbsp;
                     </div></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
           <!-- Social icons ends here -->
           <!-- ********************************************************* -->
           <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
            <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
             <tbody>
              <tr>
               <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
                <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
                 <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                   <tr>
                    <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                     <div style="height:2px">
                      &nbsp;
                     </div></td>
                   </tr>
                   <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                     <div style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                      <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span style="color:#84919e;font-size:12px"><i>© 2021</i></span><span style="font-size:12px"><i><span>&nbsp;</span></i></span><span style="color:#888888;font-size:12px">Famstar.in</span></p>
                      <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:10px"><i>Made with ❤️ in India</i></span></p>
                     </div></td>
                   </tr>
                  </tbody>
                 </table>
                </div></td>
              </tr>
             </tbody>
            </table>
           </div>
      </body>
    </html>
    
    `;
};

templates.INFLUENCER_PWD_RESET = (name, token) => {
    return `
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <style>
          .button {
            background-color: #FF8F99;
            border-top: 10px solid #FF8F99;
            border-right: 18px solid #FF8F99;
            border-bottom: 10px solid #FF8F99;
            border-left: 18px solid #FF8F99;
            display: inline-block;
            color: #fff;
            text-decoration: none;
            border-radius: 3px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
            -webkit-text-size-adjust: none;
            box-sizing: border-box;
            font-size: 1.3rem;
          }
          a {
              color: white;
          }
          a:visited {
              color: white;
          }
      </style>
  </head>
  <body>
      
      <!-- Reset Text -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
      <div
      style="
        color: #000000;
        font-size: 0px;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-weight: 400;
        letter-spacing: normal;
        text-align: center;
        text-indent: 0px;
        text-transform: none;
        white-space: normal;
        word-spacing: 0px;
        background: 0px 0px transparent;
        text-decoration-style: initial;
        text-decoration-color: initial;
        margin: 0px auto;
        max-width: 600px;
      "
    >
      <table
        style="
          border-collapse: collapse;
          background: 0px 0px transparent;
          width: 590.545px;
        "
        cellspacing="0"
        cellpadding="0"
        border="0"
        align="center"
      >
        <tbody>
          <tr>
            <td
              style="
                border-collapse: collapse;
                direction: ltr;
                font-size: 0px;
                padding: 0px;
                text-align: center;
              "
            >
              <div
                style="
                  max-width: 100%;
                  width: 590.545px;
                  font-size: 0px;
                  text-align: left;
                  direction: ltr;
                  display: inline-block;
                  vertical-align: top;
                "
              >
                <table
                  style="border-collapse: collapse"
                  width="100%"
                  cellspacing="0"
                  cellpadding="0"
                  border="0"
                >
                  <tbody>
                    <tr>
                      <td
                        style="
                          border-collapse: collapse;
                          background-color: transparent;
                          vertical-align: top;
                          padding: 0px;
                        "
                      >
                        <table
                          style="border-collapse: collapse"
                          width="100%"
                          cellspacing="0"
                          cellpadding="0"
                          border="0"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="
                                  border-collapse: collapse;
                                  font-size: 0px;
                                  padding: 0px;
                                  word-break: break-word;
                                "
                                align="center"
                              >
                                <table
                                  style="
                                    border-collapse: collapse;
                                    border-spacing: 0px;
                                  "
                                  cellspacing="0"
                                  cellpadding="0"
                                  border="0"
                                >
                                  <tbody>
                                    <tr>
                                      <td
                                        style="
                                          border-collapse: collapse;
                                          width: 100px;
                                        "
                                      >
                                        <img
                                          alt="Famstar Logo"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/logo.png"
                                          style="
                                            border: 0px;
                                            height: auto;
                                            line-height: 13px;
                                            outline: 0px;
                                            text-decoration: none;
                                            border-radius: 0px;
                                            display: block;
                                            width: 100.364px;
                                            font-size: 13px;
                                            margin-top:  10px;
                                          "
                                          width="100"
                                          height="auto"
                                          class="CToWUd"
                                        />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><h2>Hi ${name},</h2></p>
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px">You are receiving this email because we received a password reset request for your account. Click on button below to reset your password</p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
      <!-- *************** Reset Text Ends -->
  
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:24px;letter-spacing:0px;line-height:1.5;text-align:left;color:#000000">
                    <p style="display:block;margin:0px;text-align:center"><a href="https://api.famstar.in/influencer/auth/email/reset-password?token=${token}" class="button">
                      Reset Password
                  </a></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
         <!-- Button Ends -->
  
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px">This link is valid for next 1 hour. Unable to click the button above? paste this link in your browser <br/>
                      <a href="https://api.famstar.in/influencer/auth/email/reset-password?token=${token}" style="color: blue;text-decoration: underline;">https://api.famstar.in/influencer/auth/email/reset-password?token=${token}</a></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Contact info starts -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#f8faf6;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#f8faf6;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px 16px 0px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 16px 5px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px">In case you have any queries or feedback, please write to us at <a href="mailto:hello@famstar.in" style="color: #393262;">hello@famstar.in</a></p>
  
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px"><span style="color:#1b2631">We read every email :)</span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- contact info ends -->
         <!-- ******************************************* -->
         
         <!-- End text starts  -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><span style="color:#1b2631">Happy Influencing,</span></p>
                    <p style="display:block;margin:0px"><span style="color:#393262"><b>Team Famstar.</b></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- End text ends -->
         <!-- *************************************************** -->
         <!-- Social Icons starts here -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:12px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://twitter.com/famstarhq" rel="noopener" target="_blank" data-saferedirecturl="https://twitter.com/famstarhq"><img alt="twitter" src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.linkedin.com/company/famstar/" rel="noopener" target="_blank" data-saferedirecturl="https://www.linkedin.com/company/famstar/"><img alt="linkedin" src="https://ci5.googleusercontent.com/proxy/D79Rrzm2yqxpbyaq9obVZRGTSzhvMEgIqLu463HdX9OIutpMOcEoE78JXfpHrslgXMmE87Bv-jTJrIo9ill_sj1nW9WkHBIMjjRjvqcWktcmvBrFpFYwXINwpJvLz6jRYtK35wLfZfrjPsNNeSJyyQKMd6yFX6T9pUxJYm43Sst3-cy0O04F-GCymI1Fqk0KSKc8tLm0yw=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845188311_linkedin.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218493000&amp;usg=AFQjCNETS6jnN8bYjJe-ymkGNd_Ab1fP_w"><img alt="youtube" src="https://ci4.googleusercontent.com/proxy/W1l9kK-GAECVakgi6Jz4Nxb78e7vl33uaFoSVjDxD_wjLUl3uzCd8_wzD-4AR2AV5Urcvc9lHix0ZhmYwW-Fdph4vKhj49Jqz4cCcfdgIDUJgXJusc20b-X1vQ87TjOT60KwRsJXSuO62ja5H20KCLfnKRcBk_wFZLfqYWyrowYELbPGzl2gwriCHBYqzyNXkmlu3Owb=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845199657_youtube.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.facebook.com/famstarHQ" rel="noopener" target="_blank" data-saferedirecturl="https://www.facebook.com/famstarHQ"><img alt="facebook" src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.instagram.com/famstarhq/" rel="noopener" target="_blank" data-saferedirecturl="https://www.instagram.com/famstarhq/"><img alt="instagram" src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word" align="left">
                   <div style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                    <p style="display:block;margin-top:10px;text-align:center">You are receiving this mail because you are a member at Famstar</p>
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:8px">
                    &nbsp;
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Social icons ends here -->
         <!-- ********************************************************* -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:2px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span style="color:#84919e;font-size:12px"><i>© 2021</i></span><span style="font-size:12px"><i><span>&nbsp;</span></i></span><span style="color:#888888;font-size:12px">Famstar.in</span></p>
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:10px"><i>Made with ❤️ in India</i></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
  </body>
  </html>
  
  `;
};

templates.EMAIL_VERIFICATION = (name, url) => {
    return `
  <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Verify Email | Famstar</title>
  <style>
    div, p {
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 1.6;
    }

    .button {
      background-color: #ffc344;
      display: inline-block;
      padding: 5px 20px;
      color: #fff;
      text-decoration: none;
      border-radius: 3px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
      -webkit-text-size-adjust: none;
      box-sizing: border-box;
    }

    a {
      color: white;
    }

    a:visited {
      color: white;
    }

    .card-body {
      border: 1px solid #ccc;
      border-radius: 3px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
      -webkit-text-size-adjust: none;
      box-sizing: border-box;
    }

  </style>
</head>

<body >

  <!-- Reset Text -->
  <div 
    style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
    <div style="
        color: #000000;
        font-size: 0px;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-weight: 400;
        letter-spacing: normal;
        text-align: center;
        text-indent: 0px;
        text-transform: none;
        white-space: normal;
        word-spacing: 0px;
        background: 0px 0px transparent;
        text-decoration-style: initial;
        text-decoration-color: initial;
        margin: 0px auto;
        max-width: 600px;
      ">
      <table style="
          border-collapse: collapse;
          background: 0px 0px transparent;
          width: 590.545px;
        " cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
          <tr>
            <td style="
                border-collapse: collapse;
                direction: ltr;
                font-size: 0px;
                padding: 0px;
                text-align: center;
              ">
              <div style="
                  max-width: 100%;
                  width: 590.545px;
                  font-size: 0px;
                  text-align: left;
                  direction: ltr;
                  display: inline-block;
                  vertical-align: top;
                ">
                <table style="border-collapse: collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                    <tr>
                      <td style="
                          border-collapse: collapse;
                          background-color: transparent;
                          vertical-align: top;
                          padding: 0px;
                        ">
                        <table style="border-collapse: collapse" width="100%" cellspacing="0" cellpadding="0"
                          border="0">
                          <tbody>
                            <tr>
                              <td style="
                                  border-collapse: collapse;
                                  font-size: 0px;
                                  padding: 0px;
                                  word-break: break-word;
                                " align="center">
                                <table style="
                                    border-collapse: collapse;
                                    border-spacing: 0px;
                                  " cellspacing="0" cellpadding="0" border="0">
                                  <tbody>
                                    <tr>
                                      <td style="
                                          border-collapse: collapse;
                                          width: 100px;
                                        ">
                                        <img alt="Famstar Logo"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/famstar_full_logo.png" style="
                                            border: 0px;
                                            height: auto;
                                            line-height: 13px;
                                            outline: 0px;
                                            text-decoration: none;
                                            border-radius: 0px;
                                            display: block;
                                            width: 100.364px;
                                            font-size: 13px;
                                            margin-top:  10px;
                                          " width="100" height="auto" class="CToWUd" />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
      border="0" align="center">
      <tbody>
        <tr>
          <td
            style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:0px;text-align:center">
            <div
              style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
              <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%"
                cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word"
                      align="left">
                      <div
                        style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                        <p style="display:block;margin:0px">
                        <h2>Hey ${name}!</h2>
                        </p>
                        <p style="display:block;margin:0px">&nbsp;</p>
                        <p style="display:block;margin:0px">Welcome to Famstar. To activate your account please click
                          the button and verify your email address.</p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <!-- *************** Reset Text Ends -->

  <div
    style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
    <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
      border="0" align="center">
      <tbody>
        <tr>
          <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:16px;text-align:center">
            <div
              style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
              <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%"
                cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                      <div style="font-size:18px;letter-spacing:1px;line-height:1.5;text-align:left;color:#000000;font-family: Arial, Helvetica, sans-serif;">
                        <p style="display:block;margin:0px;text-align:center"><a
                            href=\`${url}\` class="button">
                            Activate Your Account
                          </a></p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <!-- Button Ends -->

  <div
    style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
    <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
      border="0" align="center">
      <tbody>
        <tr>
          <td
            style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
            <div
              style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
              <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%"
                cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word"
                      align="left">
                      <div
                        style="font-size:16px;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                        <p style="display:block;margin:0px">
                          Verification link is valid for 1 hour.</p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- Contact info starts -->
  <div
    style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#f8faf6;margin:0px auto;max-width:600px">
    <table style="border-collapse:collapse;background:#f8faf6;width:590.545px" cellspacing="0" cellpadding="0"
      border="0" align="center">
      <tbody>
        <tr>
          <td
            style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px 16px 0px;text-align:center">
            <div
              style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
              <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%"
                cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px 16px 5px;word-break:break-word"
                      align="left">
                      <div
                        style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                        <p style="display:block;margin:0px">In case you have any queries or feedback, please write to us
                          at <a href="mailto:hello@famstar.in" style="color: #393262;">hello@famstar.in</a></p>

                        <p style="display:block;margin:0px">&nbsp;</p>
                        <p style="display:block;margin:0px"><span style="color:#1b2631">We read every email :)</span>
                        </p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- contact info ends -->
  <!-- ******************************************* -->

  <!-- End text starts  -->
  <div
    style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
    <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
      border="0" align="center">
      <tbody>
        <tr>
          <td
            style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
            <div
              style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
              <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%"
                cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word"
                      align="left">
                      <div
                        style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                        <p style="display:block;margin:0px"><span style="color:#1b2631">Happy Influencing,</span></p>
                        <p style="display:block;margin:0px"><span style="color:#393262"><b>Team Famstar.</b></span></p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- End text ends -->
  <!-- *************************************************** -->
  <!-- Social Icons starts here -->
  <div
    style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
    <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0"
      border="0" align="center">
      <tbody>
        <tr>
          <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
            <div
              style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
              <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%"
                cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                      <div style="height:12px">
                        &nbsp;
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                      <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0"
                        cellpadding="0" border="0" align="center">
                        <tbody>
                          <tr>
                            <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                              <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0"
                                cellpadding="0" border="0">
                                <tbody>
                                  <tr>
                                    <td
                                      style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px">
                                      <a href="https://twitter.com/famstarhq" rel="noopener" target="_blank"
                                        data-saferedirecturl="https://twitter.com/famstarhq"><img alt="twitter"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png"
                                          style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block"
                                          width="24" height="24" class="CToWUd"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0"
                        cellpadding="0" border="0" align="center">
                        <tbody>
                          <tr>
                            <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                              <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0"
                                cellpadding="0" border="0">
                                <tbody>
                                  <tr>
                                    <td
                                      style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px">
                                      <a href="https://www.facebook.com/famstarHQ" rel="noopener" target="_blank"
                                        data-saferedirecturl="https://www.facebook.com/famstarHQ"><img alt="facebook"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png"
                                          style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block"
                                          width="24" height="24" class="CToWUd"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0"
                        cellpadding="0" border="0" align="center">
                        <tbody>
                          <tr>
                            <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                              <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0"
                                cellpadding="0" border="0">
                                <tbody>
                                  <tr>
                                    <td
                                      style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px">
                                      <a href="https://www.instagram.com/famstarhq/" rel="noopener" target="_blank"
                                        data-saferedirecturl="https://www.instagram.com/famstarhq/"><img alt="instagram"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png"
                                          style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block"
                                          width="24" height="24" class="CToWUd"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word"
                      align="left">
                      <div style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                        <p style="display:block;margin-top:10px;text-align:center">You are receiving this mail because
                          you are a member at Famstar</p>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                      <div style="height:8px">
                        &nbsp;
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- Social icons ends here -->
  <!-- ********************************************************* -->
  <div
    style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
    <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0"
      cellpadding="0" border="0" align="center">
      <tbody>
        <tr>
          <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
            <div
              style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
              <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%"
                cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                      <div style="height:2px">
                        &nbsp;
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                      <div style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                        <p style="display:block;margin:0px;text-align:center"><span
                            style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span
                            style="color:#84919e;font-size:12px"><i>© 2021</i></span><span
                            style="font-size:12px"><i><span>&nbsp;</span></i></span><span
                            style="color:#888888;font-size:12px">Famstar.in</span></p>
                        <p style="display:block;margin:0px;text-align:center"><span
                            style="color:#888888;font-size:10px"><i>Made with ❤️ in India</i></span></p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>

</html>`;
};

templates.EMAIL_PASSWORD_RESET = (name, url) => {
    return `
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <style>
          div, p {
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 1.6;
    }

          .button {
            background-color: #ffc344;
            padding: 5px 12px;
            display: inline-block;
            color: #fff;
            text-decoration: none;
            border-radius: 8px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
            -webkit-text-size-adjust: none;
            box-sizing: border-box;
          }
          a {
              color: white;
          }
          a:visited {
              color: white;
          }
      </style>
  </head>
  <body>
      
      <!-- Reset Text -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
      <div
      style="
        color: #000000;
        font-size: 0px;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-weight: 400;
        letter-spacing: normal;
        text-align: center;
        text-indent: 0px;
        text-transform: none;
        white-space: normal;
        word-spacing: 0px;
        background: 0px 0px transparent;
        text-decoration-style: initial;
        text-decoration-color: initial;
        margin: 0px auto;
        max-width: 600px;
      "
    >
      <table
        style="
          border-collapse: collapse;
          background: 0px 0px transparent;
          width: 590.545px;
        "
        cellspacing="0"
        cellpadding="0"
        border="0"
        align="center"
      >
        <tbody>
          <tr>
            <td
              style="
                border-collapse: collapse;
                direction: ltr;
                font-size: 0px;
                padding: 0px;
                text-align: center;
              "
            >
              <div
                style="
                  max-width: 100%;
                  width: 590.545px;
                  font-size: 0px;
                  text-align: left;
                  direction: ltr;
                  display: inline-block;
                  vertical-align: top;
                "
              >
                <table
                  style="border-collapse: collapse"
                  width="100%"
                  cellspacing="0"
                  cellpadding="0"
                  border="0"
                >
                  <tbody>
                    <tr>
                      <td
                        style="
                          border-collapse: collapse;
                          background-color: transparent;
                          vertical-align: top;
                          padding: 0px;
                        "
                      >
                        <table
                          style="border-collapse: collapse"
                          width="100%"
                          cellspacing="0"
                          cellpadding="0"
                          border="0"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="
                                  border-collapse: collapse;
                                  font-size: 0px;
                                  padding: 0px;
                                  word-break: break-word;
                                "
                                align="center"
                              >
                                <table
                                  style="
                                    border-collapse: collapse;
                                    border-spacing: 0px;
                                  "
                                  cellspacing="0"
                                  cellpadding="0"
                                  border="0"
                                >
                                  <tbody>
                                    <tr>
                                      <td
                                        style="
                                          border-collapse: collapse;
                                          width: 100px;
                                        "
                                      >
                                        <img
                                          alt="Famstar Logo"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/famstar_full_logo.png"
                                          style="
                                            border: 0px;
                                            height: 120px;
                                            width: auto;
                                            line-height: 13px;
                                            outline: 0px;
                                            text-decoration: none;
                                            border-radius: 0px;
                                            display: block;
                                            font-size: 13px;
                                            margin-top:  10px;
                                          "
                                          height="auto"
                                          class="CToWUd"
                                        />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><h2>Hi ${name},</h2></p>
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px">You are receiving this email because we received a password reset request for your account. Click on button below to reset your password</p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
      <!-- *************** Reset Text Ends -->
  
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:14px;letter-spacing:0px;line-height:1.5;text-align:left;color:#000000">
                    <p style="display:block;margin:0px;text-align:center"><a href="${url}" class="button">
                      Reset Password
                  </a></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
         <!-- Button Ends -->
  
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px">This link is valid for few minutes only.</p><br/>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Contact info starts -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#f8faf6;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#f8faf6;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px 16px 0px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 16px 5px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px">In case you have any queries or feedback, please write to us at <a href="mailto:hello@famstar.in" style="color: #393262;">hello@famstar.in</a></p>
  
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px"><span style="color:#1b2631">We read every email :)</span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- contact info ends -->
         <!-- ******************************************* -->
         
         <!-- End text starts  -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><span style="color:#1b2631">Happy Influencing,</span></p>
                    <p style="display:block;margin:0px"><span style="color:#393262"><b>Team Famstar.</b></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- End text ends -->
         <!-- *************************************************** -->
         <!-- Social Icons starts here -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:12px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://twitter.com/famstarhq" rel="noopener" target="_blank" data-saferedirecturl="https://twitter.com/famstarhq"><img alt="twitter" src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.facebook.com/famstarHQ" rel="noopener" target="_blank" data-saferedirecturl="https://www.facebook.com/famstarHQ"><img alt="facebook" src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.instagram.com/famstarhq/" rel="noopener" target="_blank" data-saferedirecturl="https://www.instagram.com/famstarhq/"><img alt="instagram" src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word" align="left">
                   <div style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                    <p style="display:block;margin-top:10px;text-align:center">You are receiving this mail because you are a member at Famstar</p>
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:8px">
                    &nbsp;
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Social icons ends here -->
         <!-- ********************************************************* -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:2px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span style="color:#84919e;font-size:12px"><i>© 2021</i></span><span style="font-size:12px"><i><span>&nbsp;</span></i></span><span style="color:#888888;font-size:12px">Famstar.in</span></p>
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:10px"><i>Made with ❤️ in India</i></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
  </body>
  </html>
  `;
};

templates.INFLUENCER_EMAIL_VERIFICATION_MAIL = (name, token, host) => {
    return `
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Verify Email | Famstar</title>
      <style>
          .button {
            background-color: #FF8F99;
            border-top: 10px solid #FF8F99;
            border-right: 18px solid #FF8F99;
            border-bottom: 10px solid #FF8F99;
            border-left: 18px solid #FF8F99;
            display: inline-block;
            color: #fff;
            text-decoration: none;
            border-radius: 3px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
            -webkit-text-size-adjust: none;
            box-sizing: border-box;
          }
          a {
              color: white;
          }
          a:visited {
              color: white;
          }
      </style>
  </head>
  <body>
      
      <!-- Reset Text -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
      <div
      style="
        color: #000000;
        font-size: 0px;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-weight: 400;
        letter-spacing: normal;
        text-align: center;
        text-indent: 0px;
        text-transform: none;
        white-space: normal;
        word-spacing: 0px;
        background: 0px 0px transparent;
        text-decoration-style: initial;
        text-decoration-color: initial;
        margin: 0px auto;
        max-width: 600px;
      "
    >
      <table
        style="
          border-collapse: collapse;
          background: 0px 0px transparent;
          width: 590.545px;
        "
        cellspacing="0"
        cellpadding="0"
        border="0"
        align="center"
      >
        <tbody>
          <tr>
            <td
              style="
                border-collapse: collapse;
                direction: ltr;
                font-size: 0px;
                padding: 0px;
                text-align: center;
              "
            >
              <div
                style="
                  max-width: 100%;
                  width: 590.545px;
                  font-size: 0px;
                  text-align: left;
                  direction: ltr;
                  display: inline-block;
                  vertical-align: top;
                "
              >
                <table
                  style="border-collapse: collapse"
                  width="100%"
                  cellspacing="0"
                  cellpadding="0"
                  border="0"
                >
                  <tbody>
                    <tr>
                      <td
                        style="
                          border-collapse: collapse;
                          background-color: transparent;
                          vertical-align: top;
                          padding: 0px;
                        "
                      >
                        <table
                          style="border-collapse: collapse"
                          width="100%"
                          cellspacing="0"
                          cellpadding="0"
                          border="0"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="
                                  border-collapse: collapse;
                                  font-size: 0px;
                                  padding: 0px;
                                  word-break: break-word;
                                "
                                align="center"
                              >
                                <table
                                  style="
                                    border-collapse: collapse;
                                    border-spacing: 0px;
                                  "
                                  cellspacing="0"
                                  cellpadding="0"
                                  border="0"
                                >
                                  <tbody>
                                    <tr>
                                      <td
                                        style="
                                          border-collapse: collapse;
                                          width: 100px;
                                        "
                                      >
                                        <img
                                          alt="Famstar Logo"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/logo.png"
                                          style="
                                            border: 0px;
                                            height: auto;
                                            line-height: 13px;
                                            outline: 0px;
                                            text-decoration: none;
                                            border-radius: 0px;
                                            display: block;
                                            width: 100.364px;
                                            font-size: 13px;
                                            margin-top:  10px;
                                          "
                                          width="100"
                                          height="auto"
                                          class="CToWUd"
                                        />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><h2>Hi ${name},</h2></p>
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px">Welcome to Famstar. To activate your account please click the button and verify your email address.</p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
      <!-- *************** Reset Text Ends -->
  
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:24px;letter-spacing:0px;line-height:1.5;text-align:left;color:#000000">
                    <p style="display:block;margin:0px;text-align:center"><a href="https://api.famstar.in/influencer/auth/email/confirm/${token}" class="button">
                      Activate Your Account
                  </a></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
         <!-- Button Ends -->
  
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px">
                      This link is valid for next 1 hour. Unable to click on the button above. Paste this link in your browser. <br/>
                      <a href="https://api.famstar.in/influencer/auth/email/confirm/${token}" style="color: blue;text-decoration: underline;">https://api.famstar.in/influencer/auth/email/confirm/${token}</a>                 </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Contact info starts -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#f8faf6;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#f8faf6;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px 16px 0px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 16px 5px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px">In case you have any queries or feedback, please write to us at <a href="mailto:hello@famstar.in" style="color: #393262;">hello@famstar.in</a></p>
  
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px"><span style="color:#1b2631">We read every email :)</span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- contact info ends -->
         <!-- ******************************************* -->
         
         <!-- End text starts  -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><span style="color:#1b2631">Happy Influencing,</span></p>
                    <p style="display:block;margin:0px"><span style="color:#393262"><b>Team Famstar.</b></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- End text ends -->
         <!-- *************************************************** -->
         <!-- Social Icons starts here -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:12px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://twitter.com/famstarhq" rel="noopener" target="_blank" data-saferedirecturl="https://twitter.com/famstarhq"><img alt="twitter" src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.linkedin.com/company/famstar/" rel="noopener" target="_blank" data-saferedirecturl="https://www.linkedin.com/company/famstar/"><img alt="linkedin" src="https://ci5.googleusercontent.com/proxy/D79Rrzm2yqxpbyaq9obVZRGTSzhvMEgIqLu463HdX9OIutpMOcEoE78JXfpHrslgXMmE87Bv-jTJrIo9ill_sj1nW9WkHBIMjjRjvqcWktcmvBrFpFYwXINwpJvLz6jRYtK35wLfZfrjPsNNeSJyyQKMd6yFX6T9pUxJYm43Sst3-cy0O04F-GCymI1Fqk0KSKc8tLm0yw=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845188311_linkedin.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218493000&amp;usg=AFQjCNETS6jnN8bYjJe-ymkGNd_Ab1fP_w"><img alt="youtube" src="https://ci4.googleusercontent.com/proxy/W1l9kK-GAECVakgi6Jz4Nxb78e7vl33uaFoSVjDxD_wjLUl3uzCd8_wzD-4AR2AV5Urcvc9lHix0ZhmYwW-Fdph4vKhj49Jqz4cCcfdgIDUJgXJusc20b-X1vQ87TjOT60KwRsJXSuO62ja5H20KCLfnKRcBk_wFZLfqYWyrowYELbPGzl2gwriCHBYqzyNXkmlu3Owb=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845199657_youtube.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.facebook.com/famstarHQ" rel="noopener" target="_blank" data-saferedirecturl="https://www.facebook.com/famstarHQ"><img alt="facebook" src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.instagram.com/famstarhq/" rel="noopener" target="_blank" data-saferedirecturl="https://www.instagram.com/famstarhq/"><img alt="instagram" src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word" align="left">
                   <div style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                    <p style="display:block;margin-top:10px;text-align:center">You are receiving this mail because you are a member at Famstar</p>
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:8px">
                    &nbsp;
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Social icons ends here -->
         <!-- ********************************************************* -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:2px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span style="color:#84919e;font-size:12px"><i>© 2021</i></span><span style="font-size:12px"><i><span>&nbsp;</span></i></span><span style="color:#888888;font-size:12px">Famstar.in</span></p>
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:10px"><i>Made with ❤️ in India</i></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  </body>
  </html>
  `;
};

templates.INFLUENCER_CAMPAIGN_ACCEPTANCE_MAIL = (name, company) => {
    return `
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <style>
          .button {
            background-color: #FF8F99;
            border-top: 10px solid #FF8F99;
            border-right: 18px solid #FF8F99;
            border-bottom: 10px solid #FF8F99;
            border-left: 18px solid #FF8F99;
            display: inline-block;
            color: #fff;
            text-decoration: none;
            border-radius: 3px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
            -webkit-text-size-adjust: none;
            box-sizing: border-box;
            font-size: 1.3rem;
          }
          a {
              color: white;
          }
          a:visited {
              color: white;
          }
      </style>
  </head>
  <body>
      
      <!-- Reset Text -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
      <div
      style="
        color: #000000;
        font-size: 0px;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-weight: 400;
        letter-spacing: normal;
        text-align: center;
        text-indent: 0px;
        text-transform: none;
        white-space: normal;
        word-spacing: 0px;
        background: 0px 0px transparent;
        text-decoration-style: initial;
        text-decoration-color: initial;
        margin: 0px auto;
        max-width: 600px;
      "
    >
      <table
        style="
          border-collapse: collapse;
          background: 0px 0px transparent;
          width: 590.545px;
        "
        cellspacing="0"
        cellpadding="0"
        border="0"
        align="center"
      >
        <tbody>
          <tr>
            <td
              style="
                border-collapse: collapse;
                direction: ltr;
                font-size: 0px;
                padding: 0px;
                text-align: center;
              "
            >
              <div
                style="
                  max-width: 100%;
                  width: 590.545px;
                  font-size: 0px;
                  text-align: left;
                  direction: ltr;
                  display: inline-block;
                  vertical-align: top;
                "
              >
                <table
                  style="border-collapse: collapse"
                  width="100%"
                  cellspacing="0"
                  cellpadding="0"
                  border="0"
                >
                  <tbody>
                    <tr>
                      <td
                        style="
                          border-collapse: collapse;
                          background-color: transparent;
                          vertical-align: top;
                          padding: 0px;
                        "
                      >
                        <table
                          style="border-collapse: collapse"
                          width="100%"
                          cellspacing="0"
                          cellpadding="0"
                          border="0"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="
                                  border-collapse: collapse;
                                  font-size: 0px;
                                  padding: 0px;
                                  word-break: break-word;
                                "
                                align="center"
                              >
                                <table
                                  style="
                                    border-collapse: collapse;
                                    border-spacing: 0px;
                                  "
                                  cellspacing="0"
                                  cellpadding="0"
                                  border="0"
                                >
                                  <tbody>
                                    <tr>
                                      <td
                                        style="
                                          border-collapse: collapse;
                                          width: 100px;
                                        "
                                      >
                                        <img
                                          alt="Famstar Logo"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/logo.png"
                                          style="
                                            border: 0px;
                                            height: auto;
                                            line-height: 13px;
                                            outline: 0px;
                                            text-decoration: none;
                                            border-radius: 0px;
                                            display: block;
                                            width: 100.364px;
                                            font-size: 13px;
                                            margin-top:  10px;
                                          "
                                          width="100"
                                          height="auto"
                                          class="CToWUd"
                                        />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><h2>Hi ${name},</h2></p>
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px">Congratulations! You have been approved for campaign collaboration by ${company}. We look forward to your work and Let’s make this collaboration worth remembering! </p>
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px">p.s. You can check the status of your campaign in your profile tab under My Campaigns.
                    </p>

                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
      <!-- *************** Reset Text Ends -->
  
      
         <!-- End text starts  -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><span style="color:#1b2631">Cheers,</span></p>
                    <p style="display:block;margin:0px"><span style="color:#393262"><b>Team Famstar.</b></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- End text ends -->
         <!-- *************************************************** -->
         <!-- Social Icons starts here -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:12px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://twitter.com/famstarhq" rel="noopener" target="_blank" data-saferedirecturl="https://twitter.com/famstarhq"><img alt="twitter" src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.linkedin.com/company/famstar/" rel="noopener" target="_blank" data-saferedirecturl="https://www.linkedin.com/company/famstar/"><img alt="linkedin" src="https://ci5.googleusercontent.com/proxy/D79Rrzm2yqxpbyaq9obVZRGTSzhvMEgIqLu463HdX9OIutpMOcEoE78JXfpHrslgXMmE87Bv-jTJrIo9ill_sj1nW9WkHBIMjjRjvqcWktcmvBrFpFYwXINwpJvLz6jRYtK35wLfZfrjPsNNeSJyyQKMd6yFX6T9pUxJYm43Sst3-cy0O04F-GCymI1Fqk0KSKc8tLm0yw=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845188311_linkedin.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218493000&amp;usg=AFQjCNETS6jnN8bYjJe-ymkGNd_Ab1fP_w"><img alt="youtube" src="https://ci4.googleusercontent.com/proxy/W1l9kK-GAECVakgi6Jz4Nxb78e7vl33uaFoSVjDxD_wjLUl3uzCd8_wzD-4AR2AV5Urcvc9lHix0ZhmYwW-Fdph4vKhj49Jqz4cCcfdgIDUJgXJusc20b-X1vQ87TjOT60KwRsJXSuO62ja5H20KCLfnKRcBk_wFZLfqYWyrowYELbPGzl2gwriCHBYqzyNXkmlu3Owb=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845199657_youtube.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.facebook.com/famstarHQ" rel="noopener" target="_blank" data-saferedirecturl="https://www.facebook.com/famstarHQ"><img alt="facebook" src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.instagram.com/famstarhq/" rel="noopener" target="_blank" data-saferedirecturl="https://www.instagram.com/famstarhq/"><img alt="instagram" src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word" align="left">
                   <div style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                    <p style="display:block;margin-top:10px;text-align:center">You are receiving this mail because you are a member at Famstar</p>
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:8px">
                    &nbsp;
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Social icons ends here -->
         <!-- ********************************************************* -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:2px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span style="color:#84919e;font-size:12px"><i>© 2021</i></span><span style="font-size:12px"><i><span>&nbsp;</span></i></span><span style="color:#888888;font-size:12px">Famstar.in</span></p>
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:10px"><i>Made with ❤️ in India</i></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
  </body>
  </html>
 
  `;
};

templates.INFLUENCER_CAMPAIGN_REJECTION_MAIL = (name, company) => {
    return `
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <style>
          .button {
            background-color: #FF8F99;
            border-top: 10px solid #FF8F99;
            border-right: 18px solid #FF8F99;
            border-bottom: 10px solid #FF8F99;
            border-left: 18px solid #FF8F99;
            display: inline-block;
            color: #fff;
            text-decoration: none;
            border-radius: 3px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
            -webkit-text-size-adjust: none;
            box-sizing: border-box;
            font-size: 1.3rem;
          }
          a {
              color: white;
          }
          a:visited {
              color: white;
          }
      </style>
  </head>
  <body>
      
      <!-- Reset Text -->
      <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
      <div
      style="
        color: #000000;
        font-size: 0px;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-weight: 400;
        letter-spacing: normal;
        text-align: center;
        text-indent: 0px;
        text-transform: none;
        white-space: normal;
        word-spacing: 0px;
        background: 0px 0px transparent;
        text-decoration-style: initial;
        text-decoration-color: initial;
        margin: 0px auto;
        max-width: 600px;
      "
    >
      <table
        style="
          border-collapse: collapse;
          background: 0px 0px transparent;
          width: 590.545px;
        "
        cellspacing="0"
        cellpadding="0"
        border="0"
        align="center"
      >
        <tbody>
          <tr>
            <td
              style="
                border-collapse: collapse;
                direction: ltr;
                font-size: 0px;
                padding: 0px;
                text-align: center;
              "
            >
              <div
                style="
                  max-width: 100%;
                  width: 590.545px;
                  font-size: 0px;
                  text-align: left;
                  direction: ltr;
                  display: inline-block;
                  vertical-align: top;
                "
              >
                <table
                  style="border-collapse: collapse"
                  width="100%"
                  cellspacing="0"
                  cellpadding="0"
                  border="0"
                >
                  <tbody>
                    <tr>
                      <td
                        style="
                          border-collapse: collapse;
                          background-color: transparent;
                          vertical-align: top;
                          padding: 0px;
                        "
                      >
                        <table
                          style="border-collapse: collapse"
                          width="100%"
                          cellspacing="0"
                          cellpadding="0"
                          border="0"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="
                                  border-collapse: collapse;
                                  font-size: 0px;
                                  padding: 0px;
                                  word-break: break-word;
                                "
                                align="center"
                              >
                                <table
                                  style="
                                    border-collapse: collapse;
                                    border-spacing: 0px;
                                  "
                                  cellspacing="0"
                                  cellpadding="0"
                                  border="0"
                                >
                                  <tbody>
                                    <tr>
                                      <td
                                        style="
                                          border-collapse: collapse;
                                          width: 100px;
                                        "
                                      >
                                        <img
                                          alt="Famstar Logo"
                                          src="https://famstar.s3.ap-south-1.amazonaws.com/logo.png"
                                          style="
                                            border: 0px;
                                            height: auto;
                                            line-height: 13px;
                                            outline: 0px;
                                            text-decoration: none;
                                            border-radius: 0px;
                                            display: block;
                                            width: 100.364px;
                                            font-size: 13px;
                                            margin-top:  10px;
                                          "
                                          width="100"
                                          height="auto"
                                          class="CToWUd"
                                        />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 20px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><h2>Hi ${name},</h2></p>
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px">We thank you for applying to the campaign for ${company}. We are deeply sorry to inform you that you have not been approved for the campaign this time. </p>
                    <p style="display:block;margin:0px">&nbsp;</p>
                    <p style="display:block;margin:0px">Do not dishearten, there are many other opportunities in line for you! Keep Applying :)</p>

                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
      <!-- *************** Reset Text Ends -->
  
      
         <!-- End text starts  -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;border:0px solid transparent;direction:ltr;font-size:0px;padding:16px;text-align:center">
              <div style="max-width:100%;width:558.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px 0px 0px 16px;word-break:break-word" align="left">
                   <div style="font-size:16px;font-weight:400;letter-spacing:0px;line-height:1.5;text-align:left;color:#444444">
                    <p style="display:block;margin:0px"><span style="color:#1b2631">Luck,</span></p>
                    <p style="display:block;margin:0px"><span style="color:#393262"><b>Team Famstar.</b></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- End text ends -->
         <!-- *************************************************** -->
         <!-- Social Icons starts here -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;text-decoration-style:initial;text-decoration-color:initial;background:#ffffff;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:#ffffff;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:12px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="center">
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://twitter.com/famstarhq" rel="noopener" target="_blank" data-saferedirecturl="https://twitter.com/famstarhq"><img alt="twitter" src="https://famstar.s3.ap-south-1.amazonaws.com/twitter.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.linkedin.com/company/famstar/" rel="noopener" target="_blank" data-saferedirecturl="https://www.linkedin.com/company/famstar/"><img alt="linkedin" src="https://ci5.googleusercontent.com/proxy/D79Rrzm2yqxpbyaq9obVZRGTSzhvMEgIqLu463HdX9OIutpMOcEoE78JXfpHrslgXMmE87Bv-jTJrIo9ill_sj1nW9WkHBIMjjRjvqcWktcmvBrFpFYwXINwpJvLz6jRYtK35wLfZfrjPsNNeSJyyQKMd6yFX6T9pUxJYm43Sst3-cy0O04F-GCymI1Fqk0KSKc8tLm0yw=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845188311_linkedin.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <!-- <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://post.spmailtechnolo.com/f/a/VRNgBcvbA16oE3AArQMiKA~~/AAQNhwA~/RgRimhYYP4SyAWh0dHBzOi8vYy53ZWJlbmdhZ2UuY29tL2x3L2cxLmpwZz9wPWV5SnNZeUk2SW40eU1ESTBZbUUzTnlJc0ltd2lPaUk1WW1abE5ESXdOVEExWmpVelltWm1ZamcyWWpabU9EVm1ZakZtTjJRMU9TSXNJbU1pT2lJeE9UVTJOeUlzSW1WdElqb2laMkYxY21GMmMyRnBibWt3TTBCbmJXRnBiQzVqYjIwaUxDSmxJam9pZm1weFltMDFOeUlzSW5ZaU9pSXpaVEUwTkdReElpd2ljeUk2SWpObVl6VmpOakl4TFRZM05qUXROREV6TnkxaU5XSTBMV00wWkdFM1ltRmtPR1psWlNJc0ltb2lPaUolMkJOamhsTURjMk9DSXNJbVYyWlc1MElqb2laVzFoYVd4ZlkyeHBZMnNpTENKamRHRWlPaUkzWXpWbE16UmhOaUlzSW5SdlZWSk1Jam9pYUhSMGNITTZMeTkzZDNjdWVXOTFkSFZpWlM1amIyMHZZMmhoYm01bGJDOVZRMmRuVUdRelZtWTViMjlITW5JMFNWOWFUbGRDZWtFaWZRJTNEJTNEVwNzcGNCCmCuGJG3YJKMvoZSF2dhdXJhdnNhaW5pMDNAZ21haWwuY29tWAQAAADM&amp;source=gmail&amp;ust=1623318218493000&amp;usg=AFQjCNETS6jnN8bYjJe-ymkGNd_Ab1fP_w"><img alt="youtube" src="https://ci4.googleusercontent.com/proxy/W1l9kK-GAECVakgi6Jz4Nxb78e7vl33uaFoSVjDxD_wjLUl3uzCd8_wzD-4AR2AV5Urcvc9lHix0ZhmYwW-Fdph4vKhj49Jqz4cCcfdgIDUJgXJusc20b-X1vQ87TjOT60KwRsJXSuO62ja5H20KCLfnKRcBk_wFZLfqYWyrowYELbPGzl2gwriCHBYqzyNXkmlu3Owb=s0-d-e1-ft#https://mmstoragedebug.blob.core.windows.net/userdata/054b169c-d294-411e-aea7-240187ca465f%2Ftemplates%2F1609845199657_youtube.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table> -->
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.facebook.com/famstarHQ" rel="noopener" target="_blank" data-saferedirecturl="https://www.facebook.com/famstarHQ"><img alt="facebook" src="https://famstar.s3.ap-south-1.amazonaws.com/facebook.new.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table>
                   <table style="border-collapse:collapse;float:none;display:inline-table" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                     <tr>
                      <td style="border-collapse:collapse;padding:0px 2px 0px 10px;vertical-align:middle">
                       <table style="border-collapse:collapse;border-radius:3px;width:24px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                         <tr>
                          <td style="border-collapse:collapse;font-size:0px;height:24px;vertical-align:middle;width:24px"><a href="https://www.instagram.com/famstarhq/" rel="noopener" target="_blank" data-saferedirecturl="https://www.instagram.com/famstarhq/"><img alt="instagram" src="https://famstar.s3.ap-south-1.amazonaws.com/instagram.png.png" style="border:0px;height:auto;line-height:0px;outline:0px;text-decoration:none;border-radius:3px;display:block" width="24" height="24" class="CToWUd"></a></td>
                         </tr>
                        </tbody>
                       </table></td>
                     </tr>
                    </tbody>
                   </table></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:5px 0px 0px 5px;word-break:break-word" align="left">
                   <div style="font-size:10px;letter-spacing:0.4px;line-height:1.4;text-align:left;color:#444444">
                    <p style="display:block;margin-top:10px;text-align:center">You are receiving this mail because you are a member at Famstar</p>
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:8px">
                    &nbsp;
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
         <!-- Social icons ends here -->
         <!-- ********************************************************* -->
         <div style="color:#000000;font-size:0px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:center;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background:0px 0px transparent;text-decoration-style:initial;text-decoration-color:initial;margin:0px auto;max-width:600px">
          <table style="border-collapse:collapse;background:0px 0px transparent;width:590.545px" cellspacing="0" cellpadding="0" border="0" align="center">
           <tbody>
            <tr>
             <td style="border-collapse:collapse;direction:ltr;font-size:0px;padding:0px 8px;text-align:center">
              <div style="max-width:100%;width:574.545px;font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top">
               <table style="border-collapse:collapse;background-color:transparent;vertical-align:top" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                 <tr>
                  <td style="border-collapse:collapse;background:0px 0px;font-size:0px;word-break:break-word">
                   <div style="height:2px">
                    &nbsp;
                   </div></td>
                 </tr>
                 <tr>
                  <td style="border-collapse:collapse;font-size:0px;padding:0px;word-break:break-word" align="left">
                   <div style="font-size:12px;letter-spacing:0.4px;line-height:1.6;text-align:left;color:#444444">
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:12px">Copyright<span>&nbsp;</span></span><span style="color:#84919e;font-size:12px"><i>© 2021</i></span><span style="font-size:12px"><i><span>&nbsp;</span></i></span><span style="color:#888888;font-size:12px">Famstar.in</span></p>
                    <p style="display:block;margin:0px;text-align:center"><span style="color:#888888;font-size:10px"><i>Made with ❤️ in India</i></span></p>
                   </div></td>
                 </tr>
                </tbody>
               </table>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
  
  </body>
  </html>

  
  `;
};

module.exports = templates;
