# Famstar Backend API

<div align="center">
  <img
      src="https://www.famstar.in/images/logo.png"
      alt="Hoppscotch Logo"
      height="64"
    />
  <br />
  <p>
    <h3>
      <b>
        Famstar
      </b>
    </h3>
  </p>
  <p>
    <b>
      Platform made for Influencers
    </b>
  </p>
  <p>

  </p>
  <p>
    <sub>
      Built with ❤︎ by NodeJS
    </sub>
  </p>
</div>

## Coding style guide and best practices

-   [Node-JS Best Practices](https://github.com/goldbergyoni/nodebestpractices#1-project-structure-practices)
-   [Google Javascript Coding Style Guide](https://google.github.io/styleguide/jsguide.html)

<details open>
  <summary><b>Table of contents</b></summary>

---

-   [Developing](#developing)
-   [Changelog](#changelog)
-   [Authors](#authors)

---

</details>

## **Developing**

All the config of backend are given in config.js file including environment variables like MONGO_URI, SERVER ADDR, etc.

### Local development environment

1. [Clone this repo](https://help.github.com/en/articles/cloning-a-repository) with git.
2. Install dependencies by running `npm install` within the directory that you cloned.
3. Start the development server with `npm run dev`.
4. Open development API by going to [`http://localhost:4000`](http://localhost:4000) in any REST client.

Check if you have ufw (uncomplicated firewall) set up.

to check if you have ufw running do:

`sudo ufw status`

if it is running, to allow port 3000 simply do the command

`sudo ufw allow 3000`

this solved the problem for me. i forgot that i had setup ufw a while back, and recently starting using my aws instance again.

-   Elastic-search server portal

```
http://52.66.245.31:5601/app/home#/
```

-   REST API dev server

```
http://3.7.226.212
```

## **Changelog**

See the [`CHANGELOG`](CHANGELOG.md) file for details.
