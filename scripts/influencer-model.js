const mongoose = require('mongoose');

const Influencer = require('../models/influencer/influencer');
const Facebook = require('../models/social/facebook');

const config = require('../config');

mongoose
    .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to MongoDB instance');

        Influencer.find().then(async (influencers) => {
            for (let influencer of influencers) {
                console.log('hello');
                await Facebook.findOne({ influencer: influencer._id }).then(async (facebook) => {
                    if (facebook != null && facebook.profilePicture) {
                        influencer.profilePicture = facebook.profilePicture;
                    } else {
                        if (influencer.gender == 'male')
                            influencer.profilePicture = 'https://famstar.s3.ap-south-1.amazonaws.com/male-avatar.jpeg';
                        else influencer.profilePicture = 'https://famstar.s3.ap-south-1.amazonaws.com/female-avatar.jpeg';
                    }

                    await influencer.save();
                });
            }
        });
    })
    .catch((error) => {
        console.log(error);
    });
