const moment = require('moment');

/**
 * Conver the long numbers to abbrevated string
 * @param val Double
 * @returns {string}
 */
function convertNumToAbb(val) {
    // thousands, millions, billions etc..
    const s = ['', 'k', 'm', 'b', 't'];

    // dividing the value by 3.
    const sNum = Math.floor(('' + val).length / 3);

    // calculating the precised value.
    let sVal = parseFloat((sNum !== 0 ? val / Math.pow(1000, sNum) : val).toPrecision(3));

    if (sVal % 1 !== 0) {
        sVal = sVal.toFixed(1);
    }

    // appending the letter to precised val.
    return sVal + s[sNum];
}

function getTimeSince(val) {
    return moment(val, 'YYYY/MM/DD').minute(0).from(moment().minute(0));
}

module.exports = {
    convertNumToAbb,
    getTimeSince,
};
