const mongoose = require('mongoose');
const config = require('../config');

const EmailAuth = require('../models/influencer/email-auth');
const Influencer = require('../models/influencer/influencer');
const PointsTransaction = require('../models/reward/points-transaction');
const RewardWallet = require('../models/reward/rewards-wallet');
const Wallet = require('../models/wallet/wallet');

mongoose.set('useCreateIndex', true);
mongoose.promise = global.Promise;

async function removeAllCollections() {
    const collections = Object.keys(mongoose.connection.collections);
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName];
        await collection.deleteMany();
    }
}

async function dropAllCollections() {
    const collections = Object.keys(mongoose.connection.collections);
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName];
        try {
            await collection.drop();
        } catch (error) {
            // Sometimes this error happens, but you can safely ignore it
            if (error.message === 'ns not found') return;
            // This error occurs when you use it.todo. You can
            // safely ignore this error too
            if (error.message.includes('a background operation is currently running')) return;
            console.log(error.message);
        }
    }
}

module.exports = {
    setupDB() {
        // Connect to Mongoose
        beforeAll(async () => {
            const url = config.DB_URL;
            await mongoose.connect(url, { useNewUrlParser: true });
            const emailAuth = new EmailAuth({
                _id: '60a39728e6709d6db24cf6ed',
                userId: '60a39982e6709d6db24cf6ee',
                isVerified: true,
                password: '$2a$10$XxBLyga.At4G.qz0qPjJ/eCjmQ6a73Eo1qqww400ozZQqQ8SeEFHS',
                email: 'hello@famstar.in',
                createdAt: '1621333800345',
                updatedAt: '1621334402740',
                __v: 0,
            });

            const influencer = new Influencer({
                _id: '60a39982e6709d6db24cf6ee',
                name: 'Ria',
                gender: 'female',
                city: 'Delhi',
                email: 'hello@famstar.in',
                categories: ['5f9177eba78bad52bc5c0580'],
                phoneNumber: '9999002702',
                dob: '1618617600000',
                createdAt: '1621334402687',
                updatedAt: '1621334402687',
                profilePicture: 'https://famstar.s3.ap-south-1.amazonaws.com/1611147980923',
                __v: 0,
            });

            const pointsTransaction = new PointsTransaction({
                _id: '60a39982e6709d6db24cf6f1',
                metadata: { source: 'Entry Bonus', sourceId: '60a39982e6709d6db24cf6ee' },
                transactionType: 'Credit',
                userId: '60a39982e6709d6db24cf6ee',
                points: 100,
                createdAt: '1621334402870',
                updatedAt: '1621334402870',
                __v: 0,
            });

            const rewardWallet = new RewardWallet({
                _id: '60a39982e6709d6db24cf6f0',
                points: 100,
                influencer: '60a39982e6709d6db24cf6ee',
                createdAt: '1621334402823',
                updatedAt: '1621334402823',
                __v: 0,
            });

            const wallet = new Wallet({
                _id: '60a39982e6709d6db24cf6ef',
                totalEarnings: 0,
                moneyToBeWithdrawn: 0,
                kyc: null,
                influencer: '60a39982e6709d6db24cf6ee',
                createdAt: '1621334402777',
                updatedAt: '1621334402777',
                __v: 0,
            });

            try {
                await emailAuth.save();
                await influencer.save();
                await pointsTransaction.save();
                await rewardWallet.save();
                await wallet.save();
            } catch (err) {
                console.log(err);
            }
        });

        // // Cleans up database between each test
        // afterEach(async () => {
        //   await removeAllCollections()
        // })

        // Disconnect Mongoose
        afterAll(async () => {
            await dropAllCollections();
            await mongoose.connection.close();
        });
    },
};
