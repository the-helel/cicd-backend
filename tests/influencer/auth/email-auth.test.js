const mongoose = require('mongoose');
const config = require('../../../config');
const { setupDB } = require('../../setUpDB');
const app = require('../../../app');
const EmailAuth = require('../../../models/influencer/email-auth');
const Influencer = require('../../../models/influencer/influencer');
const supertest = require('supertest');
const request = supertest(app);

setupDB();

describe('Testing Email Authentication user login', () => {
    it('should not login the user', async () => {
        const response = await request.post('/influencer/auth/email/login/').send({ email: 'fake@gmail.com', password: 'password' });
        expect(response.body.error).toBeTruthy();
        expect(response.statusCode).toBe(404);
    });

    it('should login the user', async () => {
        const response = await request.post('/influencer/auth/email/login/').send({ email: 'hello@famstar.in', password: 'famstar123' });

        expect(response.statusCode).toBe(200);
        expect(response.body.error).toBeFalsy();
        expect(response.body.result.auth).toBeTruthy();
        expect(response.body.result.token).toBeDefined();
    });

    it('should throw invalid credentials with 401', async () => {
        const response = await request.post('/influencer/auth/email/login/').send({ email: 'hello@famstar.in', password: 'famstar12' });

        expect(response.statusCode).toBe(401);
        expect(response.body.error).toBeTruthy();
        expect(response.body.message).toBeDefined();
    });

    it('should throw 400 status on invalid request body', async () => {
        const response = await request.post('/influencer/auth/email/login/').send({ password: 'famstar12' });

        expect(response.statusCode).toBe(400);
        expect(response.body.error).toBeTruthy();
        expect(response.body.message).toBeDefined();
    });
});

describe('Testing Email Auth Signup', () => {
    it('Should throw 400 error due to email not provided', async () => {
        const response = await request.post('/influencer/auth/email/token/').send({ password: 'famstar12' });

        expect(response.statusCode).toBe(400);
        expect(response.body.error).toBeTruthy();
        expect(response.body.message).toBeDefined();
    });

    it('Should throw 400 due to email taken', async () => {
        const response = await request.post('/influencer/auth/email/token/').send({ email: 'hello@famstar.com', password: 'famstar123' });

        // const res = await Influencer.findOne({ email: 'hello@famstar.in'})
        // console.log(res)

        expect(response.statusCode).toBe(400);
        expect(response.body.error).toBeTruthy();
        expect(response.body.message).toBeDefined();
    });
});
