const { calculateTDS } = require('../controllers/influencer/wallet/wallet');

test('Calculate TDS', async () => {
    let wallet = {
        totalEarnings: 2000,
        moneyToBeWithdrawn: 2000,
    };
    let amount = 2000;
    let data = calculateTDS(wallet, amount);
    expect(data).toEqual({
        deductibleAmount: 0,
        deductionAmount: 0,
        amountAfterDeduction: 2000,
    });

    wallet = {
        totalEarnings: 30000,
        moneyToBeWithdrawn: 2000,
    };
    amount = 2000;
    data = calculateTDS(wallet, amount);
    expect(data).toEqual({
        deductibleAmount: 0,
        deductionAmount: 0,
        amountAfterDeduction: 2000,
    });

    wallet = {
        totalEarnings: 31000,
        moneyToBeWithdrawn: 2000,
    };
    amount = 2000;
    data = calculateTDS(wallet, amount);
    expect(data).toEqual({
        deductibleAmount: 31000,
        deductionAmount: 3100,
        amountAfterDeduction: -1100,
    });

    wallet = {
        totalEarnings: 31000,
        moneyToBeWithdrawn: 1000,
    };
    amount = 1000;
    data = calculateTDS(wallet, amount);
    expect(data).toEqual({
        deductibleAmount: 31000,
        deductionAmount: 3100,
        amountAfterDeduction: -2100,
    });

    wallet = {
        totalEarnings: 33000,
        moneyToBeWithdrawn: 2000,
    };
    amount = 2000;
    data = calculateTDS(wallet, amount);
    expect(data).toEqual({
        deductibleAmount: 2000,
        deductionAmount: 200,
        amountAfterDeduction: 1800,
    });

    wallet = {
        totalEarnings: 33000,
        moneyToBeWithdrawn: 2002,
    };
    amount = 2002;
    data = calculateTDS(wallet, amount);
    expect(data).toEqual({
        deductibleAmount: 2002,
        deductionAmount: 200.2,
        amountAfterDeduction: 1801.8,
    });
});
