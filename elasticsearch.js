const { Client } = require('@elastic/elasticsearch');
const config = require('./config');

const client = new Client({
    node: `http://${config.ELASTICSEARCH_HOST}:${config.ELASTICSEARCH_PORT}`,
    auth: {
        username: config.ELASTICSEARCH_USERNAME,
        password: config.ELASTICSEARCH_PASSWORD,
    },
});

module.exports = client;
