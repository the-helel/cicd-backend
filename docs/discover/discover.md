# Discover platform - Influencer attributes and analysis

<em>Note: The attributes marked as (Public) can be scraped publicly from influencer’s profile and attributes marked as (Famstar) can only be extracted for famstar registered influencer</em>

1. Search based on influencer content related to brand product

    - Find influencers based on mentions of your brand, competitors, topics of interest, and hashtags. (https://keyhole.co/hashtagtracking/)

2. Search by influencer attributes

    Search by specifying influencer criteria such as:

    - Gender (Public)
    - Location (Famstar)
    - Platform ( Availability on platform )
    - All available influencer
    - All Famstar influencer
    - Follower count (Public)
    - Engagement rate (Public)
    - Search by audience criteria (Famstar)

    We get these insights for Famstar registered Instagram users:

    - audience_country
    - audience_city
    - audience_gender_age

3. Historical content

    Instagram post data: <b>caption, media_type, media_url, like_count, engagement, shortCode, comment (Public)</b>

    Instagram media insight: <b>impressions, engagement, saved, reach, video_views (Famstar)</b>

    <em><b>Note</b>: <b>video_views</b> is not available for media type of IMAGE.</em>

    Record time based engagement activity on influencer’s posts <b>(Public)</b>

    Record time based followers growth activity of influencer <b>(Public)</b>

4. Audience demographics

    Insights on their audience’s age, gender, location (Famstar)

5. Audience Quality & Fake Follower Analysis (Data generation) (Public)

    Scrape the followers list of a influencer and use [Instagram Business Discover API](https://developers.facebook.com/docs/instagram-api/guides/business-discovery/) to extract followers and following count

    - Avg Enagement rate
    - Standard deviation
    - Growth rates (followers and engagement)

6. Brand safety (Data generation) <b>(Public)</b>

    Protect brand’s reputation with automated safety checks on influencers post activity for brand risks. Gauge influencers post and bio, for explicit, politically biased content, etc

> <b>TODO</b>: Make a listing of data which is available from scrape and GraphAPI. Assigned to Ajay Singh
> Implement these API

### Influencer Discovery platforms:

-   Traackr
-   Qoruz

# Data Scraping

-   IP based filtering (daily limit - 5000)
-   Profiles based (daily limits - 500) - instagram

# Search Schema

```plaintext
"id": 261445,
"name": "",
"profile_image": "test",
"city":"", //famstar platform
"gender":"", //famstar platform
"dob":"", //famstar platform
"famstar_verified":"", //default false and make true if he signup with famstar
"famstar_score":"",//famstar platform
"email": null, //famstar platform
"categories": {
	"Arts & Entertainment": 99,
	"Television": 99,
	"Style & Fashion": 99
},
"EMAIL":"",//famstar platform
"PHONE":"",//famstar platform
"SOURCE":"",//from where this data got scraped or fetched
"BARTER_READY":"", //famstar platform default null, later update with true or false
"signed_up_at": ,// newly added
"updated_at": ,//newly added
"twitter_handle": "siddnigam_off", //add insta data also,
"twitter_followers": "112.1k",
"twitter_profile_link": "https:\/\/twitter.com\/siddnigam_off",
"youtube_handle": "Siddharth Nigam",
"youtube_subscribers": "1.5m",
"youtube_profile_link": "https:\/\/youtube.com\/channel\/UCQNzkIbwSbuWapwOjX8R5ug",
"facebook_handle": "TheSiddharthNigam",
"facebook_page_likes": "619.4k",
"facebook_profile_link": "https:\/\/facebook.com\/TheSiddharthNigam"
```

Profile Schema

```plaintext
"instagram": {
	"insta_id":
	"insta_handle": "am_akankshasingh",
	"insta_url": "https://www.instagram.com/am_akankshasingh/",
	"bio": "",
	"secondary_email": "",
	"posts_count":
	"insta_followers":
	"insta_following":
	"insta_engagement_rate":17.96,] (array)
	"avg_likes": array of date:value,
	"avg_comments": array of date:value,
	"avg_views": array of date:value,
	"insta_real_reach":"average reach on last 10 posts",
	"avg_interactions": profilevisits or other acivity,
	"insta_verified": true,
	"promo_posts_count":
	"promo_avg_likes":
	"promo_avg_comments":
	"business_category_name"
	"category_name"
	"ffratio":"follower/following ratio, higher is better. will be helpful in detemining fake following"
	"invite_notes":"this influencer is exclusive to winkl",
	"costing":{"reel":22,"post":322,"story":33232}
	"top_mentions":[array of profiles],
	"top_hashtags":[],
	"top_keywords":[array of keywords], // above two items are frequently fetched from posts
	"recent_collabs":{object with brand and pricing details and ratings}
	"audience":{ //official api
		"demographics":{},
		"age":{},
		"city":{}
		//any other items which can be scaped or fetched via graphql
	},
	"follower_growth_analysis": {
		"followers_growth_last_30days": 4.61, //percentage
		"followers_gained_last_30days": "311.4k",
		"followers_growth_series": [
		  "Jan2021":6777802,
		  "Feb2021":6777802,
		  "March2021":6777802,
		],
		"y_axis_min": 6777802,
		"success": true
    	},
	engagement_stats:{
		"engagement_rate":32
		"last_30_days":32 //need to think on this
		"q_recent_engagements":[13212,3212,32131,223123],
		"recent_engagement_rate":[],

	},

}
```

\
Insta Posts Collections

```plaintext
{
	"owner":"insta_handle"
	"post_id":dsadsa, //shortcode
	"video_title":
	"engagement": //Sum of likes_count, comment_count and saved counts
	"like_count":
	"comments_count":
	"saved_count":
	"impressions"://Total number of times the IG Media object has been seen.
	"reach"://Total number of unique Instagram accounts that have seen the IG Media object.
	"video_views"://
	"image_url":
	"caption":
	"media_product_type"://Can be AD, FEED, IGTV, or STORY.
	"media_type"://Can be CAROUSEL_ALBUM, IMAGE, or VIDEO.
	"accessibility_caption": "Photo by Gaurav Taneja (Flying Beast) on July 19, 2021. Maybe an image of 2 people, people standing and body of water", 	//to be scrapped will be helpful in detecting what is in the photo
	"posted_on": //timestamp
	"hashtag_used":
	"profiles_tagged":
}
```

### Discussion about famstar score

-   following of any user should be high P^1
-   engagement = (following \* engagement_rate) should high
-   engagement rate will depend on category i.e beauty, tech
