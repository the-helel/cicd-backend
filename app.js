const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const helmet = require('helmet');
const fs = require('fs');
const bodyParser = require('body-parser');
const rateLimit = require('express-rate-limit');

const agenda = require('./api/jobs/agenda');

const config = require('./config');

const apiRoutes = require('./api');

const backupDB = require('./backup');
const elastic = require('./elasticsearch');
const load_elk_data = require('./utility');
const app = express();

// view engine setup
app.set('views', './views');
app.set('view engine', 'pug');

// Use JSON body parser
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json({ limit: '50mb' }));

// for parsing application/xwww-form-urlencoded
app.use(express.urlencoded({ extended: true, limit: '50mb' }));

var whitelist = ['http://dev-brands.famstar.in', 'http://localhost:4000', 'https://dev.famstar.in', 'http://3.7.226.212'];
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    },
};

// Enable CORS requests
app.use(cors());

app.use((req, res, next) => {
    res.header({ 'Access-Control-Allow-Origin': '*' });
    next();
});

// Rate limit
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 400, // limit each IP to 400 requests per windowMs
});

app.use(limiter);

// Use HTTP request console logging
app.use(morgan('combined'));

// Use security headers for API
app.use(helmet());

// serving static assets
app.use(express.static('public'));

// Connect to MongoDB Atlas instance
mongoose
    .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true })
    .then(() => {
        console.log(`Connected to MongoDB instance of ${config.DB_URL}`);
    })
    .catch((error) => {
        console.log(error);
    });

// Load data to elastic
// Note: Currenlty you need to add data properly in utility.js file before uncommenting below line
// load_elk_data.intializingES()

app.use(apiRoutes);

/**
 * Jobs scheduled to run by Agenda
 *
 * 1. process-transitional-transactions : Processes all the pending transactions
 * 2. update-instagram-profile-data : Updates instagram profile data
 */
agenda.every('30 minutes', 'process-transitional-transactions');
agenda.every('7 days', 'update-instagram-profile');

global.__basedir = __dirname;

let dir = 'resources';
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    dir = 'resources/csv';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
}

// backup
// backupDB();

// Configure Express app
const appConfig = app.listen(config.PORT, config.HOST, () => {
    console.log(`Listening on ${appConfig.address().address}:${appConfig.address().port}`);
});

module.exports = app;
