let constants = {};

constants.INFLUENCER = 'Influencer';
constants.BRAND = 'Brand';
constants.COMPANY = 'Company';

constants.ADMIN = 'Admin';
constants.ORGANIZATION = 'Organization';
constants.MEMBER = 'Member';

constants.CASH_CAMPAIGN = 'Cash Campaign';
constants.PRODUCT_CAMPAIGN = 'Product Campaign';
constants.ENGAGEMENT_CAMPAIGN = 'Engagement Campaign';

constants.VALIDATION_ERROR = 'Validation error in the user fields.';
constants.RESOURCE_NOT_FOUND_ERROR = (resource) => {
    return `${resource} not found.`;
};
constants.SERVER_ERR = 'Internal server error';
constants.SUCCESS = 'Success';

constants.DRAFT = 'Draft';
constants.ACTIVE = 'Active';
constants.PAUSE = 'Pause';
constants.EXPIRED = 'Expired';
constants.COMPLETED = 'Completed';
constants.ARCHIVE = 'Archive';

constants.APPLIED = 'Applied';
constants.SHORTLISTED = 'Shortlisted';
constants.REJECTED = 'Rejected';
constants.APPROVED = 'Approved';
constants.SUBMITTED = 'Submitted';

constants.APPROVE = 'Approve';
constants.REJECT = 'Reject';
constants.CROSSBID = 'Cross Bid';
constants.SHORTLIST = 'Shortlist';

// KYC
constants.PENDING = 'Pending';
constants.VERIFYING = 'Verifying';
constants.VERIFIED = 'Verified';

constants.BLOG_OR_JOURNEY_APPROVAL_POINTS = 50;

constants.CREDIT = 'Credit'; // Transaction type when points  are added ( earned )
constants.REDEEM = 'Redeem'; // Transaction type when points are redeemed

// KYC document names
constants.AADHAAR_CARD = 'Aadhaar Card';
constants.PASSPORT = 'Passport';
constants.DRIVING_LICENSE = 'Driving License';

// Payout modes
constants.IMPS = 'IMPS';
constants.UPI = 'UPI';

// Payout statuses
constants.QUEUED = 'queued';
constants.PROCESSING = 'processing';
constants.PROCESSED = 'processed';
constants.CANCELLED = 'cancelled';
constants.REVERSED = 'reversed';
constants.FAILED = 'failed';

// Currency
constants.INR = 'INR';

// Purpose
constants.WALLET_REDEEM = 'Wallet Redeem';
constants.PAYOUT = 'payout';

// Activities
constants.BLOG = 'Blog';
constants.JOURNEY = 'Journey';
constants.REFERRAL = 'Referral';
constants.INSTAGRAM_CONNECT = 'Instagram Connect';

// Product Types
constants.VOUCHER = 'Voucher';
constants.GOODIE = 'Goodie';
constants.COUPON = 'Coupon';
constants.CASH = 'Cash';
constants.SCRATCHCARD = 'Scratch Card';

// Socials
constants.INSTAGRAM = 'Instagram';
constants.FACEBOOK = 'Facebook';
constants.TWITTER = 'Twitter';
constants.YOUTUBE = 'Youtube';
constants.LINKEDIN = 'Linkedin';
constants.SNAPCHAT = 'Snapchat';

// Payment Methods
constants.BANK_ACCOUNT = 'Bank Account';
constants.UPI = 'UPI';

// Entry bonus
constants.ENTRY_BONUS = 'Entry Bonus';
constants.ENTRY_BONUS_POINTS = 100;

// TDS deduction
constants.CUMULATIVE_WITHDRAW_AMOUNT = 30000;
constants.PERCENT_DEDUCTION = 10;

// email auth
constants.ACCESS_TOKEN_EXPIRES_AFTER_MINUTES = '1h';

// Product campaign product types
constants.PRODUCT = 'Product';
constants.SERVICE = 'Service';

// Notification APIs subtypes
constants.NOTIFICATION_API_APPLICANT_RESPONSE = 'applicant_response';

//page size
constants.PAGE_SIZE = 10;

constants.YT_DATA_API_URL =
    'https://www.googleapis.com/youtube/v3/channels?mine=true&part=snippet&maxResults=100&page=subscribersCount&part=statistics&part=contentOwnerDetails&part=status&part=topicDetails&part=brandingSettings&part=contentDetails&part=id&part=localizations';

module.exports = constants;
