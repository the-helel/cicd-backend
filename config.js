const config = {};
const constants = require('./constants');
// REST API Host Parameters
config.HOST = '127.0.0.1';
config.PORT = '4000';

require('dotenv').config();

// MongoDB Config Parameters
config.PROD_DB_NAME = 'famstar';
config.DEV_DB_NAME = 'famstar_dev';
config.DB_USERNAME = 'famstar';
config.DB_PASSWORD = 'famstar123';
config.DB_URL = `mongodb+srv://${config.DB_USERNAME}:${config.DB_PASSWORD}@famstar-dev-cluster.9uxzl.mongodb.net/${config.PROD_DB_NAME}?retryWrites=true&w=majority`;
// config.DB_URL = `mongodb://localhost:27017/test`;

if (process.env.NODE_ENV === 'dev') {
    config.DB_URL = `mongodb+srv://${config.DB_USERNAME}:${config.DB_PASSWORD}@famstar-dev-cluster.9uxzl.mongodb.net/${config.DEV_DB_NAME}?retryWrites=true&w=majority`;
}

// AWS SNS Service Parameters
config.ACCESS_KEY_ID = 'AKIAI4YZFFHLSITFVOCQ';
config.SECRET_ACCESS_KEY = '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';
config.REGION = 'ap-southeast-1';

// AWS SES Service Parameters
config.SES_ACCESS_KEY_ID = 'AKIAS5D3DVLAM6QJ3RXV'; // 'AKIAS5D3DVLALLALBWDC'; //'AKIAS5D3DVLAECGEF2XQ';
config.SES_SECRET_ACCESS_KEY = '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'; // '4CVIA6cKndlP5e+MT6LfrDRmVwCl9HYqcOn+ZyqH';//'15rrAj2pBrpnO6F4urN7CM1XrE9IDZ0lLQI2/JBT';
config.SES_REGION = 'ap-south-1';
config.SES_FROM_DEFAULT = '"Famstar" <hello@famstar.in>';

// AWS S3 Bucket Service Parameters
config.S3_ACCESS_KEY_ID = 'AKIAJMTHVVLUJXBO4DRA';
config.S3_SECRET_ACCESS_KEY = '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';
config.S3_REGION = 'ap-south-1';
config.S3_BUCKET_NAME = 'famstar';

// Razorpay Configurations for Test and Live mode
config.TEST_RAZORPAY_KEY_ID = 'rzp_test_VrEjN34Abko7YT';
config.TEST_RAZORPAY_KEY_SECRET = 'DgfslPFae10HapeTTkqYxUz4';
config.TEST_RAZORPAY_ACCOUNT = '2323230009936640';

config.LIVE_RAZORPAY_KEY_ID = 'rzp_live_xaHJYwDwXDBUzL';
config.LIVE_RAZORPAY_KEY_SECRET = 'tHMQEAmWA0ytLJCAp37vfzfC';
config.LIVE_RAZORPAY_ACCOUNT = '4564566761005902';

config.VIRTUAL_ACCOUNT = '2323230009936640';

// JWT secret
config.JWT_SECRET = 'famstar';

// Instagram display API Parameters
config.APP_ID = '321928952409659';
config.APP_SECRET = '4c3ae710c6e1d3e8b2212881ac5b62d6';
config.REDIRECT_URI = 'https://www.google.com/';

// Truecaller App credentials
config.TRUECALLER_APP_KEY = 'NK6PF5adf1f3519b14277b6d06891cafa2429';

// ghost API Keys
config.GHOST_API_KEY = '315f7b24d56eca1b0171591ee2';

// Facebook graph api uri
config.FACEBOOK_GRAPH = 'https://graph.facebook.com/v10.0';

// Firebase WEB API key
config.FIREBASE_WEB_API = 'AIzaSyDDrxidzIRqFl42JsznWYZ83pf-GcG2Ae0';

// Elastic search Configurations for local env
// config.ELASTICSEARCH_USERNAME = 'elastic';
// config.ELASTICSEARCH_PASSWORD = 'changeme';
// config.ELASTICSEARCH_HOST = 'localhost';
// config.ELASTICSEARCH_PORT = 9200;
// config.ELASTICSEARCH_INDEX = 'famstar';
// config.ELASTICSEARCH_TYPE = 'winkl';

// Elastic search Configurations for dev env
config.ELASTICSEARCH_USERNAME = 'famstar';
config.ELASTICSEARCH_PASSWORD = 'famstar@2021';
config.ELASTICSEARCH_HOST = '52.66.245.31';
config.ELASTICSEARCH_PORT = 9200;
config.ELASTICSEARCH_INDEX = 'influencers';
config.ELASTICSEARCH_TYPE = 'profiles';
config.RESULTS_PER_PAGE = 10;

// Twitter API keys
config.TWITTER_API_KEY = 'JRJn9grnCHey2UYZAh2meryYH';
config.TWITTER_SECRET_KEY = '7TQ8TxukmHpFThNe9esy4WWie8R1PSvc1me9paxX7yuaV9mScZ';
config.TWITTER_BEARER_TOKEN =
    'AAAAAAAAAAAAAAAAAAAAANSrRgEAAAAASESrd%2FkBALKxlOTxGOU6p1bfJUE%3DmjxC6KGlhcoe5HezTfJVWhT0c3AWV4MA2H5tcd7N8opd5j1AWB';
config.TWITTER_ACCESS_TOKEN = '1305563755820937216-zpGZva2C6KmCB4yRKOnNsna1FjjIzB';
config.TWITTER_ACCESS_TOKEN_SECRET = 'KF0mVsYIMXDRANIvaEx6eXSy83mglZOhT7rMyVBW33p83';
config.TWITTER_CALLBACK_URL = 'http://localhost:4000/';

//Roles and access object
//NOTE: pls try to check its use before changing
config.ROLES = {
    Admin: {
        userType: constants.ADMIN,
        role: constants.ADMIN,
        label: 'Super Admin',
        value: 50,
    },
    Organization: {
        userType: constants.COMPANY,
        role: constants.ORGANIZATION,
        label: 'Org Admin',
        value: 30,
    },
    Member: {
        userType: constants.COMPANY,
        role: constants.MEMBER,
        label: 'Org Member',
        value: 20,
    },
};

module.exports = config;
