const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const config = require('../config');

const s3 = new AWS.S3({
    secretAccessKey: config.S3_SECRET_ACCESS_KEY,
    accessKeyId: config.S3_ACCESS_KEY_ID,
    region: config.S3_REGION,
});

const uploadPublicImage = multer({
    storage: multerS3({
        s3: s3,
        bucket: config.S3_BUCKET_NAME,
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        storageClass: 'REDUCED_REDUNDANCY',
        metadata: function (req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            cb(null, Date.now().toString());
        },
    }),
});

var uploadPrivateImage = multer({
    storage: multerS3({
        s3: s3,
        bucket: config.S3_BUCKET_NAME,
        acl: 'authenticated-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        serverSideEncryption: 'AES256',
        storageClass: 'REDUCED_REDUNDANCY',
        metadata: function (req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            cb(null, Date.now().toString());
        },
    }),
});

module.exports = { uploadPublicImage, uploadPrivateImage };
