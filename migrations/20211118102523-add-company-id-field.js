const constants = require('../constants.js');

// TODO: Resolve this query to add company field to every table
module.exports = {
    async up(db, client) {
        // Update companyId with _id of mongo document in companies
        let cursor = await db.collection('companies').find({ role: constants.ORGANIZATION });

        // cursor.forEach(async (company) => {
        await db.collection('companies').update({}, { $set: { companyId: '$$_id' } });
        // });
    },

    async down(db, client) {
        await db.collection('companies').update(
            {
                role: constants.ORGANIZATION,
            },
            {
                $set: {
                    companyId: null,
                },
            }
        );
    },
};
