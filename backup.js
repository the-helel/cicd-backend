/**
 * Creates a child process to backup database with gzip compression
 * using mongodump. Child process so that it dosen't blocks the main
 * thread while performing backups.
 */

const { spawn } = require('child_process');
const events = require('events');
const path = require('path');
const config = require('./config');

const DB_NAME = 'famstar_dev';
let timeStamp = Date.now().toString();
const ARCHIVE_PATH = path.join(__dirname, 'backups', `${DB_NAME}_${timeStamp}.gzip`);
const BACKUP_BUCKET = 'famstardbbackups';

const emitter = new events.EventEmitter();

function backupDB() {
    console.log('backup started');

    // spawnning a new child process to run the backup task
    const backupChildProcess = spawn('mongodump', [
        `--uri=mongodb+srv://famstar:famstar123@famstar-dev-cluster.9uxzl.mongodb.net/famstar_dev`,
        `--out=${ARCHIVE_PATH}`,
        '--forceTableScan',
        '--gzip',
    ]);

    // logging output while running the above child process
    backupChildProcess.stdout.on('data', (data) => {
        console.log('Stdout = \n', data.toString('ascii'));
    });

    // logging error
    backupChildProcess.stderr.on('data', (data) => {
        console.log('StdError = \n', data.toString('ascii'));
    });

    // actual error
    backupChildProcess.on('error', (error) => console.log(error));

    // exiting
    backupChildProcess.on('exit', (code, signal) => {
        if (code) console.log('Process exit with code : ', code);
        else if (signal) console.log('Process killed with signal : ', signal);
        else {
            // emitt event 'backupOK' when backup is successfull
            emitter.emit('backupOK');
            console.log('Backup Successfull ✅');
        }
    });

    // on completion of backu
    emitter.on('backupOK', () => {
        console.log('Uploading to s3 started ....');
        const s3ChildProcess = spawn('aws', ['s3', 'cp', `s3://${BACKUP_BUCKET}`, `${ARCHIVE_PATH}`]);

        // logging output while running the above child process
        s3ChildProcess.stdout.on('data', (data) => {
            console.log('Stdout = \n', data.toString('ascii'));
        });

        // logging error
        s3ChildProcess.stderr.on('data', (data) => {
            console.log('StdError = \n', data.toString('ascii'));
        });

        // actual error
        s3ChildProcess.on('error', (error) => console.log(error));

        // exiting
        s3ChildProcess.on('exit', (code, signal) => {
            if (code) console.log('Process exit with code : ', code);
            else if (signal) console.log('Process killed with signal : ', signal);
            else {
                // emitt event 's3UploadOK' when upload to s3 is successfull
                emitter.emit('s3UploadOk');
                console.log('Upload to S3 Successfull ✅');
            }
        });
    });

    // emitter.on('s3UploadOk', () => {
    //     const removeLocalBackupChildProcess = spawn('rm', ['-rf', 'backups/']);

    //     // logging output while running the above child process
    //     removeLocalBackupChildProcess.stdout.on('data', (data) => {
    //         console.log('Stdout = \n', data);
    //     });

    //     // logging error
    //     removeLocalBackupChildProcess.stderr.on('data', (data) => {
    //         console.log('StdError = \n', data);
    //     });

    //     // actual error
    //     removeLocalBackupChildProcess.on('error', (error) => console.log(error));

    //     // exiting
    //     removeLocalBackupChildProcess.on('exit', (code, signal) => {
    //         if (code) console.log('Process exit with code : ', code);
    //         else if (signal) console.log('Process killed with signal : ', signal);
    //         else {
    //             console.log('Removing local backups Successfull ✅');
    //         }
    //     });
    // });
}

module.exports = backupDB;
